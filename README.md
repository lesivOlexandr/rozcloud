# Rozcloud

Is an app for managing universities schedules

## Building an app

You may install app using docker.
 1. Copy .env files from `docker/docker-environment.example` to `docker/docker-environment`. You can do this by running command `cp docker-environment.example docker-environment -r`
 2. Install Docker and Docker Compose on your machine following instruction by this [link](https://docs.docker.com/get-docker/)
 3. Just run command `$: [sudo] docker-compose build`

Or install every service separately without docker.

## Running app

 1. Make sure that docker and docker compose is installed on your computer. Move to `docker` directory.
 2. 
    -  `$: [sudo] docker-compose up` runs the project in prod mode
    -  `$: [sudo] docker-compose -f docker-compose.yml -f docker-compose.development.yml up` runs the project in dev mode (with hot reload)
  By default backend will run on 5001 port. You can change this behavior by overriding environment variables.
  In the development mode also will exposed port 7474 which used by Neo4j GUI.
  Default credentials for DB access login: neo4j, password: test 
Or run every service separately without docker.

Default app credentials:
  - admin
    - login: admin@gmail.com
    - password: MasTer268
  
  - student
    - login: student@email.com
    - password: 13FAadffsdfsdf
  
  - teacher
    - teacher@email.com
    - DFasfdf4sjdfsdf

## Release new version
Running `$: lerna version --conventional-commits` will cause new packages version to be applied.

## Some useful scripts

 - `$: [sudo] docker-compose build service_name` builds one service without rebuilding whole project
 - `$: [sudo] docker-compose ps` shows running containers
 - `$: [sudo] docker exec -it container_name sh` opens shell in docker container (note instead of sh you may put any command that should be executed in your container

## Technology stack
 - [Docker](https://www.docker.com/) / [Docker-compose](https://docs.docker.com/compose/) - make it easy to spin up the project and create platform agnostic executable containers
 - [Nginx](https://www.nginx.com/) - Serves as reverse proxy when all the projects is set up with docker
 - [Lerna](https://lerna.js.org/) - Monorepos management tool 
 - [Node.js](https://nodejs.org/en/) - Tool for running server-side js
 - [Fastify](https://www.fastify.io/) - Highly productive Node.js framework for serving requests
 - [Swagger](https://swagger.io/specification/v2/) - Specification for API documentation generation
 - [Neo4j](https://neo4j.com/) - Graph oriented database
 - [KeyDB](https://keydb.dev/) - redis-like cache storage with support for concurrency
 - [Typescript](https://www.fastify.io/) - Brings static typification to js
 - [Vue.js](https://vuejs.org/) / [Vue Router](https://router.vuejs.org/) / [Vuex](https://vuex.vuejs.org/) - frontend framework and set of tools for building reactive interfaces
