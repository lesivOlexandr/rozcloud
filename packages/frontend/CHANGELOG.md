# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.1](https://bitbucket.org/lesivOlexandr/rozcloud/compare/v0.5.0...v0.5.1) (2021-05-29)

**Note:** Version bump only for package frontend
