import { getEnvironmentVariable } from '../helpers/config.helpers';

export const serverUrl = getEnvironmentVariable('VITE_SERVER_URL', 'VITE_SERVER_URL') as string;
export const appBaseUrl = getEnvironmentVariable('VITE_BASE_URL', 'http://localhost:3000');
