import { useRoute } from 'vue-router';

export const useParam = (paramName: string) => {
  const route = useRoute();
  const param = route.params[paramName];
  if (param instanceof Array) {
    return param[0];
  }
  return param;
};

export const useUniversityId = () => useParam('universityId');
export const useStructureId = () => useParam('structureId');
export const useFacultyId = () => useParam('facultyId');
export const useDepartmentId = () => useParam('departmentId');
export const useSuperstructureId = () => useParam('superstructure');
export const useCorpusId = () => useParam('corpusId');
export const useGroupId = () => useParam('groupId');
export const useSubjectId = () => useParam('subjectId');
