import { employeeGroupsKey, jsonTokenKey, scheduleKey } from '../constants';
import { ISelectedScheduleItems } from '../interfaces/components/schedule-week.selection';

export const getItem = (key: string): string | null => {
  return localStorage?.getItem(key) || null;
};

export const setItem = (key: string, value: string): string => {
  localStorage?.setItem(key, value);
  return value;
};

export const removeItem = (key: string): string | null => {
  const value = localStorage?.getItem(key);
  localStorage?.removeItem(key);
  return value || null;
};

export const setToken = (value: string) => setItem(jsonTokenKey, value);
export const getToken = () => getItem(jsonTokenKey);
export const removeToken = () => removeItem(jsonTokenKey);

export const setEmployeeGroupsIds = (groupIds: string[]) => setItem(employeeGroupsKey, JSON.stringify(groupIds));
export const getEmployeeGroupsIds = () => JSON.parse(getItem(employeeGroupsKey) || '[]') as string[];

export const setSelectedScheduleItems = (scheduleItems: ISelectedScheduleItems) => {
  const selectedSubjects = scheduleItems.selectedSubjects.map(subjectItems =>
    subjectItems.map(item => ({ subject: item.subject }))
  );
  const selectedTeachers = scheduleItems.selectedTeachers.map(teachersItems =>
    teachersItems.map(item => ({ teacher: item.teacher }))
  );
  const selectedAuditories = scheduleItems.selectedAuditories.map(auditoriesItems =>
    auditoriesItems.map(item => ({ auditory: item.auditory }))
  );
  const selectedSubjectTypes = scheduleItems.selectedSubjectTypes.map(subjectTypeItems =>
    subjectTypeItems.map(item => ({ type: item.type }))
  );

  const items = {
    selectedSubjects,
    selectedTeachers,
    selectedAuditories,
    selectedSubjectTypes,
  };
  return setItem(scheduleKey, JSON.stringify(items));
};

export const getSelectedScheduleItems = (): ISelectedScheduleItems | null => JSON.parse(getItem(scheduleKey) || 'null');
