export const getEnvironmentVariable = (variableName: string, defaultValue: string) => {
  const variableValue = import.meta.env[variableName];
  if (variableValue === undefined) {
    console.warn(`Envinronment variable ${variableName} is not set`);
    return defaultValue;
  }
  return variableValue;
};
