import { ICellCoords } from '../interfaces/components/schedule-week';

export const getEventPageCoords = (e: MouseEvent) => ({ pageX: e.pageX, pageY: e.pageY });
export const getElementAbsoluteCoords = (el: HTMLElement) => {
  const { left, top } = el.getBoundingClientRect();
  const documentScrollTop = document.documentElement?.scrollTop || window.scrollY;
  const documentScrollLeft = document.documentElement?.scrollLeft || window.scrollX;

  // const elementScrollTop = el.scrollTop;
  // const elementScrollLeft = el.scrollLeft;

  const scrollTop = documentScrollTop;
  const scrollLeft = documentScrollLeft;

  return {
    top: scrollTop + top,
    left: scrollLeft + left,
  };
};

export const getElementRectangle = (el: HTMLElement): ICellCoords => {
  const startCoords = getElementAbsoluteCoords(el);
  return {
    startX: startCoords.left,
    endX: startCoords.left + el.offsetWidth,
    startY: startCoords.top,
    endY: startCoords.top + el.offsetHeight,
  };
};
