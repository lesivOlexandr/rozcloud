import { ApiError } from '../../data/models/api-error';

export const isApiError = (e: unknown) => e instanceof ApiError;
