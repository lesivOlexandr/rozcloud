import { IDateMeta } from '@rozcloud/shared/interfaces';

export const sortByWeekDaysOrder = (dates: IDateMeta[]) => {
  // sunday has index 0. So to sort it right we calculate it as if it was 7
  const datesByDayNumber = dates?.sort((date1, date2) => {
    if (date1.dayName === '0') {
      return 7 - +date2.dayName;
    }
    if (date2.dayName === '0') {
      return +date1.dayName - 7;
    }
    return +date1.dayName - +date2.dayName;
  });
  return datesByDayNumber;
};

export function debounce(fn: (...args: unknown[]) => unknown, wait: number) {
  let t: number;
  return function(this: unknown, ...args: unknown[]) {
    clearTimeout(t);
    t = setTimeout(() => fn.apply(this, args), wait);
  };
}

export const isArrayEquals = <T>(arr1?: T[] | null, arr2?: T[] | null) => {
  if (!arr1 || !arr2) {
    return false;
  }
  if (arr1.length !== arr2.length) {
    return false;
  }
  const arr2Set = new Set(arr2);
  for (const el of arr1) {
    if (!arr2Set.has(el)) {
      return false;
    }
  }
  return true;
};
