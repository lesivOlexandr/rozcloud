export interface Command {
  execute: () => void;
}

export interface UndoableCommand extends Command {
  undo: () => void;
}
