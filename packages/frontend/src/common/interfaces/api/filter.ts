export interface ICommonFilter {
  search?: string;
}

export interface IGroupsFilter extends ICommonFilter {
  excludeIds?: string[];
}
