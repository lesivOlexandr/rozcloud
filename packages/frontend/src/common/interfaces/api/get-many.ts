export interface ApiGetMany<T> {
  data: T[];
}
