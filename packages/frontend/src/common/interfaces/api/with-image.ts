export type WithImage<T> = T & { image?: null | File };
