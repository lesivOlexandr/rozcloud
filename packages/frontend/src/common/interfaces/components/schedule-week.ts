import { SubjectTypes } from '@rozcloud/shared/enums';
import {
  IAuditory,
  IDateMeta,
  IGroup,
  IMonthInverted,
  InvertedDateHierarchy,
  ISemesterInverted,
  ISubject,
  ITeacher,
  IWeekInverted,
  IYearInverted,
} from '@rozcloud/shared/interfaces';
import { subjectTypes } from '../../constants';

export interface ScheduleWeekUi {
  datesData: InvertedDateHierarchy | null;
  isLoadingDates: boolean;
  selectedYear: number | null;
  selectedSemesters: ISemesterInverted[];
  selectedMonths: IMonthInverted[];
  selectedWeeks: IWeekInverted[];

  // selectedDays: IDateMeta[];
  chosenYear: IYearInverted | null;
  chosenSemester: ISemesterInverted | null;
  chosenMonth: IMonthInverted | null;
  chosenWeek: IWeekInverted | null;
}

export interface IGroupScheduleItemData {
  itemNumber: number;
  isHighlighted?: boolean;
  scheduleItemData: IScheduleItemData[];
}

export interface IScheduleItemData {
  id: string;
  group: IGroup;
  teacher: ITeacher | null;
  subject: ISubject | null;
  auditory: IAuditory | null;

  subjectInputValue: string | null;
  teacherInputValue: string | null;
  auditoryInputValue: string | null;
  subjectTypeInputValue: string | null;

  subjectSelected?: boolean;
  teacherSelected?: boolean;
  auditorySelected?: boolean;
  subjectTypeSelected?: boolean;

  teacherSelectOptions: ITeacher[];
  auditorySelectOptions: IAuditory[];
  subjectSelectOptions: ISubject[];
  subjectTypesSelectOptions: typeof subjectTypes;

  teacherInputEl?: HTMLInputElement;
  auditoryInputEl?: HTMLInputElement;
  subjectInputEl?: HTMLInputElement;
  subjectTypeInputEl?: HTMLInputElement;

  type: SubjectTypes | null;
}

export interface IScheduleWeekData {
  week: IWeekInverted;
  groupItems: IScheduleWeekGroupItem[];
  scheduleData: IScheduleDayData[];
  isLoading: boolean;
}

export interface IScheduleWeekGroupItem {
  group: IGroup;
  isHighlighted?: boolean;
}

export interface IScheduleDayData {
  day: IDateMeta;
  scheduleItems: IGroupScheduleItemData[];
}

export interface IScheduleWeeksData {
  weeks: Map<number, IScheduleWeekData>;
  selectedWeek: IScheduleWeekData | null;
  scheduleItemsData: IScheduleItemData[];
  daysData: IWeekDateData[];
}

export interface IWeekDateData {
  day: IDateMeta;
  formattedDate: string;
}

export interface AddGroupScheduleData {
  groupItems: IAddGroupListItem[];
  isDialogOpen: boolean;
}

export interface IAddGroupListItem {
  group: IGroup | null;
  selectGroups: IGroup[];
  inputValue: string;
  itemKey: string;
}

export interface ICellCoords {
  startX: number;
  startY: number;
  endX: number;
  endY: number;
}

export interface IScheduleWeekGroupCoords {
  subject: ICellCoords;
  teacher: ICellCoords;
  auditory: ICellCoords;
  subjectType: ICellCoords;
}

export interface IScheduleItemDataWithCoords {
  item: IScheduleItemData;
  coords: IScheduleWeekGroupCoords;
}
