import { ScheduleWeekCellTypes } from '../../enums/cell-type.enum';
import { ICellCoords, IScheduleItemData } from './schedule-week';

export interface IScheduleWeekSelectionGroup {
  elements: IScheduleWeekSelectionElement[];
}

export interface IScheduleWeekSelectionElement {
  item: IScheduleItemData;
  type: ScheduleWeekCellTypes;
  coords: ICellCoords;
}

export interface ISelectedScheduleItems {
  selectedSubjects: IScheduleItemData[][];
  selectedTeachers: IScheduleItemData[][];
  selectedAuditories: IScheduleItemData[][];
  selectedSubjectTypes: IScheduleItemData[][];
}

export interface IPastePayload {
  itemDataElIndex: number;
  itemNumber: number;
  dayName: number;
}
