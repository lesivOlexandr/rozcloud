import {
  AuditoryTypes,
  EmployeeTypes,
  Genders,
  ScienceDegrees,
  StudyPaymentForms,
  StudyForms,
  SubjectTypes,
} from '@rozcloud/shared/enums';
import { HigherEducationType } from '@rozcloud/shared/enums/higher-education-type.enum';

export const khersonUniversity = ``;

export const jsonTokenKey = 'jsonToken';
export const employeeGroupsKey = 'employeeGroups';
export const scheduleKey = 'scheduleKey';

export const higherEducationTypes = [
  {
    value: HigherEducationType.University,
    label: 'Університет',
  },
  {
    value: HigherEducationType.Institute,
    label: 'Інститут',
  },
] as const;

export const employeeTypesSelect = [
  {
    value: EmployeeTypes.Admin,
    label: 'Адміністратор',
  },
  {
    value: EmployeeTypes.University,
    label: 'Університет',
  },
  {
    value: EmployeeTypes.Faculty,
    label: 'Факультет',
  },
  {
    value: EmployeeTypes.Department,
    label: 'Кафедра',
  },
] as const;

export const scienceDegreeSelect = [
  {
    value: ScienceDegrees.Aspirant,
    label: 'Аспірант',
  },
  {
    value: ScienceDegrees.Candidate,
    label: 'Кандидат наук',
  },
  {
    value: ScienceDegrees.Doctor,
    label: 'Доктор наук',
  },
  {
    value: ScienceDegrees.Professor,
    label: 'Професор',
  },
] as const;

export const auditoryTypes = [
  {
    value: AuditoryTypes.Lecture,
    label: 'Лекціна',
  },
  {
    value: AuditoryTypes.Practice,
    label: 'Практична',
  },
  {
    value: AuditoryTypes.Virtual,
    label: 'Віртуальна',
  },
  {
    value: AuditoryTypes.Seminar,
    label: 'Семінари',
  },
] as const;

export const genders = [
  {
    value: Genders.Male,
    label: 'Чоловіча',
  },
  {
    value: Genders.Female,
    label: 'Жіноча',
  },
] as const;

export const studyPaymentForms = [
  {
    value: StudyPaymentForms.Budget,
    label: 'Бюджетна',
  },
  {
    value: StudyPaymentForms.Contract,
    label: 'Контрактна',
  },
] as const;

export const studyForms = [
  {
    value: StudyForms.Distance,
    label: 'Заочна',
  },
  {
    value: StudyForms.FaceToFace,
    label: 'Очна',
  },
] as const;

export const subjectTypesMap: Record<SubjectTypes, string> = {
  [SubjectTypes.Lecture]: 'Лекція',
  [SubjectTypes.Practice]: 'Практика',
  [SubjectTypes.Seminar]: 'Семінар',
  [SubjectTypes.Test]: 'Залік',
  [SubjectTypes.DiffTest]: 'Дифференційований залік',
  [SubjectTypes.Exam]: 'Екзамен',
};

export const subjectTypes = [
  {
    value: SubjectTypes.Lecture,
    label: subjectTypesMap[SubjectTypes.Lecture],
  },
  {
    value: SubjectTypes.Practice,
    label: subjectTypesMap[SubjectTypes.Practice],
  },
  {
    value: SubjectTypes.Seminar,
    label: subjectTypesMap[SubjectTypes.Seminar],
  },
  {
    value: SubjectTypes.Test,
    label: subjectTypesMap[SubjectTypes.Test],
  },
  {
    value: SubjectTypes.DiffTest,
    label: subjectTypesMap[SubjectTypes.DiffTest],
  },
  {
    value: SubjectTypes.Exam,
    label: subjectTypesMap[SubjectTypes.Exam],
  },
];

export const dayNumberNameMapUa: Record<number, string> = {
  0: 'Неділя',
  1: 'Понеділок',
  2: 'Вівторок',
  3: 'Середа',
  4: 'Четверг',
  5: "П'ятниця",
  6: 'Субота',
};

export const scheduleItemNumbers = [1, 2, 3, 4, 5, 6];
export const imageUploadUrl = 'file';
export const fallbackImageUrl =
  'https://ninajohansson.se/wp-content/themes/koji/assets/icons/images/default-fallback-image.png';
