export enum StructureItemType {
  Univesity = 'university',
  Faculty = 'faculty',
  Department = 'department',
  AcademicGroup = 'academicGroup',

  Student = 'student',
  Employee = 'employee',
  Teacher = 'teacher',

  Corpus = 'corpus',
  Auditory = 'auditory',

  Subject = 'subject',
}
