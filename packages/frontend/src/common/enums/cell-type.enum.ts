export enum ScheduleWeekCellTypes {
  Subject = 'subject',
  Teacher = 'teacher',
  Auditory = 'auditory',
  SubjectType = 'subjectType',
}
