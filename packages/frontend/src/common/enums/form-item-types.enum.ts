export enum FormItemTypes {
  select = 'select',
  text = 'text',
  number = 'number',
}
