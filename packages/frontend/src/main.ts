import { createApp } from 'vue';
import App from './App.vue';
import { appRouter } from './router';

const VueApp = createApp(App);

VueApp.use(appRouter);

VueApp.mount('#app');
