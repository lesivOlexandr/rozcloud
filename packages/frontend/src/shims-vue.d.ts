declare module "*.vue" {
  import { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any, {}, {}>;
  export default component;
}

declare module "*.svg" {
  import { Component } from "vue";
  export const VueComponent: Component;
}
