import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { userStore } from './data/stores/user/user.store';
import { getToken, removeToken } from './common/helpers/storage.helpers';

import LoginPage from './view/pages/login/login.vue';
import StructureControlPage from './view/pages/structures-control/structure-control.vue';

import UniversitySidebar from './view/pages/structures-control/university/university-sidebar.vue';
import UniversityMainContent from './view/pages/structures-control/university/university-main.vue';

import CorpusesSidebar from './view/pages/structures-control/corpus/corpus-sidebar.vue';
import CorpusesMain from './view/pages/structures-control/corpus/corpuses-main.vue';

import EmployeesMain from './view/pages/structures-control/employees/employees-main.vue';
import EmployeesSidebar from './view/pages/structures-control/employees/employees-sidebar.vue';

import FacultiesMain from './view/pages/structures-control/faculties/faculties-main.vue';
import FacultiesSidebar from './view/pages/structures-control/faculties/faculties-sidebar.vue';

import DepartmentsMain from './view/pages/structures-control/departments/departments-main.vue';
import DepartmentsSidebar from './view/pages/structures-control/departments/departments-sidebar.vue';

import GroupsMain from './view/pages/structures-control/groups/groups-main.vue';
import GroupsSidebar from './view/pages/structures-control/groups/groups-sidebar.vue';

import AuditoriesMain from './view/pages/structures-control/auditories/auditories-main.vue';
import AuditoriesSidebar from './view/pages/structures-control/auditories/auditories-sidebar.vue';

import StudentsMain from './view/pages/structures-control/students/students-main.vue';
import StudentsSidebar from './view/pages/structures-control/students/students-sidebar.vue';

import SubjectsMain from './view/pages/structures-control/subjects/subjects.main.vue';
import SubjectsSidebar from './view/pages/structures-control/subjects/subjects-sidebar.vue';

import TeachersMain from './view/pages/structures-control/teachers/teacher-main.vue';
import TeachersSidebar from './view/pages/structures-control/teachers/teacher-sidebar.vue';

import SubgroupsMain from './view/pages/structures-control/subgroups/subgroups-main.vue';
import SubgroupSidebar from './view/pages/structures-control/subgroups/subgroup-sidebar.vue';
import SchedulePage from './view/pages/plans/schedule-week.vue';

import ExternalAppAccessPage from './view/pages/external-app/external-app-access.vue';
import { authorizationStore } from './data/stores/authorization/authorization.store';
import { UserTypes } from '@rozcloud/shared/enums';

export const routes: RouteRecordRaw[] = [
  {
    path: '/external-app/:appName',
    meta: {
      title: 'External App',
      requiresAuth: true,
      onlyEmployees: false,
    },
    component: ExternalAppAccessPage,
  },
  {
    path: '/schedule',
    meta: {
      title: 'Schedule',
      requiresAuth: true,
      onlyEmployees: true,
    },
    component: SchedulePage,
  },
  {
    path: '/structures',
    meta: {
      title: 'Structure',
      requiresAuth: true,
      onlyEmployees: true,
    },
    redirect: '/structures/universities',
    component: StructureControlPage,
    children: [
      {
        path: 'universities/:universityId/corpuses/:corpusId/auditories',
        components: {
          'main-content': AuditoriesMain,
          'right-sidebar': AuditoriesSidebar,
        },
      },
      {
        path: 'universities/:universityId/corpuses',
        components: {
          'main-content': CorpusesMain,
          'right-sidebar': CorpusesSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments/:departmentId/groups',
        components: {
          'main-content': GroupsMain,
          'right-sidebar': GroupsSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments/:departmentId/groups/:groupId/subgroups',
        components: {
          'main-content': SubgroupsMain,
          'right-sidebar': SubgroupSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments/:departmentId/subjects/:subjectId/teachers',
        components: {
          'main-content': TeachersMain,
          'right-sidebar': TeachersSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments/:departmentId/subjects',
        components: {
          'main-content': SubjectsMain,
          'right-sidebar': SubjectsSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments/:departmentId/teachers',
        components: {
          'main-content': TeachersMain,
          'right-sidebar': TeachersSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties/:facultyId/departments',
        components: {
          'main-content': DepartmentsMain,
          'right-sidebar': DepartmentsSidebar,
        },
      },
      {
        path: 'universities/:universityId/faculties',
        components: {
          'main-content': FacultiesMain,
          'right-sidebar': FacultiesSidebar,
        },
      },
      {
        path: 'universities',
        components: {
          'main-content': UniversityMainContent,
          'right-sidebar': UniversitySidebar,
        },
      },
      {
        path: 'users/:structureId/employees',
        components: {
          'right-sidebar': EmployeesSidebar,
          'main-content': EmployeesMain,
        },
      },
      {
        path: 'users/:groupId/students',
        components: {
          'main-content': StudentsMain,
          'right-sidebar': StudentsSidebar,
        },
      },
    ],
  },
  {
    path: '/:catchAll(.*)',
    meta: {
      title: 'Login Page',
      forbidAuthenticated: true,
    },
    component: LoginPage,
  },
];

export const history = createWebHistory();

export const appRouter = createRouter({
  routes,
  history,
});

appRouter.beforeEach(async (to, from, next) => {
  const userStoreState = userStore.getState();
  const { user } = userStoreState;
  const { requiresAuth, forbidAuthenticated, onlyEmployees } = to.meta;

  if (onlyEmployees && user && user.userType !== UserTypes.Employee) {
    return next('/');
  }

  if (user && requiresAuth) {
    return next();
  }

  if (!user && requiresAuth) {
    authorizationStore.originatedFrom = to.fullPath;
    const jwtToken = getToken();
    try {
      if (jwtToken) {
        await userStore.getData({ jwtToken });
        return next();
      }
      throw new Error('No jwt token provided during access to protected route');
    } catch (e) {
      removeToken();
      return next('/');
    }
  }

  if (user && forbidAuthenticated) {
    return next('/structures');
  }

  if (!user && forbidAuthenticated) {
    const jwtToken = getToken();
    try {
      if (jwtToken) {
        await userStore.getData({ jwtToken });
        return next('/structures');
      }
    } catch (e) {
      removeToken();
      return next('/');
    }
  }
  return next();
});
