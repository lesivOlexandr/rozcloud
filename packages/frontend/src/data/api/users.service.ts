import { IBindUserToTelegramPayload, IBindUserToTelegramRequestBody } from '@rozcloud/shared/interfaces';
import { ApiService } from './api.service';

export class UsersService {
  static async bindToTelegram(id: string, payload: IBindUserToTelegramPayload) {
    const apiUrl = `users/bind-telegram`;
    const requestPayload: IBindUserToTelegramRequestBody = { id, payload };
    const result: IBindUserToTelegramRequestBody = await ApiService.post(apiUrl, requestPayload);
    return result;
  }
}
