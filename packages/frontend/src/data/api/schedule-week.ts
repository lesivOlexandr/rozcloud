import { ICreateScheduleWeekPayload, IWeekInverted, IWeekSchedulesFull } from '@rozcloud/shared/interfaces';
import { ApiService } from './api.service';

export class ScheduleWeekApiService {
  static async save(payload: ICreateScheduleWeekPayload) {
    const apiUrl = `schedule/week`;
    const result = await ApiService.post(apiUrl, payload);
    return result;
  }

  static async getFromServer(data: IWeekInverted, groupIds: string[]) {
    const apiUrl = `schedule/week/groups/${data.weekNumber}`;
    const result: IWeekSchedulesFull[] = await ApiService.get(apiUrl, { groupIds });

    return result;
  }
}
