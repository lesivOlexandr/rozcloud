import {
  FileResponse,
  ICreateDepartmentPayload,
  IDepartment,
  IUpdateDepartmentPayload,
} from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

export class DepartmentsService {
  static async createOne(data: ICreateDepartmentPayload) {
    await this.uploadImage(data);
    const apiUrl = 'structures/departments';
    const createdFaculty: IDepartment = await ApiService.post(apiUrl, data);
    return createdFaculty;
  }

  static async getByFaculty(facultyId: string) {
    const apiUrl = `structures/faculties/${facultyId}/departments`;
    const faculties: IDepartment[] = await ApiService.get(apiUrl);
    const facultiesData = { data: faculties };
    return facultiesData as ApiGetMany<IDepartment>;
  }

  static async updateOne(departmentId: string, data: IUpdateDepartmentPayload) {
    await this.uploadImage(data);
    const apiUrl = 'structures/departments';
    const updatedFaculty: IDepartment = await ApiService.put(apiUrl, departmentId, data);
    return updatedFaculty;
  }

  private static async uploadImage(data: WithImage<IUpdateDepartmentPayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.logo = imageData.fileUrl;
    }
  }
}
