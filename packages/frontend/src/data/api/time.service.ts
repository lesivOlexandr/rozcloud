import { DateHierarchy } from '@rozcloud/shared/interfaces';
import { ApiService } from './api.service';

export class TimeService {
  static async getTime(yearNumber: number) {
    const apiUrl = `time/${yearNumber}`;
    const timeResponse: DateHierarchy = await ApiService.get(apiUrl);
    return timeResponse;
  }
}
