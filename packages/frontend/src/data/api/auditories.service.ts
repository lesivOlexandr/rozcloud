import {
  FileResponse,
  IAuditory,
  IAuditoryFilter,
  ICreateAuditoryPayload,
  IUpdateAuditoryPayload,
} from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ICommonFilter } from '../../common/interfaces/api/filter';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

const apiUrlPart = 'locations/auditories';

export class AuditoriesService {
  static async getMany(filters: IAuditoryFilter) {
    const apiUrl = 'locations/auditories';
    const auditories = await ApiService.get(apiUrl, filters);
    const auditoriesData = { data: auditories };
    return auditoriesData;
  }

  static async createOne(data: ICreateAuditoryPayload) {
    await this.uploadImage(data);
    const createdAuditory: IAuditory = await ApiService.post(apiUrlPart, data);
    return createdAuditory;
  }

  static async getByFilters(filter: ICommonFilter) {
    const apiUrl = `locations/auditories`;
    const auditories: IAuditory[] = await ApiService.get(apiUrl, filter);
    const auditoriesData = { data: auditories };
    return auditoriesData;
  }

  static async updateOne(auditoryId: string, data: IUpdateAuditoryPayload) {
    await this.uploadImage(data);
    const updatedAuditory: IAuditory = await ApiService.put(apiUrlPart, auditoryId, data);
    return updatedAuditory;
  }

  static async getAuditoriesByCorpus(corpusId: string) {
    const apiUrl = `locations/corpuses/${corpusId}/auditories`;
    const auditories: IAuditory[] = await ApiService.get(apiUrl);
    const auditoriesData = { data: auditories };
    return auditoriesData as ApiGetMany<IAuditory>;
  }

  private static async uploadImage(data: WithImage<IUpdateAuditoryPayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.logo = imageData.fileUrl;
    }
  }
}
