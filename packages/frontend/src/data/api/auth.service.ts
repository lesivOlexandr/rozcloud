import {
  ILoginUserResponse,
  IUserDataPayload,
  IUserDataResponse,
  IUserLoginPayload,
} from '@rozcloud/shared/interfaces';
import { ApiService } from './api.service';

export class AuthenticationService {
  static async login(payload: IUserLoginPayload) {
    const response: ILoginUserResponse = await ApiService.post('auth/login', payload);
    return response;
  }

  static async userData(payload: IUserDataPayload) {
    const apiUrl = 'auth/user-data';
    const response: IUserDataResponse = await ApiService.post(apiUrl, payload);
    return response;
  }
}
