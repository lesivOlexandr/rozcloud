import {
  FileResponse,
  ICreateUniversityPayload,
  IUniversity,
  IUpdateUniversityPayload,
} from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

const apiUrlPart = 'structures/universities';

export class UniversitiesService {
  static async getMany() {
    const universities: IUniversity[] = await ApiService.get(apiUrlPart);
    const universitiesData = { data: universities };
    return universitiesData as ApiGetMany<IUniversity>;
  }

  static async createOne(data: WithImage<ICreateUniversityPayload>) {
    await this.uploadImage(data);
    const university: IUniversity = await ApiService.post(apiUrlPart, data);
    return university;
  }

  static async updateOne(id: string, data: IUpdateUniversityPayload) {
    await this.uploadImage(data);
    const unviersity: IUniversity = await ApiService.put(apiUrlPart, id, data);
    return unviersity;
  }

  private static async uploadImage(data: WithImage<IUpdateUniversityPayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.logo = imageData.fileUrl;
    }
  }
}
