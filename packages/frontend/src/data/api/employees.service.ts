import { FileResponse, ICreateEmployeePayload, IEmployee, IUpdateEmployeePayload } from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

export class EmployeesService {
  static async getByStructure(structureId: string) {
    const apiUrl = `structures/${structureId}/employees`;
    const employees: IEmployee[] = await ApiService.get(apiUrl);
    const employeesData = { data: employees };
    return employeesData as ApiGetMany<IEmployee>;
  }

  static async createOne(data: ICreateEmployeePayload) {
    await this.uploadImage(data);

    const apiUrl = `users/employees`;
    const createdEmployee = await ApiService.post(apiUrl, data);
    return createdEmployee;
  }

  static async updateOne(employeeId: string, data: IUpdateEmployeePayload) {
    await this.uploadImage(data);

    const apiUrl = 'users/employees';
    const updatedEmployee = await ApiService.put(apiUrl, employeeId, data);
    return updatedEmployee;
  }

  private static async uploadImage(data: WithImage<IUpdateEmployeePayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.avatar = imageData.fileUrl;
    }
  }
}
