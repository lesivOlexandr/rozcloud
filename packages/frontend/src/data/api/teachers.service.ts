import {
  FileResponse,
  ICreateTeacherPayload,
  ITeacher,
  ITeacherFilter,
  IUpdateEmployeePayload,
  IUpdateTeacherPayload,
} from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ICommonFilter } from '../../common/interfaces/api/filter';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

export class TeachersService {
  static async getMany(filters?: ITeacherFilter) {
    const apiUrl = 'users/teachers';
    const teachers: ITeacher[] = await ApiService.get(apiUrl, filters);
    const teachersData = { data: teachers };
    return teachersData;
  }

  static async getByDepartment(departmentId: string) {
    const apiUrl = `structures/departments/${departmentId}/teachers`;
    const teachers: ITeacher[] = await ApiService.get(apiUrl);
    const teachersData = { data: teachers };
    return teachersData as ApiGetMany<ITeacher>;
  }

  static async getByFilters(filter: ICommonFilter) {
    const apiUrl = `users/teachers`;
    const teachers: ITeacher[] = await ApiService.get(apiUrl, filter);
    const teachersData = { data: teachers };
    return teachersData;
  }

  static async getBySubject(subjectId: string) {
    const apiUrl = `schedule/subjects/${subjectId}/teachers`;
    const teachers: ITeacher[] = await ApiService.get(apiUrl);
    const teachersData = { data: teachers };
    return teachersData as ApiGetMany<ITeacher>;
  }

  static async createOne(data: ICreateTeacherPayload) {
    await this.uploadImage(data);
    const apiUrl = `users/teachers`;
    const createdTeacher = await ApiService.post(apiUrl, data);
    return createdTeacher;
  }

  static async updateOne(teacherId: string, data: IUpdateTeacherPayload) {
    await this.uploadImage(data);
    const apiUrl = `users/teachers`;
    const updatedTeacher = await ApiService.put(apiUrl, teacherId, data);
    return updatedTeacher;
  }

  private static async uploadImage(data: WithImage<IUpdateEmployeePayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.avatar = imageData.fileUrl;
    }
  }
}
