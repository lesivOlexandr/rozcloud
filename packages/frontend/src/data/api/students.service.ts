import {
  FileResponse,
  ICreateStudentPayload,
  IStudent,
  IUpdateEmployeePayload,
  IUpdateStudentPayload,
} from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

export class StudentsService {
  static async createOne(data: ICreateStudentPayload) {
    await this.uploadImage(data);

    const apiUrl = 'users/students';
    const createdStudent = await ApiService.post(apiUrl, data);
    return createdStudent;
  }

  static async updateOne(studentId: string, data: IUpdateStudentPayload) {
    await this.uploadImage(data);
    const apiUrl = 'users/students';
    const updatedStudent: IStudent = await ApiService.put(apiUrl, studentId, data);
    return updatedStudent;
  }

  static async getStudentsByGroup(groupId: string) {
    const apiUrl = `structures/groups/${groupId}/students`;
    const students: IStudent[] = await ApiService.get(apiUrl);
    const studentsData = { data: students };
    return studentsData;
  }

  private static async uploadImage(data: WithImage<IUpdateEmployeePayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.avatar = imageData.fileUrl;
    }
  }
}
