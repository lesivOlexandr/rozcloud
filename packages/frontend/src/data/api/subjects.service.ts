import { IAuditoryFilter, ICreateSubjectPayload, ISubject, IUpdateSubjectPayload } from '@rozcloud/shared/interfaces';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { ApiService } from './api.service';

export class SubjectsService {
  static async createOne(data: ICreateSubjectPayload) {
    const apiUrl = 'schedule/subjects';
    const createdFaculty: ISubject = await ApiService.post(apiUrl, data);
    return createdFaculty;
  }

  static async getByDepartment(departmentId: string) {
    const apiUrl = `structures/departments/${departmentId}/subjects`;
    const subjects: ISubject[] = await ApiService.get(apiUrl);
    const subjectsData = { data: subjects };
    return subjectsData as ApiGetMany<ISubject>;
  }

  static async getByFilters(filter: IAuditoryFilter) {
    const apiUrl = `schedule/subjects`;
    const teachers: ISubject[] = await ApiService.get(apiUrl, filter);
    const teachersData = { data: teachers };
    return teachersData;
  }

  static async updateOne(subjectId: string, data: IUpdateSubjectPayload) {
    const apiUrl = 'schedule/subjects';
    const updatedSubject: ISubject = await ApiService.put(apiUrl, subjectId, data);
    return updatedSubject;
  }
}
