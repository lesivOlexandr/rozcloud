import { FileResponse, ICreateFacultyPayload, IFaculty, IUpdateFacultyPayload } from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

const apiUrlPart = 'structures/faculties';

export class FacultiesService {
  static async getByUniversity(universityId: string) {
    const apiUrl = `structures/universities/${universityId}/faculties`;
    const faculties: IFaculty[] = await ApiService.get(apiUrl);
    const facultiesData = { data: faculties };
    return facultiesData as ApiGetMany<IFaculty>;
  }

  static async createOne(data: WithImage<ICreateFacultyPayload>) {
    const createdFaculty: IFaculty = await ApiService.post(apiUrlPart, data);
    return createdFaculty;
  }

  static async updateOne(facultyId: string, data: WithImage<IUpdateFacultyPayload>) {
    await this.uploadImage(data);
    const updatedFaculty: IFaculty = await ApiService.put(apiUrlPart, facultyId, data);
    return updatedFaculty;
  }

  private static async uploadImage(data: WithImage<IUpdateFacultyPayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.logo = imageData.fileUrl;
    }
  }
}
