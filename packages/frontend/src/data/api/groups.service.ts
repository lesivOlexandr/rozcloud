import { ICreateGroupPayload, IGroup, IUpdateGroupPayload } from '@rozcloud/shared/interfaces';
import { IGroupsFilter } from '../../common/interfaces/api/filter';
import { ApiService } from './api.service';

export class GroupsService {
  static async createOne(data: ICreateGroupPayload) {
    const apiUrl = 'structures/groups';
    const createdGroup: IGroup = await ApiService.post(apiUrl, data);
    return createdGroup;
  }

  static async updateOne(groupId: string, data: IUpdateGroupPayload) {
    const apiUrl = 'structures/groups';
    const updatedGroup: IGroup = await ApiService.put(apiUrl, groupId, data);
    return updatedGroup;
  }

  static async getGroups(filter: IGroupsFilter) {
    const apiUrl = 'structures/groups';
    const params = filter;
    const groups: IGroup[] = await ApiService.get(apiUrl, params);
    return groups;
  }

  static async getByIds(groupIds: string[]) {
    if (!groupIds.length) {
      return [];
    }

    const apiUrl = `structures/groups`;
    const params = {
      groupIds,
    };
    const groups: IGroup[] = await ApiService.get(apiUrl, params);
    return groups;
  }

  static async getAcademicGroupsByDepartment(departmentId: string) {
    const apiUrl = `structures/departments/${departmentId}/academic-groups`;
    const groups: IGroup[] = await ApiService.get(apiUrl);
    const groupsData = { data: groups };
    return groupsData;
  }

  static async getSubgroups(groupId: string) {
    const apiUrl = `structures/groups/${groupId}/subgroups`;
    const subgroups: IGroup[] = await ApiService.get(apiUrl);
    const subGroupsData = { data: subgroups };
    return subGroupsData;
  }

  static async bindStudentToGroup(studentId: string, groupId: string) {
    const apiUrl = 'structures/groups/bind-student-to-group';
    const payload = { studentId, groupId };
    const result: IGroup = await ApiService.put(apiUrl, null, payload);
    return result;
  }
}
