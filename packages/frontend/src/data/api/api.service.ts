import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { serverUrl } from '../../common/config/app.config';
import { getToken } from '../../common/helpers/storage.helpers';
import { ApiError } from '../models/api-error';

export class ApiService {
  static instance: AxiosInstance = axios.create({
    baseURL: serverUrl + '/api',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  static async get<T = unknown>(url: string, params?: T) {
    return await this.instance
      .get(url, {
        headers: {
          Authorization: `Bearer ${await getToken()}`,
        },
        params,
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  static async post<T = unknown>(url: string, data: T) {
    return await this.instance
      .post(url, data, {
        headers: {
          Authorization: `Bearer ${await getToken()}`,
        },
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  static async image(url: string, image: Blob) {
    const data = new FormData();
    data.append('file', image);
    return await this.instance
      .post(url, data, {
        headers: {
          Authorization: `Bearer ${getToken()}`,
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  static async put<T = unknown>(url: string, id: string | null, data: T) {
    const apiUrl = url + (id ? '/' + id : '');
    return await this.instance
      .put(apiUrl, data, {
        headers: {
          Authorization: `Bearer ${await getToken()}`,
        },
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  static async delete<T = unknown>(url: string, data?: T) {
    return await this.instance
      .delete(url, {
        headers: {
          Authorization: `Bearer ${await getToken()}`,
        },
        data,
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  private static handleResponse(response: AxiosResponse) {
    return response.data;
  }

  private static handleError(e: AxiosError) {
    if (e.response) {
      const { error, status } = e.response.data;
      throw new ApiError(error, +status);
    } else if (e.request) {
      throw new Error(e.request.responseText);
    } else {
      throw new Error(e.message);
    }
  }
}
