import { FileResponse, ICorpus, ICreateCorpusPayload, IUpdateCorpusPayload } from '@rozcloud/shared/interfaces';
import { imageUploadUrl } from '../../common/constants';
import { ApiGetMany } from '../../common/interfaces/api/get-many';
import { WithImage } from '../../common/interfaces/api/with-image';
import { ApiService } from './api.service';

const apiUrlPart = 'locations/corpuses';

export class CorpusesService {
  static async getAllByUniversity(universityId: string) {
    const apiUrlPart = `structures/universities/${universityId}/corpuses`;
    const corpuses = await ApiService.get(apiUrlPart);
    const corpusesData = { data: corpuses };
    return corpusesData as ApiGetMany<ICorpus>;
  }

  static async createOne(corpus: WithImage<ICreateCorpusPayload>) {
    await this.uploadImage(corpus);
    const createdCorpus: ICorpus = await ApiService.post(apiUrlPart, corpus);
    return createdCorpus;
  }

  static async updateOne(corpusId: string, corpus: WithImage<IUpdateCorpusPayload>) {
    await this.uploadImage(corpus);
    const updatedCorpus: ICorpus = await ApiService.put(apiUrlPart, corpusId, corpus);
    return updatedCorpus;
  }

  private static async uploadImage(data: WithImage<IUpdateCorpusPayload>) {
    const image = data?.image;
    if (image) {
      const imageData: FileResponse = await ApiService.image(imageUploadUrl, image);
      delete data.image;
      data.logo = imageData.fileUrl;
    }
  }
}
