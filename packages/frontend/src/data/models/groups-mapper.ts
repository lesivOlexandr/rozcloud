import { IDateMeta, IGroup, IWeekInverted } from '@rozcloud/shared/interfaces';
import { IScheduleItemData } from '../../common/interfaces/components/schedule-week';

export class GroupsMapper {
  groupsMap = new Map<string, { index: number; group: IGroup }>();

  constructor(groups: IGroup[]) {
    groups.forEach((group, index) => this.groupsMap.set(group.id, { group, index }));
  }

  getById(groupId: string) {
    return this.groupsMap.get(groupId);
  }
}

export class GroupScheduleItemDataMapper {
  private groupsMap = new Map<string, IScheduleItemData>();

  add(week: IWeekInverted, day: IDateMeta, itemNumber: number, group: IGroup, scheduleItemData: IScheduleItemData) {
    const keyString = week.id + day.id + itemNumber + group.id;
    return this.groupsMap.set(keyString, scheduleItemData);
  }

  get(week: IWeekInverted, day: IDateMeta, itemNumber: number, group: IGroup) {
    const keyString = week.id + day.id + itemNumber + group.id;
    return this.groupsMap.get(keyString);
  }

  clear() {
    this.groupsMap.clear();
  }
}
