import { Store } from '../store.base';
import { ICreateFacultyPayload } from '@rozcloud/shared/interfaces';

export class EditedFacultyStore extends Store<EditedFacultyState> {
  private createNullishFaculty(): EditedFacultyState {
    return {
      id: null,
      abbreviation: '',
      name: '',
      logo: null,
      image: null,
      universityId: (null as unknown) as string,
    };
  }

  protected data(): EditedFacultyState {
    return this.createNullishFaculty();
  }

  setNullishValues(universityId: string | null) {
    if (universityId) {
      const nullishData = this.createNullishFaculty();
      nullishData.universityId = universityId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedFacultyState>, universityId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.abbreviation = data.abbreviation || null;
    this.state.name = data.name || '';
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.universityId = data.universityId || ((universityId as unknown) as string);
  }

  setUniversityId(universityId: string) {
    this.state.universityId = universityId;
  }
}

export const editedFacultyStore = new EditedFacultyStore();

export interface EditedFacultyState extends ICreateFacultyPayload {
  id: string | null;
  image: File | null;
}
