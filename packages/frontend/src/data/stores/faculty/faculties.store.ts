import { ICreateFacultyPayload, IFaculty, IUpdateFacultyPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { FacultiesService } from '../../api/faculties.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedFacultyStore } from './edited-faculty.store';

export class FacultiesStore extends Store<IFacultiesStore> {
  data(): IFacultiesStore {
    return {
      faculties: [],
      facultiesCount: 0,
      activeFacultyId: null,
      creatingFaculty: false,
    };
  }

  getByUniversity() {
    const universityId = editedFacultyStore.getState().universityId;
    if (universityId) {
      return this.fetchFacultiesByUniversity(universityId);
    }
  }

  private async fetchFacultiesByUniversity(universityId: string) {
    const facultiesData = await FacultiesService.getByUniversity(universityId);
    this.state.faculties = facultiesData?.data || [];
    this.state.facultiesCount = facultiesData.data?.length || 0;
    return facultiesData;
  }

  async getFaculty(id: string) {
    const faculty = this.state.faculties.find((u) => u.id === id);
    return faculty;
  }

  async createFaculty(faculty: WithImage<ICreateFacultyPayload>) {
    const createdFaculty = await FacultiesService.createOne(faculty);
    this.setFacultyData(createdFaculty, faculty.universityId);
    this.setActiveFaculty(createdFaculty.id);
    snackbarStore.showMessage('Факультет успішно створено');
    return this.getByUniversity();
  }

  async updateFaculty(id: string, faculty: WithImage<IUpdateFacultyPayload>) {
    await FacultiesService.updateOne(id, faculty);
    snackbarStore.showMessage('Дані факультету успішно змінено');
    return this.getByUniversity();
  }

  private setFacultyData(editedFaculty: IFaculty, univevrsityId: string) {
    editedFacultyStore.setData(editedFaculty, univevrsityId);
  }

  async setActiveFaculty(facultyId: string) {
    const universityId = editedFacultyStore.getState().universityId;
    this.state.activeFacultyId = facultyId;
    this.state.creatingFaculty = false;
    const faculty = await this.getFaculty(facultyId);
    if (faculty) {
      editedFacultyStore.setData(faculty, universityId);
    }
  }

  setCreatingFaculty() {
    if (!this.state.creatingFaculty) {
      const universityId = editedFacultyStore.getState().universityId;
      this.state.activeFacultyId = null;
      this.state.creatingFaculty = true;
      editedFacultyStore.setNullishValues(universityId);
    }
  }
}

export const facultiesStore = new FacultiesStore();

export interface IFacultiesStore {
  faculties: IFaculty[];
  facultiesCount: number;
  activeFacultyId: string | null;
  creatingFaculty: boolean;
}
