import { Store } from '../store.base';
import { ICreateStudentPayload } from '@rozcloud/shared/interfaces';
import { StudyPaymentForms } from '@rozcloud/shared/enums';

export class EditedStudentStore extends Store<EditedStudentState> {
  private createNullishStudent(): EditedStudentState {
    return {
      id: null,
      firstName: null,
      middleName: null,
      lastName: null,
      address: null,
      avatar: null,
      image: null,
      academicGroupId: null!,
      studTicketNum: null,
      studTicketSeria: null,
      birthday: null,
      email: null,
      phone: null,
      login: '',
      password: '',
      gender: null,
      paymentForm: StudyPaymentForms.Contract,
      groupId: null,
    };
  }

  protected data(): EditedStudentState {
    return this.createNullishStudent();
  }

  setNullishValues(groupId: string) {
    if (groupId) {
      const nullishData = this.createNullishStudent();
      this.setData(nullishData, groupId);
    }
  }

  setData(data: Partial<EditedStudentState>, groupId: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.address = data.address || null;
    this.state.avatar = data.avatar || null;
    this.state.birthday = data.birthday || null;
    this.state.email = data.email || null;
    this.state.firstName = data.firstName || null;
    this.state.middleName = data.middleName || null;
    this.state.lastName = data.lastName || null;
    this.state.login = data.login || '';
    this.state.password = data.password || null!;
    this.state.gender = data.gender || null;
    this.state.phone = data.phone || null;
    this.state.paymentForm = data.paymentForm || StudyPaymentForms.Contract;
    this.state.academicGroupId = data.academicGroupId || null!;
    this.state.groupId = groupId || null;
  }

  setGroupId(groupId: string) {
    this.state.groupId = groupId;
  }
}

export const editedStudentStore = new EditedStudentStore();

export interface EditedStudentState extends Partial<ICreateStudentPayload> {
  id: string | null;
  image: File | null;
  groupId?: string | null;
}
