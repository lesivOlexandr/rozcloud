import { ICreateStudentPayload, IStudent, IUpdateStudentPayload } from '@rozcloud/shared/interfaces';
import { userToFullName } from '../../../common/helpers/mappers';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { SelectOption } from '../../../common/interfaces/components';
import { GroupsService } from '../../api/groups.service';
import { StudentsService } from '../../api/students.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedStudentStore } from './edited-student.store';

export class StudentsStore extends Store<IStudentsStore> {
  data(): IStudentsStore {
    return {
      students: [],
      studentsCount: 0,
      activeStudentId: null,
      creatingStudent: false,
      supergroupStudents: [],
      showAddExisting: false,
      existingStudentIdToAdd: null,
    };
  }

  async getStudents() {
    const groupId = editedStudentStore.getState().groupId;
    const studentsData = await this.fetchByGroup(groupId!);
    this.state.students = studentsData.data;
    this.state.studentsCount = studentsData.data?.length || 0;
    return studentsData;
  }

  async getSupergroupStudents(supergroupId: string) {
    const studentsData = await this.fetchByGroup(supergroupId);
    this.state.supergroupStudents = studentsData.data.map(student => ({
      label: userToFullName(student),
      value: student.id,
    }));
  }

  async bindStudentToGroup(studentId: string, groupId: string) {
    const result = await GroupsService.bindStudentToGroup(studentId, groupId);
    await this.getStudents();
    snackbarStore.showMessage('Студента успішно додано до групи');
    return result;
  }

  private fetchByGroup(groupId: string) {
    return StudentsService.getStudentsByGroup(groupId);
  }

  async createStudent(student: WithImage<ICreateStudentPayload>, superGroups?: string[]) {
    const groupId = editedStudentStore.getState().groupId!;
    const createdStudent = await StudentsService.createOne(student);
    if (Array.isArray(superGroups)) {
      const bindStudentToGroup = GroupsService.bindStudentToGroup.bind(GroupsService, createdStudent.id);
      await Promise.all(superGroups.map(bindStudentToGroup));
    }
    this.setStudentData(createdStudent, groupId);
    this.setActiveStudent(createdStudent.id);
    snackbarStore.showMessage('Студента успішно додано');
    return this.getStudents();
  }

  async updateStudent(id: string, student: WithImage<IUpdateStudentPayload>) {
    const groupId = editedStudentStore.getState().groupId;
    editedStudentStore.setData(student, groupId!);
    const studentData = editedStudentStore.getState();
    await StudentsService.updateOne(id, studentData);
    snackbarStore.showMessage('Дані студента успішно змінено');
    return this.getStudents();
  }

  private setStudentData(editedStudent: IStudent, structureId: string) {
    editedStudentStore.setData(editedStudent, structureId);
  }

  async getStudent(id: string) {
    const department = this.state.students.find(e => e.id === id);
    return department;
  }

  async setActiveStudent(studentId: string) {
    const groupId = editedStudentStore.getState().groupId;
    this.state.activeStudentId = studentId;
    this.state.creatingStudent = false;
    const student = await this.getStudent(studentId);
    if (student) {
      editedStudentStore.setData(student, groupId!);
    }
  }

  setCreatingStudent() {
    if (!this.state.creatingStudent) {
      const groupId = editedStudentStore.getState().groupId;
      this.state.activeStudentId = null;
      this.state.creatingStudent = true;
      editedStudentStore.setNullishValues(groupId!);
    }
  }
}

export const studentsStore = new StudentsStore();

export interface IStudentsStore {
  students: IStudent[];
  studentsCount: number;
  activeStudentId: null | string;
  creatingStudent: boolean;
  supergroupStudents: SelectOption[];
  showAddExisting: boolean;
  existingStudentIdToAdd: null | string;
}
