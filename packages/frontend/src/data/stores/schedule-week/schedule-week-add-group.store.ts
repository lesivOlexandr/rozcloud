import bind from 'bind-decorator';
import { IGroup } from '@rozcloud/shared/interfaces';
import { reactive } from '@vue/reactivity';
import { AddGroupScheduleData, IAddGroupListItem } from '../../../common/interfaces/components/schedule-week';
import { scheduleEntitiesFlyweight } from './schedule-entities-flyweight';
import { generateNanoid } from '@rozcloud/shared/helpers/text.helpers';
import { IGroupsFilter } from '../../../common/interfaces/api/filter';
import { setEmployeeGroupsIds } from '../../../common/helpers/storage.helpers';
import { snackbarStore } from '../ui/snackbar.store';
import { groupsScheduleStore } from './schedule-week.group-schedule';

export class ScheduleAddGroupStore {
  data: AddGroupScheduleData = reactive({
    groupItems: [],
    isDialogOpen: false,
  });

  async setGroups() {
    const groups = await scheduleEntitiesFlyweight.getEmployeeGroups();
    this.data.groupItems = groups.map(group => ({
      group,
      itemKey: group.id,
      selectGroups: [],
      inputValue: group.name || '',
    }));
    this.addEmptyItem();
    return this;
  }

  @bind
  toggleDialogVisibility() {
    this.data.isDialogOpen = !this.data.isDialogOpen;

    if (this.data.isDialogOpen) {
      this.setGroups();
    }
  }

  @bind
  selectGroup(groupItem: IAddGroupListItem, group: IGroup) {
    groupItem.inputValue = group.name;
    groupItem.group = group;
    this.filterOutExtraElements();
    const groupItemIndex = this.data.groupItems.findIndex(item => item === groupItem);
    if (groupItemIndex !== -1 && groupItemIndex === this.data.groupItems.length - 1) {
      this.addEmptyItem();
    }
  }

  @bind
  removeGroup(groupItem: IAddGroupListItem) {
    this.data.groupItems = this.data.groupItems.filter(item => item.itemKey !== groupItem.itemKey);
    return groupItem;
  }

  @bind
  saveGroupItems() {
    const groups = this.data.groupItems.map(groupItem => groupItem.group).filter(Boolean) as IGroup[];
    const groupItemIds = groups.map(group => group.id);
    setEmployeeGroupsIds(groupItemIds);
    snackbarStore.showMessage('Зміни успішно збережено');
    this.toggleDialogVisibility();
    groupsScheduleStore.applyGroupsToCurrentWeek(groups);
    scheduleEntitiesFlyweight.setEmployeeGroups(groups);
    return groupItemIds;
  }

  async getGroupsForSelect(groupItem: IAddGroupListItem): Promise<IGroup[]> {
    const excludeIds = this.data.groupItems.map(item => item.group?.id).filter(Boolean) as string[];

    const filters: IGroupsFilter = {
      excludeIds,
      search: groupItem.inputValue,
    };

    const groups = await scheduleEntitiesFlyweight.getGroupsByFilters(filters);
    groupItem.selectGroups = groups;
    return groups;
  }

  private filterOutExtraElements() {
    const groupItems = this.data.groupItems;
    this.data.groupItems = groupItems.filter((item, index) => item.group || index === groupItems.length - 1);
  }

  private addEmptyItem() {
    const emptyItem: IAddGroupListItem = reactive({
      group: null,
      selectGroups: [],
      inputValue: '',
      itemKey: generateNanoid(),
    });
    this.data.groupItems.push(emptyItem);
  }
}

export const scheduleAddGroupStore = new ScheduleAddGroupStore();
