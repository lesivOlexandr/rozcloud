import bind from 'bind-decorator';
import { groupsScheduleStore } from '.';
import { subjectTypesMap } from '../../../common/constants';
import { ScheduleWeekCellTypes } from '../../../common/enums/cell-type.enum';
import { getElementRectangle, getEventPageCoords } from '../../../common/helpers/dom.helpers';
import { userToFullName } from '../../../common/helpers/mappers';
import { setSelectedScheduleItems } from '../../../common/helpers/storage.helpers';
import { copyToClipboard } from '../../../common/helpers/text';
import { ICellCoords } from '../../../common/interfaces/components/schedule-week';
import {
  IScheduleWeekSelectionElement,
  IScheduleWeekSelectionGroup,
} from '../../../common/interfaces/components/schedule-week.selection';

export class SheetSelectionStore {
  data: ICellCoords = {
    startX: 0,
    startY: 0,

    endX: 0,
    endY: 0,
  };

  selectionGroupCache: IScheduleWeekSelectionGroup = {
    elements: [],
  };
  hasSelected = false;

  @bind
  startTrackSelection(e: Event) {
    const { pageX, pageY } = getEventPageCoords(e as MouseEvent);
    this.data.startX = pageX;
    this.data.startY = pageY;

    document.addEventListener('mousemove', this.trackSelectionCoords);
    document.addEventListener('mousemove', this.trackCoordsShift);
    document.addEventListener('mouseup', this.removeListeners);
  }

  @bind
  trackSelectionCoords(e: MouseEvent) {
    const { pageX, pageY } = getEventPageCoords(e as MouseEvent);
    const deltaX = pageX - this.data.startX;
    const deltaY = pageY - this.data.startY;

    this.data.endX = this.data.startX + deltaX;
    this.data.endY = this.data.startY + deltaY;

    this.checkIfItemsUnderSelection();
  }

  getElementsWithCoords() {
    if (this.selectionGroupCache.elements?.length) {
      return this.selectionGroupCache;
    }

    const selectionGroupCache: IScheduleWeekSelectionElement[] = [];
    for (const scheduleDataDay of groupsScheduleStore.data.selectedWeek?.scheduleData || []) {
      for (const scheduleDataItem of scheduleDataDay.scheduleItems) {
        for (const scheduleDataItemEl of scheduleDataItem.scheduleItemData) {
          const { subjectInputEl, teacherInputEl, auditoryInputEl, subjectTypeInputEl } = scheduleDataItemEl;
          if (subjectInputEl && teacherInputEl && auditoryInputEl && subjectTypeInputEl) {
            const subjectElCoords = getElementRectangle(subjectInputEl);
            const teacherElCoords = getElementRectangle(teacherInputEl);
            const auditoryElCoords = getElementRectangle(auditoryInputEl);
            const subjectTypeElCoords = getElementRectangle(subjectTypeInputEl);

            const scheduleItemSubject: IScheduleWeekSelectionElement = {
              coords: subjectElCoords,
              item: scheduleDataItemEl,
              type: ScheduleWeekCellTypes.Subject,
            };
            const scheduleItemTeacher: IScheduleWeekSelectionElement = {
              coords: teacherElCoords,
              item: scheduleDataItemEl,
              type: ScheduleWeekCellTypes.Teacher,
            };
            const scheduleItemAuditory: IScheduleWeekSelectionElement = {
              coords: auditoryElCoords,
              item: scheduleDataItemEl,
              type: ScheduleWeekCellTypes.Auditory,
            };
            const scheduleItemSubjectType: IScheduleWeekSelectionElement = {
              coords: subjectTypeElCoords,
              item: scheduleDataItemEl,
              type: ScheduleWeekCellTypes.SubjectType,
            };

            selectionGroupCache.push(
              scheduleItemSubject,
              scheduleItemTeacher,
              scheduleItemAuditory,
              scheduleItemSubjectType
            );
          }
        }
      }
    }
    this.selectionGroupCache.elements = selectionGroupCache;
    return this.selectionGroupCache;
  }

  clearSelectionCache() {
    this.selectionGroupCache.elements = [];
  }

  checkIfItemsUnderSelection() {
    if (this.data.startX === this.data.endX || this.data.startY == this.data.endY) {
      return;
    }

    const selectionElements = this.getElementsWithCoords().elements;
    this.checkOverlappingItemsItems(selectionElements);
  }

  checkOverlappingItemsItems(selectionCrossData: IScheduleWeekSelectionElement[]) {
    for (const selectionItem of selectionCrossData) {
      const isSelected = this.isRectanglesOverlap(selectionItem);
      if (isSelected) {
        this.selectItemByType(selectionItem);
      }
      if (!isSelected) {
        this.deselectItemByType(selectionItem);
      }
    }
  }

  @bind
  unselectAll() {
    if (this.hasSelected) {
      this.hasSelected = false;
      this.doUnselectAll();
    }
    document.removeEventListener('keydown', this.listenUserReaction);
  }

  private doUnselectAll() {
    for (const scheduleDataDay of groupsScheduleStore.data.selectedWeek?.scheduleData || []) {
      for (const scheduleDataItem of scheduleDataDay.scheduleItems) {
        for (const scheduleDataItemEl of scheduleDataItem.scheduleItemData) {
          scheduleDataItemEl.subjectSelected = false;
          scheduleDataItemEl.teacherSelected = false;
          scheduleDataItemEl.auditorySelected = false;
          scheduleDataItemEl.subjectTypeSelected = false;
        }
      }
    }
  }

  @bind
  selectItemByType(selectionItem: IScheduleWeekSelectionElement) {
    this.hasSelected = true;
    if (selectionItem.type === ScheduleWeekCellTypes.Subject) {
      return (selectionItem.item.subjectSelected = true);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.Teacher) {
      return (selectionItem.item.teacherSelected = true);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.Auditory) {
      return (selectionItem.item.auditorySelected = true);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.SubjectType) {
      return (selectionItem.item.subjectTypeSelected = true);
    }
  }

  deselectItemByType(selectionItem: IScheduleWeekSelectionElement) {
    if (selectionItem.type === ScheduleWeekCellTypes.Subject) {
      return (selectionItem.item.subjectSelected = false);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.Teacher) {
      return (selectionItem.item.teacherSelected = false);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.Auditory) {
      return (selectionItem.item.auditorySelected = false);
    }
    if (selectionItem.type === ScheduleWeekCellTypes.SubjectType) {
      return (selectionItem.item.subjectTypeSelected = false);
    }
  }

  isRectanglesOverlap(selectionItem: IScheduleWeekSelectionElement) {
    const startX = Math.min(this.data.startX, this.data.endX);
    const endX = Math.max(this.data.startX, this.data.endX);
    const startY = Math.min(this.data.startY, this.data.endY);
    const endY = Math.max(this.data.startY, this.data.endY);

    if (startX > selectionItem.coords.endX || endX < selectionItem.coords.startX) {
      return false;
    }
    if (startY > selectionItem.coords.endY || endY < selectionItem.coords.startY) {
      return false;
    }

    return true;
  }

  @bind
  removeListeners() {
    document.addEventListener('keydown', this.listenUserReaction);
    document.removeEventListener('mousemove', this.trackSelectionCoords);
    document.removeEventListener('mouseup', this.removeListeners);
    document.removeEventListener('mousemove', this.trackCoordsShift);
    this.clearCoordsTimers();
    this.clearSelectionCache();
  }

  @bind
  listenUserReaction(e: KeyboardEvent) {
    if ((e.ctrlKey || e.metaKey) && e.code === 'KeyC') {
      this.copyHighlightedToExcelFormat();
      const highlightedItems = groupsScheduleStore.getSelected();
      setSelectedScheduleItems(highlightedItems);
    }
    if (e.code === 'Delete') {
      this.deleteHighlightedArea();
    }
  }

  scrollTimers = {
    leftStart: null as null | number,
    leftEnd: null as null | number,
    topStart: null as null | number,
    topEnd: null as null | number,
  };

  @bind
  trackCoordsShift(e: MouseEvent) {
    const viewWidth = window.outerWidth;
    const viewHeight = window.outerHeight;

    this.clearCoordsTimers();

    const { screenX, screenY } = e;
    if (screenX + 300 > viewWidth) {
      const scrollPayload = { left: 200, behavior: 'smooth' } as const;
      this.scrollTimers.leftEnd = setInterval(() => window.scrollBy(scrollPayload), 100);
    }
    if (screenX < 320) {
      const scrollPayload = { left: -200, behavior: 'smooth' } as const;
      this.scrollTimers.leftEnd = setInterval(() => window.scrollBy(scrollPayload), 100);
    }
    if (screenY + 60 > viewHeight) {
      const scrollPayload = { top: 200, behavior: 'smooth' } as const;
      this.scrollTimers.topEnd = setInterval(() => window.scrollBy(scrollPayload), 100);
    }
    if (screenY < 200) {
      const scrollPayload = { top: -200, behavior: 'smooth' } as const;
      this.scrollTimers.topStart = setInterval(() => window.scrollBy(scrollPayload), 100);
    }
  }

  clearCoordsTimers() {
    const { leftEnd, leftStart, topEnd, topStart } = this.scrollTimers;
    if (topStart) {
      clearInterval(topStart);
    }
    if (topEnd) {
      clearInterval(topEnd);
    }
    if (leftStart) {
      clearInterval(leftStart);
    }
    if (leftEnd) {
      clearInterval(leftEnd);
    }
  }

  deleteHighlightedArea() {
    const highlightedItems = groupsScheduleStore.getSelected();

    highlightedItems.selectedSubjects.flat(1).forEach(subjectItem => {
      subjectItem.subjectInputValue = null;
      subjectItem.subject = null;
      subjectItem.subjectSelectOptions = [];
    });

    highlightedItems.selectedTeachers.flat(1).forEach(teacherItem => {
      teacherItem.teacherInputValue = null;
      teacherItem.teacher = null;
      teacherItem.teacherSelectOptions = [];
    });

    highlightedItems.selectedAuditories.flat(1).forEach(auditoryItem => {
      auditoryItem.auditoryInputValue = null;
      auditoryItem.auditory = null;
      auditoryItem.auditorySelectOptions = [];
    });

    highlightedItems.selectedSubjectTypes.flat(1).forEach(subjectTypeItem => {
      subjectTypeItem.subjectTypeInputValue = null;
      subjectTypeItem.type = null;
    });
  }

  private copyHighlightedToExcelFormat() {
    const highlightedItems = groupsScheduleStore.getSelected();
    const maxItemsIndex = Math.max(
      highlightedItems.selectedSubjects.length,
      highlightedItems.selectedTeachers.length,
      highlightedItems.selectedAuditories.length,
      highlightedItems.selectedSubjectTypes.length
    );

    let csvStr = '';

    for (let i = 0; i < maxItemsIndex; i++) {
      const subjectNamesString = highlightedItems.selectedSubjects[i]
        ?.map(dataItem => dataItem.subject?.name || '')
        ?.join('	')
        .trim();

      const teacherNamesString = highlightedItems.selectedTeachers[i]
        ?.map(dataItem => (dataItem.teacher ? userToFullName(dataItem.teacher) : ''))
        .join('	')
        .trim();

      const auditoriesString = highlightedItems.selectedAuditories[i]
        ?.map(dataItem => dataItem.auditory?.name || '')
        .join('	')
        .trim();

      const subjectTypesString = highlightedItems.selectedSubjectTypes[i]
        ?.map(dataItem => (dataItem.type ? subjectTypesMap[dataItem.type] : ''))
        .join('	')
        .trim();

      if (subjectNamesString) {
        csvStr += 'Предмет:	' + subjectNamesString + '\n';
      }
      if (teacherNamesString) {
        csvStr += 'Викладач:	' + teacherNamesString + '\n';
      }
      if (auditoriesString) {
        csvStr += 'Аудиторія:	' + auditoriesString + '\n';
      }
      if (subjectTypesString) {
        csvStr += 'Тип пари:	' + subjectTypesString + '\n';
      }
    }
    copyToClipboard(csvStr);
    return csvStr;
  }
}

export const sheetSelectionStore = new SheetSelectionStore();
