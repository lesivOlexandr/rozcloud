import { formatDate } from '@rozcloud/shared/helpers/time.helpers';
import {
  DateHierarchy,
  IAuditory,
  IAuditoryFilter,
  IGroup,
  ISubject,
  ISubjectFilter,
  ITeacher,
  ITeacherFilter,
  IWeekSchedulesFull,
} from '@rozcloud/shared/interfaces';
import { reactive } from '@vue/reactivity';
import { getEmployeeGroupsIds } from '../../../common/helpers/storage.helpers';
import { IGroupsFilter } from '../../../common/interfaces/api/filter';
import { AuditoriesService } from '../../api/auditories.service';
import { GroupsService } from '../../api/groups.service';
import { SubjectsService } from '../../api/subjects.service';
import { TeachersService } from '../../api/teachers.service';

export class ScheduleEntitiesFlyweight {
  groups: Map<string, IGroup> = new Map();
  teachers: Map<string, ITeacher> = new Map();
  subjects: Map<string, ISubject> = new Map();
  auditories: Map<string, IAuditory> = new Map();

  employeeGroupsList: IGroup[] = [];

  daysFormattedDateMap: Map<string, string> = new Map();

  async getEmployeeGroups() {
    if (!this.employeeGroupsList?.length) {
      this.employeeGroupsList = await this.fetchEmployeeGroups();
    }
    return this.employeeGroupsList;
  }

  setEmployeeGroups(groups: IGroup[]) {
    this.employeeGroupsList = groups;
  }

  async getAuditoriesByFilters(filters: IAuditoryFilter) {
    const auditoriesData = await AuditoriesService.getMany(filters);
    const auditories = auditoriesData.data;
    const filteredAuditories = await this.filterAuditories(auditories);
    return filteredAuditories;
  }

  async getTeachersByFilters(filters: ITeacherFilter) {
    const teachersData = await TeachersService.getByFilters(filters);
    const teachers = teachersData.data;
    const filteredTeachers = await this.filterTeachers(teachers);
    return filteredTeachers;
  }

  async getSubjectsByFilters(filters: ISubjectFilter) {
    const subjectsData = await SubjectsService.getByFilters(filters);
    const subjects = subjectsData.data;
    const filteredSubjects = this.filterSubjects(subjects);
    return filteredSubjects;
  }

  async getGroupsByFilters(filters: IGroupsFilter) {
    const params: IGroupsFilter = filters;
    const groups = await GroupsService.getGroups(params);
    const filteredGroups = this.filterGroups(groups);
    return filteredGroups;
  }

  private async fetchEmployeeGroups() {
    const groupIds = getEmployeeGroupsIds();
    const groups = await GroupsService.getByIds(groupIds);
    const filteredGroups = this.filterGroups(groups);
    return filteredGroups;
  }

  private filterTeachers(teachers: ITeacher[]) {
    return teachers.map(teacher => {
      if (!this.teachers.has(teacher.id)) {
        this.teachers.set(teacher.id, reactive(teacher));
      }
      return this.teachers.get(teacher.id)!;
    });
  }

  private filterSubjects(subjects: ISubject[]) {
    return subjects.map(subject => {
      if (!this.subjects.has(subject.id)) {
        this.subjects.set(subject.id, reactive(subject));
      }
      return this.subjects.get(subject.id)!;
    });
  }

  private filterAuditories(auditories: IAuditory[]) {
    return auditories.map(auditory => {
      if (!this.auditories.has(auditory.id)) {
        this.auditories.set(auditory.id, reactive(auditory));
      }
      return this.auditories.get(auditory.id)!;
    });
  }

  private filterGroups(groups: IGroup[]) {
    return groups.map(group => {
      if (!this.groups.has(group.id)) {
        this.groups.set(group.id, reactive(group));
      }
      return this.groups.get(group.id)!;
    });
  }

  handleDatesHierarchy(dateHierarchy: DateHierarchy): DateHierarchy {
    const yearNumber = dateHierarchy.studyYear;
    dateHierarchy.months.forEach(month => {
      const monthNumber = month.monthNumber - 1;
      month.dates.forEach(day => {
        const dateNumber = day.date;
        const date = new Date(yearNumber, monthNumber, dateNumber);
        const formattedDate = formatDate(date);
        this.daysFormattedDateMap.set(day.id, formattedDate);
      });
    });
    return dateHierarchy;
  }

  getFormattedDay(dayId: string): string | null {
    const month = this.daysFormattedDateMap.get(dayId);
    return month || null;
  }

  traverseGroupsSchedule(schedules: IWeekSchedulesFull[]) {
    return schedules;
  }
}

export const scheduleEntitiesFlyweight = new ScheduleEntitiesFlyweight();
