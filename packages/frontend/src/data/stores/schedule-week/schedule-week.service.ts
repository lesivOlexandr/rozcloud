import { ICreateScheduleWeekPayload, IDaySchedules, IScheduleDay, IScheduleItem } from '@rozcloud/shared/interfaces';
import bind from 'bind-decorator';
import { IScheduleItemData, IScheduleWeekData } from '../../../common/interfaces/components/schedule-week';
import { ScheduleWeekApiService } from '../../api/schedule-week';
import { snackbarStore } from '../ui/snackbar.store';
import { groupsScheduleStore } from './schedule-week.group-schedule';

export class ScheduleWeekService {
  @bind
  async saveSchedule() {
    const schedule = groupsScheduleStore.data.selectedWeek;
    if (!schedule) {
      return;
    }

    const weekNumber = schedule.week.weekNumber;
    const daySchedules = this.extractSchedule(schedule);

    const createSchedulePayload: ICreateScheduleWeekPayload = {
      weekNumber,
      daySchedules,
    };

    await ScheduleWeekApiService.save(createSchedulePayload);
    snackbarStore.showMessage('Розклад оновлено успішно');
  }

  @bind
  extractSchedule(schedule: IScheduleWeekData): IDaySchedules {
    const daySchedules: IDaySchedules = ({} as unknown) as IDaySchedules;
    for (const scheduleDayData of schedule.scheduleData) {
      const dayKey = scheduleDayData.day.dayName;
      const scheduleItems: IScheduleDay = ({} as unknown) as IScheduleDay;

      for (const scheduleItem of scheduleDayData.scheduleItems) {
        scheduleItems[(scheduleItem.itemNumber as unknown) as keyof IScheduleDay] = scheduleItem.scheduleItemData.map(
          this.extractScheduleDataItems
        );
      }
      daySchedules[(dayKey as unknown) as keyof IDaySchedules] = (scheduleItems as unknown) as IScheduleDay;
    }
    return daySchedules;
  }

  @bind
  extractScheduleDataItems(scheduleItemData: IScheduleItemData): IScheduleItem {
    return {
      auditoryId: scheduleItemData.auditory?.id || null,
      groupId: scheduleItemData.group.id,
      teacherId: scheduleItemData.teacher?.id || null,
      subjectId: scheduleItemData.subject?.id || null,
      type: scheduleItemData.type,
    };
  }
}

export const scheduleWeekService = new ScheduleWeekService();
