import bind from 'bind-decorator';
import { subjectTypes } from '../../../common/constants';
import { IScheduleItemData } from '../../../common/interfaces/components/schedule-week';

export class ScheduleWeekItemsController {
  @bind
  deleteSubjectData(scheduleItemDataEl: IScheduleItemData) {
    scheduleItemDataEl.subject = null;
    scheduleItemDataEl.subjectInputValue = '';
    scheduleItemDataEl.subjectSelectOptions = [];
  }
  @bind
  deleteTeacherData(scheduleItemDataEl: IScheduleItemData) {
    scheduleItemDataEl.teacher = null;
    scheduleItemDataEl.teacherInputValue = '';
    scheduleItemDataEl.teacherSelectOptions = [];
  }
  @bind
  deleteAuditoryData(scheduleItemDataEl: IScheduleItemData) {
    scheduleItemDataEl.auditory = null;
    scheduleItemDataEl.auditoryInputValue = '';
    scheduleItemDataEl.auditorySelectOptions = [];
  }
  @bind
  deleteSubjectTypeData(scheduleItemDataEl: IScheduleItemData) {
    scheduleItemDataEl.type = null;
    scheduleItemDataEl.subjectTypeInputValue = '';
    scheduleItemDataEl.subjectTypesSelectOptions = subjectTypes;
  }
}

export const scheduleWeekItemsController = new ScheduleWeekItemsController();
