import {
  IDateMeta,
  IGroup,
  IScheduleDayFull,
  IScheduleItemFull,
  IWeekInverted,
  IWeekSchedulesFull,
} from '@rozcloud/shared/interfaces';
import {
  IGroupScheduleItemData,
  IScheduleItemData,
  IScheduleWeekData,
  IScheduleWeekGroupItem,
  IScheduleWeeksData,
} from '../../../common/interfaces/components/schedule-week';
import bind from 'bind-decorator';
import { reactive } from '@vue/reactivity';
import { scheduleItemNumbers, subjectTypes, subjectTypesMap } from '../../../common/constants';
import { generateNanoid } from '@rozcloud/shared/helpers/text.helpers';
import { GroupScheduleItemDataMapper } from '../../models/groups-mapper';
import { isArrayEquals } from '../../../common/helpers/utils';
import { ISelectedScheduleItems } from '../../../common/interfaces/components/schedule-week.selection';
import { ScheduleWeekApiService } from '../../api/schedule-week';
import { getEmployeeGroupsIds } from '../../../common/helpers/storage.helpers';
import { scheduleEntitiesFlyweight } from './schedule-entities-flyweight';
import { userToFullName } from '../../../common/helpers/mappers';

export class GroupsScheduleStore {
  data: IScheduleWeeksData = reactive({
    weeks: new Map(),
    selectedWeek: null,
    scheduleItemsData: [],
    daysData: [],
  });

  selectWeek(week: IWeekInverted, weekDays: IDateMeta[], groups: IGroup[]): IScheduleWeekData {
    const selectedWeek = this.data.weeks.get(week.id);
    if (selectedWeek) {
      this.selectExistingWeek(selectedWeek);

      return selectedWeek;
    }
    if (!selectedWeek) {
      const addedWeek = this.addWeek(week, weekDays, groups);
      this.data.weeks.set(week.id, addedWeek);
      this.data.selectedWeek = addedWeek;
      this.getWeeksDataFromServer(week);
    }

    return this.data.selectedWeek!;
  }

  applyGroupsToCurrentWeek(groups: IGroup[]) {
    const selectedWeek = this.data.selectedWeek;
    if (selectedWeek) {
      this.applyGroupsToWeek(selectedWeek, groups);
    }
  }

  private selectExistingWeek(selectedWeek: IScheduleWeekData) {
    const oldGroups = this.data.selectedWeek?.groupItems.map(groupItem => groupItem.group) || [];
    const oldGroupIds = oldGroups?.map(group => group.id);
    const newGroups = selectedWeek.groupItems;
    const newGroupIds = newGroups.map(groupItem => groupItem.group).map(group => group.id);
    this.data.selectedWeek = selectedWeek;

    if (!isArrayEquals(oldGroupIds, newGroupIds)) {
      this.applyGroupsToCurrentWeek(oldGroups);
    }
  }

  private applyGroupsToWeek(weekData: IScheduleWeekData, groups: IGroup[]) {
    const oldScheduleItemsDataMapper = new GroupScheduleItemDataMapper();
    const currentWeek = weekData.week;

    weekData.groupItems = groups.map(this.createScheduleGroupItem);

    const scheduleData = weekData.scheduleData;
    scheduleData.forEach(scheduleDayData => {
      const scheduleDay = scheduleDayData.day;
      const scheduleItemsOld = scheduleDayData.scheduleItems;
      const scheduleItemsNew = scheduleItemNumbers.map(itemNumber => this.createScheduleItemData(itemNumber, groups));

      scheduleItemsOld.forEach(scheduleItem => {
        scheduleItem.scheduleItemData.forEach(item => {
          oldScheduleItemsDataMapper.add(currentWeek, scheduleDay, scheduleItem.itemNumber, item.group, item);
        });
      });

      scheduleItemsNew.forEach(scheduleItem => {
        scheduleItem.scheduleItemData.forEach(scheduleItemData => {
          const oldScheduleItemData = oldScheduleItemsDataMapper.get(
            currentWeek,
            scheduleDay,
            scheduleItem.itemNumber,
            scheduleItemData.group
          );
          if (oldScheduleItemData) {
            Object.assign(scheduleItemData, oldScheduleItemData);
          }
        });
        scheduleDayData.scheduleItems = scheduleItemsNew;
      });
    });
    oldScheduleItemsDataMapper.clear();
    return weekData;
  }

  highlightGroup(index: number) {
    const groupItem = this.data.selectedWeek?.groupItems?.[index];
    if (groupItem) {
      groupItem.isHighlighted = true;
    }
  }

  unHighlightGroup(index: number) {
    const groupItem = this.data.selectedWeek?.groupItems?.[index];
    if (groupItem) {
      groupItem.isHighlighted = false;
    }
  }

  addWeek(week: IWeekInverted, weekDays: IDateMeta[], groups: IGroup[]): IScheduleWeekData {
    const weekId = week.id;
    const weekData = this.createWeekData(week, weekDays, groups);
    const reactiveWeekData = reactive(weekData);
    this.data.weeks.set(weekId, reactiveWeekData);
    return this.data.weeks.get(weekId)!;
  }

  @bind
  private createWeekData(week: IWeekInverted, weekDays: IDateMeta[], groups: IGroup[]): IScheduleWeekData {
    return {
      week,
      isLoading: false,
      groupItems: groups.map(this.createScheduleGroupItem),
      scheduleData: weekDays.map(day => ({
        day,
        scheduleItems: scheduleItemNumbers.map(itemNumber => this.createScheduleItemData(itemNumber, groups)),
      })),
    };
  }

  @bind
  private createScheduleGroupItem(group: IGroup): IScheduleWeekGroupItem {
    return {
      group,
      isHighlighted: false,
    };
  }

  @bind
  private createScheduleItemData(itemNumber: number, groups: IGroup[]): IGroupScheduleItemData {
    return {
      itemNumber,
      scheduleItemData: groups.map(group => ({
        id: generateNanoid(),
        group,
        auditory: null,
        subject: null,
        teacher: null,
        type: null,

        auditoryInputValue: null,
        subjectInputValue: null,
        teacherInputValue: null,
        subjectTypeInputValue: null,

        auditorySelectOptions: [],
        subjectSelectOptions: [],
        teacherSelectOptions: [],
        subjectTypesSelectOptions: subjectTypes,
      })),
    };
  }

  getSelected(): ISelectedScheduleItems {
    const selectedSubjects: IScheduleItemData[][] = [];
    const selectedTeachers: IScheduleItemData[][] = [];
    const selectedAuditories: IScheduleItemData[][] = [];
    const selectedSubjectTypes: IScheduleItemData[][] = [];

    let lastUsedSubject_ij = -1;
    let lastUsedTeacher_ij = -1;
    let lastUsedAuditory_ij = -1;
    let lastUsedSubjectType_ij = -1;

    this.data.selectedWeek?.scheduleData.forEach((scheduleDayData, i) => {
      scheduleDayData.scheduleItems.forEach((scheduleItem, j) => {
        scheduleItem.scheduleItemData.forEach(scheduleItemData => {
          const ij = i + j;
          if (scheduleItemData.subjectSelected && lastUsedSubject_ij !== ij) {
            selectedSubjects.push([]);
            lastUsedSubject_ij = ij;
          }
          if (scheduleItemData.subjectSelected) {
            const lastSubjectsArrayIndex = selectedSubjects.length - 1;
            selectedSubjects[lastSubjectsArrayIndex].push(scheduleItemData);
          }

          if (scheduleItemData.teacherSelected && lastUsedTeacher_ij !== ij) {
            selectedTeachers.push([]);
            lastUsedTeacher_ij = ij;
          }
          if (scheduleItemData.teacherSelected) {
            const lastTeachersArrayIndex = selectedTeachers.length - 1;
            selectedTeachers[lastTeachersArrayIndex].push(scheduleItemData);
          }

          if (scheduleItemData.auditorySelected && lastUsedAuditory_ij !== ij) {
            selectedAuditories.push([]);
            lastUsedAuditory_ij = ij;
          }
          if (scheduleItemData.auditorySelected) {
            const lastUsedAuditoryIndex = selectedAuditories.length - 1;
            selectedAuditories[lastUsedAuditoryIndex].push(scheduleItemData);
          }

          if (scheduleItemData.subjectTypeSelected && lastUsedSubjectType_ij !== ij) {
            selectedSubjectTypes.push([]);
            lastUsedSubjectType_ij = ij;
          }
          if (scheduleItemData.subjectTypeSelected) {
            const lastSubjectTypeArrayIndex = selectedSubjectTypes.length - 1;
            selectedSubjectTypes[lastSubjectTypeArrayIndex].push(scheduleItemData);
          }
        });
      });
    });

    return {
      selectedSubjects: selectedSubjects,
      selectedTeachers: selectedTeachers,
      selectedAuditories: selectedAuditories,
      selectedSubjectTypes: selectedSubjectTypes,
    };
  }

  async getWeeksDataFromServer(week: IWeekInverted) {
    const groupIds = getEmployeeGroupsIds();
    const scheduleWeek = this.data.weeks.get(week.weekNumber);
    if (!scheduleWeek) {
      return;
    }
    scheduleWeek.isLoading = true;
    const groupsSchedules = await ScheduleWeekApiService.getFromServer(week, groupIds);
    const groupsSchedulesTraversed = scheduleEntitiesFlyweight.traverseGroupsSchedule(groupsSchedules);
    scheduleWeek.isLoading = false;
    this.applyGroupSchedule(groupsSchedulesTraversed, week);
  }

  groupIdxCache = new Map<string, number>();

  getGroupIdx(week: IWeekInverted, groupId: string) {
    const cachedValue = this.groupIdxCache.get(groupId);
    if (cachedValue) {
      return cachedValue;
    }
    const weekSchedule = this.data.weeks.get(week.weekNumber);
    if (!weekSchedule) {
      return -1;
    }
    const groupIdx = weekSchedule.groupItems.findIndex(groupItem => groupItem.group.id === groupId);
    this.groupIdxCache.set(groupId, groupIdx);
    return groupIdx;
  }

  clearGroupIdxCache() {
    this.groupIdxCache.clear();
  }

  applyGroupSchedule(schedules: IWeekSchedulesFull[], week: IWeekInverted) {
    const weekSchedule = this.data.weeks.get(week.weekNumber)!;

    for (const schedule of schedules) {
      const dayKeys = Object.keys(schedule) as (keyof IWeekSchedulesFull)[];
      for (const dayKey of dayKeys) {
        if (!schedule[dayKey]) continue;
        const itemKeys = (Object.keys(schedule[dayKey]!) as unknown) as (keyof IScheduleDayFull)[];
        for (const itemKey of itemKeys) {
          if (!schedule[dayKey]![itemKey]) continue;
          const groupId = schedule[dayKey]![itemKey]!.group.id;
          const groupIdx = this.getGroupIdx(week, groupId);
          const dayIndex = dayKey === '0' ? 6 : +dayKey - 1;
          const scheduleItemIndex = itemKey - 1;

          const scheduleItemDataEl =
            weekSchedule.scheduleData[dayIndex].scheduleItems[scheduleItemIndex].scheduleItemData[groupIdx];
          const scheduleItemData = schedule[dayKey]![itemKey]!;

          this.assignScheduleItemValues(scheduleItemDataEl, scheduleItemData);
        }
      }
    }
  }

  assignScheduleItemValues(scheduleItemDataEl?: IScheduleItemData, scheduleItemData?: IScheduleItemFull) {
    if (!scheduleItemDataEl || !scheduleItemData) {
      return;
    }

    scheduleItemDataEl.subject = scheduleItemData.subject;
    scheduleItemDataEl.subjectInputValue = scheduleItemData.subject?.name || '';

    scheduleItemDataEl.teacher = scheduleItemData.teacher;
    scheduleItemDataEl.teacherInputValue = userToFullName(scheduleItemData.teacher);

    scheduleItemDataEl.auditoryInputValue = scheduleItemData.auditory?.name || '';
    scheduleItemDataEl.auditory = scheduleItemData.auditory;

    scheduleItemDataEl.subjectTypeInputValue = scheduleItemData.type ? subjectTypesMap[scheduleItemData.type] : '';
    scheduleItemDataEl.type = scheduleItemData.type;
  }
}

export const groupsScheduleStore = new GroupsScheduleStore();
