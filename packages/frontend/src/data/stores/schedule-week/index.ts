import {
  DateHierarchy,
  IMonthInverted,
  InvertedDateHierarchy,
  ISemesterInverted,
  IWeekInverted,
} from '@rozcloud/shared/interfaces';
import { reactive } from '@vue/reactivity';
import { inverseDateHierarchy } from '@rozcloud/shared/helpers/time.helpers';
import { TimeService } from '../../api/time.service';
import { ScheduleWeekUi } from '../../../common/interfaces/components/schedule-week';
import { groupsScheduleStore } from './schedule-week.group-schedule';
import { scheduleEntitiesFlyweight } from './schedule-entities-flyweight';
import { sortByWeekDaysOrder } from '../../../common/helpers/utils';
import { snackbarStore } from '../ui/snackbar.store';
import bind from 'bind-decorator';
export { groupsScheduleStore } from './schedule-week.group-schedule';

export class ScheduleStore {
  datesStore = reactive<ScheduleWeekUi>({
    datesData: null,
    isLoadingDates: false,
    selectedYear: null,
    selectedSemesters: [],
    selectedMonths: [],
    selectedWeeks: [],

    chosenYear: null,
    chosenMonth: null,
    chosenSemester: null,
    chosenWeek: null,
  });

  async getDates(year: number = new Date().getFullYear()) {
    this.setDatesLoading(true);
    const dateHierarchy: DateHierarchy = await TimeService.getTime(year);
    scheduleEntitiesFlyweight.handleDatesHierarchy(dateHierarchy);
    this.setDatesLoading(false);
    const invertedDateHierarchy = inverseDateHierarchy(dateHierarchy);
    this.setFetchedDates(invertedDateHierarchy);
  }

  @bind
  doChoseSemester(semester: ISemesterInverted) {
    if (semester.id !== this.datesStore.chosenSemester?.id) {
      this.chooseSemester(semester);
      this.chooseFirstMonthWeek();

      if (this.datesStore.chosenMonth?.semester !== semester.id) {
        this.choseMonth(this.datesStore.selectedMonths[0]);
      }

      if (!this.datesStore.chosenWeek?.months.includes(this.datesStore.chosenMonth?.id)) {
        this.chooseFirstMonthWeek();
      }
    }
  }

  @bind
  doChooseMonth(month: IMonthInverted) {
    if (month !== this.datesStore.chosenMonth) {
      const semester = this.datesStore.chosenSemester;
      if (semester?.id !== month.semester) {
        const semester = this.datesStore?.datesData?.semesters.find(semester => semester.id === month.semester);
        if (semester) {
          this.chooseSemester(semester);
        }
        if (!semester) {
          snackbarStore.showMessage('Виникла помилка, спробуйте перезавантажити сторінку');
        }
      }
      this.choseMonth(month);
      this.chooseFirstMonthWeek();
    }
  }

  @bind
  doChooseWeek(week: IWeekInverted) {
    const isAppropriateMonth = week.months.includes(this.datesStore.chosenMonth?.id);
    if (!isAppropriateMonth) {
      const weekMonthId = week.months[0];
      const month = this.datesStore.datesData?.months.find(month => month.id === weekMonthId);
      if (month) {
        this.choseMonth(month);
      }
      if (!month) {
        snackbarStore.showMessage('Виникла помилка, спробуйте перезавантажити сторінку');
      }
    }

    const month = this.datesStore.chosenMonth;
    const semester = this.datesStore.chosenSemester;

    if (semester?.id !== month?.semester) {
      const semester = this.datesStore?.datesData?.semesters.find(semester => semester.id === month?.semester);
      if (semester) {
        this.chooseSemester(semester);
      }
      if (!semester) {
        snackbarStore.showMessage('Виникла помилка, спробуйте перезавантажити сторінку');
      }
    }

    this.chooseWeek(week);
  }

  private setDatesLoading(loadingState: boolean) {
    this.datesStore.isLoadingDates = loadingState;
  }

  private setFetchedDates(data: InvertedDateHierarchy) {
    this.datesStore.datesData = reactive(data);
    this.datesStore.selectedYear = data.years[0].yearNumber;
    this.datesStore.selectedSemesters = data.semesters;
    this.datesStore.selectedMonths = data.months;
    this.datesStore.selectedWeeks = data.weeks;
    this.choseFirstDates(data);
  }

  private choseFirstDates(data: InvertedDateHierarchy) {
    this.datesStore.chosenYear = data.years[0];
    this.chooseSemester(data.semesters[0]);
    this.choseMonth(data.months[6]);
    this.chooseFirstMonthWeek();
  }

  private chooseSemester(semester: ISemesterInverted) {
    this.datesStore.chosenSemester = semester;
    const months = this.datesStore.datesData?.months;
    this.datesStore.selectedMonths = months?.filter(month => month.semester === semester.id) || [];
  }

  private choseMonth(month: IMonthInverted) {
    this.datesStore.chosenMonth = month;
    const weeks = this.datesStore.datesData?.weeks;
    this.datesStore.selectedWeeks = weeks?.filter(week => week.months.includes(month.id)) || [];
  }

  private async chooseFirstMonthWeek() {
    const firstWeek = this.datesStore.selectedWeeks[0];
    if (firstWeek) {
      return this.chooseWeek(firstWeek);
    }
  }

  private async chooseWeek(week: IWeekInverted) {
    const groups = await scheduleEntitiesFlyweight.getEmployeeGroups();
    this.datesStore.chosenWeek = week;

    const year = this.datesStore.chosenYear;
    const month = this.datesStore.chosenMonth;
    const dates = this.datesStore.datesData?.dates;
    const weekDates = dates?.filter(date => date.weekNumber === week.weekNumber);

    const weekDatesOrdered = sortByWeekDaysOrder(weekDates || []);

    if (month && year) {
      groupsScheduleStore.selectWeek(week, weekDatesOrdered, groups);
    }
  }
}

export const scheduleStore = new ScheduleStore();
