import { Store } from '../store.base';
import { ICreateAuditoryPayload } from '@rozcloud/shared/interfaces';
import { AuditoryTypes } from '@rozcloud/shared/enums';

export class EditedAuditoryStore extends Store<EditedAuditoryState> {
  private createNullishDepartment(): EditedAuditoryState {
    return {
      id: null,
      name: '',
      image: null,
      auditoryTypes: defaultAuditoryTypes(),
      corpusId: null!,
      universityId: null,
      logo: null,
    };
  }

  protected data(): EditedAuditoryState {
    return this.createNullishDepartment();
  }

  setNullishValues(corpusId: string | null) {
    if (corpusId) {
      const nullishData = this.createNullishDepartment();
      nullishData.corpusId = corpusId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedAuditoryState>, corpusId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.name = data.name || '';
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.corpusId = data.corpusId || corpusId!;
    this.state.auditoryTypes = data.auditoryTypes || defaultAuditoryTypes();
  }

  setCorpusId(corpusId: string) {
    this.state.corpusId = corpusId;
  }

  setUniversityId(universityId: string) {
    this.state.universityId = universityId;
  }
}

const defaultAuditoryTypes = () => [AuditoryTypes.Lecture, AuditoryTypes.Practice];

export const editedAuditoryStore = new EditedAuditoryStore();

export interface EditedAuditoryState extends ICreateAuditoryPayload {
  id: string | null;
  image: File | null;
}
