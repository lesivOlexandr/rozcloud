import { IAuditory, ICreateAuditoryPayload, IUpdateAuditoryPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { AuditoriesService } from '../../api/auditories.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedAuditoryStore } from './edited-auditory.store';

export class DepartmentsStore extends Store<IAuditoriesStore> {
  data(): IAuditoriesStore {
    return {
      auditories: [],
      auditoriesCount: 0,
      activeAuditoryId: null,
      creatingAuditory: false,
    };
  }

  getByUniversity() {
    const universityId = editedAuditoryStore.getState().universityId;
    if (universityId) {
      // return this.fetchDepartmentsByFaculty(facultyId);
    }
  }

  getByCorpus() {
    const corpusId = editedAuditoryStore.getState().corpusId;
    if (corpusId) {
      return this.fetchAuditoriesByCorpus(corpusId);
    }
  }

  private async fetchAuditoriesByCorpus(corpusId: string) {
    const auditoriesData = await AuditoriesService.getAuditoriesByCorpus(corpusId);
    this.state.auditories = auditoriesData?.data || [];
    this.state.auditoriesCount = auditoriesData.data?.length || 0;
    return auditoriesData;
  }

  async getAuditory(id: string) {
    const auditory = this.state.auditories.find((a) => a.id === id);
    return auditory;
  }

  async createAuditory(auditory: WithImage<ICreateAuditoryPayload>) {
    const createdAuditory = await AuditoriesService.createOne(auditory);
    this.setAudotoriesData(createdAuditory, auditory.corpusId);
    this.setActiveAuditory(createdAuditory.id);
    snackbarStore.showMessage('Аудиторія успішно створено');
    return this.getByCorpus();
  }

  async updateAuditory(id: string, auditory: WithImage<IUpdateAuditoryPayload>) {
    await AuditoriesService.updateOne(id, auditory);
    snackbarStore.showMessage('Дані аудиторії успішно змінено');
    return this.getByCorpus();
  }

  private setAudotoriesData(editedAuditory: IAuditory, corpusId: string) {
    editedAuditoryStore.setData(editedAuditory, corpusId);
  }

  async setActiveAuditory(auditoryId: string) {
    const corpusId = editedAuditoryStore.getState().corpusId;
    this.state.activeAuditoryId = auditoryId;
    this.state.creatingAuditory = false;
    const auditory = await this.getAuditory(auditoryId);
    if (auditory) {
      editedAuditoryStore.setData(auditory, corpusId);
    }
  }

  setCreatingAuditory() {
    if (!this.state.creatingAuditory) {
      const corpusId = editedAuditoryStore.getState().corpusId;
      this.state.activeAuditoryId = null;
      this.state.creatingAuditory = true;
      editedAuditoryStore.setNullishValues(corpusId);
    }
  }
}

export const auditoriesStore = new DepartmentsStore();

export interface IAuditoriesStore {
  auditories: IAuditory[];
  auditoriesCount: number;
  activeAuditoryId: string | null;
  creatingAuditory: boolean;
}
