/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { reactive } from 'vue';

export abstract class Store<T extends Object> {
  protected state: T;

  constructor() {
    const data = this.data();
    this.setup(data);
    this.state = reactive(data) as T;
  }

  protected abstract data(): T;

  protected setup(_: T): void {}

  public getState(): T {
    return this.state as T;
  }
}
