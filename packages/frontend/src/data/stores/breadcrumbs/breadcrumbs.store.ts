import { BreadcrumbItem } from '../../../common/interfaces/components/breadcrumbs';
import { Store } from '../store.base';

export class BreadcrumbsStore extends Store<BreadcrumbsState> {
  data(): BreadcrumbsState {
    return {
      items: [],
    };
  }

  addItem(label: string, link: string) {
    this.addBreadcrumb({ label, link });
  }

  private addBreadcrumb(item: BreadcrumbItem) {
    this.state.items.push(item);
  }

  clear() {
    this.state.items = [];
    return this;
  }
}

export const breadcrumbsStore = new BreadcrumbsStore();

export interface BreadcrumbsState {
  items: BreadcrumbItem[];
}
