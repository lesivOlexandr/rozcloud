import { Store } from '../store.base';
import { ICreateUniversityPayload, IUniversity, IUpdateUniversityPayload } from '@rozcloud/shared/interfaces';
import { UniversitiesService } from '../../api/universities.service';
import { editedUniversityStore } from './edited-university.store';
import { universirsitiesUiStateStore } from './universities-ui-state.store';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { snackbarStore } from '../ui/snackbar.store';

export class UniversitiesStore extends Store<IUniversitiesStore> {
  protected data(): IUniversitiesStore {
    return {
      universitiesCount: 0,
      universities: [],
    };
  }

  public async getUniversities() {
    return this.fetchUniversities();
  }

  private async fetchUniversities() {
    const universitiesData = await UniversitiesService.getMany();
    this.state.universitiesCount = universitiesData.data?.length || 0;
    this.state.universities = universitiesData.data;
    return universitiesData;
  }

  async getUniversity(id: string) {
    const university = this.state.universities.find((u) => u.id === id);
    return university;
  }

  async createUniversity(university: WithImage<ICreateUniversityPayload>) {
    const createdUniversity = await UniversitiesService.createOne(university);
    editedUniversityStore.setData(createdUniversity);
    universirsitiesUiStateStore.setActiveUniversityId(createdUniversity.id);
    snackbarStore.showMessage('Університет успішно створено');
    return this.getUniversities();
  }

  async updateUniversity(id: string, univevrsity: IUpdateUniversityPayload) {
    await UniversitiesService.updateOne(id, univevrsity);
    snackbarStore.showMessage('Дані університету успішно змінено');
    return this.getUniversities();
  }
}

export const universitiesStore = new UniversitiesStore();

export interface IUniversitiesStore {
  universities: IUniversity[];
  universitiesCount: number;
}
