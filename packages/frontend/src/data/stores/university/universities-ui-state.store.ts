import { Store } from "../store.base";

export class UniversirsitiesUiStateStore extends Store<UniversityUiState> {
  data(): UniversityUiState {
    return {
      activeUniversityId: null,
      createUniversity: false,
    };
  }

  setActiveUniversityId(universityId: string) {
    this.state.activeUniversityId = universityId;
    this.state.createUniversity = false;
  }

  setCreatingUniversity() {
    this.state.createUniversity = true;
    this.state.activeUniversityId = null;
  }
}

export const universirsitiesUiStateStore = new UniversirsitiesUiStateStore();

export interface UniversityUiState {
  activeUniversityId: string | null;
  createUniversity: boolean;
}
