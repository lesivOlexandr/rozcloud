import { Store } from "../store.base";
import { ICreateUniversityPayload } from "@rozcloud/shared/interfaces";
import { HigherEducationType } from "@rozcloud/shared/enums";

export class EditedUniversityStore extends Store<EditedUniversityData> {
  private createNullishUniversity() {
    return {
      id: null,
      abbreviation: "",
      name: "",
      city: "",
      logo: null,
      image: null,
      type: HigherEducationType.University,
    };
  }

  protected data(): EditedUniversityData {
    return this.createNullishUniversity();
  }

  setNullishValues() {
    this.setData(this.createNullishUniversity());
  }

  setData(data: Partial<EditedUniversityData>) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.abbreviation = data.abbreviation || null;
    this.state.name = data.name || "";
    this.state.city = data.city || null;
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.type = data.type || HigherEducationType.University;
  }
}

export const editedUniversityStore = new EditedUniversityStore();

export interface EditedUniversityData extends ICreateUniversityPayload {
  id: string | null;
  image: File | null;
}
