import { ICreateTeacherPayload, ISubject, ITeacher, IUpdateTeacherPayload } from '@rozcloud/shared/interfaces';
import { ApiGetMany } from '../../../common/interfaces/api/get-many';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { SubjectsService } from '../../api/subjects.service';
import { TeachersService } from '../../api/teachers.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedTeacherStore } from './edited-teacher.store';

export class TeachersStore extends Store<ITeachersStore> {
  data(): ITeachersStore {
    return {
      teachers: [],
      teachersCount: 0,
      activeTeacherId: null,
      creatingTeacher: false,
      departmentSubjects: [],
    };
  }

  async getTeachers() {
    const { departmentId, originatedSubjectId } = editedTeacherStore.getState();
    let teachersData: ApiGetMany<ITeacher> = null!;
    if (originatedSubjectId) {
      teachersData = await this.fetchBySubject(originatedSubjectId);
    } else if (departmentId) {
      teachersData = await this.fetchByGroup(departmentId!);
    }
    this.state.teachers = teachersData.data;
    this.state.teachersCount = teachersData.data?.length || 0;
    const subjectsData = await SubjectsService.getByDepartment(departmentId!);
    this.state.departmentSubjects = subjectsData.data;

    return teachersData;
  }

  private fetchByGroup(departmentId: string) {
    return TeachersService.getByDepartment(departmentId);
  }

  private fetchBySubject(subjectId: string) {
    return TeachersService.getBySubject(subjectId);
  }

  async createTeacher(teacher: WithImage<ICreateTeacherPayload>) {
    const departmentId = editedTeacherStore.getState().departmentId!;
    const createdTeacher = await TeachersService.createOne(teacher);
    this.setTeacherData(createdTeacher, departmentId);
    this.setActiveTeacher(createdTeacher.id);
    snackbarStore.showMessage('Викладача успішно додано');
    return this.getTeachers();
  }

  async updateTeacher(id: string, teacher: WithImage<IUpdateTeacherPayload>) {
    const departmentId = editedTeacherStore.getState().departmentId;
    editedTeacherStore.setData(teacher, departmentId!);
    const teacherData = editedTeacherStore.getState();
    await TeachersService.updateOne(id, teacherData);
    snackbarStore.showMessage('Дані викладача успішно змінено');
    return this.getTeachers();
  }

  private setTeacherData(editedTeacher: ITeacher, departmentId: string) {
    editedTeacherStore.setData(editedTeacher, departmentId);
  }

  async getTeacher(id: string) {
    const teacher = this.state.teachers.find((t) => t.id === id);
    return teacher;
  }

  async setActiveTeacher(teacherId: string) {
    const departmentId = editedTeacherStore.getState().departmentId;
    this.state.activeTeacherId = teacherId;
    this.state.creatingTeacher = false;
    const teacher = await this.getTeacher(teacherId);
    if (teacher) {
      editedTeacherStore.setData(teacher, departmentId!);
    }
  }

  setCreatingTeacher() {
    if (!this.state.creatingTeacher) {
      const departmentId = editedTeacherStore.getState().departmentId;
      this.state.activeTeacherId = null;
      this.state.creatingTeacher = true;
      editedTeacherStore.setNullishValues(departmentId!);
    }
  }
}

export const teachersStore = new TeachersStore();

export interface ITeachersStore {
  teachers: ITeacher[];
  teachersCount: number;
  departmentSubjects: ISubject[];
  activeTeacherId: null | string;
  creatingTeacher: boolean;
}
