import { Store } from '../store.base';
import { ICreateTeacherPayload } from '@rozcloud/shared/interfaces';
import { ScienceDegrees } from '@rozcloud/shared/enums';

export class EditedTeacherStore extends Store<EditedTeacherState> {
  private createNullishTeacher(): EditedTeacherState {
    return {
      id: null,
      firstName: null,
      middleName: null,
      lastName: null,
      address: null,
      avatar: null,
      image: null,
      birthday: null,
      email: null,
      phone: null,
      login: '',
      password: '',
      gender: null,
      departmentId: null!,
      scienceDegree: ScienceDegrees.Candidate,
      subjectIds: [],
    };
  }

  protected data(): EditedTeacherState {
    return this.createNullishTeacher();
  }

  setNullishValues(departmentId: string) {
    if (departmentId) {
      const nullishData = this.createNullishTeacher();
      this.setData(nullishData, departmentId);
      if (this.state.originatedSubjectId) {
        nullishData.subjectIds?.push(this.state.originatedSubjectId);
      }
    }
  }

  setData(data: Partial<EditedTeacherState>, departmentId: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.address = data.address || null;
    this.state.avatar = data.avatar || null;
    this.state.birthday = data.birthday || null;
    this.state.email = data.email || null;
    this.state.firstName = data.firstName || null;
    this.state.middleName = data.middleName || null;
    this.state.lastName = data.lastName || null;
    this.state.login = data.login || '';
    this.state.password = data.password || null!;
    this.state.gender = data.gender || null;
    this.state.phone = data.phone || null;
    this.state.scienceDegree = data.scienceDegree || ScienceDegrees.Candidate;
    this.state.subjectIds = data.subjectIds || [];
    this.state.departmentId = data.departmentId || departmentId!;
  }

  setDepartmentId(departmentId: string) {
    this.state.departmentId = departmentId;
  }

  setOriginatedFrom(subjectId: string | null) {
    this.state.originatedSubjectId = subjectId;
  }
}

export const editedTeacherStore = new EditedTeacherStore();

export interface EditedTeacherState extends Partial<ICreateTeacherPayload> {
  id: string | null;
  image: File | null;
  originatedSubjectId?: string | null;
}
