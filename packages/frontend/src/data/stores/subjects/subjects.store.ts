import { ICreateSubjectPayload, ISubject, IUpdateSubjectPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { SubjectsService } from '../../api/subjects.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedSubjectStore } from './edited-subject.store';

export class SubjectsStore extends Store<ISubjectsStore> {
  data(): ISubjectsStore {
    return {
      subjects: [],
      subjectsCount: 0,
      activeSubjectId: null,
      creatingSubject: false,
    };
  }

  getByDepartment() {
    const departmentId = editedSubjectStore.getState().departmentId;
    if (departmentId) {
      return this.fetchSubjectsByFaculty(departmentId);
    }
  }

  private async fetchSubjectsByFaculty(departmentId: string) {
    const subjectsData = await SubjectsService.getByDepartment(departmentId);
    this.state.subjects = subjectsData?.data || [];
    this.state.subjectsCount = subjectsData.data?.length || 0;
    return subjectsData;
  }

  async getSubject(id: string) {
    const subject = this.state.subjects.find((s) => s.id === id);
    return subject;
  }

  async createSubject(subject: WithImage<ICreateSubjectPayload>) {
    const createdSubject = await SubjectsService.createOne(subject);
    this.setSubjectData(createdSubject, subject.departmentId!);
    this.setActiveSubject(createdSubject.id);
    snackbarStore.showMessage('Навчальна дисципліна успішно створено');
    return this.getByDepartment();
  }

  async updateSubject(id: string, subject: WithImage<IUpdateSubjectPayload>) {
    await SubjectsService.updateOne(id, subject);
    snackbarStore.showMessage('Дані навчальної дисципліни успішно змінено');
    return this.getByDepartment();
  }

  private setSubjectData(editedSubject: ISubject, univevrsityId: string) {
    editedSubjectStore.setData(editedSubject, univevrsityId);
  }

  async setActiveSubject(subjectId: string) {
    const departmentId = editedSubjectStore.getState().departmentId;
    this.state.activeSubjectId = subjectId;
    this.state.creatingSubject = false;
    const subject = await this.getSubject(subjectId);
    if (subject) {
      editedSubjectStore.setData(subject, departmentId);
    }
  }

  setCreatingSubject() {
    if (!this.state.creatingSubject) {
      const departmentId = editedSubjectStore.getState().departmentId;
      this.state.activeSubjectId = null;
      this.state.creatingSubject = true;
      editedSubjectStore.setNullishValues(departmentId!);
    }
  }
}

export const subjectsStore = new SubjectsStore();

export interface ISubjectsStore {
  subjects: ISubject[];
  subjectsCount: number;
  activeSubjectId: string | null;
  creatingSubject: boolean;
}
