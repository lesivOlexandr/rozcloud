import { Store } from '../store.base';
import { ICreateSubjectPayload } from '@rozcloud/shared/interfaces';

export class EditedSubjectStore extends Store<EditedSubjectState> {
  private createNullishSubject(): EditedSubjectState {
    return {
      id: null,
      name: '',
      departmentId: null!,
    };
  }

  protected data(): EditedSubjectState {
    return this.createNullishSubject();
  }

  setNullishValues(departmentId: string | null) {
    if (departmentId) {
      const nullishData = this.createNullishSubject();
      nullishData.departmentId = departmentId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedSubjectState>, departmentId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.name = data.name || '';
    this.state.departmentId = data.departmentId || departmentId!;
  }

  setDepartmentId(departmentId: string) {
    this.state.departmentId = departmentId;
  }
}

export const editedSubjectStore = new EditedSubjectStore();

export interface EditedSubjectState extends ICreateSubjectPayload {
  id: string | null;
}
