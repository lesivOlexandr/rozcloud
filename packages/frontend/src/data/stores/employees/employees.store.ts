import { ICreateEmployeePayload, IEmployee, IUpdateEmployeePayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { EmployeesService } from '../../api/employees.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedEmployeeStore } from './edited-employee.store';

export class EmployeesStore extends Store<IEmployeesStore> {
  data(): IEmployeesStore {
    return {
      employees: [],
      employeesCount: 0,
      activeEmployeeId: null,
      creatingEmployee: false,
    };
  }

  async getEmployees() {
    const structureId = editedEmployeeStore.getState().structureId;
    const employeesData = await this.fetchByStructure(structureId);
    this.state.employees = employeesData.data;
    this.state.employeesCount = employeesData.data?.length || 0;
    return employeesData;
  }

  private fetchByStructure(structureId: string) {
    return EmployeesService.getByStructure(structureId);
  }

  async createEmployee(employee: WithImage<ICreateEmployeePayload>) {
    const createdEmployee = await EmployeesService.createOne(employee);
    this.setEmployeeData(createdEmployee, employee.structureId);
    this.setActiveEmployee(createdEmployee.id);
    snackbarStore.showMessage('Робітника успішно додано');
    return this.getEmployees();
  }

  async updateEmployee(id: string, employee: WithImage<IUpdateEmployeePayload>) {
    editedEmployeeStore.setData(employee);
    const employeeData = editedEmployeeStore.getState();
    await EmployeesService.updateOne(id, employeeData);
    snackbarStore.showMessage('Дані робітника успішно змінено');
    return this.getEmployees();
  }

  private setEmployeeData(editedEmployee: IEmployee, structureId: string) {
    editedEmployeeStore.setData(editedEmployee, structureId);
  }

  async getEmployee(id: string) {
    const department = this.state.employees.find((e) => e.id === id);
    return department;
  }

  async setActiveEmployee(employeeId: string) {
    const structureId = editedEmployeeStore.getState().structureId;
    this.state.activeEmployeeId = employeeId;
    this.state.creatingEmployee = false;
    const employee = await this.getEmployee(employeeId);
    if (employee) {
      editedEmployeeStore.setData(employee, structureId);
    }
  }

  setCreatingEmployee() {
    if (!this.state.creatingEmployee) {
      const structureId = editedEmployeeStore.getState().structureId;
      this.state.activeEmployeeId = null;
      this.state.creatingEmployee = true;
      editedEmployeeStore.setNullishValues(structureId);
    }
  }
}

export const employeesStore = new EmployeesStore();

export interface IEmployeesStore {
  employees: IEmployee[];
  employeesCount: number;
  activeEmployeeId: null | string;
  creatingEmployee: boolean;
}
