import { Store } from '../store.base';
import { ICreateEmployeePayload } from '@rozcloud/shared/interfaces';
import { EmployeeTypes } from '@rozcloud/shared/enums';

export class EditedEmployeeStore extends Store<EditedEmployeeState> {
  private createNullishEmployee(): EditedEmployeeState {
    return {
      id: null,
      firstName: null,
      middleName: null,
      lastName: null,
      address: null,
      avatar: null,
      image: null,
      structureId: null!,
      birthday: null,
      email: null,
      employeeType: EmployeeTypes.University,
      phone: null,
      login: '',
      password: '',
      gender: null,
    };
  }

  protected data(): EditedEmployeeState {
    return this.createNullishEmployee();
  }

  setNullishValues(structureId: string) {
    if (structureId) {
      const nullishData = this.createNullishEmployee();
      nullishData.structureId = structureId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedEmployeeState>, structureId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.address = data.address || null;
    this.state.avatar = data.avatar || null;
    this.state.birthday = data.birthday || null;
    this.state.email = data.email || null;
    this.state.employeeType = data.employeeType || EmployeeTypes.University;
    this.state.firstName = data.firstName || null;
    this.state.middleName = data.middleName || null;
    this.state.lastName = data.lastName || null;
    this.state.login = data.login || '';
    this.state.password = data.password || null!;
    this.state.gender = data.gender || null;
    this.state.phone = data.phone || null;
    this.state.structureId = data.structureId || structureId!;
  }

  setStructureId(structureId: string) {
    this.state.structureId = structureId;
  }
}

export const editedEmployeeStore = new EditedEmployeeStore();

export interface EditedEmployeeState extends Partial<ICreateEmployeePayload> {
  id: string | null;
  image: File | null;
  structureId: string;
}
