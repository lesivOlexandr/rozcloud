import { IUserDataPayload, IUserLoginPayload, UnknownTypeUser } from '@rozcloud/shared/interfaces';
import { setToken } from '../../../common/helpers/storage.helpers';
import { AuthenticationService } from '../../api/auth.service';
import { Store } from '../store.base';

export class UserStore extends Store<UserStoreState> {
  data() {
    return { user: null };
  }

  async login(payload: IUserLoginPayload) {
    const loginResponse = await AuthenticationService.login(payload);
    this.state.user = loginResponse.user;
    setToken(loginResponse.jwtToken);
    return this.state.user;
  }

  async getData(payload: IUserDataPayload) {
    const userDataResponse = await AuthenticationService.userData(payload);
    this.state.user = userDataResponse.user;
    return this.state.user;
  }
}

export const userStore = new UserStore();

export interface UserStoreState {
  user: UnknownTypeUser | null;
}
