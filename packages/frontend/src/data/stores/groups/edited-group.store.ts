import { Store } from '../store.base';
import { ICreateGroupPayload, IGroupExtended, IStudent } from '@rozcloud/shared/interfaces';
import { StudentsService } from '../../api/students.service';
import { SelectOption } from '../../../common/interfaces/components';
import { userToFullName } from '../../../common/helpers/mappers';

export class EditedGroupStore extends Store<EditedGroupState> {
  private createNullishGroup(): EditedGroupState {
    return {
      id: null,
      name: '',
      logo: null,
      image: null,
      departmentId: null,
      groupCreatedYear: new Date().getFullYear(),
      superGroupId: null,
      specializationId: null,
      curatorId: null,
      groupPresidentId: null,
      students: [],
    };
  }

  protected data(): EditedGroupState {
    return this.createNullishGroup();
  }

  setNullishValues(universityId: string | null) {
    if (universityId) {
      const nullishData = this.createNullishGroup();
      nullishData.departmentId = universityId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedGroupState>, departmentId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.name = data.name || '';
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.departmentId = data.departmentId || departmentId || null;
    this.state.superGroupId = data.superGroupId || null;
    this.state.curatorId = data.curatorId || null;
    this.state.groupPresidentId = data.groupPresidentId || null;
    this.state.specializationId = data.specializationId || null;
    this.state.groupCreatedYear = data.groupCreatedYear || null;
    if (this.state.id) {
      this.fetchGroupStudents((this.state as unknown) as IGroupExtended);
    }
  }

  private async fetchGroupStudents(group: IGroupExtended) {
    const studentsData = await StudentsService.getStudentsByGroup(group.id);
    const studentsSelectData: SelectOption[] = studentsData.data.map((student) => ({
      value: student.id,
      label: userToFullName(student),
    }));
    group.students = (studentsSelectData as unknown) as IStudent[];
  }

  setDepartmentId(departmentId: string) {
    this.state.departmentId = departmentId;
  }
}

export const editedGroupStore = new EditedGroupStore();

export interface EditedGroupState extends ICreateGroupPayload {
  id: string | null;
  image: File | null;
  students: SelectOption[];
}
