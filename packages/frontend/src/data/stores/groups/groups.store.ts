import {
  ICreateGroupPayload,
  IGroup,
  IGroupExtended,
  ITeacher,
  IUpdateGroupPayload,
} from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { GroupsService } from '../../api/groups.service';
import { TeachersService } from '../../api/teachers.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { EditedGroupState, editedGroupStore } from './edited-group.store';

export class GroupsStore extends Store<IGroupsStore> {
  data(): IGroupsStore {
    return {
      groups: [],
      groupsCount: 0,
      activeGroupId: null,
      creatingGroup: false,
      departmentTeachers: [],
    };
  }

  getByDepartment() {
    const departmentId = editedGroupStore.getState().departmentId;
    if (departmentId) {
      return this.fetchGroupsByDepartment(departmentId);
    }
  }

  private async fetchGroupsByDepartment(departmentId: string) {
    const groupsData = await GroupsService.getAcademicGroupsByDepartment(departmentId);
    this.state.groups = groupsData?.data || [];
    this.state.groupsCount = groupsData.data?.length || 0;
    const teachersData = await TeachersService.getByDepartment(departmentId);
    this.state.departmentTeachers = teachersData.data;
    return groupsData;
  }

  async getGroup(id: string) {
    const group = this.state.groups.find((g) => g.id === id);
    return group;
  }

  async createGroup(group: WithImage<ICreateGroupPayload>) {
    const createdGroup = await GroupsService.createOne(group);
    this.setGroupData(createdGroup, group.departmentId);
    this.setActiveGroup(createdGroup.id);
    snackbarStore.showMessage('Група успішно створено');
    return this.getByDepartment();
  }

  async updateGroup(id: string, group: WithImage<IUpdateGroupPayload>) {
    await GroupsService.updateOne(id, group);
    snackbarStore.showMessage('Дані групи успішно змінено');
    return this.getByDepartment();
  }

  private setGroupData(editedGroup: IGroup, departmentId: string | null) {
    editedGroupStore.setData(editedGroup, departmentId);
  }

  async setActiveGroup(groupId: string) {
    const universityId = editedGroupStore.getState().departmentId;
    this.state.activeGroupId = groupId;
    this.state.creatingGroup = false;
    const group = await this.getGroup(groupId);
    if (group) {
      editedGroupStore.setData((group as unknown) as EditedGroupState, universityId);
    }
  }

  setCreatingGroup() {
    if (!this.state.creatingGroup) {
      const universityId = editedGroupStore.getState().departmentId;
      this.state.activeGroupId = null;
      this.state.creatingGroup = true;
      editedGroupStore.setNullishValues(universityId);
    }
  }

  clear() {
    this.state.groups = [];
    this.state.groupsCount = 0;
    this.state.creatingGroup = false;
    this.state.activeGroupId = null;
  }
}

export const groupsStore = new GroupsStore();

export interface IGroupsStore {
  groups: IGroupExtended[];
  groupsCount: number;
  activeGroupId: string | null;
  creatingGroup: boolean;
  departmentTeachers: ITeacher[];
}
