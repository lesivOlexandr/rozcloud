import { Store } from '../store.base';

export class SnackbarStore extends Store<ISnackbarStore> {
  data(): ISnackbarStore {
    return { message: null };
  }

  private timer: null | number = null;

  showMessage(message: string) {
    this.state.message = message;

    clearTimeout(this.timer || undefined);

    this.timer = setTimeout(() => {
      this.hideMessage();
    }, 3000);
  }

  hideMessage() {
    this.state.message = null;
  }
}

export const snackbarStore = new SnackbarStore();

export interface ISnackbarStore {
  message: string | null;
}
