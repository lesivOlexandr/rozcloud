import { Store } from '../store.base';
import { ICreateDepartmentPayload } from '@rozcloud/shared/interfaces';

export class EditedDepartmentStore extends Store<EditedDepartmentState> {
  private createNullishDepartment(): EditedDepartmentState {
    return {
      id: null,
      abbreviation: '',
      name: '',
      logo: null,
      image: null,
      facultyId: (null as unknown) as string,
    };
  }

  protected data(): EditedDepartmentState {
    return this.createNullishDepartment();
  }

  setNullishValues(facultyId: string | null) {
    if (facultyId) {
      const nullishData = this.createNullishDepartment();
      nullishData.facultyId = facultyId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedDepartmentState>, facultyId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.abbreviation = data.abbreviation || null;
    this.state.name = data.name || '';
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.facultyId = data.facultyId || ((facultyId as unknown) as string);
  }

  setFacultyId(facultyId: string) {
    this.state.facultyId = facultyId;
  }
}

export const editedDepartmentStore = new EditedDepartmentStore();

export interface EditedDepartmentState extends ICreateDepartmentPayload {
  id: string | null;
  image: File | null;
}
