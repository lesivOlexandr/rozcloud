import { ICreateDepartmentPayload, IDepartment, IUpdateDepartmentPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { DepartmentsService } from '../../api/departments.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { editedDepartmentStore } from './edited-department.store';

export class DepartmentsStore extends Store<IDepartmentsStore> {
  data(): IDepartmentsStore {
    return {
      departments: [],
      departmentsCount: 0,
      activeDepartmentId: null,
      creatingDepartment: false,
    };
  }

  getByFaculty() {
    const facultyId = editedDepartmentStore.getState().facultyId;
    if (facultyId) {
      return this.fetchDepartmentsByFaculty(facultyId);
    }
  }

  private async fetchDepartmentsByFaculty(facultyId: string) {
    const departmentsData = await DepartmentsService.getByFaculty(facultyId);
    this.state.departments = departmentsData?.data || [];
    this.state.departmentsCount = departmentsData.data?.length || 0;
    return departmentsData;
  }

  async getDepartment(id: string) {
    const department = this.state.departments.find((u) => u.id === id);
    return department;
  }

  async createDepartment(department: WithImage<ICreateDepartmentPayload>) {
    const createdDepartment = await DepartmentsService.createOne(department);
    this.setDepartmentData(createdDepartment, department.facultyId);
    this.setActiveDepartment(createdDepartment.id);
    snackbarStore.showMessage('Факультет успішно створено');
    return this.getByFaculty();
  }

  async updateDepartment(id: string, department: WithImage<IUpdateDepartmentPayload>) {
    await DepartmentsService.updateOne(id, department);
    snackbarStore.showMessage('Дані факультету успішно змінено');
    return this.getByFaculty();
  }

  private setDepartmentData(editedDepartment: IDepartment, univevrsityId: string) {
    editedDepartmentStore.setData(editedDepartment, univevrsityId);
  }

  async setActiveDepartment(departmentId: string) {
    const facultyId = editedDepartmentStore.getState().facultyId;
    this.state.activeDepartmentId = departmentId;
    this.state.creatingDepartment = false;
    const department = await this.getDepartment(departmentId);
    if (department) {
      editedDepartmentStore.setData(department, facultyId);
    }
  }

  setCreatingDepartment() {
    if (!this.state.creatingDepartment) {
      const facultyId = editedDepartmentStore.getState().facultyId;
      this.state.activeDepartmentId = null;
      this.state.creatingDepartment = true;
      editedDepartmentStore.setNullishValues(facultyId);
    }
  }
}

export const departmentsStore = new DepartmentsStore();

export interface IDepartmentsStore {
  departments: IDepartment[];
  departmentsCount: number;
  activeDepartmentId: string | null;
  creatingDepartment: boolean;
}
