export class AuthorizationStore {
  login: string | null = 'null';
  password: string | null = null;

  originatedFrom: string | null = null;
}

export const authorizationStore = new AuthorizationStore();
