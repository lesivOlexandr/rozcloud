import { Store } from '../store.base';
import { ICreateGroupPayload, IGroupExtended, IStudent } from '@rozcloud/shared/interfaces';
import { StudentsService } from '../../api/students.service';
import { SelectOption } from '../../../common/interfaces/components';
import { userToFullName } from '../../../common/helpers/mappers';

export class EditedSubgroupStore extends Store<EditedGroupState> {
  private createNullishGroup(): EditedGroupState {
    return {
      id: null,
      name: '',
      logo: null,
      image: null,
      departmentId: null,
      curatorId: null,
      groupCreatedYear: null,
      specializationId: null,
      superGroupId: null,
      groupPresidentId: null,
      students: [],
    };
  }

  protected data(): EditedGroupState {
    return this.createNullishGroup();
  }

  setNullishValues(universityId: string | null) {
    if (universityId) {
      const nullishData = this.createNullishGroup();
      nullishData.departmentId = universityId;
      this.setData(nullishData);
    }
  }

  setData(data: Partial<EditedGroupState>, groupId?: string | null) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.name = data.name || '';
    this.state.logo = data.logo || null;
    this.state.image = data.image || null;
    this.state.superGroupId = data.superGroupId || groupId || null;
    this.state.groupPresidentId = data.groupPresidentId || null;
    if (this.state.id) {
      this.fetchGroupStudents((this.state as unknown) as IGroupExtended);
    }
  }

  private async fetchGroupStudents(group: IGroupExtended) {
    const studentsData = await StudentsService.getStudentsByGroup(group.id);
    const studentsSelectData: SelectOption[] = studentsData.data.map((student) => ({
      value: student.id,
      label: userToFullName(student),
    }));
    group.students = (studentsSelectData as unknown) as IStudent[];
  }

  setSuperGroupId(groupId: string) {
    this.state.superGroupId = groupId;
  }
}

export const editedSubgroupStore = new EditedSubgroupStore();

export interface EditedGroupState extends ICreateGroupPayload {
  id: string | null;
  image: File | null;
  students: SelectOption[];
}
