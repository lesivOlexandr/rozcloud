import { ICreateGroupPayload, IGroup, IGroupExtended, IUpdateGroupPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { GroupsService } from '../../api/groups.service';
import { Store } from '../store.base';
import { snackbarStore } from '../ui/snackbar.store';
import { EditedGroupState, editedSubgroupStore } from './edited-subgroup.store';

export class SubroupsStore extends Store<IGroupsStore> {
  data(): IGroupsStore {
    return {
      groups: [],
      groupsCount: 0,
      activeGroupId: null,
      creatingGroup: false,
    };
  }

  getByGroup() {
    const superGroupId = editedSubgroupStore.getState().superGroupId;
    if (superGroupId) {
      return this.fetchGroupsBySupergroup(superGroupId);
    }
  }

  private async fetchGroupsBySupergroup(superGroupId: string) {
    const groupsData = await GroupsService.getSubgroups(superGroupId);
    this.state.groups = groupsData?.data || [];
    this.state.groupsCount = groupsData.data?.length || 0;
    return groupsData;
  }

  async getGroup(id: string) {
    const group = this.state.groups.find((g) => g.id === id);
    return group;
  }

  async createGroup(group: WithImage<ICreateGroupPayload>) {
    const createdGroup = await GroupsService.createOne(group);
    this.setGroupData(createdGroup, group.departmentId);
    this.setActiveGroup(createdGroup.id);
    snackbarStore.showMessage('Група успішно створено');
    return this.getByGroup();
  }

  async updateGroup(id: string, group: WithImage<IUpdateGroupPayload>) {
    await GroupsService.updateOne(id, group);
    snackbarStore.showMessage('Дані групи успішно змінено');
    return this.getByGroup();
  }

  private setGroupData(editedGroup: IGroup, groupId: string | null) {
    editedSubgroupStore.setData(editedGroup, groupId);
  }

  async setActiveGroup(groupId: string) {
    const superGroupId = editedSubgroupStore.getState().superGroupId;
    this.state.activeGroupId = groupId;
    this.state.creatingGroup = false;
    const group = await this.getGroup(groupId);
    if (group) {
      editedSubgroupStore.setData((group as unknown) as EditedGroupState, superGroupId);
    }
  }

  setCreatingGroup() {
    if (!this.state.creatingGroup) {
      const superGroupId = editedSubgroupStore.getState().superGroupId;
      this.state.activeGroupId = null;
      this.state.creatingGroup = true;
      editedSubgroupStore.setNullishValues(superGroupId);
    }
  }

  clear() {
    this.state.groups = [];
    this.state.groupsCount = 0;
    this.state.creatingGroup = false;
    this.state.activeGroupId = null;
  }
}

export const subgroupsStore = new SubroupsStore();

export interface IGroupsStore {
  groups: IGroupExtended[];
  groupsCount: number;
  activeGroupId: string | null;
  creatingGroup: boolean;
}
