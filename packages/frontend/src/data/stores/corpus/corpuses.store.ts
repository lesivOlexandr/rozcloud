import { Store } from '../store.base';
import { ICorpus, ICreateCorpusPayload, IUpdateCorpusPayload } from '@rozcloud/shared/interfaces';
import { WithImage } from '../../../common/interfaces/api/with-image';
import { snackbarStore } from '../ui/snackbar.store';
import { CorpusesService } from '../../api/corpuses.service';
import { editedCorpusStore } from './edited-corpus';

export class CorpusesStore extends Store<ICorpusesStore> {
  protected data(): ICorpusesStore {
    return {
      corpuses: [],
      corpusesCount: 0,
      activeCorpusId: null,
      creatingCorpus: false,
    };
  }

  async getByUniversity() {
    const universityId = editedCorpusStore.getState().universityId;
    if (universityId) {
      return this.fetchByUniversity(universityId);
    }
  }

  private async fetchByUniversity(universityId: string) {
    const corpusesData = await CorpusesService.getAllByUniversity(universityId);
    this.state.corpusesCount = corpusesData.data?.length || 0;
    this.state.corpuses = corpusesData.data;
    return corpusesData;
  }

  async getCorpus(id: string) {
    const corpus = this.state.corpuses.find((u) => u.id === id);
    return corpus;
  }

  async createCorpus(corpus: WithImage<ICreateCorpusPayload>) {
    const createdCorpus = await CorpusesService.createOne(corpus);
    this.setCorpusData(createdCorpus, corpus.universityId);
    this.setActiveCorpus(createdCorpus.id);
    snackbarStore.showMessage('Корпус успішно створено');
    return this.getByUniversity();
  }

  async updateCorpus(id: string, corpus: WithImage<IUpdateCorpusPayload>) {
    const updateCorpus = await CorpusesService.updateOne(id, corpus);
    snackbarStore.showMessage('Дані корпусу успішно змінено');
    return this.getByUniversity();
  }

  private setCorpusData(editedCorpus: ICorpus, univevrsityId: string) {
    editedCorpusStore.setData(editedCorpus, univevrsityId);
  }

  async setActiveCorpus(corpusId: string) {
    const universityId = editedCorpusStore.getState().universityId;
    this.state.activeCorpusId = corpusId;
    this.state.creatingCorpus = false;
    const corpus = await this.getCorpus(corpusId);
    if (corpus) {
      editedCorpusStore.setData(corpus, universityId);
    }
  }

  setCreatingCorpus() {
    if (!this.state.creatingCorpus) {
      const universityId = editedCorpusStore.getState().universityId;
      this.state.activeCorpusId = null;
      this.state.creatingCorpus = true;
      editedCorpusStore.setNullishValues(universityId);
    }
  }
}

export const corpusesStore = new CorpusesStore();

export interface ICorpusesStore {
  corpuses: ICorpus[];
  corpusesCount: number;
  activeCorpusId: string | null;
  creatingCorpus: boolean;
}
