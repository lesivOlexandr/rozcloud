import { Store } from '../store.base';
import { ICreateCorpusPayload } from '@rozcloud/shared/interfaces';

export class EditedCorpusStore extends Store<EditedCorpusState> {
  private createNullishCorpus(): EditedCorpusState {
    return {
      name: '',
      address: null,
      universityId: (null as unknown) as string,
      logo: null,
      id: null,
      image: null,
    };
  }

  protected data(): EditedCorpusState {
    return this.createNullishCorpus();
  }

  setNullishValues(universityId: string) {
    const nullishData = this.createNullishCorpus();
    nullishData.universityId = universityId;
    this.setData(nullishData, universityId);
  }

  setData(data: Partial<EditedCorpusState>, universityId: string) {
    if (!data) {
      return;
    }

    this.state.id = data.id || null;
    this.state.name = data.name || '';
    this.state.address = data.address || null;
    this.state.universityId = data.universityId || universityId;
    this.state.image = data.image || null;
    this.state.logo = data.logo || null;
  }

  setUniversityId(universityId: string) {
    this.state.universityId = universityId;
  }
}

export const editedCorpusStore = new EditedCorpusStore();

export interface EditedCorpusState extends ICreateCorpusPayload {
  id: string | null;
  image: File | null;
}
