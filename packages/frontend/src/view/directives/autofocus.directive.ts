export const autofocus = {
  mounted(el: HTMLElement) {
    el.focus();
  },
};
