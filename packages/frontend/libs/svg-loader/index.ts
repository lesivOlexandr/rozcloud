/* eslint-disable @typescript-eslint/no-var-requires */
const { extname } = require('path');
const fs = require('fs').promises;
const { compileTemplate } = require('@vue/compiler-sfc');
import { Plugin } from 'vite';

export default function svgLoader(): Plugin {
  return {
    name: 'svg-loader',
    enforce: 'pre',

    async load(id) {
      const [path, parameter] = id.split('?');

      if (!extname(path).startsWith('.svg') || parameter === 'url') {
        return null;
      }

      const svg = await fs.readFile(path, 'utf-8');

      const { code } = compileTemplate({
        id: JSON.stringify(id),
        source: svg,
        transformAssetUrls: false,
      });

      return `${code}\n export const VueComponent = render`;
    },
  };
}
