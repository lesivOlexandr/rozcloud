# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.1](https://bitbucket.org/lesivOlexandr/rozcloud/compare/v0.5.0...v0.5.1) (2021-05-29)

**Note:** Version bump only for package @rozcloud/server





# [0.4.0](https://bitbucket.org/lesivOlexandr/rozcloud/compare/v0.3.0...v0.4.0) (2021-01-10)


### Features

* **server:** add some get queries for entities ([d902f44](https://bitbucket.org/lesivOlexandr/rozcloud/commits/d902f444869681f3d63c0fe5c176ae090328cee2))
* **server:** connected all repositories to routes ([d39a112](https://bitbucket.org/lesivOlexandr/rozcloud/commits/d39a1121f12093550ead48ce0134fd7dd3fb230e))





# [0.3.0](https://bitbucket.org/lesivOlexandr/rozcloud/compare/v0.2.0...v0.3.0) (2021-01-09)


### Features

* **server:** add authentication functionality ([cd045bc](https://bitbucket.org/lesivOlexandr/rozcloud/commits/cd045bcd888bf2bb6e20fb00e99bd8c0e0bb838c))
* **server:** add create-update operations for teachers ([f26b7e9](https://bitbucket.org/lesivOlexandr/rozcloud/commits/f26b7e9877acdcbcb5e9cb3b40cf22214fe2ebb1))
* **server:** add create/update operation for auditories, corpuses, employees, subjects ([269acea](https://bitbucket.org/lesivOlexandr/rozcloud/commits/269aceafbcd11684bf884680c4a71895813f0111))
* **server:** allow CRUD operations on working plan ([6ef3738](https://bitbucket.org/lesivOlexandr/rozcloud/commits/6ef3738bedcfe72f5db21806c06b4a6107738710))
* **server:** allow to creaete schedules ([11cba3b](https://bitbucket.org/lesivOlexandr/rozcloud/commits/11cba3b51a9d993e2eccad56fa1d7864b79e5f4e))
* **server:** allow to CRUD educational plan ([85aaf7b](https://bitbucket.org/lesivOlexandr/rozcloud/commits/85aaf7b1114fa2e90856cf1c923546d75d194789))
* **server:** allow to get week schedules to be read ([880c727](https://bitbucket.org/lesivOlexandr/rozcloud/commits/880c7276e5ca93d5365326db89fcfb77ac94968c))
* **server:** involve JWT into autentication ([f23b75a](https://bitbucket.org/lesivOlexandr/rozcloud/commits/f23b75a8f3bb9cb8dc524b53a09431546e014daf))
* **shared:** setup linter ([d98a2cf](https://bitbucket.org/lesivOlexandr/rozcloud/commits/d98a2cfabc4a319c14187642f0932b5ec57da98a))





# 0.2.0 (2021-01-02)


### Features

* **server:** add commitlint ([a2737c2](https://bitbucket.org/lesivOlexandr/rozcloud/commits/a2737c2d17fba9d489f346f1294c2b715ca3a137))
