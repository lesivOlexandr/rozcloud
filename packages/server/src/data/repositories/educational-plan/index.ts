import { genCreationDbMeta, timePointDateValue } from 'src/data/database/helpers';
import {
  connectEducationalPlanItemToSemesterQuery,
  connectEducationalPlanSubjectQuery,
  createEducationalPlanQuery,
  deleteOneByGroupIdQuery,
  getOneEducationPlanByIdQuery,
  getOneEducPlanByGroupId,
} from './db-queries';
import {
  BaseCRUDRepository,
  IDBCreateUpdateEducationalPlanPayload,
  IDbEducationalPlanSubjectHoursMap,
  IDBSemesterSubjectControl,
  IEducPlanFull,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { generateUUID } from 'src/common/helpers/global.helper';
import { SemanticNode } from 'src/common/interfaces/repositories/semantic-node.interface';

@staticImplements<BaseCRUDRepository>()
export class EducationalPlansRepository extends BaseRepository {
  static async getOne(educationalPlanId: string) {
    const dbData = { educationalPlanId };
    const result = this.executeQuery<IEducPlanFull>(getOneEducationPlanByIdQuery, dbData);
    return result;
  }

  static async createOne(data: IDBCreateUpdateEducationalPlanPayload) {
    const dbData = {
      groupId: data.groupId,
      educPlanData: {
        id: generateUUID(),
        updatedAt: timePointDateValue(),
        createdAt: timePointDateValue(),
      },
    };
    // at first we should try to delete previous educational plan, cause it unique for group
    await this.deleteOneByGroupId(data.groupId);
    const result = await this.executeQuery<SemanticNode>(createEducationalPlanQuery, dbData);
    const connectEducPlanToSubject = this.connectEducPlanToSubject.bind(this, result.id);

    await Promise.all(data.subjectsHoursMap.map((item) => connectEducPlanToSubject(item)));
    const educPlan = await this.getOneByGroupId(data.groupId);
    return educPlan;
  }

  static async getOneByGroupId(groupId: string) {
    const dbData = { groupId };
    const educPlan = this.executeQuery<IEducPlanFull>(getOneEducPlanByGroupId, dbData);
    return educPlan;
  }

  static async deleteOneByGroupId(groupId: string) {
    const dbData = { groupId };
    await this.executeQuery<null>(deleteOneByGroupIdQuery, dbData, { silent: true });
  }

  static async updateOne(data: IDBCreateUpdateEducationalPlanPayload) {
    // the procedure is the same: delete previous if exist, create new one
    return EducationalPlansRepository.createOne(data);
  }

  private static async connectEducPlanToSubject(educationalPlanId: string, data: IDbEducationalPlanSubjectHoursMap) {
    const { subjectId, semesterSubjectControls, subjectPlanType, ...rest } = data;
    const dbData = genCreationDbMeta(rest, [], { subjectId, educationalPlanId, subjectPlanType });
    const result = await this.executeQuery<{ id: string }>(connectEducationalPlanSubjectQuery, dbData);
    const connectEducPlanItemToSemester = this.connectEducPlanItemToSemester.bind(this, result.id);
    await Promise.all(semesterSubjectControls.map(connectEducPlanItemToSemester));
    return result;
  }

  private static async connectEducPlanItemToSemester(educPlanItemId: string, data: IDBSemesterSubjectControl) {
    const dbData = {
      ...data,
      semesterData: {
        hoursPerWeek: data.hoursPerWeek,
        controlType: data.controlType,
      },
      educPlanItemId,
    };
    const result = await this.executeQuery(connectEducationalPlanItemToSemesterQuery, dbData);
    return result;
  }
}
