MATCH (educPlan:EducationalPlan)-[:OF_GROUP]-(group:AcademicGroup { id: $groupId })
OPTIONAL MATCH (educPlan)-[:HAS_ITEM]->(items:EducationalPlanItem)-[:HAS_SEMESTER_DATA]->(semesterData)
DETACH DELETE educPlan, items, semesterData