MATCH (group:AcademicGroup { id: $groupId })
CREATE (educationalPlan:EducationalPlan $educPlanData)-[:OF_GROUP]->(group)
MERGE (group)-[:HAS_EDUCATIONAL_PLAN]->(educationalPlan)
return properties(educationalPlan)
