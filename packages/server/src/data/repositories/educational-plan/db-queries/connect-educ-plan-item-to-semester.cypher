MATCH (semester:Semester { semesterNum: $semesterRealNumber })-[:OF_YEAR]->(:StudyYear { studyYear: $semesterYear })
MATCH (item:EducationalPlanItem { id: $educPlanItemId })
CREATE (item)-[:HAS_SEMESTER_DATA]->(semesterSubjectData:SemesterSubjectData $semesterData)
MERGE (groupSemester:SemesterOfGroup { semesterNumber: $semesterNumber })
MERGE (semesterSubjectData)-[:OF_NUMERIC_SEMESTER]->(groupSemester)
MERGE (semesterSubjectData)-[:OF_DATE_SEMESTER]->(semester)
WITH DISTINCT semesterSubjectData
RETURN properties(semesterSubjectData)
