MATCH(educationalPlan:EducationalPlan { id: $educationalPlanId })
return educationalPlan { 
	.*,
  group: properties(group),
  subjectsHoursMap: [
    (educationalPlan)-[:HAS_ITEM]-(educationalPlanItem) | educationalPlanItem { 
      .*,
      subjectPlanType: head([(educationalPlanItem)-[:HAS_PLAN_SUBJECT_TYPE]->(planSubject) | planSubject.planSubjectType ]),
      subject: head([(educationalPlanItem)-[:HAS_SUBJECT]->(subject) | subject { .* }]),
      semesterSubjectControls: [(educationalPlanItem)-[:HAS_SEMESTER_DATA]->(semesterData)-[:OF_NUMERIC_SEMESTER]-(semester) | semesterData  {
        .*,
        semesterNumber: semester.semesterNumber
      }]
    }
  ]
}