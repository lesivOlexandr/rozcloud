import { readDbQuerySync } from 'src/data/database/helpers';

export const getOneEducationPlanByIdQuery = readDbQuerySync(__dirname, './get-one-by-id.cypher');
export const createEducationalPlanQuery = readDbQuerySync(__dirname, './create-educ-plan.cypher');
export const connectEducationalPlanSubjectQuery = readDbQuerySync(__dirname, './connect-educ-plan-to-subject.cypher');
export const getOneEducPlanByGroupId = readDbQuerySync(__dirname, './get-educ-plan-by-group-id.cypher');
export const deleteOneByGroupIdQuery = readDbQuerySync(__dirname, './delete-educ-plan-by-group-id.cypher');
export const connectEducationalPlanItemToSemesterQuery = readDbQuerySync(
  __dirname,
  './connect-educ-plan-item-to-semester.cypher'
);
