import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import { createStudentQuery, getOneStudentQuery, getStudentGroups, updateOneStudentQuery } from './db-queries';
import {
  BaseCRUDRepository,
  IStudent,
  IDbUpdateStudentPayload,
  IDbCreateStudentPayload,
  IGroup,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from '../base-repository';

@staticImplements<BaseCRUDRepository>()
export class StudentsRepository extends BaseRepository {
  static async getOne(studentId: string) {
    const dbData = { studentId };
    const result = await this.executeQuery<IStudent>(getOneStudentQuery, dbData);
    return result;
  }

  static async createOne(data: IDbCreateStudentPayload) {
    const { academicGroupId, ...rest } = data;
    const dbData = genCreationDbMeta(rest, [], { academicGroupId });
    const result = await this.executeQuery<IStudent>(createStudentQuery, dbData);
    return result;
  }

  static async updateOne(studentId: string, data: IDbUpdateStudentPayload) {
    const dbData = genUpdatingDbMeta(data, { studentId });
    const result = await this.executeQuery<IStudent>(updateOneStudentQuery, dbData);
    return result;
  }

  static async getStudentGroups(studentId: string) {
    const dbData = { studentId };
    const result = await this.executeQuery<IGroup[]>(getStudentGroups, dbData);
    return result;
  }
}
