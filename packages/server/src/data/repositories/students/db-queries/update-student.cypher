MATCH (student:Student { id: $studentId })
SET student += $data
RETURN properties(student)