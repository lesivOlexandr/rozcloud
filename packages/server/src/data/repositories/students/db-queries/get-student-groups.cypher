MATCH (:Student { id: $studentId } )-[:BELONG_TO|HAS_SUPER_GROUP*1..2]->(group:Group)
RETURN collect(DISTINCT properties(group)) as studentGroups