MATCH (student:Student { id: $studentId }),
MATCH (group:Group { id: $groupId })
OPTIONAL MATCH (group)-[rel:HAS_PRESIDENT]-()
DELETE rel
MERGE (group)-[:HAS_PRESIDENT]->(student)
RETURN properties(group)