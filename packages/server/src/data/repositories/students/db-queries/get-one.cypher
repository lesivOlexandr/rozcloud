MATCH (student:Student { id: $studentId })
RETURN properties(student)
LIMIT 1