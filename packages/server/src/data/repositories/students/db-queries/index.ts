import { readDbQuerySync } from 'src/data/database/helpers';

export const createStudentQuery = readDbQuerySync(__dirname, './create-student.cypher');
export const getOneStudentQuery = readDbQuerySync(__dirname, './get-one.cypher');
export const updateOneStudentQuery = readDbQuerySync(__dirname, './update-student.cypher');
export const makeStudentPresidentQuery = readDbQuerySync(__dirname, './make-student-group-president.cypher');
export const getStudentGroups = readDbQuerySync(__dirname, './get-student-groups.cypher');
