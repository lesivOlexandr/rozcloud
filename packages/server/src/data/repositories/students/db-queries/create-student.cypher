MATCH(group:AcademicGroup { id: $academicGroupId })
CREATE(student:Student:User $data)-[:BELONG_TO]->(group)
MERGE (group)-[:HAS_STUDENT]->(student)
SET group.studentsQuantity = group.studentsQuantity + 1

WITH group, student
MATCH(group)-[:BELONG_TO*1..3]->(structure:Structure)
SET structure.studentsQuantity = structure.studentsQuantity + 1

RETURN properties(student)