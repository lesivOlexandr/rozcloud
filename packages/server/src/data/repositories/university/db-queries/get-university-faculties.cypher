MATCH (university:University { id: $universityId })-[:HOLDS]->(faculty)
RETURN collect(properties(faculty))