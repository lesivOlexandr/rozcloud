import { readDbQuerySync } from 'src/data/database/helpers';

export const getAllUniversitiesQuery = readDbQuerySync(__dirname, './get-all-universities.cypher');
export const createOneUniversityQuery = readDbQuerySync(__dirname, './create-university.cypher');
export const updateOneUniversityQuery = readDbQuerySync(__dirname, './update-university.cypher');
export const getUniversityQuery = readDbQuerySync(__dirname, './get-university.cypher');
export const getUniversityFacultiesQuery = readDbQuerySync(__dirname, './get-university-faculties.cypher');
export const getUniversityAuditoriesQuery = readDbQuerySync(__dirname, './get-university-auditories.cypher');
export const getUniversityCorpusesQuery = readDbQuerySync(__dirname, './get-university-corpuses.cypher');
