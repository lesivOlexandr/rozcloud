MATCH (university:University { id: $universityId })
RETURN properties(university)