MATCH (university:University { id: $universityId })-[:HAS_AUDITORY]->(auditory)
RETURN collect(properties(auditory))