MATCH (university:University { id: $universityId })
SET university += $data
RETURN properties(university)