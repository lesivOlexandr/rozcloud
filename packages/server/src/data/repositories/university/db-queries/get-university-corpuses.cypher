MATCH (university:University { id: $universityId })-[:HAS_CORPUS]->(corpus)
RETURN collect(properties(corpus))