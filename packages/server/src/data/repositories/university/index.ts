import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import {
  createOneUniversityQuery,
  getAllUniversitiesQuery,
  getUniversityAuditoriesQuery,
  getUniversityCorpusesQuery,
  getUniversityFacultiesQuery,
  getUniversityQuery,
  updateOneUniversityQuery,
} from 'src/data/repositories/university/db-queries';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import {
  IUniversity,
  BaseCRUDRepository,
  ICreateUniversityPayload,
  IUpdateUniversityPayload,
  IAuditory,
  ICorpus,
  IFaculty,
} from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';

@staticImplements<BaseCRUDRepository>()
export class UniversitiesRepository extends BaseRepository {
  static async getAll() {
    const universities = await this.executeQuery<IUniversity[]>(getAllUniversitiesQuery, {});
    return universities;
  }

  static async getOne(universityId: string) {
    const dbData = { universityId };
    const result = await this.executeQuery<IUniversity>(getUniversityQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateUniversityPayload) {
    const dbData = genCreationDbMeta(data, [
      'facultiesQuantity',
      'departmentsQuantity',
      'academicGroupsQuantity',
      'studentsQuantity',
      'teachersQuantity',
    ]);
    const result = await this.executeQuery<IUniversity>(createOneUniversityQuery, dbData);
    return result;
  }

  static async updateOne(universityId: string, data: IUpdateUniversityPayload) {
    const dbData = genUpdatingDbMeta(data, { universityId });
    const result = await this.executeQuery<IUniversity>(updateOneUniversityQuery, dbData);
    return result;
  }

  static async getUniversityAuditories(universityId: string) {
    const dbData = { universityId };
    const result = await this.executeQuery<IAuditory[]>(getUniversityAuditoriesQuery, dbData);
    return result;
  }

  static async getUniversityCorpuses(universityId: string) {
    const dbData = { universityId };
    const result = await this.executeQuery<ICorpus[]>(getUniversityCorpusesQuery, dbData);
    return result;
  }

  static async getUniversityFaculties(universityId: string) {
    const dbData = { universityId };
    const result = await this.executeQuery<IFaculty[]>(getUniversityFacultiesQuery, dbData);
    return result;
  }
}
