import { readDbQuerySync } from 'src/data/database/helpers';

export const createStudyYearQuery = readDbQuerySync(__dirname, './create-study-year.cypher');
export const createMonthQuery = readDbQuerySync(__dirname, './create-month.cypher');
export const createDateQuery = readDbQuerySync(__dirname, './create-date.cypher');
export const createWeekQuery = readDbQuerySync(__dirname, './create-week.cypher');
export const createSemesterQuery = readDbQuerySync(__dirname, './create-semester.cypher');
export const checkIfWeekExistsQuery = readDbQuerySync(__dirname, './check-week-exists.cypher');
export const getWeekWithDaysQuery = readDbQuerySync(__dirname, './get-week-with-days.cypher');
export const getYearDataQuery = readDbQuerySync(__dirname, './get-year-data.cypher');
export const getWeekDatesQuery = readDbQuerySync(__dirname, './get-week-dates.cypher');
