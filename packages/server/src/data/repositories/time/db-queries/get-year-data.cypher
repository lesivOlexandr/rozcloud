MATCH(studyYear:StudyYear { studyYear: $studyYear })
RETURN studyYear {
  .studyYear,
  months: [(studyYear)-[:HAS_MONTH]->(month) | month { 
    .id,
  	.monthName,
    .monthNumber,
    dates: [(month)-[:HAS_DAY]->(day) | day { .id, .dayName, .date, weekNumber: head([(day)-[:OF_WEEK]-(week) | week.weekNumber ]) }] 
  }]
}