MATCH (studyYear:StudyYear)-[:HAS_MONTH]->(studyMonth:StudyMonth)-[:HAS_DAY]->(date)-[:OF_WEEK]->(week:StudyWeek { weekNumber: $weekNumber })
WITH properties(date) AS dates, week, studyYear, studyMonth
ORDER BY dates.dayName
RETURN {
  weekNumber: week.weekNumber,
  dates: collect(dates {.*, studyYear: studyYear.studyYear, studyMonth: studyMonth.monthNumber })
}