MATCH (week { weekNumber: $weekNumber })
RETURN week { .*, days: [(week)-[:HAS_DAY]->(day) | properties(day)] } as week