MATCH (studyYear:StudyYear { studyYear: $year })
CREATE (semester:Semester $data)-[:OF_YEAR]->(studyYear)
MERGE (studyYear)-[:HAS_SEMESTER]->(semester)
RETURN properties(semester)