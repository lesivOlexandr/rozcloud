MERGE (week:StudyWeek { weekNumber: $weekNumber })
WITH(week)
UNWIND ($dateIds) AS dateId
MATCH (date:StudyDay { id: dateId })
MERGE (week)-[:HAS_DAY]->(date)
MERGE (date)-[:OF_WEEK]->(week)
WITH DISTINCT (week)
RETURN properties(week)