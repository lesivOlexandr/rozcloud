import { formatDate } from '@rozcloud/shared/helpers/time.helpers';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { generateUUID } from 'src/common/helpers/global.helper';
import {
  DateHierarchy,
  DatePayload,
  ICreateSemesterPayload,
  ICreateStudyYearPayload,
  IDateMeta,
  IGetWeekPayload,
  IMonthMeta,
  IMonthMetaExtended,
  ISemester,
  IStudyYear,
  IWeekMetaExtended,
  IWeekPayload,
  WithDbMeta,
} from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';
import {
  checkIfWeekExistsQuery,
  createDateQuery,
  createMonthQuery,
  createSemesterQuery,
  createStudyYearQuery,
  createWeekQuery,
  getWeekDatesQuery,
  getWeekWithDaysQuery,
  getYearDataQuery,
} from './db-queries';

export class TimeOperationsRepository extends BaseRepository {
  static async createStudyYear(data: ICreateStudyYearPayload) {
    const dbData = { ...data, id: generateUUID() };
    const studyYear = await this.executeQuery<IStudyYear>(createStudyYearQuery, dbData);
    return studyYear;
  }

  static async createSemester(year: number, data: ICreateSemesterPayload) {
    const dbData = { data, year };
    const semester = await this.executeQuery<ISemester>(createSemesterQuery, dbData);
    return semester;
  }

  static async insertDateHierarchy(data: DateHierarchy) {
    const { studyYear, months } = data;
    await this.createStudyYear({ studyYear: studyYear, id: generateUUID() });
    const insertMonthHierarchy = this.insertMonthHierarchy.bind(this, studyYear);
    return await Promise.all(months.map(insertMonthHierarchy));
  }

  private static async insertMonthHierarchy(studyYear: number, data: IMonthMetaExtended) {
    const { monthNumber, monthName, dates } = data;
    const monthMeta = {
      monthName,
      monthNumber,
      id: generateUUID(),
    };
    const dbData = {
      studyYear,
      data: monthMeta,
    };

    await this.executeQuery(createMonthQuery, dbData);
    const insertDayIntoHierarchy = this.insertDayIntoHierarchy.bind(this, studyYear, monthMeta);
    const daysMeta = await Promise.all(dates.map(insertDayIntoHierarchy));
    return daysMeta;
  }

  static async getHierarchy(studyYear: number) {
    const dbData = { studyYear };
    const result: DateHierarchy = await this.executeQuery(getYearDataQuery, dbData);
    return result;
  }

  static async getWeekPayload(weekNumber: number): Promise<IWeekPayload> {
    const dbData = { weekNumber };
    const result: IGetWeekPayload = await this.executeQuery(getWeekDatesQuery, dbData);

    const dates: DatePayload[] = result.dates
      .map(date => ({
        dayName: date.dayName,
        formattedDate: formatDate(new Date(date.studyYear, date.studyMonth, date.date)),
      }))
      .sort((date1, date2) => +new Date(date1.formattedDate) - +new Date(date2.formattedDate));

    return {
      weekNumber,
      dates,
    };
  }

  private static async insertDayIntoHierarchy(studyYear: number, monthMeta: IMonthMeta, data: IDateMeta) {
    const dbData = {
      studyYear,
      data: {
        ...data,
        id: generateUUID(),
      },
      ...monthMeta,
    };
    return await this.executeQuery<WithDbMeta<IDateMeta>>(createDateQuery, dbData);
  }

  static async createWeeks(data: IWeekMetaExtended[]) {
    for (const weekData of data) {
      const dbData = {
        weekNumber: weekData.weekNumber,
        dateIds: weekData.dates.map(date => date.id),
      };
      await this.executeQuery(createWeekQuery, dbData);
    }
  }

  static async checkIfWeekExists(weekNumber: number) {
    const dbData = { weekNumber: weekNumber };
    const isExist = await this.executeQuery(checkIfWeekExistsQuery, dbData);
    return isExist;
  }

  static async getWeekWithDays(weekNumber: number) {
    const isExist = await this.checkIfWeekExists(weekNumber);
    if (!isExist) {
      triggerServerError('The given week is not in database', 400);
    }
    const dbData = { weekNumber };
    const result = await this.executeQuery(getWeekWithDaysQuery, dbData);
    return result;
  }
}
