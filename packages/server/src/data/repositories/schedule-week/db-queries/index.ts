import { readDbQuerySync } from 'src/data/database/helpers';

export const createScheduleItemQuery = readDbQuerySync(__dirname, './create-schedule-item.cypher');
export const createWeekTemplateQuery = readDbQuerySync(__dirname, './create-week-template.cypher');
export const getScheduleTeacherQuery = readDbQuerySync(__dirname, './get-schedule-week-teacher.cypher');
export const getScheduleGroupQuery = readDbQuerySync(__dirname, './get-schedule-group.cypher');
export const getArrayScheduleGroupQuery = readDbQuerySync(__dirname, './get-array-schedule-group.cypher');
export const getScheduleStudentQuery = readDbQuerySync(__dirname, './get-schedule-week-student.cypher');
export const deleteScheduleByDaysGroupsQuery = readDbQuerySync(__dirname, './delete-schedule-by-days-groups.cypher');
