MATCH (week:StudyWeek { weekNumber: $weekNumber })-[:HAS_DAY]->(studyDay:StudyDay)
CALL {
	WITH studyDay
  MATCH (studyDay)-[rel:HAS_SCHEDULE_ITEM]-(item)-[:HAS_TEACHER]->(teacher { id: $teacherId })
	RETURN apoc.map.groupByMulti(collect(item {
      .*,
      itemNumber: rel.itemNumber,
      subject: head([(item)-[:HAS_SUBJECT]->(subject) | properties(subject)]),
      teacher: properties(teacher),
      group: head([(item)-[:HAS_GROUP]->(group) | properties(group)]),
      auditory: head([(item)-[:HAS_AUDITORY]->(auditory) | properties(auditory)])
  }), 'itemNumber') as daySchedule
}
return week { .*, daySchedules: apoc.map.mergeList(collect(apoc.map.fromValues([studyDay.dayName, daySchedule]) )) } as weekSchedule