MATCH (week:StudyWeek { weekNumber: $weekNumber })-[:HAS_DAY]->(studyDay:StudyDay)
WHERE studyDay.dayName IN $dayNames
CALL {
	WITH studyDay
  MATCH (studyDay)-[rel:HAS_SCHEDULE_ITEM]-(item)-[:HAS_GROUP]->(group)
  WHERE group.id IN $groupIds
  RETURN item as groupItems
}
DETACH DELETE groupItems
RETURN collect(studyDay);
