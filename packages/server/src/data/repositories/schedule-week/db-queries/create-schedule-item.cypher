MATCH(group:Group { id: $groupId })
MATCH(:StudyWeek { weekNumber: $weekNumber })-[:HAS_DAY]-(day { dayName: $dayName })
MATCH(wt:WeekTemplate { id: $weekTemplateId })

CREATE(scheduleItem:ScheduleItem $data)
MERGE(scheduleItem)-[:HAS_GROUP]->(group)
MERGE(day)-[:HAS_SCHEDULE_ITEM { itemNumber: $itemNumber }]->(scheduleItem)
MERGE(wt)-[:HAS_ITEM]-(scheduleItem)

WITH(scheduleItem)
OPTIONAL MATCH(auditory:Auditory { id: $auditoryId })
MERGE(scheduleItem)-[:HAS_AUDITORY]->(auditory)

WITH(scheduleItem)
OPTIONAL MATCH(subject:Subject { id: $subjectId })
MERGE(scheduleItem)-[:HAS_SUBJECT]->(subject)

WITH(scheduleItem)
OPTIONAL MATCH(teacher:Teacher { id: $teacherId })
MERGE(scheduleItem)-[:HAS_TEACHER]->(teacher)

RETURN properties(scheduleItem)