MATCH (week:StudyWeek { weekNumber: $weekNumber })-[:HAS_DAY]->(studyDay:StudyDay)
CALL {
	WITH studyDay
  MATCH (studyDay)-[rel:HAS_SCHEDULE_ITEM]-(item)-[:HAS_GROUP]->(group { id: $groupId })
  RETURN 
    apoc.map.groupByMulti(collect(item { 
    .*,
          itemNumber: rel.itemNumber,
          subject: head([(item)-[:HAS_SUBJECT]->(subject) | properties(subject)]),
          teacher: head([(item)-[:HAS_TEACHER]->(teacher) | properties(teacher)]),
          group: properties(group),
          auditory: head([(item)-[:HAS_AUDITORY]->(auditory) | properties(auditory)])
  }), 'itemNumber') as b
}
return week {
  .*,
  daySchedules: apoc.map.mergeList(collect(apoc.map.fromValues([studyDay.dayName, b])))
}