import { genCreationDbMeta } from 'src/data/database/helpers';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import {
  BaseCRUDRepository,
  ICreateScheduleWeekPayload,
  IScheduleItem,
  IWeekSchedulesFull,
} from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { SemanticNode } from 'src/common/interfaces/repositories/semantic-node.interface';
import {
  createScheduleItemQuery,
  createWeekTemplateQuery,
  deleteScheduleByDaysGroupsQuery,
  getArrayScheduleGroupQuery,
  getScheduleGroupQuery,
  getScheduleTeacherQuery,
} from './db-queries';
import { CreateOneScheduleItemPayload } from 'src/common/interfaces/repositories/schedule/create-one-schedule-item-payload';
import { extractScheduleItemsFromWeek } from './helpers';
import { UserTypes } from '@rozcloud/shared/enums';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { StudentsRepository } from '../students';
import { isArray, mergeWith } from 'lodash';

@staticImplements<BaseCRUDRepository>()
export class ScheduleWeekRepository extends BaseRepository {
  static async getOne(userId: string, weekNumber: number, userType: UserTypes) {
    if (userType === UserTypes.Teacher) {
      return this.getScheduleTeacher(userId, weekNumber);
    }
    if (userType === UserTypes.Student) {
      return this.getScheduleStudent(userId, weekNumber);
    }
    return triggerServerError('Only user of type "student" and "teacher" can have schedule', 400);
  }

  static async createOne(data: ICreateScheduleWeekPayload) {
    const weekTemplateData = await this.createWeekTemplateNode();
    const scheduleItems: [IScheduleItem, string, string][] = extractScheduleItemsFromWeek(data);

    const scheduleItemsCreatePromises: Promise<SemanticNode>[] = [];
    for (const scheduleItemData of scheduleItems) {
      const payload: CreateOneScheduleItemPayload = {
        dayName: scheduleItemData[1],
        itemNumber: scheduleItemData[2],
        weekNumber: data.weekNumber,
        weekTemplateId: weekTemplateData.id,
      };
      const scheduleItemCreatePromise = this.createOneScheduleItem(payload, scheduleItemData[0]);
      scheduleItemsCreatePromises.push(scheduleItemCreatePromise);
    }
    await Promise.all(scheduleItemsCreatePromises);
    return data;
  }

  static async updateOne(data: ICreateScheduleWeekPayload) {
    this.createOne(data);
  }

  static async createWeekTemplateNode() {
    const dbData = genCreationDbMeta({});
    const weekTemplate = await this.executeQuery<SemanticNode>(createWeekTemplateQuery, dbData);
    return weekTemplate;
  }

  static async createOneScheduleItem(data: CreateOneScheduleItemPayload, scheduleItemData: IScheduleItem) {
    const dbData = { ...data, ...scheduleItemData, data: { type: scheduleItemData.type } };
    const createdScheduleItem = await this.executeQuery<SemanticNode>(createScheduleItemQuery, dbData);
    return createdScheduleItem;
  }

  private static async getScheduleTeacher(teacherId: string, weekNumber: number) {
    const dbData = { teacherId, weekNumber };
    const scheduleTeacher = await this.executeQuery<IWeekSchedulesFull>(getScheduleTeacherQuery, dbData);
    return scheduleTeacher;
  }

  private static async getScheduleStudent(studentId: string, weekNumber: number) {
    const studentsGroups = await StudentsRepository.getStudentGroups(studentId);
    const studentGroupsIds = studentsGroups.map(group => group.id);

    const getScheduleGroup = this.getArrayScheduleGroup.bind(null, weekNumber);
    const groupsSchedule = await Promise.all(studentGroupsIds.map(getScheduleGroup));

    const customizer = (objValue: Record<string, unknown>, srcValue: Record<string, unknown>) => {
      if (isArray(objValue)) {
        return objValue.concat(srcValue);
      }
    };

    const studentSchedule = groupsSchedule.reduce((acc, curr) => mergeWith(acc, curr, customizer));
    return studentSchedule;
  }

  static async getScheduleGroup(groupId: string, weekNumber: number) {
    const dbData = { groupId, weekNumber };
    const scheduleGroup = await this.executeQuery<IWeekSchedulesFull>(getScheduleGroupQuery, dbData);
    return scheduleGroup;
  }

  static async getArrayScheduleGroup(weekNumber: number, groupId: string) {
    const dbData = { groupId, weekNumber };
    const scheduleGroup = await ScheduleWeekRepository.executeQuery<IWeekSchedulesFull>(
      getArrayScheduleGroupQuery,
      dbData
    );
    return scheduleGroup;
  }

  static async deleteScheduleByDaysGroups(groupIds: string[], dayNames: string[], weekNumber: number) {
    const dbData = { groupIds, dayNames, weekNumber };
    await this.executeQuery(deleteScheduleByDaysGroupsQuery, dbData);
  }
}
