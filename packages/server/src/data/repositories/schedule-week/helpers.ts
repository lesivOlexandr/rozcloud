import { ICreateScheduleWeekPayload, IScheduleDay, IScheduleItem } from '@rozcloud/shared/interfaces';

export const extractScheduleItemsFromWeek = (data: ICreateScheduleWeekPayload) => {
  return extractScheduleItems(extractDaySchedules(data));
};

export const extractDaySchedules = (data: ICreateScheduleWeekPayload) => {
  const daySchedules: [IScheduleDay, string][] = [];
  for (const key of Object.keys(data.daySchedules)) {
    const daySchedule = data.daySchedules[key as keyof typeof data.daySchedules];
    if (!daySchedule) continue;
    daySchedules.push([daySchedule, key]);
  }
  return daySchedules;
};

export const extractScheduleItems = (data: ReturnType<typeof extractDaySchedules>) => {
  const scheduleItems: [IScheduleItem, string, string][] = [];
  for (const dayScheduleData of data) {
    for (const key of Object.keys(dayScheduleData[0])) {
      const scheduleItemList = dayScheduleData[0][(key as unknown) as keyof typeof dayScheduleData[0]];
      if (!scheduleItemList) continue;
      for (const scheduleItem of scheduleItemList) {
        scheduleItems.push([scheduleItem, dayScheduleData[1], key]);
      }
    }
  }
  return scheduleItems;
};
