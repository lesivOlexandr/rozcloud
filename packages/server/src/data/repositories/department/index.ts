import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import {
  createOneDepartmentQuery,
  detDepartmentTeachersQuery,
  getDepartmentAcademicGroupsQuery,
  getDepartmentByIdQuery,
  getDepartmentSubjectsQuery,
  updateOneDepartmentQuery,
} from './db-queries';
import {
  BaseCRUDRepository,
  IDepartment,
  ICreateDepartmentPayload,
  IUpdateDepartmentPayload,
  IGroup,
  ISubject,
  ITeacher,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from '../base-repository';

@staticImplements<BaseCRUDRepository>()
export class DepartmentsRepository extends BaseRepository {
  static async getOne(departmentId: string) {
    const dbData = { departmentId };
    const result = await this.executeQuery<IDepartment>(getDepartmentByIdQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateDepartmentPayload) {
    const { facultyId, ...rest } = data;
    const dbData = genCreationDbMeta(rest, ['academicGroupsQuantity', 'studentsQuantity', 'teachersQuantity'], {
      facultyId,
    });

    const result = await this.executeQuery<IDepartment>(createOneDepartmentQuery, dbData);
    return result;
  }

  static async updateOne(departmentId: string, data: IUpdateDepartmentPayload) {
    const dbData = genUpdatingDbMeta(data, { departmentId });

    const result = await this.executeQuery<IDepartment>(updateOneDepartmentQuery, dbData);
    return result;
  }

  static async getDepartmentAcademicGroups(departmentId: string) {
    const dbData = { departmentId };
    const result = await this.executeQuery<IGroup>(getDepartmentAcademicGroupsQuery, dbData);
    return result;
  }

  static async getDepartmentSubjects(departmentId: string) {
    const dbData = { departmentId };
    const result = await this.executeQuery<ISubject[]>(getDepartmentSubjectsQuery, dbData);
    return result;
  }

  static async getDepartmentTeachers(departmentId: string) {
    const dbData = { departmentId };
    const result = await this.executeQuery<ITeacher[]>(detDepartmentTeachersQuery, dbData);
    return result;
  }
}
