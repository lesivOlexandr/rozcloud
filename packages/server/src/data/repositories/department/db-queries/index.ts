import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneDepartmentQuery = readDbQuerySync(__dirname, './create-department.cypher');
export const updateOneDepartmentQuery = readDbQuerySync(__dirname, './update-one-department.cypher');
export const getDepartmentByIdQuery = readDbQuerySync(__dirname, './get-department-by-id.cypher');
export const getDepartmentAcademicGroupsQuery = readDbQuerySync(__dirname, './get-department-academic-groups.cypher');
export const getDepartmentSubjectsQuery = readDbQuerySync(__dirname, './get-department-subjects.cypher');
export const detDepartmentTeachersQuery = readDbQuerySync(__dirname, './get-department-teachers.cypher');
