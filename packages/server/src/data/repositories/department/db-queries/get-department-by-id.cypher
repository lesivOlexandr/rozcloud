MATCH(department:Department { id: $departmentId })
RETURN properties(department)