MATCH (:Department { id: $departmentId })-[:HOLDS]->(academicGroup:AcademicGroup)
RETURN collect(academicGroup {
  .*,
  curatorId: head([(academicGroup)-[:HAS_CURATOR]->(curator) | curator.id]),
  groupCreatedYear: head([(academicGroup)-[:ENTERED_AT_YEAR]->(studyYear:StudyYear) | studyYear.studyYear]),
  groupPresidentId: head([(academicGroup)-[:HAS_PRESIDENT]->(president) | president.id]),
  specializationId: head([(academicGroup)-[:HAS_SPECIALIZATION]->(specialization) | specialization.id])
})