MATCH (department:Department { id: $departmentId })-[:HAS_SUBJECT]->(subject)
RETURN collect(properties(subject))