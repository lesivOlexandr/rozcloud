MATCH (department:Department { id: $departmentId })-[:HAS_TEACHER]->(teacher)
RETURN collect(teacher {
  .*,
  subjectIds: [(teacher)-[:HAS_SUBJECT]->(subject) | subject.id]
})