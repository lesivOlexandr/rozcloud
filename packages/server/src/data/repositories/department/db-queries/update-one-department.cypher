MATCH (department:Department { id: $departmentId })
SET department += $data
RETURN properties(department)