MATCH(faculty:Faculty { id: $facultyId })
CREATE(department:Department:Structure $data)-[:BELONG_TO]->(faculty)
MERGE(faculty)-[:HOLDS]->(department)
SET faculty.departmentsQuantity = faculty.departmentsQuantity + 1

WITH faculty, department
MATCH(faculty)-[:BELONG_TO]->(university:University)
SET university.departmentsQuantity = university.departmentsQuantity + 1

RETURN properties(department)