import { genCreationDbMeta, genUpdatingDbMeta, runDynamicDbQuery } from 'src/data/database/helpers';
import { ICreateGroupPayload, IUpdateGroupPayload, BaseCRUDRepository, IGroup, IStudent } from 'src/common/interfaces';
import {
  createOneGroupQuery,
  createOneAcademicGroupQuery,
  updateOneGroupQuery,
  bindToSpecializationQuery,
  bindToSuperGroupQuery,
  bindToDepartmentQuery,
  getAcademicGroupById,
  bindGroupToYear,
  bindStudentToGroupQuery,
  getGroupStudentsQuery,
  getGroupSubgroupsQuery,
  getGroupByIdQuery,
  bindGroupToCuratorQuery,
  bindGroupToGroupPresidentQuery,
  dynamicGetGroupsQuery,
} from './db-queries';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { IGroupFilters } from 'src/common/interfaces/repositories/filters/group';
import { DynamicQueryData } from 'src/common/interfaces/repositories/dynamic-query-data.inteface';

@staticImplements<BaseCRUDRepository>()
export class GroupsRepository extends BaseRepository {
  static async getOne(groupId: string) {
    const dbData = { groupId };
    const result = await this.executeQuery<IGroup>(getGroupByIdQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateGroupPayload) {
    const {
      departmentId,
      superGroupId,
      specializationId,
      groupCreatedYear,
      curatorId,
      groupPresidentId,
      ...rest
    } = data;
    const dbData = genCreationDbMeta(rest, ['studentsQuantity'], {
      ...(departmentId && { departmentId }),
    });

    let result: IGroup = (null as unknown) as IGroup;
    if (superGroupId) {
      const superGroup = await this.getOne(superGroupId);
      dbData.data.name = superGroup.name + ' -> ' + dbData.data.name;
    }
    if (departmentId) {
      result = await this.executeQuery<IGroup>(createOneAcademicGroupQuery, dbData);
      await this.bindGroupToDepartment(result.id, departmentId);
    } else {
      result = await this.executeQuery<IGroup>(createOneGroupQuery, dbData);
    }

    if (specializationId) {
      await this.bindGroupToSpecialization(result.id, specializationId);
    }
    if (curatorId) {
      await this.bindGroupToCurator(result.id, curatorId);
    }
    if (superGroupId) {
      await this.bindGroupToSuperGroup(result.id, superGroupId);
    }
    if (groupCreatedYear) {
      this.bindGroupToYear(result.id, groupCreatedYear);
    }
    if (groupPresidentId) {
      this.bindPresidentToGroup(result.id, groupPresidentId);
    }

    return result;
  }

  static async updateOne(groupId: string, data: IUpdateGroupPayload) {
    const { groupCreatedYear, curatorId, groupPresidentId, specializationId, departmentId, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { groupId });
    const result = await this.executeQuery<IGroup>(updateOneGroupQuery, dbData);
    if (groupCreatedYear) {
      this.bindGroupToYear(result.id, groupCreatedYear);
    }
    if (groupPresidentId) {
      await this.bindPresidentToGroup(result.id, groupPresidentId);
    }
    if (curatorId) {
      await this.bindGroupToCurator(result.id, curatorId);
    }
    if (specializationId) {
      await this.bindGroupToSpecialization(result.id, specializationId);
    }
    if (departmentId) {
      await this.bindGroupToDepartment(result.id, departmentId);
    }
    return result;
  }

  static async getGroups(filters: IGroupFilters) {
    const dynamicQueryData: DynamicQueryData = {
      data: [
        {
          queryString: 'groups.id in',
          parameter: filters.groupIds,
        },
        {
          queryString: 'not groups.id in',
          parameter: filters.excludeIds,
        },
        {
          queryString: 'groups.name contains',
          parameter: filters.search,
        },
      ],
    };

    const result = await runDynamicDbQuery<IGroup[]>(dynamicGetGroupsQuery, dynamicQueryData);
    return result;
  }

  static async getOneAcademicGroupById(groupId: string) {
    const dbData = { groupId };
    const result = await this.executeQuery<IGroup>(getAcademicGroupById, dbData);
    return result;
  }

  private static async bindGroupToSpecialization(groupId: string, specializationId: string) {
    const dbData = {
      groupId,
      specializationId,
    };
    const result = await this.executeQuery<IGroup>(bindToSpecializationQuery, dbData);
    return result;
  }

  private static async bindGroupToDepartment(groupId: string, departmentId: string) {
    const dbData = {
      groupId,
      departmentId,
    };
    const result = await this.executeQuery<IGroup>(bindToDepartmentQuery, dbData);
    return result;
  }

  private static async bindGroupToSuperGroup(groupId: string, superGroupId: string) {
    if (groupId === superGroupId) {
      triggerServerError('Group cannot be subgroup of itself', 400);
    }
    const dbData = {
      groupId,
      superGroupId,
    };
    const result = await this.executeQuery<IGroup>(bindToSuperGroupQuery, dbData);
    return result;
  }

  private static async bindGroupToYear(groupId: string, yearNumber: number) {
    const dbData = {
      groupId,
      yearNumber,
    };
    const result = await this.executeQuery(bindGroupToYear, dbData);
    return result;
  }

  static async bindStudentToGroup(groupId: string, studentId: string) {
    const dbData = {
      groupId,
      studentId,
    };
    const result = await this.executeQuery<IGroup>(bindStudentToGroupQuery, dbData);
    return result;
  }

  private static async bindGroupToCurator(groupId: string, curatorId: string) {
    const dbData = {
      groupId,
      curatorId,
    };
    const result = await this.executeQuery<IGroup>(bindGroupToCuratorQuery, dbData);
    return result;
  }

  private static async bindPresidentToGroup(groupId: string, studentId: string) {
    const dbData = {
      groupId,
      studentId,
    };
    const result = await this.executeQuery<IGroup>(bindGroupToGroupPresidentQuery, dbData);
    return result;
  }

  static async getGroupStudents(groupId: string) {
    const dbData = { groupId };
    const result = await this.executeQuery<IStudent[]>(getGroupStudentsQuery, dbData);
    return result;
  }

  static async getGroupSubgroups(groupId: string) {
    const dbData = { groupId };
    const result = await this.executeQuery<IGroup[]>(getGroupSubgroupsQuery, dbData);
    return result;
  }
}
