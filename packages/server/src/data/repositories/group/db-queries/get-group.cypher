MATCH(group:Group { id: $groupId })
RETURN properties(group)
LIMIT 1