MATCH(group:Group { id: $groupId })
OPTIONAL MATCH (group)-[:HAS_STUDENT]->(groupStudent:Student)
WITH collect(properties(groupStudent)) as groupStudents, group
OPTIONAL MATCH(group)-[:HAS_SUBGROUP*1..2]->(subgroup)-[:HAS_STUDENT]->(students:Student)
WITH distinct students as subgroupStudents, groupStudents
RETURN apoc.coll.toSet(apoc.coll.flatten([collect(properties(subgroupStudents)), groupStudents]))