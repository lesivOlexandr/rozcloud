MATCH (group:Group { id: $groupId })
OPTIONAL MATCH (group)-[rel1:HAS_SPECIALIZATION]->(spec), (spec)-[rel2:IS_SPECIALIZATION_OF_GROUP]->(group)
DELETE rel1, rel2

WITH DISTINCT group
MATCH (specialization:Specialization { id: $specializationId })
MERGE (group)-[:HAS_SPECIALIZATION]->(specialization)
MERGE (specialization)-[:IS_SPECIALIZATION_OF_GROUP]->(group)
RETURN properties(group)