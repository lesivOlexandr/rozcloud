MATCH(department:Department { id: $departmentId })
CREATE (group:Group:AcademicGroup $data)-[:BELONG_TO]->(department)

MERGE(department)-[:HOLDS]->(group)
SET department.academicGroupsQuantity = department.academicGroupsQuantity + 1

WITH(department)
MATCH(department)-[:BELONG_TO]->(faculty:Faculty)
SET faculty.academicGroupsQuantity = faculty.academicGroupsQuantity + 1

WITH(faculty)
MATCH (faculty)-[:BELONG_TO]->(university:University)
SET university.academicGroupsQuantity = university.academicGroupsQuantity + 1

WITH(university)
MATCH(group:AcademicGroup { id: $data.id })
RETURN properties(group)