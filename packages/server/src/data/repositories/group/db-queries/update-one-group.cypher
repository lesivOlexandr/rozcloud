MATCH (group:Group { id: $groupId })
SET group += $data
RETURN properties(group)