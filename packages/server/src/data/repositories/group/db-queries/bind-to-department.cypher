MATCH (group:Group { id: $groupId })
OPTIONAL MATCH(group)-[rel:BELONG_TO]->(department:Department), (department)-[:HOLDS]->(group)
DELETE rel
WITH DISTINCT group
MATCH (department:Department { id: $departmentId })
MERGE (group)-[:BELONG_TO]->(department)
MERGE (department)-[:HOLDS]->(group)
RETURN properties(group)