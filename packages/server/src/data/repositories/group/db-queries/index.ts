import { readDbQuerySync, readDynamicDbQuerySync } from 'src/data/database/helpers';

export const createOneGroupQuery = readDbQuerySync(__dirname, './create-group.cypher');
export const createOneAcademicGroupQuery = readDbQuerySync(__dirname, './create-academic-group.cypher');
export const updateOneGroupQuery = readDbQuerySync(__dirname, './update-one-group.cypher');
export const bindToSpecializationQuery = readDbQuerySync(__dirname, './bind-to-specialization.cypher');
export const bindToSuperGroupQuery = readDbQuerySync(__dirname, './bind-to-super-group.cypher');
export const bindToDepartmentQuery = readDbQuerySync(__dirname, './bind-to-department.cypher');
export const getAcademicGroupById = readDbQuerySync(__dirname, './get-academic-group.cypher');
export const bindGroupToYear = readDbQuerySync(__dirname, './bind-group-to-year.cypher');
export const bindStudentToGroupQuery = readDbQuerySync(__dirname, './bind-student-to-group.cypher');
export const bindGroupToCuratorQuery = readDbQuerySync(__dirname, './bind-group-to-curator.cypher');
export const bindGroupToGroupPresidentQuery = readDbQuerySync(__dirname, './bind-president-to-group.cypher');
export const getGroupStudentsQuery = readDbQuerySync(__dirname, './get-group-students.cypher');
export const getGroupSubgroupsQuery = readDbQuerySync(__dirname, './get-group-subgroups.cypher');
export const getGroupByIdQuery = readDbQuerySync(__dirname, './get-group.cypher');

export const dynamicGetGroupsQuery = readDynamicDbQuerySync(__dirname, './get-groups.cypher');
