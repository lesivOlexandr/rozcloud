MATCH(group:Group { id: $groupId })
OPTIONAL MATCH(group)-[rel:HAS_PRESIDENT]->(president)
DELETE rel

WITH DISTINCT group
OPTIONAL MATCH(group)-[:HAS_STUDENT]->(student { id: $studentId })
MERGE(group)-[:HAS_PRESIDENT]-(student)

WITH DISTINCT group

OPTIONAL MATCH (group)-[:HAS_SUBGROUP*1..2]->()-[:HAS_STUDENT]->(student { id: $studentId })

WITH DISTINCT student, group
MERGE(group)-[:HAS_PRESIDENT]->(student)

RETURN properties(group)