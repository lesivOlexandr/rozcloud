MATCH(group:Group { id: $groupId })-[:HAS_SUBGROUP]->(subgroup:Group)
WITH subgroup {
  .*,
  groupPresidentId: head([(academicGroup)-[:HAS_PRESIDENT]->(president) | president.id])
} as subgroups
ORDER BY subgroups.name
RETURN collect(subgroups);