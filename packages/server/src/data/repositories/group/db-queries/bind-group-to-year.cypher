MATCH (group:Group { id: $groupId })
OPTIONAL MATCH (group)-[rel:ENTERED_AT_YEAR|HAS_GROUP_ENTERED_AT]-()
DELETE rel
WITH DISTINCT group as group
MATCH (studyYear:StudyYear { studyYear: $yearNumber })
MERGE (group)-[:ENTERED_AT_YEAR]->(studyYear)
MERGE (studyYear)-[:HAS_GROUP_ENTERED_AT]->(group)
RETURN properties(group)