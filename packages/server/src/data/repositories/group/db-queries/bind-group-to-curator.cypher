MATCH(group:Group { id: $groupId })
OPTIONAL MATCH (group)-[rel:HAS_CURATOR]->(teacher)
DELETE rel
WITH DISTINCT group
MATCH(curator:Teacher { id: $curatorId })
MERGE (group)-[:HAS_CURATOR]->(curator)

RETURN properties(group)