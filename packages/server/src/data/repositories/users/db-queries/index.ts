import { readDbQuerySync } from 'src/data/database/helpers';

export const getUserByCredentialQuery = readDbQuerySync(__dirname, './get-user-by-login-or-email.cypher');
export const getUserByIdQuery = readDbQuerySync(__dirname, './get-user-by-id.cypher');
