import { UnknownTypeUser } from '@rozcloud/shared/interfaces';
import { WithPasswordHash } from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { getUserByCredentialQuery, getUserByIdQuery } from './db-queries';

export class UsersRepository extends BaseRepository {
  static async getUserCredential(loginOrEmailOrPhoneNumber: string) {
    const dbData = { loginOrEmailOrPhoneNumber };
    const user = await this.executeQuery<WithPasswordHash<UnknownTypeUser>>(getUserByCredentialQuery, dbData);
    return user;
  }

  static async getUserById(userId: string) {
    const dbData = { userId };
    const user = this.executeQuery<WithPasswordHash<UnknownTypeUser>>(getUserByIdQuery, dbData);
    return user;
  }
}
