MATCH (specialization:Specialization { id: $specializationId })
SET specialization += $data
RETURN properties(specialization)