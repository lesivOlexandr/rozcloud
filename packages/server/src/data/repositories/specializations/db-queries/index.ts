import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneSpecializationQuery = readDbQuerySync(__dirname, './create-one.cypher');
export const updateOneSpecializationQuery = readDbQuerySync(__dirname, './update-one.cypher');
export const getSpecializationGroupsQuery = readDbQuerySync(__dirname, './get-specialization-groups.cypher');
export const getOneSpecializationQuery = readDbQuerySync(__dirname, './get-one.cypher');
export const getAllSpecializationQuery = readDbQuerySync(__dirname, './get-specializations.cypher');
