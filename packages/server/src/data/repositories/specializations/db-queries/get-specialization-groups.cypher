MATCH (:Specialization { id: $specializationId })-[:IS_SPECIALIZATION_OF_GROUP]->(groups)
RETURN collect(properties(groups))
