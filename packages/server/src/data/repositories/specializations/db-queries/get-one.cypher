MATCH(specialization:Specialization { id: $specializationId })
RETURN properties(specialization)