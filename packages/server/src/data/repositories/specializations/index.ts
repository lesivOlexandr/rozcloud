import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import {
  createOneSpecializationQuery,
  getAllSpecializationQuery,
  getOneSpecializationQuery,
  getSpecializationGroupsQuery,
  updateOneSpecializationQuery,
} from 'src/data/repositories/specializations/db-queries';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import {
  ISpecialization,
  BaseCRUDRepository,
  CreateSpecializationPayload,
  UpdateSpecializationPayload,
  IGroup,
} from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';

@staticImplements<BaseCRUDRepository>()
export class SpecializationsRepository extends BaseRepository {
  static async getOne(specializationId: string) {
    const dbData = { specializationId };
    const result = await this.executeQuery<ISpecialization>(getOneSpecializationQuery, dbData);
    return result;
  }
  static async createOne(data: CreateSpecializationPayload) {
    const dbData = genCreationDbMeta(data);
    const result = await this.executeQuery<ISpecialization>(createOneSpecializationQuery, dbData);
    return result;
  }

  static async updateOne(specializationId: string, data: UpdateSpecializationPayload) {
    const dbData = genUpdatingDbMeta(data, { specializationId });
    const result = await this.executeQuery<ISpecialization>(updateOneSpecializationQuery, dbData);
    return result;
  }

  static async getSpecializationGroups(specializationId: string) {
    const dbData = { specializationId };
    const result = await this.executeQuery<IGroup[]>(getSpecializationGroupsQuery, dbData);
    return result;
  }

  static async getAllSpecializations() {
    const result = await this.executeQuery<ISpecialization[]>(getAllSpecializationQuery, {});
    return result;
  }
}
