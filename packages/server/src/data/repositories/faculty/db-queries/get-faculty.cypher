MATCH (faculty:Faculty { id: $facultyId })
RETURN properties(faculty)