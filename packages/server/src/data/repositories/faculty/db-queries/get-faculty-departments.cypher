MATCH (faculty:Faculty { id: $facultyId })-[:HOLDS]->(department:Department)
RETURN collect(properties(department))