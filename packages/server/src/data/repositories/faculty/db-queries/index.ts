import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneFacultyQuery = readDbQuerySync(__dirname, './create-faculty.cypher');
export const updateOneFacultyQuery = readDbQuerySync(__dirname, './update-faculty.cypher');
export const getFacultyByIdQuery = readDbQuerySync(__dirname, './get-faculty.cypher');
export const getFacultyDepartmentQuery = readDbQuerySync(__dirname, './get-faculty-departments.cypher');
