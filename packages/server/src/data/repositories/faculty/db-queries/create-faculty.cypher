MATCH (university:University { id: $universityId })
CREATE (faculty:Faculty:Structure $data)-[:BELONG_TO]->(university)

MERGE (university)-[:HOLDS]->(faculty)
SET university.facultiesQuantity = university.facultiesQuantity + 1
RETURN properties(faculty)