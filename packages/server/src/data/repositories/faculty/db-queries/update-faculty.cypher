MATCH (faculty:Faculty { id: $facultyId })
SET faculty += $data
RETURN properties(faculty)