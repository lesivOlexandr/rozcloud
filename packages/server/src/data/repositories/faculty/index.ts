import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import {
  createOneFacultyQuery,
  getFacultyByIdQuery,
  getFacultyDepartmentQuery,
  updateOneFacultyQuery,
} from './db-queries';
import {
  ICreateFacultyPayload,
  IUpdateFacultyPayload,
  BaseCRUDRepository,
  IFaculty,
  IDepartment,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';

@staticImplements<BaseCRUDRepository>()
export class FacultiesRepository extends BaseRepository {
  static async getOne(facultyId: string) {
    const dbData = { facultyId };
    const result = await this.executeQuery<IFaculty>(getFacultyByIdQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateFacultyPayload) {
    const { universityId, ...rest } = data;
    const dbData = genCreationDbMeta(
      rest,
      ['departmentsQuantity', 'academicGroupsQuantity', 'studentsQuantity', 'teachersQuantity'],
      { universityId }
    );
    const result = await this.executeQuery<IFaculty>(createOneFacultyQuery, dbData);
    return result;
  }

  static async updateOne(facultyId: string, data: IUpdateFacultyPayload) {
    const dbData = genUpdatingDbMeta(data, { facultyId });
    const result = await this.executeQuery<IFaculty>(updateOneFacultyQuery, dbData);
    return result;
  }

  static async getFacultyDepartments(facultyId: string) {
    const dbData = { facultyId };
    const result = await this.executeQuery<IDepartment[]>(getFacultyDepartmentQuery, dbData);
    return result;
  }
}
