MATCH(teacher:User:Teacher { id: $teacherId })-[rel:BELONG_TO]->(department:Department)
OPTIONAL MATCH (department)-[rel2:HAS_TEACHER]->(teacher)
DELETE rel, rel2
SET department.teachersQuantity = department.teachersQuantity -  1

WITH(department)
MATCH(department)-[:BELONG_TO*1..2]->(parentStructures)
SET parentStructures.teachersQuantity = parentStructures.teachersQuantity - 1

return properties(department)