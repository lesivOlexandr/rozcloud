import { readDbQuerySync, readDynamicDbQuerySync } from 'src/data/database/helpers';

export const createOneTeacherQuery = readDbQuerySync(__dirname, './create-one-query.cypher');
export const bindTeacherToStructureQuery = readDbQuerySync(__dirname, './bind-teacher-to-department.cypher');
export const detachTeacherFromStructureQuery = readDbQuerySync(__dirname, './detach-teacher-from-department.cypher');
export const bindTeacherToSubjectsQuery = readDbQuerySync(__dirname, './bind-teacher-to-subjects.cypher');
export const updateOneTeacherQuery = readDbQuerySync(__dirname, './update-teacher.cypher');
export const getTeacherSubjectQuery = readDbQuerySync(__dirname, './get-teacher-subjects.cypher');
export const getTeacherQuery = readDbQuerySync(__dirname, './get-teacher.cypher');
export const getTeachersDynamicQuery = readDynamicDbQuerySync(__dirname, './get-teachers-dynamic.cypher');
