MATCH(teacher { id: $teacherId  })
OPTIONAL MATCH (teacher)-[rel:HAS_SUBJECT|HAS_TEACHER]-(subject:Subject)
DELETE rel
WITH DISTINCT (teacher) as t
UNWIND ($subjectIds) as subjectId
MATCH(subject:Subject { id: subjectId })
MERGE (subject)-[:HAS_TEACHER]->(t)
MERGE (t)-[:HAS_SUBJECT]->(subject)
