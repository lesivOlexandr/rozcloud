MATCH(teacher:Teacher:User { id: $teacherId })
MATCH(department:Department { id: $departmentId })
MERGE (department)-[:HAS_TEACHER]->(teacher)
MERGE (teacher)-[:BELONG_TO]->(department)

WITH(teacher)
MATCH (teacher)-[:BELONG_TO*1..3]->(parentStructures:Structure)
SET parentStructures.teachersQuantity = parentStructures.teachersQuantity + 1
RETURN properties(teacher);