MATCH(teacher:Teacher { id: $teacherId })
RETURN properties(teacher)