MATCH (teacher:Teacher { id: $teacherId })
SET teacher += $data
RETURN properties(teacher)