MATCH (teacher:Teacher { id: $teacherId })-[:HAS_SUBJECT]->(subject)
RETURN collect(properties(subject)) as subjects
