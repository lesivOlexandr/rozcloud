import { genCreationDbMeta, genUpdatingDbMeta, runDynamicDbQuery } from 'src/data/database/helpers';

import {
  BaseCRUDRepository,
  IDbCreateTeacherPayload,
  IDbUpdateTeacherPayload,
  ISubject,
  ITeacher,
  ITeacherFilter,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import {
  bindTeacherToStructureQuery,
  bindTeacherToSubjectsQuery,
  createOneTeacherQuery,
  getTeacherQuery,
  getTeachersDynamicQuery,
  getTeacherSubjectQuery,
  updateOneTeacherQuery,
} from './db-queries';
import { isArray } from 'lodash';
import { DynamicQueryData } from 'src/common/interfaces/repositories/dynamic-query-data.inteface';

@staticImplements<BaseCRUDRepository>()
export class TeachersRepository extends BaseRepository {
  static async getMany(filters: ITeacherFilter) {
    const dynamicQueryData: DynamicQueryData = {
      data: [
        {
          queryString: 'teacher.firstName contains',
          parameter: filters.search,
          operator: 'OR',
        },
        {
          queryString: 'teacher.middleName contains',
          parameter: filters.search,
          operator: 'OR',
        },
        {
          queryString: 'teacher.lastName contains',
          parameter: filters.search,
          operator: 'OR',
        },
        {
          queryString: 'teacher.address contains',
          parameter: filters.search,
          operator: 'OR',
        },
        {
          queryString: 'teacher.email contains',
          parameter: filters.search,
          operator: 'OR',
        },
      ],
    };
    const teachers = await runDynamicDbQuery(getTeachersDynamicQuery, dynamicQueryData);
    return teachers;
  }

  static async getOne(teacherId: string) {
    const dbData = { teacherId };
    const result = this.executeQuery<ITeacher>(getTeacherQuery, dbData);
    return result;
  }

  static async createOne(data: IDbCreateTeacherPayload) {
    const { departmentId, subjectIds, ...rest } = data;
    const dbData = genCreationDbMeta(rest);
    const result = await this.executeQuery<ITeacher>(createOneTeacherQuery, dbData);
    await this.updateTeacherStructureRel(result.id, departmentId);
    if (isArray(subjectIds) && subjectIds.length) {
      this.bindTeacherToSubjects(result.id, subjectIds);
    }
    return result;
  }

  static async updateOne(teacherId: string, data: IDbUpdateTeacherPayload) {
    const { departmentId, subjectIds, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { teacherId });
    const result = await this.executeQuery<ITeacher>(updateOneTeacherQuery, dbData);
    if (departmentId) {
      await this.updateTeacherStructureRel(teacherId, departmentId);
    }
    if (isArray(subjectIds) && subjectIds.length) {
      await this.bindTeacherToSubjects(result.id, subjectIds);
    }
    return result;
  }

  private static async bindTeacherToSubjects(teacherId: string, subjectIds: string[]) {
    const dbData = { teacherId, subjectIds };
    await this.executeQuery(bindTeacherToSubjectsQuery, dbData, { silent: true });
    return null;
  }

  private static async updateTeacherStructureRel(teacherId: string, departmentId: string) {
    const dbData = {
      teacherId,
      departmentId,
    };
    const result = await this.executeQuery<ITeacher>(bindTeacherToStructureQuery, dbData);
    return result;
  }

  public static async getTeacherSubjects(teacherId: string) {
    const dbData = { teacherId };
    const result = await this.executeQuery<ISubject[]>(getTeacherSubjectQuery, dbData);
    return result;
  }
}
