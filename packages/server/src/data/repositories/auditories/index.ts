import { genCreationDbMeta, genUpdatingDbMeta, runDynamicDbQuery } from 'src/data/database/helpers';

import {
  BaseCRUDRepository,
  IAuditory,
  IAuditoryFilter,
  ICreateAuditoryPayload,
  IUpdateAuditoryPayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import {
  bindAuditoryToAuditoryTypes,
  bindAuditoryToCorpus,
  bindAuditoryToUniversityQuery,
  createOneAuditoryQuery,
  detachAuditoryFromUniversityQuery,
  getAuditoryDynamicQuery,
  getAuditoryQuery,
  updateOneAuditoryQuery,
} from './db-queries';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { isArray } from 'lodash';
import { AuditoryTypes } from '@rozcloud/shared/enums';
import { DynamicQueryData } from 'src/common/interfaces/repositories/dynamic-query-data.inteface';

@staticImplements<BaseCRUDRepository>()
export class AuditoriesRepository extends BaseRepository {
  static async getMany(filters: IAuditoryFilter) {
    const dynamicQueryData: DynamicQueryData = {
      data: [
        {
          queryString: 'auditory.name contains',
          parameter: filters.search,
        },
      ],
    };
    const result = await runDynamicDbQuery<IAuditory[]>(getAuditoryDynamicQuery, dynamicQueryData);
    return result;
  }

  static async getOne(auditoryId: string) {
    const dbData = { auditoryId };
    const result = await this.executeQuery<IAuditory>(getAuditoryQuery, dbData);
    return result;
  }
  static async createOne(data: ICreateAuditoryPayload) {
    const { universityId, corpusId, auditoryTypes, ...rest } = data;
    if (!universityId && !corpusId) {
      triggerServerError('University and corpus cannot be both nullish', 400);
    }
    const dbData = genCreationDbMeta(rest, []);
    const result = await this.executeQuery<IAuditory>(createOneAuditoryQuery, dbData);
    if (universityId) {
      await this.updateAuditoryUniversityRel(result.id, universityId);
    }
    if (corpusId) {
      await this.updateAuditoryCorpusRel(result.id, corpusId);
    }
    await this.bindAuditoryAuditoryTypes(result.id, auditoryTypes);
    return result;
  }

  static async updateOne(auditoryId: string, data: IUpdateAuditoryPayload) {
    const { universityId, corpusId, auditoryTypes, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { auditoryId });
    if (universityId) {
      await this.updateAuditoryUniversityRel(auditoryId, universityId, false);
    }
    if (corpusId) {
      await this.updateAuditoryCorpusRel(auditoryId, corpusId);
    }
    const result = await this.executeQuery<IAuditory>(updateOneAuditoryQuery, dbData);
    await this.bindAuditoryAuditoryTypes(result.id, auditoryTypes);
    return result;
  }

  private static async updateAuditoryUniversityRel(auditoryId: string, universityId: string, onlyBind = true) {
    const dbData = {
      auditoryId,
      universityId,
    };
    if (!onlyBind) {
      await this.executeQuery(detachAuditoryFromUniversityQuery, dbData);
    }
    const result = await this.executeQuery<IAuditory>(bindAuditoryToUniversityQuery, dbData);
    return result;
  }

  private static async updateAuditoryCorpusRel(auditoryId: string, corpusId: string) {
    const dbData = {
      auditoryId,
      corpusId,
    };
    const result = await this.executeQuery<IAuditory>(bindAuditoryToCorpus, dbData);
    return result;
  }

  private static async bindAuditoryAuditoryTypes(auditoryId: string, auditoryTypes: AuditoryTypes[] | undefined) {
    if (isArray(auditoryTypes) && auditoryTypes.length > 0) {
      await this.executeQuery(bindAuditoryToAuditoryTypes, { auditoryId, auditoryTypes });
    }
    return null;
  }
}
