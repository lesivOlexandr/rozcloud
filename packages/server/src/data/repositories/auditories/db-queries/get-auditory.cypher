MATCH (auditory:Auditory { id: $auditoryId })
RETURN auditory { 
  .* ,
  corpus: head([(auditory)-[:BELONG_TO_CORPUS]->(corpus) | corpus { .* }]),  
  auditoryTypes: [(auditory)-[:OF_TYPE]->(auditoryType) | auditoryType.name],
  university: head([(auditory)-[:BELONG_TO_UNIVERSITY]->(university) | university { .* }])
}