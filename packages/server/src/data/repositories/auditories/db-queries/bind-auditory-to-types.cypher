MATCH(auditory:Auditory { id: $auditoryId })
OPTIONAL MATCH (auditory)-[rel:OF_TYPE|HAS_AUDITORY]-(:AuditoryType)
DELETE rel
WITH DISTINCT (auditory)
UNWIND($auditoryTypes) as auditoryTypeName
  MATCH (auditoryType:AuditoryType { name: auditoryTypeName })
  MERGE (auditory)-[:OF_TYPE]->(auditoryType)
  MERGE (auditoryType)-[:HAS_AUDITORY]->(auditory)
WITH DISTINCT auditory
RETURN properties(auditory) 