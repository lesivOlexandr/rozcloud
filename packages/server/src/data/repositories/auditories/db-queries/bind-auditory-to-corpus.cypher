MATCH(auditory:Auditory { id: $auditoryId })
OPTIONAL MATCH (auditory)-[rel:BELONG_TO_CORPUS]->(corpus:Corpus), (corpus)-[rel2:HAS_AUDITORY]->(auditory)
DELETE rel, rel2
WITH DISTINCT auditory
MATCH(corpus:Corpus { id: $corpusId })
MERGE (corpus)-[:HAS_AUDITORY]->(auditory)
MERGE (auditory)-[:BELONG_TO_CORPUS]->(corpus)

RETURN properties(auditory)