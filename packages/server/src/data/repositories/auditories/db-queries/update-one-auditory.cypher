MATCH (auditory:Auditory { id: $auditoryId })
SET auditory += $data
RETURN properties(auditory)