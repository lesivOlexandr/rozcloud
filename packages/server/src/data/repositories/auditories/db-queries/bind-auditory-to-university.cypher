MATCH(auditory:Auditory { id: $auditoryId })
MATCH(university:University { id: $universityId })
MERGE (university)-[:HAS_AUDITORY]->(auditory)
MERGE (auditory)-[:BELONG_TO_UNIVERSITY]->(university)

RETURN properties(auditory)