MATCH(auditory:Auditory { id: $auditoryId })
OPTIONAL MATCH (auditory)-[rel:BELONG_TO_UNIVERSITY]->(university:University),
               (university)-[rel2:HAS_AUDITORY]->(auditory)
DELETE rel, rel2
WITH DISTINCT auditory
RETURN properties(auditory)
LIMIT 1