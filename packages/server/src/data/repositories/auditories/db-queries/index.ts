import { readDbQuerySync, readDynamicDbQuerySync } from 'src/data/database/helpers';

export const createOneAuditoryQuery = readDbQuerySync(__dirname, './create-one.cypher');
export const bindAuditoryToUniversityQuery = readDbQuerySync(__dirname, './bind-auditory-to-university.cypher');
export const bindAuditoryToAuditoryTypes = readDbQuerySync(__dirname, './bind-auditory-to-types.cypher');
export const bindAuditoryToCorpus = readDbQuerySync(__dirname, './bind-auditory-to-corpus.cypher');
export const detachAuditoryFromUniversityQuery = readDbQuerySync(__dirname, './detach-auditory-university.cypher');
export const updateOneAuditoryQuery = readDbQuerySync(__dirname, './update-one-auditory.cypher');
export const getAuditoryQuery = readDbQuerySync(__dirname, './get-auditory.cypher');
export const getAuditoryDynamicQuery = readDynamicDbQuerySync(__dirname, './get-auditory-dynamic.cypher');
