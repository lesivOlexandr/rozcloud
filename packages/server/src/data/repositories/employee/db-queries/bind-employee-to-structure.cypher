MATCH(employee:Employee:User { id: $employeeId })
OPTIONAL MATCH (employee)-[rel:BELONG_TO|HAS_EMPLOYEE]-(employee:Structure)
DELETE rel

WITH DISTINCT (employee)
MATCH (structure:Structure { id: $structureId })
MERGE (structure)-[:HAS_EMPLOYEE]->(employee)
MERGE (employee)-[:BELONG_TO]->(structure)

RETURN properties(employee)