MATCH (employee:User:Employee { id: $employeeId })
SET employee += $data
RETURN properties(employee)