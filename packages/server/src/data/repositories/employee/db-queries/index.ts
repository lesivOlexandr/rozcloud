import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneEmployeeQuery = readDbQuerySync(__dirname, './create-one.cypher');
export const bindEmployeeToStructureQuery = readDbQuerySync(__dirname, './bind-employee-to-structure.cypher');
export const updateOneEmployeeQuery = readDbQuerySync(__dirname, './update-one-query.cypher');
export const getEmployeeStructureQuery = readDbQuerySync(__dirname, './get-employee-structure.cypher');
export const getOneEmployeeQuery = readDbQuerySync(__dirname, './get-one.cypher');
