import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import {
  createOneEmployeeQuery,
  updateOneEmployeeQuery,
  bindEmployeeToStructureQuery,
  getEmployeeStructureQuery,
  getOneEmployeeQuery,
} from './db-queries';
import {
  BaseCRUDRepository,
  IDbCreateEmployeePayload,
  IDbUpdateEmployeePayload,
  IEmployee,
  UnknownTypeStructure,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from '../base-repository';

@staticImplements<BaseCRUDRepository>()
export class EmployeesRepository extends BaseRepository {
  static async getOne(employeeId: string) {
    const dbData = { employeeId };
    const result = this.executeQuery<IEmployee>(getOneEmployeeQuery, dbData);
    return result;
  }
  static async createOne(data: IDbCreateEmployeePayload) {
    const { structureId, ...rest } = data;
    const dbData = genCreationDbMeta(rest, [], { structureId });
    const result = await this.executeQuery<IEmployee>(createOneEmployeeQuery, dbData);
    await this.bindEmployeeToStructure(result.id, structureId);
    return result;
  }

  static async updateOne(employeeId: string, data: IDbUpdateEmployeePayload) {
    const { structureId, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { employeeId });
    const result = await this.executeQuery<IEmployee>(updateOneEmployeeQuery, dbData);
    if (structureId) {
      await this.bindEmployeeToStructure(result.id, structureId);
    }
    return result;
  }

  private static async bindEmployeeToStructure(employeeId: string, structureId: string) {
    const dbData = {
      employeeId,
      structureId,
    };
    const result = await this.executeQuery<IEmployee>(bindEmployeeToStructureQuery, dbData);
    return result;
  }

  static async getEmployeeStructure(employeeId: string) {
    const dbData = { employeeId };
    const result = await this.executeQuery<UnknownTypeStructure>(getEmployeeStructureQuery, dbData);
    return result;
  }
}
