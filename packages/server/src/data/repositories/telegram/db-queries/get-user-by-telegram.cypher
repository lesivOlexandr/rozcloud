MATCH(telegramUserData:TelegramUserData { id: $telegramUserId, accessAllowed: true })-[:OF_USER]->(user:User)
WITH user, labels(user) as uLabels
call apoc.case([
  apoc.coll.contains(uLabels, 'Student'), 'RETURN "student" as userType',
  apoc.coll.contains(uLabels, 'Employee'), 'RETURN "employee" as userType',
  apoc.coll.contains(uLabels, 'Teacher'), 'RETURN "teacher" as userType'
]) YIELD value

RETURN user { .*, userType: value.userType }
