MATCH(telegramUserData:TelegramUserData { id: $telegramUserId })
DETACH DELETE telegramUserData
RETURN properties(telegramUserData)