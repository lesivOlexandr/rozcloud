import { readDbQuerySync } from 'src/data/database/helpers';

export const bindTelegramUserDataQuery = readDbQuerySync(__dirname, './bind-telegram-data-to-user.cypher');
export const getUserByTelegramIdQuery = readDbQuerySync(__dirname, './get-user-by-telegram.cypher');
export const deleteTelegramDataQuery = readDbQuerySync(__dirname, './delete-telegram-data.cypher');
