MATCH (user:User { id: $userId })
CREATE (telegramUserData:TelegramUserData $payload)
MERGE(user)-[:HAS_TELEGRAM_DATA]->(telegramUserData)
MERGE(telegramUserData)-[:OF_USER]->(user)
RETURN properties(telegramUserData)
