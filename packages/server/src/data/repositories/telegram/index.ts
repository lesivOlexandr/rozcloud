import { IBindUserToTelegramData, UnknownTypeUser, WithPasswordHash } from 'src/common/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { bindTelegramUserDataQuery, deleteTelegramDataQuery, getUserByTelegramIdQuery } from './db-queries';

export class TelegramRepository extends BaseRepository {
  static async bindTelegramData(userId: string, data: IBindUserToTelegramData) {
    const dbData = { userId, payload: { ...data, accessAllowed: true } };
    await this.deleteTelegramData(data.id);
    const result: IBindUserToTelegramData = await this.executeQuery(bindTelegramUserDataQuery, dbData);
    return result;
  }

  static async getUserByTelegram(telegramUserId: string) {
    const dbData = { telegramUserId };
    const result: WithPasswordHash<UnknownTypeUser> = await this.executeQuery(getUserByTelegramIdQuery, dbData);
    return result;
  }

  static async deleteTelegramData(telegramUserId: string) {
    const dbData = { telegramUserId };
    const result = await this.executeQuery(deleteTelegramDataQuery, dbData);
    return result;
  }
}
