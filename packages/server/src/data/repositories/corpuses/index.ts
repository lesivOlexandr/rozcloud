import { genCreationDbMeta, genUpdatingDbMeta } from 'src/data/database/helpers';
import { BaseCRUDRepository, IAuditory, ICreateCorpusPayload, IUpdateCorpusPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import {
  bindCorpusToUniversityQuery,
  createOneCorpusQuery,
  getCorpusAuditoriesQuery,
  getOneCorpusQuery,
  updateOneCorpusQuery,
} from './db-queries';
import { ICorpus } from '@rozcloud/shared/interfaces/api-responses/locations/corpus.interface';

@staticImplements<BaseCRUDRepository>()
export class CorpusesRepository extends BaseRepository {
  static async getOne(corpusId: string) {
    const dbData = { corpusId };
    const result = await this.executeQuery<ICorpus>(getOneCorpusQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateCorpusPayload) {
    const { universityId, ...rest } = data;
    const dbData = genCreationDbMeta(rest);
    const result = await this.executeQuery<ICorpus>(createOneCorpusQuery, dbData);
    if (universityId) {
      await this.updateCorpusUniversityRel(result.id, universityId);
    }
    return result;
  }

  static async updateOne(corpusId: string, data: IUpdateCorpusPayload) {
    const { universityId, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { corpusId });
    if (universityId) {
      await this.updateCorpusUniversityRel(corpusId, universityId);
    }
    const result = await this.executeQuery<ICorpus>(updateOneCorpusQuery, dbData);
    return result;
  }

  static async getCorpusAuditories(corpusId: string) {
    const dbData = { corpusId };
    const result = await this.executeQuery<IAuditory[]>(getCorpusAuditoriesQuery, dbData);
    return result;
  }

  private static async updateCorpusUniversityRel(corpusId: string, universityId: string) {
    const dbData = {
      corpusId,
      universityId,
    };
    const result = await this.executeQuery<ICorpus>(bindCorpusToUniversityQuery, dbData);
    return result;
  }
}
