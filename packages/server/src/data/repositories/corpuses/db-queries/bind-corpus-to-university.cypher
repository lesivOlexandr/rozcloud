MATCH(corpus:Corpus { id: $corpusId })
OPTIONAL MATCH (corpus)-[rel:BELONG_TO]->(university:University), (university)-[rel2:HAS_CORPUS]->(corpus)
DELETE rel, rel2

WITH DISTINCT corpus
MATCH (university:University { id: $universityId })

MERGE(university)-[:HAS_CORPUS]->(corpus)
MERGE(corpus)-[:BELONG_TO]->(university)

RETURN properties(corpus)
