MATCH(corpus:Corpus { id: $corpusId })-[:HAS_AUDITORY]->(auditories)
RETURN collect(auditories { .*, auditoryTypes: [(auditories)-[:OF_TYPE]->(auditoryType) | auditoryType.name] })