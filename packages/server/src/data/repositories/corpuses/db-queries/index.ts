import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneCorpusQuery = readDbQuerySync(__dirname, './create-corpus.cypher');
export const bindCorpusToUniversityQuery = readDbQuerySync(__dirname, './bind-corpus-to-university.cypher');
export const updateOneCorpusQuery = readDbQuerySync(__dirname, './update-corpus-query.cypher');
export const getOneCorpusQuery = readDbQuerySync(__dirname, './get-one-corpus.cypher');
export const getCorpusAuditoriesQuery = readDbQuerySync(__dirname, './get-corpus-auditories.cypher');
