MATCH (corpus:Corpus { id: $corpusId })
SET corpus += $data
RETURN properties(corpus)