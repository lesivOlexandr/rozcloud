import {
  bindWorkingPlanToSubjectQuery,
  createOneWorkingPlanQuery,
  deleteOneByGroupQuery,
  deleteWorkingPlanById,
  getAllByGroupQuery,
  getWorkingPlanByGroupQuery,
  getWorkingPlanById,
} from './db-queries';
import {
  BaseCRUDRepository,
  IDBCreateUpdateWorkingPlanPayload,
  IWorkingPlan,
  IWorkingPlanSubjectHoursMap,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { genCreationDbMeta } from 'src/data/database/helpers';
import { SemanticNode } from 'src/common/interfaces/repositories/semantic-node.interface';

@staticImplements<BaseCRUDRepository>()
export class WorkingPlansRepository extends BaseRepository {
  static async getOne(workingPlanId: string) {
    const dbData = { workingPlanId };
    const result = await this.executeQuery<IWorkingPlan>(getWorkingPlanById, dbData);
    return result;
  }

  static async createOne(data: IDBCreateUpdateWorkingPlanPayload) {
    const { subjectsHoursMap, ...rest } = data;
    const dbData = genCreationDbMeta({}, [], { ...rest });

    await this.deleteOneByGroup(data.groupId, data.semesterNumber);
    const result = await this.executeQuery<SemanticNode>(createOneWorkingPlanQuery, dbData);
    const bindWorkingPlanToSubjectData = this.bindWorkingPlanToSubjectData.bind(this, result.id);
    await Promise.all(subjectsHoursMap.map(bindWorkingPlanToSubjectData));

    const workingPlan = await this.getOneByGroup(data.groupId, data.semesterNumber);
    return workingPlan;
  }

  static async getOneByGroup(groupId: string, semesterNum: number) {
    const dbData = {
      groupId,
      semesterNum,
    };
    const workingPlan = await this.executeQuery<IWorkingPlan>(getWorkingPlanByGroupQuery, dbData);
    return workingPlan;
  }

  static async getGroupAllWorkingPlans(groupId: string) {
    const dbData = { groupId };
    const workingPlans = await this.executeQuery<IWorkingPlan[]>(getAllByGroupQuery, dbData);
    return workingPlans;
  }

  static async updateOne(data: IDBCreateUpdateWorkingPlanPayload) {
    return WorkingPlansRepository.createOne(data);
  }

  static async deleteOne(groupId: string) {
    const dbData = { groupId };
    await this.executeQuery(deleteWorkingPlanById, dbData, { silent: true });
    return null;
  }

  private static async deleteOneByGroup(groupId: string, semesterNum: number) {
    const dbData = {
      groupId,
      semesterNum,
    };
    await this.executeQuery(deleteOneByGroupQuery, dbData, { silent: true });
  }

  private static async bindWorkingPlanToSubjectData(workingPlanId: string, data: IWorkingPlanSubjectHoursMap) {
    const { teacherLectorId, teacherPracticeId, teacherSeminarId, subjectId, semesterControlType, ...rest } = data;
    const dbData = {
      teacherLectorId,
      teacherPracticeId,
      teacherSeminarId,
      subjectId,
      workingPlanId,
      semesterControlType,
      data: rest,
    };
    const result = await this.executeQuery(bindWorkingPlanToSubjectQuery, dbData);
    return result;
  }
}
