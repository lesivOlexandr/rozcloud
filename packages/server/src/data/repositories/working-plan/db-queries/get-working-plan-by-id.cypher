MATCH (workingPlan:WorkingPlan { id: $workingPlanId })
return workingPlan { 
  .*,
  group: properties(group),
  semesterNumber: head([(workingPlan)-[:OF_NUMERIC_SEMESTER]-(semester) | semester.semesterNumber]),
  subjectsHoursMap: [(workingPlan)-[:HAS_ITEM]-(workingPlanItem) | workingPlanItem { 
    .*,
    subject: head([(workingPlanItem)-[:HAS_SUBJECT]-(subject) | properties(subject) ]),
    teacherLector: head([(workingPlanItem)-[:HAS_TEACHER_LECTOR]-(teacherLector) | properties(teacherLector)]),
    teacherPractice: head([(workingPlanItem)-[:HAS_TEACHER_PRACTIC]-(teacherPractice) | properties(teacherPractice)]),
    teacherSeminar: head([(workingPlanItem)-[:HAS_TEACHER_SEMINAR]-(teacherSeminar) | properties(teacherSeminar)]),
    semesterControlType: head([(workingPlanItem)-[:OF_SEMESTER_CONTROL_TYPE]-(semesterControlType) | semesterControlType.controlTypeName])
  }]
} as workingPlan