MATCH (group:AcademicGroup)-[:HAS_WORKING_PLAN]-(workingPlan)-[:OF_DATE_SEMESTER]-({ semesterNum: $semesterNum })
OPTIONAL MATCH (workingPlan)-[:HAS_ITEM]-(workingPlanItem)
DETACH DELETE workingPlan, workingPlanItem