MATCH(workingPlan:WorkingPlan { id: $workingPlanId })
OPTIONAL MATCH (workingPlan)-[:HAS_ITEM]-(workingPlanItem)
DETACH DELETE workingPlan, workingPlanItem