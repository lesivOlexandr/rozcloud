MATCH(workingPlan:WorkingPlan { id: $workingPlanId })
CREATE(workingPlanItem:WorkingPlanItem $data)
MERGE(workingPlan)-[:HAS_ITEM]->(workingPlanItem)
MERGE(workingPlanItem)-[:IS_ITEM_OF]->(workingPlan)
WITH(workingPlanItem)
  MATCH(teacherLector:Teacher { id: $teacherLectorId })
  MERGE(teacherLector)-[:MARKED_AS_LECTOR_AT]->(workingPlanItem)
  MERGE(workingPlanItem)-[:HAS_TEACHER]->(teacherLector)
  MERGE(workingPlanItem)-[:HAS_TEACHER_LECTOR]->(teacherLector)
WITH(workingPlanItem)
  MATCH(teacherPractice:Teacher { id: $teacherPracticeId })
  MERGE(teacherPractice)-[:MARKED_AS_PRACTIC_AT]->(workingPlanItem)
  MERGE(workingPlanItem)-[:HAS_TEACHER]->(teacherPractice)
  MERGE(workingPlanItem)-[:HAS_TEACHER_PRACTIC]->(teacherPractice)
WITH(workingPlanItem)
  MATCH(teacherSeminar:Teacher { id: $teacherSeminarId })
  MERGE(teacherSeminar)-[:MARKED_AS_SEMINAR_TEACHER_AT]->(workingPlanItem)
  MERGE(workingPlanItem)-[:HAS_TEACHER]->(teacherSeminar)
  MERGE(workingPlanItem)-[:HAS_TEACHER_SEMINAR]->(teacherSeminar)
WITH(workingPlanItem)
  MATCH(subject:Subject { id: $subjectId })
  MERGE(workingPlanItem)-[:HAS_SUBJECT]->(subject)
  MERGE(subject)-[:IN_WORKING_PLAN]->(workingPlanItem)

MERGE(semesterControlType:SemesterControl { controlTypeName: $semesterControlType })
MERGE(workingPlanItem)-[:OF_SEMESTER_CONTROL_TYPE]->(semesterControlType)

RETURN properties(workingPlanItem)

// an alternative way of quering if cypher.lenient_create_relationship will be set to false in execution environment;
// MATCH (u:User{user_id: $author_id})
//  MERGE (e:Event{uid: $event.uid})
//  ON CREATE SET e = $event
//  MERGE (e)-[:AUTHOR]->(u)
//  WITH e
//      MATCH (l:Location{uid: $location_id})
//      MERGE (e)-[:WHERE]->(l)
//  WITH e
//      MATCH (start:Dates{uid: $start_date_id})
//      MERGE (e)-[:START]->(start)
//  WITH e
//      MATCH (end:Dates{uid: $end_date_id})
//      MERGE (e)-[:END]->(end)
//  return e