import { readDbQuerySync } from 'src/data/database/helpers';

export const createOneWorkingPlanQuery = readDbQuerySync(__dirname, './create-working-plan.cypher');
export const bindWorkingPlanToSubjectQuery = readDbQuerySync(__dirname, './bind-working-plan-to-subject-data.cypher');
export const deleteOneByGroupQuery = readDbQuerySync(__dirname, './delete-working-plan-by-group.cypher');
export const getAllByGroupQuery = readDbQuerySync(__dirname, './get-group-all-working-plans.cypher');
export const getWorkingPlanByGroupQuery = readDbQuerySync(__dirname, './get-working-plan-by-group,semester.cypher');
export const getWorkingPlanById = readDbQuerySync(__dirname, './get-working-plan-by-id.cypher');
export const deleteWorkingPlanById = readDbQuerySync(__dirname, './delete-one-by-id.cypher');
