MATCH (group:AcademicGroup { id: $groupId })-[:HAS_WORKING_PLAN]->(workingPlan)
RETURN collect(workingPlan { 
  .*,
  group: properties(group),
  semesterNumber: head([(workingPlan)-[:OF_NUMERIC_SEMESTER]-(semester) | semester.semesterNumber]),
  subjectsHoursMap: [(workingPlan)-[:HAS_ITEM]-(workingPlanItem) | workingPlanItem { 
    .*,
    subject: head([(workingPlanItem)-[:HAS_SUBJECT]-(subject) | subject { .* }]),
    teacherLector: head([(workingPlanItem)-[:HAS_TEACHER_LECTOR]-(teacherLector) | teacherLector { .* }]),
    teacherPractice: head([(workingPlanItem)-[:HAS_TEACHER_PRACTIC]-(teacherPractice) | teacherPractice { .* }]),
    teacherSeminar: head([(workingPlanItem)-[:HAS_TEACHER_SEMINAR]-(teacherSeminar) | teacherSeminar { .* }]),
    semesterControlType: head([(workingPlanItem)-[:OF_SEMESTER_CONTROL_TYPE]-(semesterControlType) | semesterControlType.controlTypeName])
  }]
}) as workingPlan