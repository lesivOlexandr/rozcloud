import { DbQueryOptions } from 'src/common/interfaces';
import { runDbQuery } from 'src/data/database/helpers';

export class BaseRepository {
  protected static async executeQuery<T extends unknown>(
    query: string,
    data: Record<string, number | string | unknown>,
    options?: DbQueryOptions
  ) {
    const result = await runDbQuery(query, data, options);
    return result as T;
  }
}
