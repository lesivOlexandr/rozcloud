MATCH (:Subject { id: $subjectId })-[:HAS_TEACHER]->(teacher)
RETURN collect(teacher {
  .*,
  subjectIds: [(teacher)-[:HAS_SUBJECT]->(subject) | subject.id]
}) as teachers
