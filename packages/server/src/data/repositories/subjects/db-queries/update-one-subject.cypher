MATCH (subject:Subject { id: $subjectId })
SET subject += $data
RETURN properties(subject)