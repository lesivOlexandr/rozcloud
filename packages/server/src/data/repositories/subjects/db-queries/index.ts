import { readDbQuerySync, readDynamicDbQuerySync } from 'src/data/database/helpers';

export const createOneSubjectQuery = readDbQuerySync(__dirname, './create-one-subject.cypher');
export const bindSubjectToDepartmentQuery = readDbQuerySync(__dirname, './bind-subject-to-department.cypher');
export const updateOneSubjectQuery = readDbQuerySync(__dirname, './update-one-subject.cypher');
export const getSubjectTeachersQuery = readDbQuerySync(__dirname, './get-subject-teachers.cypher');
export const getSubjectQuery = readDbQuerySync(__dirname, './get-one-subject.cypher');
export const getSubjectsDynamicQuery = readDynamicDbQuerySync(__dirname, './get-subjects-dynamic.cypher');
