MATCH(subject:Subject { id: $subjectId })

OPTIONAL MATCH(subject)-[rel:BELONG_TO|HAS_SUBJECT]-(:Department)
DELETE rel

WITH DISTINCT subject
MATCH(department:Department { id: $departmentId })
MERGE (department)-[:HAS_SUBJECT]->(subject)
MERGE (subject)-[:BELONG_TO]->(department)

RETURN properties(subject)