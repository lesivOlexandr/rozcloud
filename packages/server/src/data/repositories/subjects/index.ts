import { genCreationDbMeta, genUpdatingDbMeta, runDynamicDbQuery } from 'src/data/database/helpers';

import {
  BaseCRUDRepository,
  ICreateSubjectPayload,
  IUpdateSubjectPayload,
  ISubject,
  ITeacher,
  ISubjectFilter,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import {
  createOneSubjectQuery,
  bindSubjectToDepartmentQuery,
  updateOneSubjectQuery,
  getSubjectTeachersQuery,
  getSubjectQuery,
  getSubjectsDynamicQuery,
} from './db-queries';
import { DynamicQueryData } from 'src/common/interfaces/repositories/dynamic-query-data.inteface';

@staticImplements<BaseCRUDRepository>()
export class SubjectsRepository extends BaseRepository {
  static async getMany(filters: ISubjectFilter) {
    const dynamicQueryData: DynamicQueryData = {
      data: [
        {
          queryString: 'subject.name contains',
          parameter: filters.search,
        },
      ],
    };
    const result = await runDynamicDbQuery<ISubject[]>(getSubjectsDynamicQuery, dynamicQueryData);
    return result;
  }

  static async getOne(subjectId: string) {
    const dbData = { subjectId };
    const result = this.executeQuery<ISubject>(getSubjectQuery, dbData);
    return result;
  }
  static async createOne(data: ICreateSubjectPayload) {
    const { departmentId, ...rest } = data;
    const dbData = genCreationDbMeta(rest);
    const result = await this.executeQuery<ISubject>(createOneSubjectQuery, dbData);
    if (departmentId) {
      await this.updateSubjectDepartment(result.id, departmentId);
    }
    return result;
  }

  static async updateOne(subjectId: string, data: IUpdateSubjectPayload) {
    const { departmentId, ...rest } = data;
    const dbData = genUpdatingDbMeta(rest, { subjectId });
    if (departmentId) {
      await this.updateSubjectDepartment(subjectId, departmentId);
    }
    const result = await this.executeQuery<ISubject>(updateOneSubjectQuery, dbData);
    return result;
  }

  private static async updateSubjectDepartment(subjectId: string, departmentId: string) {
    const dbData = {
      subjectId,
      departmentId,
    };
    const result = await this.executeQuery<ISubject>(bindSubjectToDepartmentQuery, dbData);
    return result;
  }

  static async getSubjectTeachers(subjectId: string) {
    const dbData = { subjectId };
    const result = await this.executeQuery<ITeacher[]>(getSubjectTeachersQuery, dbData);
    return result;
  }
}
