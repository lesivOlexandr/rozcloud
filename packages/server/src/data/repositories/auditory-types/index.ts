import { BaseRepository } from 'src/data/repositories/base-repository';
import { createAuditoryTypeQuery } from './db-queries';
import { IAuditoryTypesPayload } from 'src/common/interfaces/repositories/structures/auditory-types-payload.interface';
import { timePointDateValue } from 'src/data/database/helpers';
import { IAuditoryTypeEntity } from 'src/common/interfaces/repositories/structures/auditory-type.interface';

export class AuditoryTypesRepository extends BaseRepository {
  static async createOne(data: IAuditoryTypesPayload) {
    const dbData = {
      data,
      createdAt: timePointDateValue(),
      updatedAt: timePointDateValue(),
    };
    const result = await this.executeQuery<IAuditoryTypeEntity>(createAuditoryTypeQuery, dbData);
    return result;
  }
}
