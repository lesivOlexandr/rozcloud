import { readDbQuerySync } from 'src/data/database/helpers';

export const createAuditoryTypeQuery = readDbQuerySync(__dirname, './create-auditory-type.cypher');
