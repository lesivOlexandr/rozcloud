import { genCreationDbMeta } from 'src/data/database/helpers';
import {
  createAcademicPlanItem,
  createAcademicPlanItemWeekData,
  createAcademicPlanQuery,
  getAcademicPlanByGroupAndYearQuery,
  getGroupAcademicPlans,
  getOneAcademicPlanQuery,
} from './db-queries';
import {
  BaseCRUDRepository,
  ICreateUpdateAcademicPlanPayload,
  ISubjectHoursMapAcademicPlan,
  IWeekDataAcademicPlan,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { generateUUID } from 'src/common/helpers/global.helper';
import { SemanticNode } from 'src/common/interfaces/repositories/semantic-node.interface';

@staticImplements<BaseCRUDRepository>()
export class AcademicPlansRepository extends BaseRepository {
  static async getOne(academicPlanId: string) {
    const dbData = { academicPlanId };
    const result = this.executeQuery<ICreateUpdateAcademicPlanPayload>(getOneAcademicPlanQuery, dbData);
    return result;
  }

  static async createOne(data: ICreateUpdateAcademicPlanPayload) {
    const { groupId, year: studyYear, subjectsHoursMap } = data;
    const dbData = genCreationDbMeta({}, [], { groupId, studyYear });
    const academicPlan = await this.executeQuery<SemanticNode>(createAcademicPlanQuery, dbData);
    const createAcademicPlanItem = this.createAcademicPlanItem.bind(this, academicPlan.id);
    await Promise.all(subjectsHoursMap.map(createAcademicPlanItem));
    return academicPlan;
  }

  static async getGroupAcademicPlans(groupId: string) {
    const dbData = { groupId };
    const academicPlans = await this.executeQuery<ICreateUpdateAcademicPlanPayload[]>(getGroupAcademicPlans, dbData);
    return academicPlans;
  }

  static async getOneAcademicPlanByGroup(groupId: string, studyYear: number) {
    const dbData = { groupId, studyYear };
    const academicPlan = this.executeQuery<ICreateUpdateAcademicPlanPayload>(
      getAcademicPlanByGroupAndYearQuery,
      dbData
    );
    return academicPlan;
  }

  static async updateOne(data: ICreateUpdateAcademicPlanPayload) {
    this.createOne(data);
  }

  static async createAcademicPlanItem(academicPlanId: string, data: IWeekDataAcademicPlan) {
    const { subjectId, weeksData, ...rest } = data;
    const dbData = {
      subjectId,
      academicPlanId,
      data: { id: generateUUID(), ...rest },
    };
    const academicPlanItem = await this.executeQuery<{ id: string }>(createAcademicPlanItem, dbData);
    const bindWeekDetailsToPlanItem = this.createAcademicPlanItemWeekData.bind(this, academicPlanItem.id);
    await Promise.all(weeksData.map(bindWeekDetailsToPlanItem));
    return academicPlanItem;
  }

  static async createAcademicPlanItemWeekData(itemId: string, data: ISubjectHoursMapAcademicPlan) {
    const { weekNumber, ...rest } = data;
    const dbData = { itemId, weekNumber, data: rest };
    const result = await this.executeQuery(createAcademicPlanItemWeekData, dbData);
    return result;
  }
}
