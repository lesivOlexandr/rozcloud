MATCH(academicPlan:AcademicPlan { id: $academicPlanId })
RETURN academicPlan { .*,
  year: head([(item)-[:OF_YEAR]->(studyYear) | studyYear.studyYear]),
  subjectsHoursMap: [(academicPlan)-[:HAS_ITEM]->(item) | item {
  .*,
  subject: head([(item)-[:HAS_SUBJECT]->(subject) | properties(subject)]),
  weeksData: [(item)-[:HAS_WEEK_DATA]-(weekData) | weekData { 
    .*,
    weekNumber: head([(weekData)-[:OF_WEEK]->(week) | week.weekNumber])
  }]
}]}