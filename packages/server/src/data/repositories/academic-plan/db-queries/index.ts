import { readDbQuerySync } from 'src/data/database/helpers';

export const getOneAcademicPlanQuery = readDbQuerySync(__dirname, './create-academic-plan-item-week-data.cypher');
export const createAcademicPlanQuery = readDbQuerySync(__dirname, './create-academic-plan.cypher');
export const createAcademicPlanItem = readDbQuerySync(__dirname, './create-academic-pan-item.cypher');
export const getGroupAcademicPlans = readDbQuerySync(__dirname, './get-group-academic-plans.cypher');
export const getAcademicPlanById = readDbQuerySync(__dirname, './get-one-by-id.cypher');
export const getAcademicPlanByGroupAndYearQuery = readDbQuerySync(__dirname, './get-one-by-group-year.cypher');
export const createAcademicPlanItemWeekData = readDbQuerySync(
  __dirname,
  './create-academic-plan-item-week-data.cypher'
);
