MATCH (group:AcademicGroup { id: $groupId })-[:HAS_ACADEMIC_PLAN]->(academicPlan)
RETURN collect(academicPlan { .*, 
  group: properties(group),
  year: head([(item)-[:OF_YEAR]->(studyYear) | studyYear.studyYear]),
  subjectsHoursMap: [(academicPlan)-[:HAS_ITEM]->(item) | item {
  .*,
  subject: head([(item)-[:HAS_SUBJECT]->(subject) | properties(subject)]),
  weeksData: [(item)-[:HAS_WEEK_DATA]-(weekData) | weekData { 
    .*,
    weekNumber: head([(weekData)-[:OF_WEEK]->(week) | week.weekNumber])
  }]
}]})