MATCH(structure:Structure { id: $structureId })-[:HAS_EMPLOYEE]->(employee)
RETURN collect(properties(employee))