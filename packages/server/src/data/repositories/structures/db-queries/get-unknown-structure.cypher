MATCH(structure:Structure { id: $structureId })
WITH structure, labels(structure) as sLabels
call apoc.case([
  apoc.coll.contains(sLabels, 'University'), 'RETURN "university" as structureType',
  apoc.coll.contains(sLabels, 'Faculty'), 'RETURN "faculty" as structureType',
  apoc.coll.contains(sLabels, 'Department'), 'RETURN "department" as structureType'
]) YIELD value

RETURN structure { .*, structureType: value.structureType } 
LIMIT 1