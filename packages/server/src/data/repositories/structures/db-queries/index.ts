import { readDbQuerySync } from 'src/data/database/helpers';

export const getUnknownStructureQuery = readDbQuerySync(__dirname, './get-unknown-structure.cypher');
export const getStructureEmployeesQuery = readDbQuerySync(__dirname, './get-structure-employees.cypher');
