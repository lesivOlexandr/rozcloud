import { IEmployee, UnknownTypeStructure } from '@rozcloud/shared/interfaces';
import { BaseRepository } from 'src/data/repositories/base-repository';
import { getStructureEmployeesQuery, getUnknownStructureQuery } from './db-queries';

export class StructuresRepository extends BaseRepository {
  static async getStructureEmployees(structureId: string) {
    const dbData = { structureId };
    const result = await this.executeQuery<IEmployee[]>(getStructureEmployeesQuery, dbData);
    return result;
  }

  static async getUnknownStructure(structureId: string) {
    const dbData = { structureId };
    const result = await this.executeQuery<UnknownTypeStructure>(getUnknownStructureQuery, dbData);
    return result;
  }
}
