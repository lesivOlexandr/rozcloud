import dbDriver from 'redis';
import { redisConfig } from 'src/common/configs/cache-database';

export const cacheDbClient = dbDriver.createClient(redisConfig);
