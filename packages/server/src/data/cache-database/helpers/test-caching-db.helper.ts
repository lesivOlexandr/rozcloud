import { deleteFromCacheDbJSON } from 'src/data/cache-database/helpers/delete.helpers';
import { getFromCacheDbJSON } from 'src/data/cache-database/helpers/get.helpers';
import { setToCacheDbJSON } from 'src/data/cache-database/helpers/set.helpers';

export const testCachingDb = async () => {
  await setToCacheDbJSON('key', { value: 'value' });
  const value1 = await getFromCacheDbJSON('key');
  await deleteFromCacheDbJSON('key');
  console.log('successfully connected to cache database');
  return value1;
};
