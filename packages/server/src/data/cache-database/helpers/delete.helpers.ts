import { cacheDbClient } from 'src/data/cache-database/connection';
import { getFromCacheDbAsync, getFromCacheDbJSON } from 'src/data/cache-database/helpers/get.helpers';

export const deleteFromCacheDb = async (key: string) => {
  const value = await getFromCacheDbAsync(key);
  cacheDbClient.del(key);
  return value;
};

export const deleteFromCacheDbJSON = async <T = unknown>(key: string) => {
  const jsonValue = await getFromCacheDbJSON<T>(key);
  cacheDbClient.del(key);
  return jsonValue;
};
