import { cacheDbClient } from 'src/data/cache-database/connection';
import { getFromCacheDbAsync, getFromCacheDbJSON } from 'src/data/cache-database/helpers/get.helpers';

export const setToCacheDbAsync = (key: string, value: string) => {
  cacheDbClient.set(key, value);
  return getFromCacheDbAsync(key);
};

export const setToCacheDbJSON = <T = unknown>(key: string, value: Record<string, unknown> | string | number) => {
  const stringifiedValue = JSON.stringify(value);
  cacheDbClient.set(key, stringifiedValue);
  return getFromCacheDbJSON<T>(key);
};
