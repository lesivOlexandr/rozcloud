import { promisify } from 'util';
import { cacheDbClient } from 'src/data/cache-database/connection';

export const getFromCacheDbAsync = promisify(cacheDbClient.get).bind(cacheDbClient) as (key: string) => Promise<string>;

export const getFromCacheDbJSON = <T>(key: string) => getFromCacheDbAsync(key).then(JSON.parse) as Promise<T>;
