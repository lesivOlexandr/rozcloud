import { IdNode, ISeeder } from 'src/common/interfaces';
import { runDbQuery } from 'src/data/database/helpers';
import { Seeder1 } from 'src/data/seeders/1';
import { Seeder2 } from 'src/data/seeders/2';
import { Seeder3 } from 'src/data/seeders/3';
import { Seeder4 } from 'src/data/seeders/4';
import { getSeedByIdDoneQuery, markSeedIsDoneQuery } from './db-queries';

export class SeederService {
  static async runSeeds() {
    for (const seeder of SeederService.seeders) {
      if (!(await SeederService.isDone(seeder))) {
        console.log(`Running seeder with id ${seeder.id}`);
        if ('message' in seeder && typeof seeder.message === 'function') {
          seeder.message();
        }
        await seeder.run();
        await SeederService.markAsDone(seeder);
        console.log(`Seeder with id ${seeder.id} executed`);
      }
    }
  }

  static async isDone(seeder: ISeeder) {
    const dbData = {
      seederId: seeder.id,
    };
    try {
      await runDbQuery<null>(getSeedByIdDoneQuery, dbData, { silent: false });
      return true;
    } catch (e) {
      return false;
    }
  }

  static async markAsDone(seeder: ISeeder) {
    const dbData = {
      seederId: seeder.id,
    };
    const seederInDb = await runDbQuery<IdNode>(markSeedIsDoneQuery, dbData);
    return seederInDb;
  }
  static seeders = [Seeder1, Seeder3, Seeder4, Seeder2] as const;
}

export const runDbSeeders = SeederService.runSeeds;
