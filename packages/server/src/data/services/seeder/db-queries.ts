export const getSeedByIdDoneQuery = `
  MATCH (seed:Seed { id: $seederId })
  RETURN properties(seed)
`;

export const markSeedIsDoneQuery = `
  CREATE (seed:Seed { id: $seederId })
  RETURN properties(seed)
`;
