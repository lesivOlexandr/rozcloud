import { IAuditoryTypesPayload } from 'src/common/interfaces/repositories/structures/auditory-types-payload.interface';
import { AuditoryTypesRepository } from 'src/data/repositories/auditory-types';

export class AuditoryTypesService {
  static createOne(data: IAuditoryTypesPayload) {
    const createdEntity = AuditoryTypesRepository.createOne(data);
    return createdEntity;
  }
}
