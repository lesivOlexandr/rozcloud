import {
  DateHierarchy,
  ICreateSemesterPayload,
  ICreateStudyYearPayload,
  IDateMeta,
  IWeekMetaExtended,
  WithDbMeta,
} from 'src/common/interfaces';
import { TimeOperationsRepository } from 'src/data/repositories/time';
import { generateDateHierarchy, generateWeeksMeta } from '@rozcloud/shared/helpers/time.helpers';

export class TimeOperationsService {
  static async createStudyYear(data: ICreateStudyYearPayload) {
    const studyYear = await TimeOperationsRepository.createStudyYear(data);
    return studyYear;
  }

  static async createSemester(year: number, data: ICreateSemesterPayload) {
    const semester = await TimeOperationsRepository.createSemester(year, data);
    return semester;
  }

  static async fillYearDates(lowerYear: number, upperYear: number) {
    const dateHierarchies: DateHierarchy[] = [];
    for (let i = lowerYear; i <= upperYear; i++) {
      const dateHierarchy = generateDateHierarchy(i);
      dateHierarchies.push(dateHierarchy);
    }
    const newDates: WithDbMeta<IDateMeta>[] = [];
    for (const dateHierarchy of dateHierarchies) {
      const datesMeta = await TimeOperationsRepository.insertDateHierarchy(dateHierarchy);
      datesMeta.forEach((newDateArr) => newDates.push(...newDateArr));
    }
    const allWeeksMeta: IWeekMetaExtended[] = generateWeeksMeta(newDates, 0);
    await TimeOperationsRepository.createWeeks(allWeeksMeta);
  }
}
