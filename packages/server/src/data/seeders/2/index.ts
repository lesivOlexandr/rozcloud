import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { ISeeder } from 'src/common/interfaces';
import { TimeOperationsService } from 'src/data/services/time';

@staticImplements<ISeeder>()
export class Seeder2 {
  static id = 2;
  static async run() {
    await TimeOperationsService.fillYearDates(2018, 2022);
  }
  static message() {
    console.log('It may take long time to proceed');
    console.log('Inserting thousands nodes representing dates');
  }
}
