import { SpecializationsService } from 'src/api/services/specializations';
import { DepartmentsService } from 'src/api/services/department';
import { FacultiesService } from 'src/api/services/faculty';
import { GroupsService } from 'src/api/services/group';
import { UniversitiesService } from 'src/api/services/university';
import { ISeeder } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import {
  seedAuditory,
  seedAuditoryTypes,
  seedCorpus,
  seedDepartmentData,
  seedEmployee,
  seedFacultyData,
  seedGroupData,
  seedSpecializations,
  seedStudent,
  seedSubject,
  seedTeacher,
  seedTimeData,
  seedUniversityData,
} from './seed-data';
import { AuditoryTypesService } from 'src/data/services/auditory-types';
import { TimeOperationsService } from 'src/data/services/time';
import { SubjectService } from 'src/api/services/subjects';
import { TeachersService } from 'src/api/services/teachers';
import { CorpusesService } from 'src/api/services/corpuses';
import { AuditoriesService } from 'src/api/services/auditories';
import { StudentsService } from 'src/api/services/students';
import { EmployeesService } from 'src/api/services/employees';

@staticImplements<ISeeder>()
export class Seeder1 {
  static id = 1;
  static async run() {
    const ud = seedUniversityData();
    const u = await UniversitiesService.createOne(ud);
    const employee = seedEmployee(u.id);
    await EmployeesService.createOne(employee);
    const fd = seedFacultyData(u.id);
    const f = await FacultiesService.createOne(fd);
    const departmentData = seedDepartmentData(f.id);
    const department = await DepartmentsService.createOne(departmentData);
    const specializationsData = seedSpecializations();
    const specializations = await Promise.all(specializationsData.map(SpecializationsService.createOne));

    const timeData = seedTimeData();
    const { semesters, years } = timeData;
    const studyYears = await Promise.all(years.map(TimeOperationsService.createStudyYear));
    for (const studyYear of studyYears) {
      await Promise.all([
        TimeOperationsService.createSemester(studyYear.studyYear, semesters[0]),
        TimeOperationsService.createSemester(studyYear.studyYear, semesters[1]),
      ]);
    }

    const teacherData = seedTeacher(department.id);
    await TeachersService.createOne(teacherData);

    const subjectsData = seedSubject(department.id);
    await Promise.all(subjectsData.map(SubjectService.createOne));

    const group1 = seedGroupData('141', department.id, specializations[0].id, null);
    const group2 = seedGroupData('131', department.id, specializations[1].id, null);
    const group3 = seedGroupData('161', department.id, specializations[2].id, null);
    const groupsData = [group1, group2, group3];
    const groups = await Promise.all(groupsData.map(GroupsService.createOne));

    const group141 = groups[0];

    const subgroup1Data = seedGroupData('1', null, null, group141.id);
    const subgroup2Data = seedGroupData('2', null, null, group141.id);
    const subgroupsData = [subgroup1Data, subgroup2Data];
    const [subgroup1] = await Promise.all(subgroupsData.map(GroupsService.createOne));

    const auditoryTypes = seedAuditoryTypes();
    await Promise.all(auditoryTypes.map(AuditoryTypesService.createOne));

    const studentData = seedStudent(group141.id);
    const student = await StudentsService.createOne(studentData);
    await GroupsService.bindStudentToGroup(subgroup1.id, student.id);

    const corpusData = seedCorpus(u.id);
    const corpus = await CorpusesService.createOne(corpusData);

    const auditoryData = seedAuditory(u.id, corpus.id);
    await AuditoriesService.createOne(auditoryData);
  }
}
