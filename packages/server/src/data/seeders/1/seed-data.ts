/* eslint-disable quotes */
import { defaultUniversityLogoPath } from 'src/common/constants';
import {
  ICreateDepartmentPayload,
  ICreateFacultyPayload,
  ICreateGroupPayload,
  CreateSpecializationPayload,
  ICreateUniversityPayload,
  ICreateAuditoryPayload,
  ICreateCorpusPayload,
  ICreateSemesterPayload,
  ICreateStudentPayload,
  ICreateStudyYearPayload,
  ICreateSubjectPayload,
  ICreateTeacherPayload,
  ICreateEmployeePayload,
} from 'src/common/interfaces';
import {
  AuditoryTypes,
  EmployeeTypes,
  Genders,
  HigherEducationType,
  ScienceDegrees,
  StudyPaymentForms,
} from '@rozcloud/shared/enums';
import { IAuditoryTypesPayload } from 'src/common/interfaces/repositories/structures/auditory-types-payload.interface';
import { formatDate } from '@rozcloud/shared/helpers/time.helpers';
import { generateUUID } from 'src/common/helpers/global.helper';

export const seedUniversityData = () => {
  const data: ICreateUniversityPayload = {
    name: 'Херсонський державний університет',
    abbreviation: 'ХДУ',
    city: 'Херсон',
    logo: defaultUniversityLogoPath,
    type: HigherEducationType.University,
  };
  return data;
};

export const seedFacultyData = (universityId: string) => {
  const data: ICreateFacultyPayload = {
    name: "Факультет комп'ютерних наук фізики та математики",
    abbreviation: 'ФКНФМ',
    universityId,
    logo: defaultUniversityLogoPath,
  };

  return data;
};

export const seedDepartmentData = (facultyId: string) => {
  const data: ICreateDepartmentPayload = {
    name: 'Кафедра програмної інженерії',
    abbreviation: 'КІПІЕК',
    facultyId,
    logo: defaultUniversityLogoPath,
  };
  return data;
};

export const seedSpecializations = () => {
  const specialization1: CreateSpecializationPayload = {
    name: 'Інженерія програмного забезпечення',
    specializationCode: '121',
  };
  const specialization2: CreateSpecializationPayload = {
    name: "Комп'ютерні науки",
    specializationCode: '122',
  };
  const specialization3: CreateSpecializationPayload = {
    name: 'Економічна кібернетика',
    specializationCode: '126',
  };

  const data = [specialization1, specialization2, specialization3];
  return data;
};

export const seedTimeData = (): { semesters: ICreateSemesterPayload[]; years: ICreateStudyYearPayload[] } => ({
  semesters: [{ semesterNum: 1 }, { semesterNum: 2 }],
  years: [
    { id: generateUUID(), studyYear: 2019 },
    { id: generateUUID(), studyYear: 2020 },
    { id: generateUUID(), studyYear: 2021 },
    { id: generateUUID(), studyYear: 2022 },
    { id: generateUUID(), studyYear: 2018 },
  ],
});

export const seedGroupData = (
  name: string,
  departmentId: string | null,
  specializationId: string | null,
  superGroupId: string | null
): ICreateGroupPayload => {
  return {
    name,
    departmentId,
    specializationId,
    superGroupId,
    groupCreatedYear: 2019,
    logo: defaultUniversityLogoPath,
    curatorId: null,
    groupPresidentId: null,
  };
};

export const seedSubject = (departmentId: string): ICreateSubjectPayload[] => [
  {
    name: 'subject name',
    departmentId,
  },
  {
    name: 'matan',
    departmentId,
  },
];

export const seedAuditoryTypes = (): IAuditoryTypesPayload[] => [
  {
    name: AuditoryTypes.Lecture,
  },
  {
    name: AuditoryTypes.Practice,
  },
  {
    name: AuditoryTypes.Virtual,
  },
];

export const seedTeacher = (departmentId: string): ICreateTeacherPayload => ({
  address: 'teacher address',
  avatar: 'avatar',
  birthday: formatDate(new Date()),
  email: 'teacher@email.com',
  firstName: 'Pavel',
  lastName: 'Er',
  middleName: 'Sergeevich',
  login: 'teacher',
  password: 'DFasfdf4sjdfsdf',
  phone: '3807658493435',
  gender: Genders.Male,
  scienceDegree: ScienceDegrees.Professor,
  departmentId,
  subjectIds: [],
});

export const seedStudent = (academicGroupId: string): ICreateStudentPayload => ({
  address: 'student address',
  avatar: 'http://avatar.com',
  birthday: formatDate(new Date()),
  email: 'student@email.com',
  firstName: 'Alexandr',
  lastName: 'Lesiv',
  login: 'student',
  middleName: 'Ivanovich',
  password: '13FAadffsdfsdf',
  phone: '3807658493434',
  gender: Genders.Male,
  paymentForm: StudyPaymentForms.Budget,
  studTicketNum: null,
  studTicketSeria: null,
  academicGroupId,
});

export const seedEmployee = (structureId: string): ICreateEmployeePayload => ({
  address: 'Employee address',
  avatar: 'http://avatar.com',
  birthday: formatDate(new Date()),
  email: 'admin@gmail.com',
  firstName: 'Alex',
  lastName: 'Les',
  employeeType: EmployeeTypes.Admin,
  login: 'master',
  password: 'MasTer268',
  middleName: 'Example',
  gender: Genders.Male,
  phone: '3807658993484',
  structureId: structureId,
});

export const seedCorpus = (universityId: string): ICreateCorpusPayload => ({
  address: 'main corpus address',
  name: 'main',
  logo: null,
  universityId,
});

export const seedAuditory = (universityId: string, corpusId: string): ICreateAuditoryPayload => ({
  auditoryTypes: [AuditoryTypes.Lecture],
  name: '414',
  corpusId,
  logo: null,
  universityId,
});
