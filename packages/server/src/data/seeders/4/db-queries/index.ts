import { readDbQuerySync } from 'src/data/database/helpers';

export const structureIUnique = readDbQuerySync(__dirname, './structure-id.cypher');
export const userLoginIsUnique = readDbQuerySync(__dirname, './user-name-is-unique.cypher');
export const userEmailIsUnique = readDbQuerySync(__dirname, './user-email-is-unique.cypher');
export const userPhoneIsUnique = readDbQuerySync(__dirname, './user-phone.cypher');
export const weekNumber = readDbQuerySync(__dirname, './week-number.cypher');
export const studyYearUnique = readDbQuerySync(__dirname, './study-year-is-unique.cypher');
export const groupIdUnique = readDbQuerySync(__dirname, './group-id.cypher');
export const userIdIsUnique = readDbQuerySync(__dirname, './user-id-is-unique.cypher');
