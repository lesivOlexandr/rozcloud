import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { ISeeder } from 'src/common/interfaces';
import { runDbQuery } from 'src/data/database/helpers';
import {
  groupIdUnique,
  structureIUnique,
  studyYearUnique,
  userEmailIsUnique,
  userIdIsUnique,
  userLoginIsUnique,
  userPhoneIsUnique,
  weekNumber,
} from './db-queries';

@staticImplements<ISeeder>()
export class Seeder4 {
  static id = '4_constraints';
  static async run() {
    await runDbQuery(structureIUnique, undefined, { silent: true });
    await runDbQuery(userEmailIsUnique, undefined, { silent: true });
    await runDbQuery(userLoginIsUnique, undefined, { silent: true });
    await runDbQuery(userPhoneIsUnique, undefined, { silent: true });
    await runDbQuery(weekNumber, undefined, { silent: true });
    await runDbQuery(studyYearUnique, undefined, { silent: true });
    await runDbQuery(groupIdUnique, undefined, { silent: true });
    await runDbQuery(userIdIsUnique, undefined, { silent: true });
  }

  static message() {
    console.log('Creating constraints on data');
  }
}
