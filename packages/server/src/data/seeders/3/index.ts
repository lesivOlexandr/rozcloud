import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { ISeeder } from 'src/common/interfaces';
import { runDbQuery } from 'src/data/database/helpers';
import {
  createIndexGroup,
  createIndexStudyDay,
  createIndexStudyWeek,
  createIndexUsersId,
  crateIndexUsersLogin,
  createIndexStudent,
  createIndexSubject,
  createIndexAuditory,
  createIndexGroupNameId,
} from './db-queries';

@staticImplements<ISeeder>()
export class Seeder3 {
  static id = '3__indexes';
  // according to neo4j documentation indexes are not available immediately, they're created in background
  static async run() {
    const queries = [
      createIndexUsersId,
      crateIndexUsersLogin,
      createIndexStudyDay,
      createIndexStudyWeek,
      createIndexGroup,
      createIndexStudent,
      createIndexAuditory,
      createIndexSubject,
      createIndexGroupNameId,
    ];
    for (const query of queries) {
      await runDbQuery(query, undefined, { silent: true });
    }
  }
  static message() {
    console.log('creating indexes on data');
  }
}
