CREATE INDEX groups_name IF NOT EXISTS
FOR (g:Group)
ON (g.name, g.id)