CREATE INDEX study_weeks IF NOT EXISTS
FOR (s:StudyWeek)
ON (s.weekNumber)