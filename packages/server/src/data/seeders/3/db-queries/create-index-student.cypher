CREATE INDEX student_id IF NOT EXISTS
FOR (s:Student)
ON (s.id)
