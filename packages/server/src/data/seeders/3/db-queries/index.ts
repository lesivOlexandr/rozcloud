import { readDbQuerySync } from 'src/data/database/helpers';

export const createIndexUsersId = readDbQuerySync(__dirname, './create-index-users-id.cypher');
export const crateIndexUsersLogin = readDbQuerySync(__dirname, './create-index-users-login.cypher');
export const createIndexStudyDay = readDbQuerySync(__dirname, './create-index-study-day.cypher');
export const createIndexStudyWeek = readDbQuerySync(__dirname, './create-index-study-week.cypher');
export const createIndexGroup = readDbQuerySync(__dirname, './create-index-group.cypher');
export const createIndexStudent = readDbQuerySync(__dirname, './create-index-student.cypher');
export const createIndexAuditory = readDbQuerySync(__dirname, './create-index-auditory.cypher');
export const createIndexSubject = readDbQuerySync(__dirname, './create-index-subject.cypher');
export const createIndexGroupNameId = readDbQuerySync(__dirname, './create-index-group-name-id.cypher');
