CREATE INDEX auditories_id IF NOT EXISTS
FOR (a:Auditory)
ON (a.id)