CREATE INDEX users_login IF NOT EXISTS
FOR (u:Users)
ON (u.login, u.email, u.phone)