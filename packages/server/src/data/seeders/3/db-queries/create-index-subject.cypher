CREATE INDEX subjects_id IF NOT EXISTS
FOR (s:Subject)
ON (s.id)