import { databaseConnectionMaxRetriesCount, databaseTryConnectTimeout } from 'src/common/configs';
import { databaseConnection } from './index';

/**
 * throws an error if unable to connect to database
 */

export const connectToDatabase = async () => {
  let retryCount = 0;
  const tryConnect = async (resolve: (value?: unknown) => void, reject: (reason?: unknown) => void) => {
    try {
      const session = databaseConnection.session();
      await session.run('RETURN "success"');
      console.log('successfully connected to database');
      return resolve('ok');
    } catch (e) {
      retryCount += 1;
      if (retryCount > databaseConnectionMaxRetriesCount) {
        return reject(e);
      }
      console.log(`
                Failed to connect to database.
                Retrying ${retryCount} of ${databaseConnectionMaxRetriesCount}
            `);
      setTimeout(tryConnect, databaseTryConnectTimeout, resolve, reject);
    }
  };
  return new Promise(tryConnect);
};
