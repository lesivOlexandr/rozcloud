import neo4j from 'neo4j-driver';
import { databaseConfig, databaseFullURI, databaseUserName, databaseUserPassword } from 'src/common/configs/database';

const databaseAuth = neo4j.auth.basic(databaseUserName, databaseUserPassword);

export const databaseConnection = neo4j.driver(databaseFullURI, databaseAuth, databaseConfig);
