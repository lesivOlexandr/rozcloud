/* eslint-disable @typescript-eslint/ban-types */
import { FastifyServerError } from 'src/common/models';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { databaseConnection } from 'src/data/database/connection';
import { DbQueryOptions } from 'src/common/interfaces';
import { generateUUID } from 'src/common/helpers/global.helper';
import neo4j from 'neo4j-driver';
import fs from 'fs';
import pathNode from 'path';
import { DynamicQueryData } from 'src/common/interfaces/repositories/dynamic-query-data.inteface';

const defaultOptions: DbQueryOptions = {
  silent: false,
};

export const runDbQuery = async <T = unknown>(
  query: string,
  params?: Record<string, number | string | unknown>,
  options: DbQueryOptions = defaultOptions
) => {
  const session = databaseConnection.session();
  try {
    const result = await session.run(query, params);
    if (options.silent) {
      return result;
    }
    if (!options.silent && !result.records[0]) {
      triggerServerError('Entity with the given id not found', 404);
    }
    if (!options.silent) {
      return result.records[0].get(0) as T;
    }
    triggerServerError('Unexpected error occurred. Please contact us', 500);
  } catch (e) {
    if (e instanceof FastifyServerError) {
      throw e;
    }
    console.log(e);
    triggerServerError('Unexpected error occurred. Please contact us', 500);
  } finally {
    session.close();
  }
};

export const convertJsNumberToNeo4jInteger = (num: number) =>
  typeof num === 'number' ? neo4j.int(num) : console.warn('You try to convert to neo4j integer not js number');

export const timePointDateValue = () => Date.now();
const assignZeroToProperties = (properties: string[]) =>
  properties.reduce((prev, propertyName) => ((prev[propertyName] = neo4j.int(0)), prev), {});

export const genUpdatingDbMeta = <T extends {}, U extends Record<string, string | number>>(data: T, rest: U) => ({
  data: {
    updatedAt: timePointDateValue(),
    ...data,
  },
  ...rest,
});

export const genCreationDbMeta = <T extends {}, U extends Record<string, unknown>>(
  data: T,
  zeroProperties: string[] = [],
  rest: U = {} as U
) => ({
  data: {
    id: generateUUID(),
    createdAt: timePointDateValue(),
    updatedAt: timePointDateValue(),
    ...assignZeroToProperties(zeroProperties),
    ...data,
  },
  ...rest,
});

export const readDbQuerySync = (dirname: string, path: string) => {
  const fileContent = fs.readFileSync(pathNode.resolve(dirname, path)).toString();
  return fileContent;
};

export const readDynamicDbQuerySync = (dirname: string, path: string) => {
  const fileContent = readDbQuerySync(dirname, path);
  const queryParts = fileContent.split('%%%');
  return queryParts as [string, string];
};

export const runDynamicDbQuery = <T>(query: [string, string], args: DynamicQueryData, options?: DbQueryOptions) => {
  let dynamicQueryString = '';
  let hasNotNullishValue = false;
  let hasAssignedFilter = false;
  const params: Record<string, string | number | string[]> = {};

  if (args.preserveNullish && args.data?.length) {
    hasNotNullishValue = true;
    dynamicQueryString = 'WHERE ';
  }
  let i = 1;
  for (const arg of args.data) {
    const isNumber = typeof arg.parameter === 'number';
    let parameterNotNullish = !!args.preserveNullish;
    if (isNumber && !hasNotNullishValue) {
      hasNotNullishValue = true;
      dynamicQueryString = 'WHERE ';
    }
    if (!isNumber && !hasNotNullishValue && (arg.parameter as string | string[])?.length) {
      hasNotNullishValue = true;
      dynamicQueryString = 'WHERE ';
    }
    if (isNumber || (arg.parameter as string | string[])?.length) {
      parameterNotNullish = true;
    }
    if (parameterNotNullish) {
      hasNotNullishValue = true;
      const parameterString = 'p' + i;
      if (hasAssignedFilter) {
        dynamicQueryString += ' ' + (arg.operator || 'AND') + ' ';
      }
      dynamicQueryString += arg.queryString + ' $' + parameterString;
      params[parameterString] = arg.parameter as string | number | string[];

      hasAssignedFilter = true;
      i += 1;
    }
  }
  const generatedQuery = query[0] + dynamicQueryString + ' ' + query[1];

  return runDbQuery(generatedQuery, params, options) as Promise<T>;
};

export const formatGetFromDb = <T extends {}>(data: T) => ({ data });
