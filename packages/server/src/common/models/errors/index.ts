export class FastifyServerError extends Error {
  status: number;
  error: string;

  constructor(message: string, statusCode: number) {
    super(message);
    this.status = statusCode;
    this.error = message;
  }
}
