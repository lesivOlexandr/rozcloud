import { ClientOpts } from 'redis';
import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

const host = <string>getEnvironmentVariable('CACHING_DATABASE_HOST', '127.0.0.1');
const port = +(<string>getEnvironmentVariable('CACHING_DATABASE_PORT', '6379'));

export const redisConfig: ClientOpts = {
  host,
  port,
};
