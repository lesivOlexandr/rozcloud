import { getEnvironmentVariable } from '../../helpers/os.helper';

export const serverPort: number = +(<string>getEnvironmentVariable('SERVER_PORT', '5001'));
