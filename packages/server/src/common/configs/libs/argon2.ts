import { argon2id } from 'argon2';
import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

export const hashingSecret = getEnvironmentVariable('HASHING_SECRET', 'SUPERSECRET') as string;
export const hashingSecretBuffer = Buffer.from(hashingSecret);

export const argon2Options = {
  type: argon2id,
  memoryCost: 32,
  hashLength: 64,
  secret: hashingSecretBuffer,
  timeCost: 1,
  raw: false,
} as const;
