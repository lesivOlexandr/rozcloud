import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

export const nodeEnv = getEnvironmentVariable('NODE_ENV');
export const isProduction = !nodeEnv;
export const isDevelopment = !isProduction;
