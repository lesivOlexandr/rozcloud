import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

export const botToken = getEnvironmentVariable('BOT_TOKEN');
