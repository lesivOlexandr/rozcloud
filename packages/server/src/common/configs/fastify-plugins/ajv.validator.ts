import { Options } from 'ajv';
import { isDevelopment } from 'src/common/configs/app';

export const fastifyAjvOptions = {
  customOptions: {
    removeAdditional: 'failing',
    useDefaults: true,
    allErrors: true,
    coerceTypes: 'array',
    keywords: [],
    strictNumbers: true,
    logger: isDevelopment ? console : null,
    verbose: isDevelopment,
  },
  plugins: [],
} as { customOptions: Options; plugins: ((...args: unknown[]) => void)[] };
