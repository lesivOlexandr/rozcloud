import { FastifyLoggerInstance, FastifyServerOptions } from 'fastify';
import { Server } from 'http';
import { fastifyAjvOptions } from '.';
import qs from 'qs';
import { qsParserOptions } from './qs-parser';

export const fastifyOptions: FastifyServerOptions<Server, FastifyLoggerInstance> | undefined = {
  ajv: fastifyAjvOptions,
  ignoreTrailingSlash: true,
  querystringParser: str => qs.parse(str, qsParserOptions),
};
