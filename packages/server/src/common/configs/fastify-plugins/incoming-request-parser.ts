export const parserOptions: ParserOptions = {
  parseAs: 'string',
  bodyLimit: 1024 * 1024,
};

interface ParserOptions {
  parseAs: 'string' | 'buffer';
  bodyLimit?: number;
}
