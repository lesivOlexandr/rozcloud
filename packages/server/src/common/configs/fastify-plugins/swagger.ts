/* eslint-disable @typescript-eslint/no-explicit-any */
import { FastifyRegisterOptions } from 'fastify';
import { FastifyDynamicSwaggerOptions } from 'fastify-swagger';
import { auditoryEntitySchema } from 'src/api/routes/locations/auditories/schema/auditory-entity.schema';
import { corpusEntitySchema } from 'src/api/routes/locations/corpuses/schema/corpus-entity.schema';
import { academicPlanEntitySchema } from 'src/api/routes/schedule/academic-plan/schema/academic-plan-entity.schema';
import { educationalPlanEntitySchema } from 'src/api/routes/schedule/educational-plan/schema/educational-plan-entity.schema';
import { scheduleWeekEntitySchema } from 'src/api/routes/schedule/schedule-week/schema/schedule-entity.schema';
import { specializationEntitySchema } from 'src/api/routes/schedule/specializations/schema/specialization-entity';
import { subjectEntitySchema } from 'src/api/routes/schedule/subjects/schema/subject-entity.schema';
import { workingPlanEntitySchema } from 'src/api/routes/schedule/working-plan/schema/working-plan-entity.schema';
import { departmentEntitySchema } from 'src/api/routes/structures/departments/schema/department-entity.schema';
import { facultyEntitySchema } from 'src/api/routes/structures/faculties/schema/faculty-entity.schema';
import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { universityEntitySchema } from 'src/api/routes/structures/universities/schema/university-entity.schema';
import { employeeEntitySchema } from 'src/api/routes/users/employees/schema/employee-entity.schema';
import { studentEntitySchema } from 'src/api/routes/users/students/schema/student-entity.schema';
import { teacherEntitySchema } from 'src/api/routes/users/teachers/schema/teacher-entity.schema';
import { isDevelopment } from 'src/common/configs/app';

export const swaggerOptions: FastifyRegisterOptions<FastifyDynamicSwaggerOptions> = {
  swagger: {
    info: {
      title: 'rozcloud',
      version: '1.0.0',
    },
    definitions: {
      Teacher: teacherEntitySchema as any,
      Student: studentEntitySchema as any,
      Employee: employeeEntitySchema as any,
      University: universityEntitySchema as any,
      Faculty: facultyEntitySchema as any,
      Department: departmentEntitySchema as any,
      Group: groupEntitySchema as any,
      Specialization: specializationEntitySchema as any,
      Subject: subjectEntitySchema as any,
      EducationalPlan: educationalPlanEntitySchema as any,
      WorkingPlan: workingPlanEntitySchema as any,
      AcademicPlan: academicPlanEntitySchema as any,
      ScheduleWeek: scheduleWeekEntitySchema as any,
      Auditory: auditoryEntitySchema as any,
      Corpus: corpusEntitySchema as any,
    },
  },
  hiddenTag: 'X-HIDDEN',
  exposeRoute: isDevelopment,
  routePrefix: '/documentation',
};
