import { FastifyJWTOptions } from 'fastify-jwt';
import { jwtWebTokenValidTime } from 'src/common/constants';
import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

const jwtSignSecret = getEnvironmentVariable('JWT_SIGN_SECRET', 'USE_WISELY') as string;

export const fastifyJWTOptions: FastifyJWTOptions = {
  secret: jwtSignSecret,
  sign: {
    algorithm: 'HS512',
    expiresIn: jwtWebTokenValidTime,
  },
};
