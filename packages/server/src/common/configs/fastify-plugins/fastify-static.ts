import { FastifyStaticOptions } from 'fastify-static';
import { staticFilesPrefix } from 'src/common/constants';

export const fastifyStaticOptions: FastifyStaticOptions = {
  root: '/uploads',
  prefix: staticFilesPrefix,
  list: true,
};
