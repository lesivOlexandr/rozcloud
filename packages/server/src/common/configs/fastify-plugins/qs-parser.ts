import { IParseOptions } from 'qs';

export const qsParserOptions: IParseOptions = {
  parseArrays: true,
};
