import { FastifyCorsOptions } from 'fastify-cors';
import { botBaseUrl } from 'src/common/constants';
import { getEnvironmentVariable } from 'src/common/helpers/os.helper';
import { isDevelopment } from '../app';

const clientBaseUrl = getEnvironmentVariable('CLIENT_BASE_URL', 'http://localhost:3000') as string;

export const fastifyCorsConfig: FastifyCorsOptions = {
  origin: [isDevelopment ? /.*/ : clientBaseUrl, botBaseUrl],
};
