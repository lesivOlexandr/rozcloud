import { Config } from 'neo4j-driver';
import { getEnvironmentVariable } from 'src/common/helpers/os.helper';

export const databasePort = getEnvironmentVariable('DATABASE_PORT', '7687');
export const databaseUrl = getEnvironmentVariable('DATABASE_URL', 'neo4j://localhost');
export const databaseFullURI = databaseUrl + ':' + databasePort;
export const databaseUserName = getEnvironmentVariable('DATABASE_USER_NAME', 'neo4j') as string;
export const databaseUserPassword = getEnvironmentVariable('DATABASE_USER_PASSWORD', 'neo4j') as string;

export const databaseConnectionMaxRetriesCount = 10;
export const databaseTryConnectTimeout = 10_000;

export const databaseConfig: Config = {
  disableLosslessIntegers: true,
};
