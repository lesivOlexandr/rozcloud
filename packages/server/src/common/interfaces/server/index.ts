import { FastifyRequest } from 'fastify';

export type AppRequest<Body = undefined, Params = undefined> = FastifyRequest<{
  Body: Body;
  Params: Params;
}>;
