export interface BaseCRUDService {
  new (): unknown;
  getOne(...args: unknown[]): unknown;
  createOne(...args: unknown[]): unknown;
  updateOne(...args: unknown[]): unknown;
}
