export interface ISeeder {
  id: string | number;
  run(): void;
  message?(): void;
}
