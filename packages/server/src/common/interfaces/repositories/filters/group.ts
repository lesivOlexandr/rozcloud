export interface IGroupFilters {
  groupIds?: string[];
  excludeIds?: string[];
  search?: string;
}
