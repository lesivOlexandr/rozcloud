import { WithDbMeta } from '@rozcloud/shared/interfaces';

// eslint-disable-next-line @typescript-eslint/ban-types
export type SemanticNode = WithDbMeta<{}>;
