import { ICreateUserBasePayload } from 'src/common/interfaces';

interface IHashPasswordProperties {
  passwordHash: string;
  salt: string;
}

export type WithPasswordHash<T> = Omit<T, 'password'> & IHashPasswordProperties;
export type WithNullablePasswordHash<T extends Partial<ICreateUserBasePayload>> = Omit<T, 'password'> &
  Partial<IHashPasswordProperties>;
