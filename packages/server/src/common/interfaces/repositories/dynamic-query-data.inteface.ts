export interface DynamicQueryData {
  preserveNullish?: boolean;
  data: {
    queryString: string;
    parameter?: string | string[] | number;
    operator?: string;
  }[];
}
