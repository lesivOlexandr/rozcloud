import { AuditoryTypes } from '@rozcloud/shared/enums';

export interface IAuditoryTypesPayload {
  name: AuditoryTypes;
}
