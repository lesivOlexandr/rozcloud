import { WithDbMeta } from '@rozcloud/shared/interfaces';
import { IAuditoryTypesPayload } from './auditory-types-payload.interface';

export type IAuditoryTypeEntity = WithDbMeta<IAuditoryTypesPayload>;
