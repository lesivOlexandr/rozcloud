import { ICreateTeacherPayload } from 'src/common/interfaces';
import { WithPasswordHash } from 'src/common/interfaces/repositories/with-password-hash';

export type IDbCreateTeacherPayload = WithPasswordHash<ICreateTeacherPayload>;
