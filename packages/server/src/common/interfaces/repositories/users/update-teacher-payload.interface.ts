import { IUpdateTeacherPayload } from 'src/common/interfaces';
import { WithNullablePasswordHash } from 'src/common/interfaces/repositories/with-password-hash';

export type IDbUpdateTeacherPayload = WithNullablePasswordHash<IUpdateTeacherPayload>;
