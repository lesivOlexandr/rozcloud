import { ICreateEmployeePayload } from 'src/common/interfaces';
import { WithPasswordHash } from 'src/common/interfaces/repositories/with-password-hash';

export type IDbCreateEmployeePayload = WithPasswordHash<ICreateEmployeePayload>;
