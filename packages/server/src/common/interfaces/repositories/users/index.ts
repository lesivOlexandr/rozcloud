export * from './create-student-payload.interface';
export * from './update-student-payload.interface';
export * from './create-teacher-payload.interface';
export * from './update-teacher-payload.interface';
export * from './create-employee-payload.interface';
export * from './update-employee-payload.interface';
