import {
  ICreateUpdateEducationalPlanPayload,
  IEducationalPlanSubjectHoursMap,
  SemesterSubjectControl,
} from '@rozcloud/shared/interfaces';

export interface IDBSemesterSubjectControl extends SemesterSubjectControl {
  semesterRealNumber: 1 | 2; // first semesterNumber is actually sequential number of semester for group e.g. 1, 2, 3, 4, 5, 6, 7, 8
  semesterYear: number;
}

export interface IDbEducationalPlanSubjectHoursMap extends IEducationalPlanSubjectHoursMap {
  semesterSubjectControls: IDBSemesterSubjectControl[];
}

export interface IDBCreateUpdateEducationalPlanPayload extends ICreateUpdateEducationalPlanPayload {
  subjectsHoursMap: IDbEducationalPlanSubjectHoursMap[];
}
