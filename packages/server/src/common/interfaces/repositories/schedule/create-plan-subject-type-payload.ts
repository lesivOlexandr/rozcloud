import { PlanSubjectTypes } from '@rozcloud/shared/enums';

export interface ICreatePlanSubjectType {
  planSubjectType: PlanSubjectTypes;
}
