export interface CreateOneScheduleItemPayload {
  weekTemplateId: string;
  dayName: string;
  weekNumber: number;
  itemNumber: string;
}
