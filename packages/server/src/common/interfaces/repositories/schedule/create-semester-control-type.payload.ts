import { SemesterControlTypes } from '@rozcloud/shared/enums';

export interface ICreateSemesterControlType {
  controlTypeName: SemesterControlTypes;
}
