import { ICreateUpdateWorkingPlanPayload } from '@rozcloud/shared/interfaces';

export interface IDBCreateUpdateWorkingPlanPayload extends ICreateUpdateWorkingPlanPayload {
  semesterRealNumber: 1 | 2;
  semesterYear: number;
}
