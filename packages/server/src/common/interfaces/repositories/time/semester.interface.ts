import { ICreateSemesterPayload } from './create-semester-payload.interface';

export type ISemester = ICreateSemesterPayload;
