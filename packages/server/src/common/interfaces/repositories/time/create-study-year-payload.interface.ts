export interface ICreateStudyYearPayload {
  id: string;
  studyYear: number;
}
