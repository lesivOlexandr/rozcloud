import { IDateMeta } from '@rozcloud/shared/interfaces';

export interface IDbGetWeekWithDays {
  weekNumber: number;
  days: IDateMeta[];
}
