import { ICreateStudyYearPayload } from './create-study-year-payload.interface';

export type IStudyYear = ICreateStudyYearPayload;
