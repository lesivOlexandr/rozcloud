export * from './create-semester-payload.interface';
export * from './create-study-year-payload.interface';
export * from './get-week-with-days.interface';
export * from './study-year.interface';
export * from './semester.interface';
