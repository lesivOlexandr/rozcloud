import { Schema } from 'src/common/interfaces/swagger';
export * from './schema';

export interface SchemaCreateObject {
  addBody: (body: Schema) => SchemaCreateObject;
  addQueryString: (queryString: Schema) => SchemaCreateObject;
  addParams: (queryString: Schema) => SchemaCreateObject;
  addTags: (tags: string[]) => SchemaCreateObject;
  responseSuccess: (response: Schema, multiple?: boolean) => SchemaCreateObject;
  responseAccessDenied: (message?: string) => SchemaCreateObject;
  responseNotFound: (message?: string) => SchemaCreateObject;
  done: () => { schema: Schema };
}
