import { Schema as CommonSchema } from 'swagger-schema-official';

export interface Schema extends CommonSchema {
  nullable?: boolean | undefined;
  tags?: string[] | undefined;
  items?: Schema | Schema[] | undefined;
  additionalProperties?: boolean | Schema | undefined;
  anyOf?: Schema[];
  properties?: {
    [schemaName: string]: Schema;
  };
}

export interface ObjectSchema extends Schema {
  properties: {
    [schemaName: string]: Schema;
  };
}
