import { IBaseSubjectHours } from '@rozcloud/shared/interfaces';

export const countAllSubjectHours = (item: IBaseSubjectHours) => {
  const { auditorialHours, independentWorkHours, laboratoryHours, practicalHours, seminarHours } = item;
  return auditorialHours + independentWorkHours + laboratoryHours + practicalHours + seminarHours;
};
