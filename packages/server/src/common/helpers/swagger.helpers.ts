import { FastifySchema } from 'fastify';
import { cloneDeep, merge } from 'lodash';
import { ObjectSchema, Schema } from 'src/common/interfaces/swagger';
import { maxTextFieldLetters } from 'src/common/constants';

export const createRouteValidationSchema = (
  fastifySchema: FastifySchema,
  responses: {
    200: Schema;
    400?: Schema | true;
    401?: Schema | true;
    403?: Schema | true;
    404?: Schema | true;
    protectedJWT?: boolean;
  }
) => {
  if (responses[400] === true) {
    responses[400] = getBadRequestHeader();
  }
  if (responses[403] === true) {
    responses[403] = getForbiddenHeader();
  }
  if (responses[404] === true) {
    responses[404] = getNotFoundSchemaObject();
  }
  if (responses[401] === true) {
    responses[401] = getNotAuthorizedHeader();
  }
  if (responses.protectedJWT) {
    fastifySchema.headers = getJWTHeader();
    delete responses.protectedJWT;
  }
  fastifySchema.response = fastifySchema.response ?? {};
  fastifySchema.response = merge(fastifySchema.response, responses);
  return fastifySchema;
};

export const createArrayOfSchema = <T extends Schema>(arrayItems: T): Schema => ({ type: 'array', items: arrayItems });

export const createObjectSchema = <T extends ObjectSchema>(
  entity: T,
  config?: {
    required?: (keyof typeof entity.properties)[];
    removeProperties?: (keyof typeof entity.properties)[];
    useProperties?: (keyof typeof entity.properties)[];
    nullable?: (keyof typeof entity.properties)[] | 'onNotRequired';
    defaultNull?: (keyof typeof entity.properties)[] | 'onNotRequired';
    mergeProperties?: [string, Schema][];
    allowAdditional?: boolean;
  }
) => {
  const copy = cloneDeep(entity);
  if (config?.required instanceof Array) {
    for (const schemaKey of config.required) {
      copy.properties[schemaKey] = { ...copy.properties[schemaKey], nullable: false };
    }
  }

  if (config?.useProperties instanceof Array) {
    const propertiesToBeUsed = new Set(config.useProperties);
    for (const key of Object.keys(copy.properties)) {
      if (!propertiesToBeUsed.has(key)) {
        delete copy.properties[key];
      }
    }
  } else if (config?.removeProperties instanceof Array) {
    for (const schemaKey of config.removeProperties) {
      delete copy.properties[schemaKey];
    }
  }

  if (config?.defaultNull instanceof Array) {
    for (const schemaKey of config.defaultNull) {
      if (copy.properties[schemaKey]?.default == null) {
        copy.properties[schemaKey] = { ...copy.properties[schemaKey], default: null, nullable: true };
      }
    }
  }

  if (config?.nullable === 'onNotRequired') {
    const requiredProps = new Set(config.required || []);
    for (const entityKey of Object.keys(copy.properties)) {
      if (!requiredProps.has(entityKey)) {
        copy.properties[entityKey] = { ...copy.properties[entityKey], nullable: true };
      }
    }
  }

  if (config?.defaultNull === 'onNotRequired') {
    const requiredProps = new Set(config.required || []);
    for (const entityKey of Object.keys(copy.properties)) {
      if (!requiredProps.has(entityKey)) {
        copy.properties[entityKey] = { ...copy.properties[entityKey], default: null, nullable: true };
      }
    }
  }

  if (config?.mergeProperties instanceof Array) {
    for (const mergePropItem of config.mergeProperties) {
      const [key, schema] = mergePropItem;
      if (typeof key === 'string' && schema instanceof Object && copy.properties[key] instanceof Object) {
        copy.properties[key] = merge(copy.properties[key], schema);
      }
    }
  }

  copy.additionalProperties = config?.allowAdditional || false;
  return copy;
};

export const createScalarSchema = (entity: Schema) => {
  return entity;
};

const getNotFoundSchemaObject = (message?: string): ObjectSchema => ({
  type: 'object',
  properties: {
    error: {
      type: 'string',
      minLength: 1,
      maxLength: maxTextFieldLetters,
      example: message || 'Item with id: 1 does not exists',
      nullable: false,
    },
    status: {
      type: 'integer',
      nullable: false,
      example: 404,
    },
  },
  nullable: false,
});

const getBadRequestHeader = (message?: string): ObjectSchema => ({
  type: 'object',
  properties: {
    error: {
      type: 'string',
      minLength: 1,
      maxLength: maxTextFieldLetters,
      example: message || 'Bad Request',
      nullable: false,
    },
    message: {
      type: 'string',
      minLength: 1,
      maxLength: 1024,
      nullable: false,
    },
    status: {
      type: 'integer',
      nullable: false,
      example: 400,
    },
  },
  nullable: false,
  additionalProperties: true,
});

const getForbiddenHeader = (message?: string): ObjectSchema => ({
  type: 'object',
  properties: {
    error: {
      type: 'string',
      minLength: 1,
      maxLength: maxTextFieldLetters,
      example: message || 'Access Denied',
      nullable: false,
    },
    status: {
      type: 'integer',
      nullable: false,
      example: 403,
    },
  },
  nullable: false,
});

const getNotAuthorizedHeader = (message?: string): ObjectSchema => ({
  type: 'object',
  properties: {
    error: {
      type: 'string',
      minLength: 1,
      maxLength: maxTextFieldLetters,
      example: message || 'Not authorized',
      nullable: false,
    },
    status: {
      type: 'integer',
      nullable: false,
      example: 403,
    },
  },
  nullable: false,
});

const getJWTHeader = (): ObjectSchema => ({
  type: 'object',
  properties: {
    authorization: {
      type: 'string',
      minLength: 1,
      maxLength: maxTextFieldLetters,
      nullable: false,
      example:
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJuYW1lIjpudWxsLCJwYXNzd29yZCI6IiQyYiQxMCQ2Wm05L2xtTC4zUlpGRUJVeUpkRWplZkhMTGh0UGQyTU8vOEYwZDVoU205bVhFSGUwbmlNQyIsImVtYWlsIjoiZXhhbXBsZUBleGFtcGxlLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJhdmF0YXIiOm51bGwsInZlcmlmeUVtYWlsVG9rZW4iOm51bGwsInJlc2V0UGFzc3dvcmRUb2tlbiI6bnVsbCwiY3JlYXRlZEF0IjoiMjAyMC0wOC0xMlQxMzoxODowNy4zNTlaIiwidXBkYXRlZEF0IjoiMjAyMC0wOC0xMlQxMzoxODowNy4zNjBaIn0sImlhdCI6MTU5ODExMjYxOCwiZXhwIjoxNTk4MTk5MDE4fQ.yC5HktPfVdi_pXgI24Wn7rP7hLRqMsdFM6SDsoykmds',
      description: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2...',
    },
  },
});
