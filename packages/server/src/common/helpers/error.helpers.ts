import { FastifyServerError } from 'src/common/models';

export const triggerServerError = (message: string, statusCode: number): never => {
  throw new FastifyServerError(message, statusCode);
};
