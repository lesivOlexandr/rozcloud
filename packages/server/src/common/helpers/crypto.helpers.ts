import argon2 from 'argon2';
import { randomBytes } from 'crypto';
import { argon2Options as argon2DefaultOptions } from 'src/common/configs';

export const getRandomBytes = (size: number) => randomBytes(size);

export const getSalt: () => Buffer = getRandomBytes.bind(null, 16);

export const hashPassword = async (password: string) => {
  const salt = getSalt();
  const hash = await getHash(password, salt);
  const stringSalt = salt.toString('hex');

  return {
    salt: stringSalt,
    passwordHash: hash,
  };
};

export const verifyPasswordHash = async (passwordString: string, passwordHash: string, salt: string) => {
  try {
    const saltBuffer = Buffer.from(salt);
    const isValid = await argon2.verify(passwordHash, passwordString, { ...argon2DefaultOptions, salt: saltBuffer });
    return isValid;
  } catch {
    return false;
  }
};

export const getHash = async (password: string, salt: Buffer) => {
  const hash = await argon2.hash(password, { ...argon2DefaultOptions, salt });
  return hash;
};
