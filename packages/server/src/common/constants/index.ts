import { getEnvironmentVariable } from '../helpers/os.helper';

export const defaultUniversityLogoPath =
  'http://www.kspu.edu/FileDownload.ashx/logo%d0%a5%d0%94%d0%a3.jpg?id=d5084993-5918-495a-b109-3e470fe90ed3';
export const maxTextFieldLetters = 255;
export const maxIntegerValue = Number.MAX_SAFE_INTEGER;
export const jwtWebTokenValidTime = '16 days';
export const staticFilesPrefix = '/public/';
export const serverBaseUrl = getEnvironmentVariable('SERVER_BASE_URL', 'http://localhost');
export const serverPort = getEnvironmentVariable('SERVER_PORT', '5001');
export const serverFullBaseUrl = serverBaseUrl + ':' + serverPort;
export const botBaseUrl = getEnvironmentVariable('TELEGRAM_BOT_BASE_URL') as string;
