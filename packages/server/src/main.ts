import { runServer } from 'src/api/server';
// import { testCachingDb } from 'src/data/cache-database';
import { connectToDatabase } from 'src/data/database/connection/test-connection';
import { runDbSeeders } from 'src/data/services/seeder';

async function main() {
  // await testCachingDb();
  await connectToDatabase();
  await runDbSeeders();
  await runServer();
}

main();
