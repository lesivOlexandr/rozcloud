import fastify from 'fastify';
import fSocketIO from 'fastify-socket.io';
import compressor from 'fastify-compress';
import fastifySwagger from 'fastify-swagger';
import fastifyCors from 'fastify-cors';
import fastifyStatic from 'fastify-static';
import routes from 'src/api/routes';
import {
  serverPort,
  swaggerOptions,
  ioServerOptions,
  compressorOptions,
  fastifyOptions,
  fastifyJWTOptions,
  fastifyCorsConfig,
  fastifyStaticOptions,
} from 'src/common/configs';
import fastifyJwt from 'fastify-jwt';
import fastifyMulter from 'fastify-multer';
import telegrafPlugin from 'fastify-telegraf';
import { prepareTgBot } from '@rozcloud/tbot/src/controllers/bot';
import { isDevelopment } from 'src/common/configs/app';
import { botToken } from 'src/common/configs/tbot';

export const runServer = async () => {
  const server = fastify(fastifyOptions);

  await server.register(fastifySwagger, swaggerOptions);
  await server.register(fastifyMulter.contentParser);
  await server.register(fSocketIO, ioServerOptions);
  await server.register(fastifyCors, fastifyCorsConfig);
  await server.register(compressor, compressorOptions);
  await server.register(fastifyJwt, fastifyJWTOptions);
  await server.register(fastifyStatic, fastifyStaticOptions);
  await server.register(routes, { prefix: 'api' });

  if (botToken) {
    const bot = await prepareTgBot(botToken);
    await server.register(telegrafPlugin, { bot, path: '/tbot' });
  }

  server.ready(err => {
    if (err) throw err;
    if (isDevelopment) {
      server.swagger();
    }
  });

  server.listen(serverPort, '0.0.0.0', (err, address) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    console.log(`Server listening at ${address}`);
  });
};
