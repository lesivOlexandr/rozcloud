import { FastifyRequest } from 'fastify';
import { JSONWebTokenDecodedPayload, UnknownTypeUser, WithPasswordHash } from 'src/common/interfaces';
import { AuthService } from '../services/auth';

/**
 * makes user available in fastify request via the property "appUser"
 */
export const jwtInjectUser = async (request: FastifyRequest) => {
  const jwtVerifyPayload: JSONWebTokenDecodedPayload = await request.jwtVerify();
  try {
    const user = await AuthService.getUserById(jwtVerifyPayload.userId);
    request.appUser = user as WithPasswordHash<UnknownTypeUser>;
  } catch {
    return null;
  }
};
