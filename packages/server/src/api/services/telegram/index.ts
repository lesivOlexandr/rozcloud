import { IBindUserToTelegramPayload } from '@rozcloud/shared/interfaces';
import { TelegramRepository } from 'src/data/repositories/telegram';

export class TelegramService {
  static async bindTelegramData(userId: string, telegramUserId: string, data: IBindUserToTelegramPayload) {
    const dbData = { id: telegramUserId, ...data };
    const result = await TelegramRepository.bindTelegramData(userId, dbData);
    const { id, ...payload } = result;
    const returnValue = { id, payload };
    return returnValue;
  }

  static async getUserByTelegramId(telegramUserId: string) {
    const user = await TelegramRepository.getUserByTelegram(telegramUserId);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { salt, passwordHash, ...userProperties } = user;
    return userProperties;
  }
}
