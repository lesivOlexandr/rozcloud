import { hashPassword } from 'src/common/helpers/crypto.helpers';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { ICreateUserBasePayload, WithPasswordHash, WithNullablePasswordHash } from 'src/common/interfaces';

export class BaseUserService {
  protected static async addHashToUser<T extends ICreateUserBasePayload>(data: T): Promise<WithPasswordHash<T>>;
  protected static async addHashToUser<T extends Partial<ICreateUserBasePayload>>(
    data: T
  ): Promise<WithNullablePasswordHash<T>>;
  protected static async addHashToUser<T extends ICreateUserBasePayload | Partial<ICreateUserBasePayload>>(
    data: T
  ): Promise<WithPasswordHash<T> | WithNullablePasswordHash<T>> {
    if ('password' in data && typeof data.password === 'string') {
      const { password, ...rest } = data;
      try {
        const { passwordHash, salt } = await hashPassword(password);
        const dbData = { ...rest, salt, passwordHash };
        return dbData as WithPasswordHash<T> | WithNullablePasswordHash<T>;
      } catch (e) {
        triggerServerError('Unexpected error occurred, please contact us', 500);
      }
    }
    return data as WithPasswordHash<T> | WithNullablePasswordHash<T>;
  }
}
