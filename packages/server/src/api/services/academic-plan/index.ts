import { BaseCRUDService, ICreateUpdateAcademicPlanPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';

import { AcademicPlansRepository } from 'src/data/repositories/academic-plan';

@staticImplements<BaseCRUDService>()
export class AcademicPlansService {
  static async getOne(academicPlanId: string) {
    const academicPlan = await AcademicPlansRepository.getOne(academicPlanId);
    return academicPlan;
  }

  static async createOne(data: ICreateUpdateAcademicPlanPayload) {
    const academicPlan = await AcademicPlansRepository.createOne(data);
    return academicPlan;
  }

  static async updateOne(academicPlanId: string, data: ICreateUpdateAcademicPlanPayload) {
    // TODO;
  }

  static async getAllOfGroup(groupId: string) {
    const academicPlans = await AcademicPlansRepository.getGroupAcademicPlans(groupId);
    return academicPlans;
  }

  static async getOneAcademicPlanByGroup(groupId: string, studyYear: number) {
    const academicPlan = await AcademicPlansRepository.getOneAcademicPlanByGroup(groupId, studyYear);
    return academicPlan;
  }
}
