import { BaseCRUDService, ICreateDepartmentPayload, IUpdateDepartmentPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { DepartmentsRepository } from 'src/data/repositories/department';

@staticImplements<BaseCRUDService>()
export class DepartmentsService {
  static async getOne(departmentId: string) {
    const department = await DepartmentsRepository.getOne(departmentId);
    return department;
  }

  static async createOne(data: ICreateDepartmentPayload) {
    const createdDepartment = await DepartmentsRepository.createOne(data);
    return createdDepartment;
  }

  static async updateOne(departmentId: string, data: IUpdateDepartmentPayload) {
    const updatedDepartment = await DepartmentsRepository.updateOne(departmentId, data);
    return updatedDepartment;
  }

  static async getDepartmentAcademicGroups(departmentId: string) {
    const department = await DepartmentsRepository.getDepartmentAcademicGroups(departmentId);
    return department;
  }

  static async getDepartmentSubjects(departmentId: string) {
    const subjects = await DepartmentsRepository.getDepartmentSubjects(departmentId);
    return subjects;
  }

  static async getDepartmentTeachers(departmentId: string) {
    const teachers = await DepartmentsRepository.getDepartmentTeachers(departmentId);
    return teachers;
  }
}
