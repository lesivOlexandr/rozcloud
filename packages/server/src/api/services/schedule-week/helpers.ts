/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { ICreateScheduleWeekPayload, IScheduleDay } from 'src/common/interfaces';

export const filterOutEmptyScheduleItems = (data: ICreateScheduleWeekPayload) => {
  const filteredScheduleItems: ICreateScheduleWeekPayload = {
    weekNumber: data.weekNumber,
    daySchedules: {},
  } as ICreateScheduleWeekPayload;
  const dayKeys = Object.keys(data.daySchedules) as (keyof typeof data.daySchedules)[];
  for (const dayName of dayKeys) {
    const itemKeys = (Object.keys(data.daySchedules[dayName]! || {}) as unknown) as (keyof IScheduleDay)[];
    for (const itemKey of itemKeys) {
      const daySchedules = data.daySchedules[dayName]?.[itemKey]?.filter(
        item => item.auditoryId || item.subjectId || item.teacherId || item.type
      );
      if (!filteredScheduleItems.daySchedules[dayName]) {
        filteredScheduleItems.daySchedules[dayName] = {} as IScheduleDay;
      }
      filteredScheduleItems.daySchedules[dayName]![itemKey] = daySchedules || [];
    }
  }
  return filteredScheduleItems;
};
