import { BaseCRUDService, ICreateScheduleWeekPayload, IDaySchedules } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { ScheduleWeekRepository } from 'src/data/repositories/schedule-week';
import { UserTypes } from '@rozcloud/shared/enums';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { filterOutEmptyScheduleItems } from './helpers';

@staticImplements<BaseCRUDService>()
export class ScheduleWeekService {
  static async getOne(userId: string, weekNumber: number, userType: UserTypes) {
    if (userType === UserTypes.Employee) {
      return triggerServerError('Employees have no schedule', 400);
    }
    const schedule = await ScheduleWeekRepository.getOne(userId, weekNumber, userType);
    return schedule;
  }

  static async createOne(data: ICreateScheduleWeekPayload) {
    const dayNames = Object.keys(data.daySchedules);
    if (!dayNames) {
      return triggerServerError('No days specified', 400);
    }
    let dayNameKey = dayNames[0] as keyof IDaySchedules;
    for (const dayName of dayNames) {
      if (Object.keys(data.daySchedules[dayName]?.[1] || {}).length) {
        dayNameKey = dayName as keyof IDaySchedules;
        break;
      }
    }

    const groupIds = data.daySchedules[dayNameKey]?.[1]?.map(scheduleItem => scheduleItem.groupId);
    if (!groupIds?.length) {
      return triggerServerError('No groups specified', 400);
    }
    const filteredItems = filterOutEmptyScheduleItems(data);
    await ScheduleWeekRepository.deleteScheduleByDaysGroups(groupIds, dayNames, data.weekNumber);
    await ScheduleWeekRepository.createOne(filteredItems);
    const scheduleWeek = await this.getScheduleGroups(groupIds, data.weekNumber);
    return scheduleWeek;
  }

  static async updateOne(data: ICreateScheduleWeekPayload) {
    const scheduleWeek = ScheduleWeekRepository.updateOne(data);
    return scheduleWeek;
  }

  static async getScheduleGroup(groupId: string, weekNumber: number) {
    const schedule = await ScheduleWeekRepository.getScheduleGroup(groupId, weekNumber);
    return schedule;
  }

  static async getScheduleGroups(groupIds: string[], weekNumber: number) {
    const groupSchedule = groupIds.map(groupId => ScheduleWeekRepository.getScheduleGroup(groupId, weekNumber));
    const resolvedGroupSchedule = Promise.all(groupSchedule);
    return resolvedGroupSchedule;
  }
}
