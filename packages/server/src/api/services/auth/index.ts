import { IUserLoginPayload, UnknownTypeUser } from '@rozcloud/shared/interfaces';
import { verifyPasswordHash } from 'src/common/helpers/crypto.helpers';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { WithPasswordHash } from 'src/common/interfaces';
import { UsersRepository } from 'src/data/repositories/users';

export class AuthService {
  static async login(authPayload: IUserLoginPayload): Promise<UnknownTypeUser> {
    const { loginOrEmailOrPhone, password } = authPayload;
    let user = (null as unknown) as WithPasswordHash<UnknownTypeUser>;
    try {
      user = await UsersRepository.getUserCredential(loginOrEmailOrPhone);
    } catch {
      return triggerServerError(`User with login and email and phone ${loginOrEmailOrPhone} not found`, 404);
    }
    const isPasswordValid = await verifyPasswordHash(password, user.passwordHash, user.salt);
    if (!isPasswordValid) return triggerServerError('The password is incorrect', 401);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { passwordHash, salt, ...userProperties } = user;
    return userProperties as UnknownTypeUser;
  }

  static async getUserById(userId: string) {
    try {
      const user = await UsersRepository.getUserById(userId);
      return user;
    } catch {
      triggerServerError('User is not authenticated', 401);
    }
  }
}
