import { FacultiesRepository } from 'src/data/repositories/faculty';
import { BaseCRUDService, ICreateFacultyPayload, IUpdateFacultyPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';

@staticImplements<BaseCRUDService>()
export class FacultiesService {
  static async getOne(facultyId: string) {
    const faculty = await FacultiesRepository.getOne(facultyId);
    return faculty;
  }

  static async createOne(data: ICreateFacultyPayload) {
    const createdFaculty = await FacultiesRepository.createOne(data);
    return createdFaculty;
  }

  static async updateOne(facultyId: string, data: IUpdateFacultyPayload) {
    const updatedFaculty = await FacultiesRepository.updateOne(facultyId, data);
    return updatedFaculty;
  }

  static async getFacultyDepartments(facultyId: string) {
    const faculty = await FacultiesRepository.getFacultyDepartments(facultyId);
    return faculty;
  }
}
