import {
  BaseCRUDService,
  ICreateEmployeePayload,
  IDbCreateEmployeePayload,
  IDbUpdateEmployeePayload,
  IUpdateEmployeePayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseUserService } from 'src/api/services/base-users-service';
import { EmployeesRepository } from 'src/data/repositories/employee';

@staticImplements<BaseCRUDService>()
export class EmployeesService extends BaseUserService {
  static async getOne(employeeId: string) {
    const employee = EmployeesRepository.getOne(employeeId);
    return employee;
  }

  static async createOne(data: ICreateEmployeePayload) {
    const dbData: IDbCreateEmployeePayload = await this.addHashToUser(data);
    const createdEmployee = EmployeesRepository.createOne(dbData);
    return createdEmployee;
  }

  static async updateOne(entityId: string, data: IUpdateEmployeePayload) {
    const dbData: IDbUpdateEmployeePayload = await this.addHashToUser(data);
    const updatedEmployee = EmployeesRepository.updateOne(entityId, dbData);
    return updatedEmployee;
  }

  static async getEmployeeStructure(employeeId: string) {
    const employee = await EmployeesRepository.getEmployeeStructure(employeeId);
    return employee;
  }
}
