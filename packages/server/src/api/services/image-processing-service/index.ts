/* eslint-disable @typescript-eslint/no-explicit-any */
import { StructureTypes } from 'src/common/enums';
import { defaultUniversityLogoPath } from 'src/common/constants';
import multer from 'fastify-multer';
import { promises, constants } from 'fs';
import path from 'path';
import { generateUUID } from 'src/common/helpers/global.helper';

const getFileName = async (fileName?: string, pathParts: string[] = ['/uploads']): Promise<string> => {
  if (!fileName) {
    return generateUUID();
  }
  const directoryPath = path.join(...pathParts);
  const fileNameParts = fileName.split('.');
  const fileExtension = fileNameParts.pop();
  if (!fileNameParts?.length) {
    return generateUUID();
  }
  let fileNameBase = fileNameParts.join('.');
  for (let i = 0; i < 1_000; i++) {
    const fullPath = path.join(directoryPath, fileNameBase + '.' + fileExtension);
    try {
      await promises.access(fullPath, constants.F_OK);
      fileNameBase = incrementFileName(fileNameBase);
    } catch (e) {
      return fileNameBase + '.' + fileExtension;
    }
  }
  return generateUUID() + '.' + fileExtension;
};

const incrementFileName = (fileName: string) => {
  const braceIndx = fileName.lastIndexOf('(');
  const secondBraceIndx = fileName.lastIndexOf(')');
  if (
    braceIndx !== -1 &&
    secondBraceIndx !== -1 &&
    secondBraceIndx === fileName.length - 1 &&
    +fileName.substring(braceIndx + 1, secondBraceIndx)
  ) {
    const number = +fileName.substring(braceIndx + 1, secondBraceIndx);
    const incrementedNumber = number + 1;
    return fileName.substring(0, braceIndx) + '(' + incrementedNumber + ')';
  }
  return fileName + '(1)';
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, '/uploads/');
  },
  filename: async function(req, file, cb) {
    const fileName = await getFileName(file.originalname);
    cb(null, fileName);
  },
});

export const uploader = multer({
  storage,
});
export const singleFile = (fileName: string) => (uploader.single(fileName) as unknown) as any;

export class ImageProcessingService {
  static getDefaultStructureLogo(structureType: StructureTypes) {
    return defaultUniversityLogoPath;
  }

  static saveImage(file: File) {
    console.log(file);
    // fs.writeFile();
  }
}
