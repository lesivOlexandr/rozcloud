import { UniversitiesRepository } from 'src/data/repositories/university';
import { ICreateUniversityPayload, IUpdateUniversityPayload, BaseCRUDService } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';

@staticImplements<BaseCRUDService>()
export class UniversitiesService {
  static async getAll() {
    const universities = await UniversitiesRepository.getAll();
    return universities;
  }

  static async getOne(universityId: string) {
    const university = await UniversitiesRepository.getOne(universityId);
    return university;
  }

  static async createOne(data: ICreateUniversityPayload) {
    const createdUniversity = await UniversitiesRepository.createOne(data);
    return createdUniversity;
  }

  static async updateOne(universityId: string, data: IUpdateUniversityPayload) {
    const updatedUniversity = await UniversitiesRepository.updateOne(universityId, data);
    return updatedUniversity;
  }

  static async getUniversityCorpuses(universityId: string) {
    const corpuses = await UniversitiesRepository.getUniversityCorpuses(universityId);
    return corpuses;
  }

  static async getUniversityAuditories(universityId: string) {
    const auditories = await UniversitiesRepository.getUniversityAuditories(universityId);
    return auditories;
  }

  static async getUniversityFaculties(universityId: string) {
    const faculties = await UniversitiesRepository.getUniversityFaculties(universityId);
    return faculties;
  }
}
