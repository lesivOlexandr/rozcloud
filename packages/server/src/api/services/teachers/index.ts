import {
  BaseCRUDService,
  ICreateTeacherPayload,
  IUpdateTeacherPayload,
  IDbCreateTeacherPayload,
  IDbUpdateTeacherPayload,
  ITeacherFilter,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseUserService } from 'src/api/services/base-users-service';
import { TeachersRepository } from 'src/data/repositories/teachers';

@staticImplements<BaseCRUDService>()
export class TeachersService extends BaseUserService {
  static async getMany(filters: ITeacherFilter) {
    const teachers = await TeachersRepository.getMany(filters);
    return teachers;
  }

  static async getOne(teacherId: string) {
    const teacher = await TeachersRepository.getOne(teacherId);
    return teacher;
  }

  static async createOne(data: ICreateTeacherPayload) {
    const dbData: IDbCreateTeacherPayload = await this.addHashToUser(data);
    const createdTeacher = TeachersRepository.createOne(dbData);
    return createdTeacher;
  }

  static async updateOne(teacherId: string, data: IUpdateTeacherPayload) {
    const dbData: IDbUpdateTeacherPayload = await this.addHashToUser(data);
    const updatedStudent = TeachersRepository.updateOne(teacherId, dbData);
    return updatedStudent;
  }

  static async getTeacherSubjects(teacherId: string) {
    const subjects = await TeachersRepository.getTeacherSubjects(teacherId);
    return subjects;
  }
}
