import { GroupsRepository } from 'src/data/repositories/group';
import { ICreateGroupPayload, IUpdateGroupPayload, BaseCRUDService } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { IGroupFilters } from 'src/common/interfaces/repositories/filters/group';

@staticImplements<BaseCRUDService>()
export class GroupsService {
  static async getOne(groupId: string) {
    const group = await GroupsRepository.getOne(groupId);
    return group;
  }

  static async getMany(filters: IGroupFilters) {
    const groups = await GroupsRepository.getGroups(filters);
    return groups;
  }

  static async createOne(data: ICreateGroupPayload) {
    const createdGroup = await GroupsRepository.createOne(data);
    return createdGroup;
  }

  static async updateOne(groupId: string, data: IUpdateGroupPayload) {
    const updatedGroup = await GroupsRepository.updateOne(groupId, data);
    return updatedGroup;
  }

  static async getGroupStudents(groupId: string) {
    const students = await GroupsRepository.getGroupStudents(groupId);
    return students;
  }

  static async getGroupSubgroups(groupId: string) {
    const subgroups = await GroupsRepository.getGroupSubgroups(groupId);
    return subgroups;
  }

  static async getGroupCreatedAtYear(groupId: string) {
    const group = await GroupsRepository.getOneAcademicGroupById(groupId);
    return group.groupCreatedYear;
  }

  static async bindStudentToGroup(groupId: string, studentId: string) {
    const group = await GroupsRepository.bindStudentToGroup(groupId, studentId);
    return group;
  }
}
