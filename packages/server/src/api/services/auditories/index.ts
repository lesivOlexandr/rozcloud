import {
  BaseCRUDService,
  IAuditoryFilter,
  ICreateAuditoryPayload,
  IUpdateAuditoryPayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { AuditoriesRepository } from 'src/data/repositories/auditories';

@staticImplements<BaseCRUDService>()
export class AuditoriesService {
  static async getMany(filters: IAuditoryFilter) {
    const auditories = AuditoriesRepository.getMany(filters);
    return auditories;
  }

  static async getOne(auditoryId: string) {
    const auditory = AuditoriesRepository.getOne(auditoryId);
    return auditory;
  }

  static async createOne(data: ICreateAuditoryPayload) {
    const createdAuditory = await AuditoriesRepository.createOne(data);
    return createdAuditory;
  }

  static async updateOne(auditoryId: string, data: IUpdateAuditoryPayload) {
    const updatedAuditory = await AuditoriesRepository.updateOne(auditoryId, data);
    return updatedAuditory;
  }
}
