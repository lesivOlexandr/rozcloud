import { BaseCRUDService, CreateSpecializationPayload, UpdateSpecializationPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { SpecializationsRepository } from 'src/data/repositories/specializations';

@staticImplements<BaseCRUDService>()
export class SpecializationsService {
  static async getOne(specializationId: string) {
    const specialization = await SpecializationsRepository.getOne(specializationId);
    return specialization;
  }

  static async createOne(data: CreateSpecializationPayload) {
    const specialization = await SpecializationsRepository.createOne(data);
    return specialization;
  }

  static async updateOne(specializationId: string, data: UpdateSpecializationPayload) {
    const specialization = await SpecializationsRepository.updateOne(specializationId, data);
    return specialization;
  }

  static async getSpecializationGroups(specializationId: string) {
    const groups = await SpecializationsRepository.getSpecializationGroups(specializationId);
    return groups;
  }

  static async getAllSpecializations() {
    const specializations = await SpecializationsRepository.getAllSpecializations();
    return specializations;
  }
}
