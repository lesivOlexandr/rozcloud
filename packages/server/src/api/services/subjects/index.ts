import { BaseCRUDService, ICreateSubjectPayload, ISubjectFilter, IUpdateSubjectPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { SubjectsRepository } from 'src/data/repositories/subjects';

@staticImplements<BaseCRUDService>()
export class SubjectService {
  static async getMany(filters: ISubjectFilter) {
    const subjects = await SubjectsRepository.getMany(filters);
    return subjects;
  }

  static async getOne(subjectId: string) {
    const subject = await SubjectsRepository.getOne(subjectId);
    return subject;
  }

  static async createOne(data: ICreateSubjectPayload) {
    const createdEntity = await SubjectsRepository.createOne(data);
    return createdEntity;
  }

  static async updateOne(entityId: string, data: IUpdateSubjectPayload) {
    const updatedEntity = await SubjectsRepository.updateOne(entityId, data);
    return updatedEntity;
  }

  static async getSubjectTeachers(subjectId: string) {
    const teachers = await SubjectsRepository.getSubjectTeachers(subjectId);
    return teachers;
  }
}
