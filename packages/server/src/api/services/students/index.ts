import { StudentsRepository } from 'src/data/repositories/students';
import {
  BaseCRUDService,
  ICreateStudentPayload,
  IDbCreateStudentPayload,
  IDbUpdateStudentPayload,
  IUpdateStudentPayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { BaseUserService } from 'src/api/services/base-users-service';

@staticImplements<BaseCRUDService>()
export class StudentsService extends BaseUserService {
  static async getOne(studentId: string) {
    const student = await StudentsRepository.getOne(studentId);
    return student;
  }

  static async createOne(data: ICreateStudentPayload) {
    const dbData: IDbCreateStudentPayload = await this.addHashToUser(data);
    const createdStudent = StudentsRepository.createOne(dbData);
    return createdStudent;
  }

  static async updateOne(studentId: string, data: IUpdateStudentPayload) {
    const dbData: IDbUpdateStudentPayload = await this.addHashToUser(data);
    const updatedStudent = StudentsRepository.updateOne(studentId, dbData);
    return updatedStudent;
  }
}
