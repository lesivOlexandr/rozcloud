import {
  BaseCRUDService,
  ICreateUpdateEducationalPlanPayload,
  IDBCreateUpdateEducationalPlanPayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { EducationalPlansRepository } from 'src/data/repositories/educational-plan';
import { GroupsService } from 'src/api/services/group';
import { countAllSubjectHours } from 'src/common/helpers/schedule';

@staticImplements<BaseCRUDService>()
export class EducationalPlansService {
  static async getOne(educationalPlanId: string) {
    const educationalPlan = await EducationalPlansRepository.getOne(educationalPlanId);
    return educationalPlan;
  }

  static async createOne(data: ICreateUpdateEducationalPlanPayload) {
    const groupCreatedYear = await GroupsService.getGroupCreatedAtYear(data.groupId);
    const dbData = data as IDBCreateUpdateEducationalPlanPayload;
    for (const subjectHourItem of dbData.subjectsHoursMap) {
      subjectHourItem.allHours = countAllSubjectHours(subjectHourItem);
      for (const semesterControl of subjectHourItem.semesterSubjectControls) {
        const semesterNumber = (semesterControl.semesterNumber % 2 || 2) as 1 | 2;
        semesterControl.semesterRealNumber = semesterNumber;
        semesterControl.semesterYear = Math.trunc(groupCreatedYear + semesterControl.semesterNumber / 2);
      }
    }
    const createdEducationalPlan = await EducationalPlansRepository.createOne(dbData);
    return createdEducationalPlan;
  }

  static async updateOne(educationalPlanId: string, data: ICreateUpdateEducationalPlanPayload) {
    this.createOne(data);
  }

  static async getOneByGroupId(groupId: string) {
    const educationalPlan = await EducationalPlansRepository.getOneByGroupId(groupId);
    return educationalPlan;
  }
}
