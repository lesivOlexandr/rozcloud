import {
  BaseCRUDService,
  ICreateUpdateWorkingPlanPayload,
  IDBCreateUpdateWorkingPlanPayload,
} from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { GroupsService } from 'src/api/services/group';
import { countAllSubjectHours } from 'src/common/helpers/schedule';
import { WorkingPlansRepository } from 'src/data/repositories/working-plan';

@staticImplements<BaseCRUDService>()
export class WorkingPlansService {
  static async getOne(workingPlanId: string) {
    const workingPlan = await WorkingPlansRepository.getOne(workingPlanId);
    return workingPlan;
  }

  static async createOne(data: ICreateUpdateWorkingPlanPayload) {
    const groupCreatedYear = await GroupsService.getGroupCreatedAtYear(data.groupId);
    const dbData = data as IDBCreateUpdateWorkingPlanPayload;
    dbData.semesterYear = Math.trunc(groupCreatedYear + data.semesterNumber / 2);
    dbData.semesterRealNumber = (dbData.semesterNumber % 2 || 2) as 1 | 2;
    for (const subjectHourItem of dbData.subjectsHoursMap) {
      subjectHourItem.allHours = countAllSubjectHours(subjectHourItem);
    }
    const createdEducationalPlan = WorkingPlansRepository.createOne(dbData);
    return createdEducationalPlan;
  }

  static async updateOne(workingPlanId: string, data: ICreateUpdateWorkingPlanPayload) {
    // TODO
  }

  static async deleteOne(workingPlanId: string) {
    return await WorkingPlansRepository.deleteOne(workingPlanId);
  }

  static async getOneByGroup(groupId: string, semesterNumber: number) {
    const workingPlan = await WorkingPlansRepository.getOneByGroup(groupId, semesterNumber);
    return workingPlan;
  }

  static async getGroupAllWorkingPlans(groupId: string) {
    const workingPlans = await WorkingPlansRepository.getGroupAllWorkingPlans(groupId);
    return workingPlans;
  }
}
