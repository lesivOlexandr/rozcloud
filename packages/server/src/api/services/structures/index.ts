import { StructuresRepository } from 'src/data/repositories/structures';

export class StructuresService {
  static async getStructureEmployees(structureId: string) {
    const employees = StructuresRepository.getStructureEmployees(structureId);
    return employees;
  }

  static async getUnknownStructure(structureId: string) {
    const structure = StructuresRepository.getUnknownStructure(structureId);
    return structure;
  }
}
