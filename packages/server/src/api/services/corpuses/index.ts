import { BaseCRUDService, ICreateCorpusPayload, IUpdateCorpusPayload } from 'src/common/interfaces';
import { staticImplements } from '@rozcloud/shared/helpers/types.helpers';
import { CorpusesRepository } from 'src/data/repositories/corpuses';

@staticImplements<BaseCRUDService>()
export class CorpusesService {
  static async getOne(corpusId: string) {
    const corpus = await CorpusesRepository.getOne(corpusId);
    return corpus;
  }

  static async createOne(data: ICreateCorpusPayload) {
    const createdCorpus = await CorpusesRepository.createOne(data);
    return createdCorpus;
  }

  static async updateOne(entityToUpdateId: string, data: IUpdateCorpusPayload) {
    const updatedCorpus = await CorpusesRepository.updateOne(entityToUpdateId, data);
    return updatedCorpus;
  }

  static async getCorpusAuditories(corpusId: string) {
    const auditories = await CorpusesRepository.getCorpusAuditories(corpusId);
    return auditories;
  }
}
