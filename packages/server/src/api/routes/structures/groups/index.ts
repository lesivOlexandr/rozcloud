import { FastifyInstance } from 'fastify';
import { GroupsService } from 'src/api/services/group';
import {
  BindStudentToGroupRequest,
  CreateGroupRequest,
  GetAnyGroupsRequest,
  GetGroupRequest,
  GetGroupStudentsRequest,
  GetGroupSubgroupsRequest,
  UpdateGroupRequest,
} from './interfaces';
import { groupCreateMiddleware } from './middlewares';
import {
  bindStudentToGroupSchema,
  createGroupSchema,
  getGroupSchema,
  getGroupsSchema,
  getGroupStudentsSchema,
  getGroupSubgroupsSchema,
  updateGroupSchema,
} from 'src/api/routes/structures/groups/schema';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/', { schema: getGroupsSchema }, async (request: GetAnyGroupsRequest) => {
    const groups = await GroupsService.getMany(request.query);
    return groups;
  });

  fastify.get('/:groupId', { schema: getGroupSchema }, async (request: GetGroupRequest) => {
    const group = await GroupsService.getOne(request.params.groupId);
    return group;
  });

  fastify.post('/', { schema: createGroupSchema }, async (request: CreateGroupRequest) => {
    await groupCreateMiddleware(request);
    const group = await GroupsService.createOne(request.body);
    return group;
  });

  fastify.put('/:groupId', { schema: updateGroupSchema }, async (request: UpdateGroupRequest) => {
    const group = await GroupsService.updateOne(request.params.groupId, request.body);
    return group;
  });

  fastify.put(
    '/bind-student-to-group',
    { schema: bindStudentToGroupSchema },
    async (request: BindStudentToGroupRequest) => {
      const group = await GroupsService.bindStudentToGroup(request.body.groupId, request.body.studentId);
      return group;
    }
  );

  fastify.get('/:groupId/students', { schema: getGroupStudentsSchema }, async (request: GetGroupStudentsRequest) => {
    const students = await GroupsService.getGroupStudents(request.params.groupId);
    return students;
  });

  fastify.get('/:groupId/subgroups', { schema: getGroupSubgroupsSchema }, async (request: GetGroupSubgroupsRequest) => {
    const subgroups = await GroupsService.getGroupSubgroups(request.params.groupId);
    return subgroups;
  });

  next();
};
