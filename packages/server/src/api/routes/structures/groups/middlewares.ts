import { ImageProcessingService } from 'src/api/services/image-processing-service';
import { StructureTypes } from 'src/common/enums';
import { CreateGroupRequest } from './interfaces';

export const groupCreateMiddleware = async (request: CreateGroupRequest) => {
  request.body.logo = request.body.logo ?? ImageProcessingService.getDefaultStructureLogo(StructureTypes.AcademicGroup);
  return request;
};
