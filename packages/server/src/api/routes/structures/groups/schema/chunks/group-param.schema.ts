import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const groupParamChunk = createObjectSchema(
  {
    type: 'object',
    properties: {
      groupId: idSchemaChunk,
    },
  },
  {
    required: ['groupId'],
  }
);
