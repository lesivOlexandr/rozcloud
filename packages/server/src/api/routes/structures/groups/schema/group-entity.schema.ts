import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { groupFullSchema } from './group-full.schema';

export const groupEntitySchema = createObjectSchema(groupFullSchema, {
  required: ['id', 'name', 'createdAt', 'updatedAt', 'teachersQuantity', 'studentsQuantity'],
  defaultNull: 'onNotRequired',
});
