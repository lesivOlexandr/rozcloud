import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { groupFullSchema } from './group-full.schema';

export const updateGroupBody = createObjectSchema(groupFullSchema, {
  required: ['name'],
  removeProperties: ['updatedAt', 'teachersQuantity', 'studentsQuantity', 'createdAt', 'id'],
  nullable: 'onNotRequired',
});
