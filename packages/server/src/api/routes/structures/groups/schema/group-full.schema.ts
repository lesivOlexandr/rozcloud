import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import {
  structureEntitiesCountSchemaChunk,
  structureLogoSchemaChunk,
  structureNameSchemaChunk,
} from 'src/api/routes/structures/_schema-chunks';
import { yearChunk } from 'src/api/routes/_schema-chunks/year.scheme';
import { teacherResponseSuccess } from 'src/api/routes/users/teachers/schema/teacher-response-success.schema';
import { studentResponseSuccess } from 'src/api/routes/users/students/schema/student-response-success.schema';

export const groupFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: structureNameSchemaChunk,
    logo: structureLogoSchemaChunk,
    curatorId: idSchemaChunk,
    groupPresidentId: idSchemaChunk,
    curator: teacherResponseSuccess,
    groupPresident: studentResponseSuccess,
    groupCreatedYear: yearChunk,
    departmentId: idSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
    teachersQuantity: structureEntitiesCountSchemaChunk,
    studentsQuantity: structureEntitiesCountSchemaChunk,
  },
});
