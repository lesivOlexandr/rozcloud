import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { groupFullSchema } from './group-full.schema';

export const createGroupBody = createObjectSchema(groupFullSchema, {
  required: ['name'],
  useProperties: ['name', 'departmentId', 'logo', 'curatorId', 'groupPresidentId', 'groupCreatedYear'],
  defaultNull: 'onNotRequired',
});
