import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { updateGroupBody } from 'src/api/routes/structures/groups/schema/update-group.schema';
import { groupParamChunk } from './chunks/group-param.schema';
import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { studentResponseSuccess } from 'src/api/routes/users/students/schema/student-response-success.schema';
import { createGroupBody } from './create-group.schema';
import { maxTextFieldLetters } from 'src/common/constants';

export const createGroupSchema = createRouteValidationSchema(
  {
    body: createGroupBody,
    tags: ['Structures'],
  },
  {
    200: groupEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateGroupSchema = createRouteValidationSchema(
  {
    body: updateGroupBody,
    params: groupParamChunk,
    tags: ['Structures'],
  },
  {
    200: groupEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getGroupsSchema = createRouteValidationSchema(
  {
    querystring: createObjectSchema(
      {
        type: 'object',
        properties: {
          groupIds: {
            type: 'array',
            items: idSchemaChunk,
          },
          excludeIds: {
            type: 'array',
            items: idSchemaChunk,
            minItems: 0,
          },
          search: {
            type: 'string',
            maxLength: maxTextFieldLetters,
          },
        },
      },
      {
        nullable: 'onNotRequired',
      }
    ),
  },
  {
    200: {
      type: 'array',
      items: groupEntitySchema,
    },
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const getGroupSchema = createRouteValidationSchema(
  {
    params: groupParamChunk,
    tags: ['Structures'],
  },
  {
    200: groupEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getGroupStudentsSchema = createRouteValidationSchema(
  {
    params: groupParamChunk,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: studentResponseSuccess },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getGroupSubgroupsSchema = createRouteValidationSchema(
  {
    params: groupParamChunk,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: groupEntitySchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const bindStudentToGroupSchema = createRouteValidationSchema(
  {
    body: { type: 'object', properties: { studentId: idSchemaChunk, groupId: idSchemaChunk } },
    params: groupParamChunk,
    tags: ['Structures'],
  },
  {
    200: groupEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
