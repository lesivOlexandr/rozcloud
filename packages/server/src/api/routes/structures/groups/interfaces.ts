import { FastifyRequest } from 'fastify';
import { ICreateGroupPayload, IUpdateGroupPayload } from 'src/common/interfaces';
import { IGroupFilters } from 'src/common/interfaces/repositories/filters/group';
import { AppRequest } from 'src/common/interfaces/server';

export type GroupParam = { groupId: string };

export type CreateGroupRequest = AppRequest<ICreateGroupPayload>;
export type UpdateGroupRequest = AppRequest<IUpdateGroupPayload, GroupParam>;
export type GetGroupRequest = AppRequest<undefined, GroupParam>;
export type GetGroupStudentsRequest = AppRequest<undefined, GroupParam>;
export type GetGroupSubgroupsRequest = AppRequest<undefined, GroupParam>;
export type BindStudentToGroupRequest = AppRequest<{ studentId: string; groupId: string }>;
export type GetAnyGroupsRequest = FastifyRequest<{ Querystring: IGroupFilters }>;
