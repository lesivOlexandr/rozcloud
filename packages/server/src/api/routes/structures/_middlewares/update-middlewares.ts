import { FastifyRequest } from 'fastify';
import { StructureBase } from 'src/common/interfaces';
import { StructureTypes } from 'src/common/enums';
import { getAbbreviation } from '@rozcloud/shared/helpers/text.helpers';

export const createPreprocessUpdateStructure = (type: StructureTypes) => {
  const handler = async (request: FastifyRequest<{ Body: Partial<StructureBase> }>) => {
    if (typeof request.body.name === 'string' && request.body.abbreviation === '') {
      request.body.abbreviation = getAbbreviation(request.body.name);
    }
    return request;
  };
  return handler;
};
