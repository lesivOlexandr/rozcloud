import { FastifyRequest } from 'fastify';
import { StructureBase } from 'src/common/interfaces';
import { StructureTypes } from 'src/common/enums';
import { getAbbreviation } from '@rozcloud/shared/helpers/text.helpers';
import { ImageProcessingService } from 'src/api/services/image-processing-service';

export const preprocessCreateStructureFactory = (type: StructureTypes) => {
  const handler = async (request: FastifyRequest<{ Body: StructureBase }>) => {
    request.body.logo = request.body.logo ?? ImageProcessingService.getDefaultStructureLogo(type);
    request.body.abbreviation = request.body.abbreviation ?? getAbbreviation(request.body.name);
    return request;
  };
  return handler;
};
