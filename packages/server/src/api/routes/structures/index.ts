import { FastifyInstance } from 'fastify';
import universityRoutes from './universities';
import facultyRoutes from './faculties';
import departmentRoutes from './departments';
import groupRoutes from './groups';
import { GetStructureEmployeesRequest, GetUnknownStructureRequest } from './interfaces';
import { StructuresService } from 'src/api/services/structures';
import { getStructureEmployees, getUnknownStructureSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(universityRoutes, { prefix: 'universities' });
  fastify.register(facultyRoutes, { prefix: 'faculties' });
  fastify.register(departmentRoutes, { prefix: 'departments' });
  fastify.register(groupRoutes, { prefix: 'groups' });

  fastify.get('/:structureId', { schema: getUnknownStructureSchema }, async (request: GetUnknownStructureRequest) => {
    const structure = await StructuresService.getUnknownStructure(request.params.structureId);
    return structure;
  });

  fastify.get(
    '/:structureId/employees',
    { schema: getStructureEmployees },
    async (request: GetStructureEmployeesRequest) => {
      const employees = await StructuresService.getStructureEmployees(request.params.structureId);
      return employees;
    }
  );

  next();
};
