import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { FacultiesService } from 'src/api/services/faculty';
import {
  CreateFacultyRequest,
  GetFacultyDepartmentsRequest,
  GetFacultyRequest,
  UpdateFacultyRequest,
} from './interfaces';
import { preprocessCreateFaculty, preprocessUpdateFaculty } from './middlewares';
import { createFacultySchema, getDepartmentsSchema, getFacultySchema, updateFacultySchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/:facultyId', { schema: getFacultySchema }, async (request: GetFacultyRequest) => {
    const faculty = await FacultiesService.getOne(request.params.facultyId);
    return faculty;
  });

  fastify.get(
    '/:facultyId/departments',
    { schema: getDepartmentsSchema },
    async (request: GetFacultyDepartmentsRequest) => {
      const departments = await FacultiesService.getFacultyDepartments(request.params.facultyId);
      return departments;
    }
  );

  fastify.post('/', { schema: createFacultySchema }, async (request: CreateFacultyRequest) => {
    await preprocessCreateFaculty(request);
    const faculty = await FacultiesService.createOne(request.body);
    return faculty;
  });

  fastify.put('/:facultyId', { schema: updateFacultySchema }, async (request: UpdateFacultyRequest) => {
    await preprocessUpdateFaculty(request);
    const faculty = await FacultiesService.updateOne(request.params.facultyId, request.body);
    return faculty;
  });

  next();
};
