import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { createFacultyBody } from 'src/api/routes/structures/faculties/schema/create-faculty.schema';
import { facultyEntitySchema } from 'src/api/routes/structures/faculties/schema/faculty-entity.schema';
import {
  updateFacultyBody,
  updateFacultyParams,
} from 'src/api/routes/structures/faculties/schema/update-faculty.schema';
import { facultyIdParamChunk } from './chunks/faculty-param.schema';
import { departmentEntitySchema } from '../../departments/schema/department-entity.schema';

export const createFacultySchema = createRouteValidationSchema(
  {
    body: createFacultyBody,
    tags: ['Structures'],
  },
  {
    200: facultyEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateFacultySchema = createRouteValidationSchema(
  {
    body: updateFacultyBody,
    params: updateFacultyParams,
    tags: ['Structures'],
  },
  {
    200: facultyEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getFacultySchema = createRouteValidationSchema(
  {
    params: facultyIdParamChunk,
    tags: ['Structures'],
  },
  {
    200: facultyEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getDepartmentsSchema = createRouteValidationSchema(
  {
    params: facultyIdParamChunk,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: departmentEntitySchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
