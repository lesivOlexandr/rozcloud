import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { facultyFullSchema } from './faculty-full.schema';

export const facultyEntitySchema = createObjectSchema(facultyFullSchema, {
  required: [
    'id',
    'createdAt',
    'updatedAt',
    'departmentsQuantity',
    'academicGroupsQuantity',
    'teachersQuantity',
    'studentsQuantity',
  ],
  removeProperties: ['universityId'],
  defaultNull: 'onNotRequired',
});
