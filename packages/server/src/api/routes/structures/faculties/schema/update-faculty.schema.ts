import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { facultyFullSchema } from './faculty-full.schema';

export const updateFacultyBody = createObjectSchema(facultyFullSchema, {
  useProperties: ['name', 'abbreviation', 'logo'],
  nullable: 'onNotRequired',
});

export const updateFacultyParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      facultyId: idSchemaChunk,
    },
  },
  {
    required: ['facultyId'],
  }
);
