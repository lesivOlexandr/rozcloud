import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { facultyFullSchema } from './faculty-full.schema';

export const createFacultyBody = createObjectSchema(facultyFullSchema, {
  useProperties: ['name', 'abbreviation', 'logo', 'universityId'],
  required: ['universityId', 'name'],
  defaultNull: 'onNotRequired',
});
