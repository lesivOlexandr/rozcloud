import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import {
  structureAbbreviationChunk,
  structureEntitiesCountSchemaChunk,
  structureLogoSchemaChunk,
  structureNameSchemaChunk,
} from 'src/api/routes/structures/_schema-chunks';

export const facultyFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: structureNameSchemaChunk,
    abbreviation: structureAbbreviationChunk,
    logo: structureLogoSchemaChunk,
    universityId: idSchemaChunk,
    departmentsQuantity: structureEntitiesCountSchemaChunk,
    academicGroupsQuantity: structureEntitiesCountSchemaChunk,
    teachersQuantity: structureEntitiesCountSchemaChunk,
    studentsQuantity: structureEntitiesCountSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
