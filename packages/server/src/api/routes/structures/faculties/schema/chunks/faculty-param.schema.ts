import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const facultyIdParamChunk = createScalarSchema({
  type: 'object',
  properties: {
    facultyId: idSchemaChunk,
  },
});
