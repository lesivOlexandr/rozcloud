import { ICreateFacultyPayload, IUpdateFacultyPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

type FacultyParam = { facultyId: string };

export type GetFacultyRequest = AppRequest<undefined, FacultyParam>;
export type GetFacultyDepartmentsRequest = AppRequest<undefined, FacultyParam>;
export type CreateFacultyRequest = AppRequest<ICreateFacultyPayload>;
export type UpdateFacultyRequest = AppRequest<IUpdateFacultyPayload, FacultyParam>;
