import { preprocessCreateStructureFactory, createPreprocessUpdateStructure } from '../_middlewares';
import { StructureTypes } from 'src/common/enums';

const structureType = StructureTypes.Faculty;

export const preprocessCreateFaculty = preprocessCreateStructureFactory(structureType);

export const preprocessUpdateFaculty = createPreprocessUpdateStructure(structureType);
