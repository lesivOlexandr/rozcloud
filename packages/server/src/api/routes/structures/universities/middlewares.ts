import { StructureTypes } from 'src/common/enums';
import {
  preprocessCreateStructureFactory,
  createPreprocessUpdateStructure,
} from 'src/api/routes/structures/_middlewares';

const structureType = StructureTypes.University;

export const preprocessCreateUniversity = preprocessCreateStructureFactory(structureType);

export const preprocessUpdateUniversity = createPreprocessUpdateStructure(structureType);
