import { maxTextFieldLetters } from 'src/common/constants';
import { HigherEducationType } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const universityTypeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 0,
  maxLength: maxTextFieldLetters,
  enum: Object.values(HigherEducationType),
});
