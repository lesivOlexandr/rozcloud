import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const universityIdChunk = createObjectSchema({
  type: 'object',
  properties: {
    universityId: idSchemaChunk,
  },
});
