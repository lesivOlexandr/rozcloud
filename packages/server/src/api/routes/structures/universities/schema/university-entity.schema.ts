import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { universityFullSchema } from './university-full.schema';

export const universityEntitySchema = createObjectSchema(universityFullSchema, {
  required: [
    'id',
    'name',
    'createdAt',
    'updatedAt',
    'facultiesQuantity',
    'departmentsQuantity',
    'academicGroupsQuantity',
    'teachersQuantity',
    'studentsQuantity',
  ],
  defaultNull: 'onNotRequired',
});
