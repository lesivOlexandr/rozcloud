import { auditoryEntitySchema } from 'src/api/routes/locations/auditories/schema/auditory-entity.schema';
import { corpusEntitySchema } from 'src/api/routes/locations/corpuses/schema/corpus-entity.schema';
import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { facultyEntitySchema } from '../../faculties/schema/faculty-entity.schema';
import { universityIdChunk } from './chunks/university-id.schema';
import { createUniversityBodySchema } from './create-university.schema';
import { universityEntitySchema } from './university-entity.schema';
import { updateUniversityBodySchema } from './update-university.schema';

export const getUniversitySchema = createRouteValidationSchema(
  {
    params: universityIdChunk,
    tags: ['Structures'],
  },
  {
    200: universityEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const createUniversitySchema = createRouteValidationSchema(
  {
    body: createUniversityBodySchema,
    tags: ['Structures'],
  },
  {
    200: universityEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateUniversitySchema = createRouteValidationSchema(
  {
    body: updateUniversityBodySchema,
    tags: ['Structures'],
  },
  {
    200: universityEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getUniversityCorpusesSchema = createRouteValidationSchema(
  {
    params: universityIdChunk,
    tags: ['Structures'],
  },
  {
    200: {
      type: 'array',
      items: corpusEntitySchema,
    },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getUniversityAuditoriesSchema = createRouteValidationSchema(
  {
    params: universityIdChunk,
    tags: ['Structures'],
  },
  {
    200: {
      type: 'array',
      items: auditoryEntitySchema,
    },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getUniversityFacultiesSchema = createRouteValidationSchema(
  {
    params: universityIdChunk,
    tags: ['Structures'],
  },
  {
    200: {
      type: 'array',
      items: facultyEntitySchema,
    },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
