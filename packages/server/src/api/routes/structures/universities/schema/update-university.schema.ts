import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { universityFullSchema } from './university-full.schema';

export const updateUniversityBodySchema = createObjectSchema(universityFullSchema, {
  useProperties: ['name', 'abbreviation', 'logo', 'city', 'type', 'image'],
  nullable: 'onNotRequired',
  required: ['name'],
});

export const updateUniversityParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      universityId: idSchemaChunk,
    },
  },
  { required: ['universityId'] }
);
