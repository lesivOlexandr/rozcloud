import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import {
  structureAbbreviationChunk,
  structureEntitiesCountSchemaChunk,
  structureLogoSchemaChunk,
  structureNameSchemaChunk,
} from 'src/api/routes/structures/_schema-chunks';
import { citySchemaChunk, universityTypeSchemaChunk } from './chunks';

export const universityFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: structureNameSchemaChunk,
    abbreviation: structureAbbreviationChunk,
    logo: structureLogoSchemaChunk,
    city: citySchemaChunk,
    type: universityTypeSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
    facultiesQuantity: structureEntitiesCountSchemaChunk,
    departmentsQuantity: structureEntitiesCountSchemaChunk,
    academicGroupsQuantity: structureEntitiesCountSchemaChunk,
    teachersQuantity: structureEntitiesCountSchemaChunk,
    studentsQuantity: structureEntitiesCountSchemaChunk,
  },
});
