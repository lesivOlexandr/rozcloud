import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { universityFullSchema } from './university-full.schema';

export const createUniversityBodySchema = createObjectSchema(universityFullSchema, {
  useProperties: ['name', 'abbreviation', 'logo', 'city', 'type', 'image'],
  required: ['name'],
  defaultNull: 'onNotRequired',
});
