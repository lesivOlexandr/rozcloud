import { ICreateUniversityPayload, IUpdateUniversityPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type UniversityParam = { universityId: string };

export type CreateUniversityRequest = AppRequest<ICreateUniversityPayload>;
export type UpdateUniversityRequest = AppRequest<IUpdateUniversityPayload, UniversityParam>;
export type GetUniversityCorpusesRequest = AppRequest<undefined, UniversityParam>;
export type GetUniversityAuditoriesRequest = AppRequest<undefined, UniversityParam>;
export type GetUniversityFacultiesRequest = AppRequest<undefined, UniversityParam>;
export type GetUniversityRequest = AppRequest<undefined, UniversityParam>;
