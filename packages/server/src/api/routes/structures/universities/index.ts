import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { UniversitiesService } from 'src/api/services/university';
import {
  CreateUniversityRequest,
  GetUniversityAuditoriesRequest,
  GetUniversityCorpusesRequest,
  GetUniversityFacultiesRequest,
  GetUniversityRequest,
  UpdateUniversityRequest,
} from './interfaces';
import { preprocessCreateUniversity, preprocessUpdateUniversity } from './middlewares';
import {
  createUniversitySchema,
  getUniversityAuditoriesSchema,
  getUniversityCorpusesSchema,
  getUniversityFacultiesSchema,
  getUniversitySchema,
  updateUniversitySchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('', { schema: { tags: ['Structures'] } }, async () => {
    const universities = await UniversitiesService.getAll();
    return universities;
  });

  fastify.get('/:universityId', { schema: getUniversitySchema }, async (request: GetUniversityRequest) => {
    const university = await UniversitiesService.getOne(request.params.universityId);
    return university;
  });

  fastify.get(
    '/:universityId/corpuses',
    { schema: getUniversityCorpusesSchema },
    async (request: GetUniversityCorpusesRequest) => {
      const corpuses = await UniversitiesService.getUniversityCorpuses(request.params.universityId);
      return corpuses;
    }
  );

  fastify.get(
    '/:universityId/auditories',
    { schema: getUniversityAuditoriesSchema },
    async (request: GetUniversityAuditoriesRequest) => {
      const auditories = await UniversitiesService.getUniversityAuditories(request.params.universityId);
      return auditories;
    }
  );
  fastify.get(
    '/:universityId/faculties',
    { schema: getUniversityFacultiesSchema },
    async (request: GetUniversityFacultiesRequest) => {
      const faculties = await UniversitiesService.getUniversityFaculties(request.params.universityId);
      return faculties;
    }
  );

  fastify.post('/', { schema: createUniversitySchema }, async (request: CreateUniversityRequest) => {
    await preprocessCreateUniversity(request);
    const university = await UniversitiesService.createOne(request.body);
    return university;
  });

  fastify.put('/:universityId', { schema: updateUniversitySchema }, async (request: UpdateUniversityRequest) => {
    await preprocessUpdateUniversity(request);
    const university = await UniversitiesService.updateOne(request.params.universityId, request.body);
    return university;
  });

  next();
};
