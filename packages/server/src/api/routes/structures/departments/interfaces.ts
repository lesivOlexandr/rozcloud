import { ICreateDepartmentPayload, IUpdateDepartmentPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type DepartmentParam = { departmentId: string };

export type CreateDepartmentRequest = AppRequest<ICreateDepartmentPayload>;
export type UpdateDepartmentRequest = AppRequest<IUpdateDepartmentPayload, DepartmentParam>;
export type GetDepartmentRequest = AppRequest<undefined, DepartmentParam>;
export type GetDepartmentAcademicGroupsRequest = AppRequest<undefined, DepartmentParam>;
export type GetDepartmentSubjectsRequest = AppRequest<undefined, DepartmentParam>;
export type GetDepartmentTeachersRequest = AppRequest<undefined, DepartmentParam>;
