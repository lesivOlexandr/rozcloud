import { FastifyInstance } from 'fastify';
import { DepartmentsService } from 'src/api/services/department';
import {
  CreateDepartmentRequest,
  GetDepartmentAcademicGroupsRequest,
  GetDepartmentRequest,
  GetDepartmentSubjectsRequest,
  GetDepartmentTeachersRequest,
  UpdateDepartmentRequest,
} from './interfaces';
import { preprocessCreateDepartment, preprocessUpdateDepartment } from './middlewares';
import {
  createDepartmentSchema,
  getDepartmentAcademicGroupsSchema,
  getDepartmentSchema,
  getDepartmentSubjectsSchema,
  getDepartmentTeachersSchema,
  updateDepartmentSchema,
} from 'src/api/routes/structures/departments/schema';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/:departmentId', { schema: getDepartmentSchema }, async (request: GetDepartmentRequest) => {
    const department = await DepartmentsService.getOne(request.params.departmentId);
    return department;
  });

  fastify.post('/', { schema: createDepartmentSchema }, async (request: CreateDepartmentRequest) => {
    await preprocessCreateDepartment(request);
    const department = await DepartmentsService.createOne(request.body);
    return department;
  });

  fastify.put('/:departmentId', { schema: updateDepartmentSchema }, async (request: UpdateDepartmentRequest) => {
    await preprocessUpdateDepartment(request);
    const department = await DepartmentsService.updateOne(request.params.departmentId, request.body);
    return department;
  });

  fastify.get(
    '/:departmentId/academic-groups',
    { schema: getDepartmentAcademicGroupsSchema },
    async (request: GetDepartmentAcademicGroupsRequest) => {
      const academicGroups = await DepartmentsService.getDepartmentAcademicGroups(request.params.departmentId);
      return academicGroups;
    }
  );

  fastify.get(
    '/:departmentId/subjects',
    { schema: getDepartmentSubjectsSchema },
    async (request: GetDepartmentSubjectsRequest) => {
      const subjects = await DepartmentsService.getDepartmentSubjects(request.params.departmentId);
      return subjects;
    }
  );

  fastify.get(
    '/:departmentId/teachers',
    { schema: getDepartmentTeachersSchema },
    async (request: GetDepartmentTeachersRequest) => {
      const teachers = await DepartmentsService.getDepartmentTeachers(request.params.departmentId);
      return teachers;
    }
  );

  next();
};
