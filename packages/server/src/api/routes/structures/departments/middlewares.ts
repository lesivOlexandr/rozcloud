import { StructureTypes } from 'src/common/enums';
import { preprocessCreateStructureFactory, createPreprocessUpdateStructure } from '../_middlewares';

const structureType = StructureTypes.Department;

export const preprocessCreateDepartment = preprocessCreateStructureFactory(structureType);

export const preprocessUpdateDepartment = createPreprocessUpdateStructure(structureType);
