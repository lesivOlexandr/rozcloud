import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import {
  structureAbbreviationChunk,
  structureEntitiesCountSchemaChunk,
  structureLogoSchemaChunk,
  structureNameSchemaChunk,
} from 'src/api/routes/structures/_schema-chunks';

export const departmentFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: structureNameSchemaChunk,
    abbreviation: structureAbbreviationChunk,
    logo: structureLogoSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
    facultyId: idSchemaChunk,
    academicGroupsQuantity: structureEntitiesCountSchemaChunk,
    teachersQuantity: structureEntitiesCountSchemaChunk,
    studentsQuantity: structureEntitiesCountSchemaChunk,
  },
});
