import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { createDepartmentBody } from 'src/api/routes/structures/departments/schema/create-department.schema';
import { departmentEntitySchema } from 'src/api/routes/structures/departments/schema/department-entity.schema';
import { updateDepartmentBody } from 'src/api/routes/structures/departments/schema/update-department.schema';
import { departmentParam } from './chunks/department-param.schema';
import { groupEntitySchema } from '../../groups/schema/group-entity.schema';
import { subjectResponseSuccessSchema } from 'src/api/routes/schedule/subjects/schema/subject-response-success.schema';
import { teacherResponseSuccess } from 'src/api/routes/users/teachers/schema/teacher-response-success.schema';

export const createDepartmentSchema = createRouteValidationSchema(
  {
    body: createDepartmentBody,
    tags: ['Structures'],
  },
  {
    200: departmentEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateDepartmentSchema = createRouteValidationSchema(
  {
    body: updateDepartmentBody,
    params: departmentParam,
    tags: ['Structures'],
  },
  {
    200: departmentEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getDepartmentSchema = createRouteValidationSchema(
  {
    params: departmentParam,
    tags: ['Structures'],
  },
  {
    200: departmentEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getDepartmentAcademicGroupsSchema = createRouteValidationSchema(
  {
    params: departmentParam,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: groupEntitySchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getDepartmentSubjectsSchema = createRouteValidationSchema(
  {
    params: departmentParam,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: subjectResponseSuccessSchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getDepartmentTeachersSchema = createRouteValidationSchema(
  {
    params: departmentParam,
    tags: ['Structures'],
  },
  {
    200: { type: 'array', items: teacherResponseSuccess },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
