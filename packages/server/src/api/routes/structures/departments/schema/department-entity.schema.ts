import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { departmentFullSchema } from './department-full.schema';

export const departmentEntitySchema = createObjectSchema(departmentFullSchema, {
  required: ['id', 'name', 'createdAt', 'updatedAt', 'academicGroupsQuantity', 'teachersQuantity', 'studentsQuantity'],
  defaultNull: 'onNotRequired',
  removeProperties: ['facultyId'],
});
