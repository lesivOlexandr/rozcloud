import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { departmentFullSchema } from './department-full.schema';

export const updateDepartmentBody = createObjectSchema(departmentFullSchema, {
  useProperties: ['name', 'logo', 'abbreviation'],
  nullable: 'onNotRequired',
});
