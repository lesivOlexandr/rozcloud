import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { departmentFullSchema } from './department-full.schema';

export const createDepartmentBody = createObjectSchema(departmentFullSchema, {
  required: ['facultyId', 'name'],
  useProperties: ['name', 'abbreviation', 'logo', 'facultyId'],
  defaultNull: 'onNotRequired',
});
