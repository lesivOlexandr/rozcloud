import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const departmentParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      departmentId: idSchemaChunk,
    },
  },
  {
    required: ['departmentId'],
  }
);
