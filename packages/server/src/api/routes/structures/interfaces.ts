import { AppRequest } from 'src/common/interfaces/server';

export type StructureParam = { structureId: string };

export type GetUnknownStructureRequest = AppRequest<undefined, StructureParam>;
export type GetStructureEmployeesRequest = AppRequest<undefined, StructureParam>;
