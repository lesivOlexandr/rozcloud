import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { idSchemaChunk } from '../../_schema-chunks';

export const structureParamSchemaChunk = createObjectSchema(
  {
    type: 'object',
    properties: {
      structureId: idSchemaChunk,
    },
  },
  {
    required: ['structureId'],
  }
);
