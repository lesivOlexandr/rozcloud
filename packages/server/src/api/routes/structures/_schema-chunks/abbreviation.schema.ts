import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { maxTextFieldLetters } from 'src/common/constants';

export const structureAbbreviationChunk = createScalarSchema({
  type: 'string',
  minLength: 0,
  maxLength: maxTextFieldLetters,
  example: 'KSU',
});
