import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { maxTextFieldLetters } from 'src/common/constants';

export const structureLogoSchemaChunk = createScalarSchema({
  type: 'string',
  // format: '',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  example: 'https://rozcloud.com/static/images/image.jpg',
});
