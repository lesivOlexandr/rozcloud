import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { StructureTypes } from 'src/common/enums';

export const structureTypeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  enum: Object.values(StructureTypes),
});
