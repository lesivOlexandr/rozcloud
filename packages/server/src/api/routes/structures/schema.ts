import { Schema } from 'src/common/interfaces/swagger';
import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { idSchemaChunk } from '../_schema-chunks';
import { employeeResponseSuccess } from '../users/employees/schema/employee-response-success.schema';
import { unknownTypeStructureChunk } from '../_schema-chunks/unknown-type-structure.schema';

export const getStructureItems: Schema = {
  type: 'object',
  properties: {
    structureId: idSchemaChunk,
  },
  additionalProperties: false,
};

export const getStructureEmployees = createRouteValidationSchema(
  {
    params: createObjectSchema({
      type: 'object',
      properties: {
        structureId: idSchemaChunk,
      },
    }),
    tags: ['Users'],
  },
  {
    200: { type: 'array', items: employeeResponseSuccess },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getUnknownStructureSchema = createRouteValidationSchema(
  {
    params: createObjectSchema({
      type: 'object',
      properties: {
        structureId: idSchemaChunk,
      },
    }),
    tags: ['Structures'],
  },
  {
    200: unknownTypeStructureChunk,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
