import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const unknownTypeStructureChunk = createScalarSchema({
  type: 'object',
  additionalProperties: true,
  description: 'This is any of structure types (university, faculty, department) with property "structureType"',
  example: {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    name: 'Some Weird Structure Name',
    abbreviation: 'KSU',
    logo: 'https://rozcloud.com/static/images/image.jpg',
    structureType: 'department',
    createdAt: 0,
    updatedAt: 0,
    academicGroupsQuantity: 0,
    teachersQuantity: 0,
    studentsQuantity: 0,
  },
});
