import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { emailSchemaChunk } from './email.schema';
import { loginSchemaChunk } from './login.schema';
import { phoneNumberSchemaChunk } from './phone.schema';

export const loginOrEmailOrPhoneChunk = createScalarSchema({
  type: 'string',
  anyOf: [loginSchemaChunk, emailSchemaChunk, phoneNumberSchemaChunk],
});
