import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const yearChunk = createScalarSchema({
  type: 'integer',
});
