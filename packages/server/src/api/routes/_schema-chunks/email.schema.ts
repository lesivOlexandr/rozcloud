import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const emailSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  format: 'email',
});
