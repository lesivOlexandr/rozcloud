import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { UserTypes } from 'src/common/enums';

export const userTypeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  enum: Object.values(UserTypes),
});
