import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const passwordSchemaChunk = createScalarSchema({
  type: 'string',
  pattern: '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
  description: `
    Password is a string value that should include lowercase and upper case letters, number, and should consist of at least 8 symbols.
    This field could be sent only by client, server never sends it back
  `,
  // writeOnly: true,
  minLength: 8,
  maxLength: maxTextFieldLetters,
  example: 'Ab12345bA',
});
