import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const phoneNumberSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 12,
  maxLength: 12,
  example: '380976859545',
});
