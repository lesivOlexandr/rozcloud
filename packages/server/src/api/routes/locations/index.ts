import { FastifyInstance } from 'fastify';
import corpusesRoutes from './corpuses';
import auditoryRoutes from './auditories';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(corpusesRoutes, { prefix: 'corpuses' });
  fastify.register(auditoryRoutes, { prefix: 'auditories' });

  next();
};
