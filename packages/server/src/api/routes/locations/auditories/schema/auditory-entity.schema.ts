import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { auditoryFullSchema } from './auditory-full.schema';

export const auditoryEntitySchema = createObjectSchema(auditoryFullSchema, {
  required: ['id', 'name', 'auditoryTypes', 'createdAt', 'updatedAt'],
  removeProperties: ['corpusId', 'universityId'],
  defaultNull: 'onNotRequired',
});
