import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { auditoryFullSchema } from './auditory-full.schema';

export const createAuditoryBody = createObjectSchema(auditoryFullSchema, {
  required: ['name'],
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  defaultNull: 'onNotRequired',
  mergeProperties: [['auditoryTypes', { default: [] }]],
});
