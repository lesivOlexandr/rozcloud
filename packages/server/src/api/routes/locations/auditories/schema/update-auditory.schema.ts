import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { auditoryFullSchema } from './auditory-full.schema';

export const updateAuditoryBody = createObjectSchema(auditoryFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  nullable: 'onNotRequired',
});

export const auditoryParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      auditoryId: idSchemaChunk,
    },
  },
  {
    required: ['auditoryId'],
  }
);
