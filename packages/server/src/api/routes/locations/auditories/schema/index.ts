import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { updateAuditoryBody } from './update-auditory.schema';
import { createAuditoryBody } from './create-auditory.schema';
import { auditoryParam } from './update-auditory.schema';
import { auditoryResponseSuccessSchema } from './auditory-response-success.schema';
import { auditoryFullSchemaResponse } from './auditory-full.schema';
import { maxTextFieldLetters } from 'src/common/constants';

export const createAuditorySchema = createRouteValidationSchema(
  {
    body: createAuditoryBody,
    tags: ['Locations'],
  },
  {
    200: auditoryResponseSuccessSchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateAuditorySchema = createRouteValidationSchema(
  {
    body: updateAuditoryBody,
    params: auditoryParam,
    tags: ['Locations'],
  },
  {
    200: auditoryResponseSuccessSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getAuditoriesSchema = createRouteValidationSchema(
  {
    querystring: createObjectSchema(
      {
        type: 'object',
        properties: {
          search: {
            type: 'string',
            maxLength: maxTextFieldLetters,
          },
        },
      },
      { nullable: 'onNotRequired' }
    ),
    tags: ['Locations'],
  },
  {
    200: {
      type: 'array',
      items: auditoryResponseSuccessSchema,
    },
    401: true,
    403: true,
    protectedJWT: true,
  }
);

export const getAuditorySchema = createRouteValidationSchema(
  {
    params: auditoryParam,
    tags: ['Locations'],
  },
  {
    200: auditoryFullSchemaResponse,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
