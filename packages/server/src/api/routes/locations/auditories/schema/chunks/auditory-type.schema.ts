import { maxTextFieldLetters } from 'src/common/constants';
import { AuditoryTypes } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const auditoryTypeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  default: AuditoryTypes.Lecture,
  nullable: true,
  enum: Object.values(AuditoryTypes),
});
