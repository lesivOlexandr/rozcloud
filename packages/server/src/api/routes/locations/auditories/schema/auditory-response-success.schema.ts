import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { auditoryFullSchema } from './auditory-full.schema';

export const auditoryResponseSuccessSchema = createObjectSchema(auditoryFullSchema, {
  required: ['id', 'name', 'createdAt', 'updatedAt'],
  removeProperties: ['universityId', 'corpusId', 'auditoryTypes'],
  defaultNull: 'onNotRequired',
});
