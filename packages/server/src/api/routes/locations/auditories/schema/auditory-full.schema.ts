import { universityEntitySchema } from 'src/api/routes/structures/universities/schema/university-entity.schema';
import { structureLogoSchemaChunk } from 'src/api/routes/structures/_schema-chunks';
import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { corpusEntitySchema } from '../../corpuses/schema/corpus-entity.schema';
import { auditoryNameSchemaChunk } from './chunks/auditory-name.schema';
import { auditoryTypeSchemaChunk } from './chunks/auditory-type.schema';

export const auditoryFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: auditoryNameSchemaChunk,
    universityId: idSchemaChunk,
    auditoryTypes: {
      type: 'array',
      items: auditoryTypeSchemaChunk,
      uniqueItems: true,
    },
    logo: structureLogoSchemaChunk,
    corpusId: idSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});

export const auditoryFullSchemaResponse = createObjectSchema(
  {
    type: 'object',
    properties: {
      id: idSchemaChunk,
      name: auditoryNameSchemaChunk,
      university: universityEntitySchema,
      auditoryTypes: {
        type: 'array',
        items: auditoryTypeSchemaChunk,
        uniqueItems: true,
      },
      corpus: corpusEntitySchema,
      createdAt: timePointSchema,
      updatedAt: timePointSchema,
    },
  },
  {
    required: ['id', 'name', 'auditoryTypes', 'createdAt', 'updatedAt'],
    defaultNull: 'onNotRequired',
  }
);
