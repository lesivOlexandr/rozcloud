import { FastifyInstance } from 'fastify';
import { AuditoriesService } from 'src/api/services/auditories';
import { CreateAuditoryRequest, GetAuditoriesRequest, GetAuditoryRequest, UpdateAuditoryRequest } from './interfaces';
import { createAuditorySchema, getAuditoriesSchema, getAuditorySchema, updateAuditorySchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/', { schema: getAuditoriesSchema }, async (request: GetAuditoriesRequest) => {
    const auditories = await AuditoriesService.getMany(request.query);
    return auditories;
  });

  fastify.get('/:auditoryId', { schema: getAuditorySchema }, async (request: GetAuditoryRequest) => {
    const auditory = await AuditoriesService.getOne(request.params.auditoryId);
    return auditory;
  });

  fastify.post('/', { schema: createAuditorySchema }, async (request: CreateAuditoryRequest) => {
    const createdAuditory = await AuditoriesService.createOne(request.body);
    return createdAuditory;
  });

  fastify.put('/:auditoryId', { schema: updateAuditorySchema }, async (request: UpdateAuditoryRequest) => {
    const updatedAuditory = await AuditoriesService.updateOne(request.params.auditoryId, request.body);
    return updatedAuditory;
  });

  next();
};
