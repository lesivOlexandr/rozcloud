import { FastifyRequest } from 'fastify';
import { IAuditoryFilter, ICreateAuditoryPayload, IUpdateAuditoryPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type AuditoryParam = { auditoryId: string };

export type CreateAuditoryRequest = AppRequest<ICreateAuditoryPayload>;
export type UpdateAuditoryRequest = AppRequest<IUpdateAuditoryPayload, AuditoryParam>;
export type GetAuditoryRequest = AppRequest<undefined, AuditoryParam>;
export type GetAuditoriesRequest = FastifyRequest<{ Querystring: IAuditoryFilter }>;
