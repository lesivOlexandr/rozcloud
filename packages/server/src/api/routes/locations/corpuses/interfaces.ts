import { ICreateCorpusPayload, IUpdateCorpusPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type CorpusParam = { corpusId: string };

export type CreateCorpusRequest = AppRequest<ICreateCorpusPayload>;
export type UpdateCorpusRequest = AppRequest<IUpdateCorpusPayload, CorpusParam>;
export type GetCorpusRequest = AppRequest<undefined, CorpusParam>;
export type GetCorpusAuditoriesRequest = AppRequest<undefined, CorpusParam>;
