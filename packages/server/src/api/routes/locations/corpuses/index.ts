import { FastifyInstance } from 'fastify';
import { CorpusesService } from 'src/api/services/corpuses';
import { CreateCorpusRequest, GetCorpusRequest, UpdateCorpusRequest } from './interfaces';
import { createCorpusSchema, getCorpusAuditoriesSchema, getCorpusSchema, updateCorpusSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/:corpusId', { schema: getCorpusSchema }, async (request: GetCorpusRequest) => {
    const corpus = await CorpusesService.getOne(request.params.corpusId);
    return corpus;
  });

  fastify.post('/', { schema: createCorpusSchema }, async (request: CreateCorpusRequest) => {
    const createdCorpus = await CorpusesService.createOne(request.body);
    return createdCorpus;
  });

  fastify.put('/:corpusId', { schema: updateCorpusSchema }, async (request: UpdateCorpusRequest) => {
    const updatedCorpus = await CorpusesService.updateOne(request.params.corpusId, request.body);
    return updatedCorpus;
  });

  fastify.get('/:corpusId/auditories', { schema: getCorpusAuditoriesSchema }, async (request: GetCorpusRequest) => {
    const auditories = await CorpusesService.getCorpusAuditories(request.params.corpusId);
    return auditories;
  });

  next();
};
