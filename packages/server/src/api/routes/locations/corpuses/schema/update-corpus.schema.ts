import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { corpusFullSchema } from './corpus-full.schema';

export const updateCorpusBody = createObjectSchema(corpusFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  nullable: 'onNotRequired',
});
