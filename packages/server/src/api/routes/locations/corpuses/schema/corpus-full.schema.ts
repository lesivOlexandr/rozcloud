import { structureLogoSchemaChunk } from 'src/api/routes/structures/_schema-chunks/structure-logo.schema';
import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { corpusAddressSchemaChunk } from './chunks/corpus-address.schema';
import { corpusNameSchemaChunk } from './chunks/corpus-name.schema';

export const corpusFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: corpusNameSchemaChunk,
    address: corpusAddressSchemaChunk,
    logo: structureLogoSchemaChunk,
    universityId: idSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
