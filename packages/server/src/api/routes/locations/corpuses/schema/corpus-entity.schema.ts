import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { corpusFullSchema } from './corpus-full.schema';

export const corpusEntitySchema = createObjectSchema(corpusFullSchema, {
  required: ['name', 'universityId', 'createdAt', 'updatedAt', 'id'],
  removeProperties: ['universityId'],
  defaultNull: 'onNotRequired',
});
