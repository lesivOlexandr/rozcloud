import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { corpusFullSchema } from './corpus-full.schema';

export const createCorpusBody = createObjectSchema(corpusFullSchema, {
  required: ['name', 'universityId'],
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  defaultNull: 'onNotRequired',
});
