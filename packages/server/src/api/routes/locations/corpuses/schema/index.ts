import { corpusParam } from 'src/api/routes/locations/corpuses/schema/chunks/corpus-param.chunk';
import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { auditoryEntitySchema } from '../../auditories/schema/auditory-entity.schema';
import { corpusEntitySchema } from './corpus-entity.schema';
import { createCorpusBody } from './create-corpus.schema';
import { updateCorpusBody } from './update-corpus.schema';

export const createCorpusSchema = createRouteValidationSchema(
  {
    body: createCorpusBody,
    tags: ['Locations'],
  },
  {
    200: corpusEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateCorpusSchema = createRouteValidationSchema(
  {
    body: updateCorpusBody,
    params: corpusParam,
    tags: ['Locations'],
  },
  {
    200: corpusEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getCorpusSchema = createRouteValidationSchema(
  {
    params: corpusParam,
    tags: ['Locations'],
  },
  {
    200: corpusEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getCorpusAuditoriesSchema = createRouteValidationSchema(
  {
    params: corpusParam,
    tags: ['Locations'],
  },
  {
    200: { type: 'array', items: auditoryEntitySchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
