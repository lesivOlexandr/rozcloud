import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { idSchemaChunk } from 'src/api/routes/_schema-chunks/id.schema';

export const corpusParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      corpusId: idSchemaChunk,
    },
  },
  {
    required: ['corpusId'],
  }
);
