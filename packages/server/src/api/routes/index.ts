import { FastifyInstance } from 'fastify';
import authRoutes from './auth';
import usersRoutes from './users';
import structuresRoutes from './structures';
import locationRoutes from './locations';
import scheduleRoutes from './schedule';
import timeRoutes from './time';
import fileRoutes from './files';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(authRoutes, { prefix: 'auth' });
  fastify.register(usersRoutes, { prefix: 'users' });
  fastify.register(structuresRoutes, { prefix: 'structures' });
  fastify.register(locationRoutes, { prefix: 'locations' });
  fastify.register(scheduleRoutes, { prefix: 'schedule' });
  fastify.register(timeRoutes, { prefix: 'time' });
  fastify.register(fileRoutes, { prefix: 'file' });

  next();
};
