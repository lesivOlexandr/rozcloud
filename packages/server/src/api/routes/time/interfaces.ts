import { AppRequest } from 'src/common/interfaces/server';

export type GetTimeDataRequest = AppRequest<undefined, { studyYear: number }>;
export type GetWeekDataRequest = AppRequest<undefined, { weekNumber: number }>;
