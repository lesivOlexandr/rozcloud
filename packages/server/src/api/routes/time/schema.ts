import {
  createScalarSchema,
  createRouteValidationSchema,
  createArrayOfSchema,
} from 'src/common/helpers/swagger.helpers';
import { maxTextFieldLetters } from 'src/common/constants';
import { idSchemaChunk } from '../_schema-chunks';
import { weekNumSchemaChunk } from '../schedule/_schema-chunks/week-num.schema';

export const dateYearDataObject = createScalarSchema({
  type: 'object',
  nullable: false,
  properties: {
    id: idSchemaChunk,
    studyYear: { type: 'integer', minimum: 1990, maximum: 2100, example: 2018 },
    months: {
      type: 'array',
      items: {
        type: 'object',
        nullable: false,
        properties: {
          id: idSchemaChunk,
          monthName: {
            type: 'string',
            minLength: 1,
            maxLength: maxTextFieldLetters,
            example: 'September',
          },
          monthNumber: {
            type: 'integer',
            minimum: 1,
            maximum: 12,
            example: 12,
          },
          dates: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                id: idSchemaChunk,
                date: { type: 'integer', minimum: 1, maximum: 31, example: 16 },
                dayName: { type: 'string', minLength: 1, maxLength: 7, example: '1' },
                weekNumber: { type: 'integer', minimum: 1, example: 3 },
              },
            },
          },
        },
      },
    },
  },
});

export const weekDataObject = createScalarSchema({
  type: 'object',
  properties: {
    weekNumber: weekNumSchemaChunk,
    dates: createArrayOfSchema({
      type: 'object',
      properties: {
        dayName: { type: 'string', minLength: 1, maxLength: 7, example: '1' },
        formattedDate: { type: 'string', minLength: 1, maxLength: 12, example: '2020-02-21' },
      },
    }),
  },
});

export const dateYearDataSchema = createRouteValidationSchema(
  {
    params: {
      type: 'object',
      properties: { studyYear: { type: 'integer', minimum: 1990, maximum: 2100, example: 2018 } },
    },
    tags: ['Time'],
  },
  { 200: dateYearDataObject, 403: true }
);

export const weekDataSchema = createRouteValidationSchema(
  {
    params: {
      type: 'object',
      properties: {
        weekNumber: weekNumSchemaChunk,
      },
    },
    tags: ['Time'],
  },
  {
    200: weekDataObject,
    400: true,
  }
);
