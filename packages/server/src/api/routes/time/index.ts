import { FastifyInstance } from 'fastify';
import { TimeOperationsRepository } from 'src/data/repositories/time';
import { GetTimeDataRequest, GetWeekDataRequest } from './interfaces';
import { dateYearDataSchema, weekDataSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/:studyYear', { schema: dateYearDataSchema }, async (request: GetTimeDataRequest) => {
    const dateHierarchy = await TimeOperationsRepository.getHierarchy(request.params.studyYear);
    return dateHierarchy;
  });

  fastify.get('/weeks-data/:weekNumber', { schema: weekDataSchema }, async (request: GetWeekDataRequest) => {
    const weekData = await TimeOperationsRepository.getWeekPayload(request.params.weekNumber);
    return weekData;
  });

  next();
};
