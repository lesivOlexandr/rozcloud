import { UserTypes } from '@rozcloud/shared/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { employeeWithUserTypeResponseSuccess } from '../../users/employees/schema/employee-response-success.schema';
import { studentWithUserTypeResponseSuccess } from '../../users/students/schema/student-response-success.schema';
import { teacherWithUserTypeResponseSuccess } from '../../users/teachers/schema/teacher-response-success.schema';

export const unknownUserSchema = createScalarSchema({
  type: 'object',
  additionalProperties: true,
  description: `
    For some reason it doesn't show details, but actually it is just any of user types(teacher, student, employee) with the property "userType" that is equal to one of the following values ${Object.values(
      UserTypes
    )}
  `,
  example: {
    userType: 'student',
    birthday: 'Fri Jan 08 2021 22:28:41 GMT+0000 (Coordinated Universal Time)',
    updatedAt: 1610144921159,
    avatar: 'http://avatar.com',
    middleName: 'Ivanovich',
    email: 'student@email.com',
    lastName: 'Lesiv',
    firstName: 'Alexandr',
    id: 'f0723ab8-050c-4ce5-a906-2f7268213272',
    createdAt: 1610144921159,
    address: 'student address',
    login: 'student',
    phone: '3807658493434',
  },
  anyOf: [teacherWithUserTypeResponseSuccess, studentWithUserTypeResponseSuccess, employeeWithUserTypeResponseSuccess],
});
