import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { jwtChunk } from '../../_schema-chunks/jwt.schema';
import { loginOrEmailOrPhoneChunk } from '../../_schema-chunks/login-or-email-or-phone.schema';
import { passwordSchemaChunk } from '../../_schema-chunks/password.schema';
import { unknownUserSchema } from './auth-response-success.schema';

export const loginBody = createObjectSchema(
  {
    type: 'object',
    properties: {
      loginOrEmailOrPhone: loginOrEmailOrPhoneChunk,
      password: passwordSchemaChunk,
    },
  },
  {
    required: ['loginOrEmailOrPhone', 'password'],
  }
);

export const loginSchema = createRouteValidationSchema(
  {
    body: loginBody,
    tags: ['Authentication'],
  },
  {
    200: { type: 'object', properties: { jwtToken: jwtChunk, user: unknownUserSchema } },
    400: true,
    404: true,
  }
);

export const userDataSchema = createRouteValidationSchema(
  {
    body: {
      type: 'object',
      tags: ['Authentication'],
      properties: {
        jwtToken: jwtChunk,
      },
    },
  },
  {
    '200': {
      type: 'object',
      properties: {
        user: unknownUserSchema,
      },
    },
    400: true,
    401: true,
    404: true,
    protectedJWT: true,
  }
);
