import { FastifyInstance } from 'fastify';
import { AuthService } from 'src/api/services/auth';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import {
  ILoginUserResponse,
  JSONWebTokenDecodedPayload,
  UnknownTypeUser,
  WithPasswordHash,
} from 'src/common/interfaces';
import { LoginRequest, UserDataRequest } from './interfaces';
import { loginSchema, userDataSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.post('/login', { schema: loginSchema }, async (request: LoginRequest, reply) => {
    const user = await AuthService.login(request.body);

    const jwtPayload = { userId: user.id };
    const jwtToken = await reply.jwtSign(jwtPayload);

    const serverResponse: ILoginUserResponse = { user, jwtToken };
    return serverResponse;
  });

  fastify.post('/user-data', { schema: userDataSchema }, async (request: UserDataRequest) => {
    const jwtVerifyPayload: JSONWebTokenDecodedPayload | null = await fastify.jwt.decode(request.body.jwtToken);
    if (!jwtVerifyPayload) {
      return triggerServerError('No user available by token', 400);
    }
    try {
      const user = await AuthService.getUserById(jwtVerifyPayload.userId);
      return user as WithPasswordHash<UnknownTypeUser>;
    } catch {
      return triggerServerError('No user available by token', 400);
    }
  });

  next();
};
