import { IUserLoginPayload, IUserDataPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type LoginRequest = AppRequest<IUserLoginPayload>;
export type UserDataRequest = AppRequest<IUserDataPayload>;
