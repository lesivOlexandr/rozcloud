import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { teacherFullSchema } from './teacher-full.schema';

export const teacherResponseSuccess = createObjectSchema(teacherFullSchema, {
  required: ['createdAt', 'updatedAt', 'id', 'login'],
  removeProperties: ['password', 'departmentId', 'userType'],
  defaultNull: 'onNotRequired',
});

export const teacherWithUserTypeResponseSuccess = createObjectSchema(teacherFullSchema, {
  required: ['createdAt', 'updatedAt', 'id', 'login'],
  removeProperties: ['password', 'departmentId'],
  defaultNull: 'onNotRequired',
});
