import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { teacherFullSchema } from './teacher-full.schema';

export const updateTeacherBody = createObjectSchema(teacherFullSchema, {
  removeProperties: ['updatedAt', 'createdAt', 'id', 'userType'],
  nullable: 'onNotRequired',
});
