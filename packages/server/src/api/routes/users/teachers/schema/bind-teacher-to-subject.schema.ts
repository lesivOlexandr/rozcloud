import { idSchemaChunk } from 'src/api/routes/_schema-chunks/id.schema';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const bindTeacherToSubjectBody = createObjectSchema(
  {
    type: 'object',
    properties: {
      teacherId: idSchemaChunk,
      subjectIds: {
        type: 'array',
        items: idSchemaChunk,
      },
    },
  },
  {
    required: ['teacherId', 'subjectIds'],
  }
);
