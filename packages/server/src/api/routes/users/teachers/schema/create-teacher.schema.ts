import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { teacherFullSchema } from './teacher-full.schema';

export const createTeacherBody = createObjectSchema(teacherFullSchema, {
  required: ['login', 'password', 'departmentId'],
  removeProperties: ['createdAt', 'updatedAt', 'id', 'userType'],
  defaultNull: 'onNotRequired',
});
