import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { teacherFullSchema } from './teacher-full.schema';

export const teacherEntitySchema = createObjectSchema(teacherFullSchema, {
  required: ['createdAt', 'updatedAt', 'login', 'id'],
  removeProperties: ['userType', 'departmentId', 'subjectIds'],
  defaultNull: 'onNotRequired',
});
