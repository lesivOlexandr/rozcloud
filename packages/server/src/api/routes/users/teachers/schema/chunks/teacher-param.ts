import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const teacherParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      teacherId: idSchemaChunk,
    },
  },
  {
    required: ['teacherId'],
  }
);
