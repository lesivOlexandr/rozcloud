import { maxTextFieldLetters } from 'src/common/constants';
import { ScienceDegrees } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const scienceDegreeChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  enum: Object.values(ScienceDegrees),
});
