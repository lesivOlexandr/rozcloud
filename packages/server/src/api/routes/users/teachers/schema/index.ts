import { subjectEntitySchema } from 'src/api/routes/schedule/subjects/schema/subject-entity.schema';
import { maxTextFieldLetters } from 'src/common/constants';
import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { teacherParam } from './chunks/teacher-param';
import { createTeacherBody } from './create-teacher.schema';
import { teacherResponseSuccess } from './teacher-response-success.schema';
import { updateTeacherBody } from './update-teacher.schema';

export const createTeacherSchema = createRouteValidationSchema(
  {
    body: createTeacherBody,
    tags: ['Users'],
  },
  {
    200: teacherResponseSuccess,
    400: true,
    401: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateTeacherSchema = createRouteValidationSchema(
  {
    body: updateTeacherBody,
    tags: ['Users'],
    params: teacherParam,
  },
  {
    200: teacherResponseSuccess,
    400: true,
    401: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getTeacherSchema = createRouteValidationSchema(
  {
    params: teacherParam,
    tags: ['Users'],
  },
  {
    200: teacherResponseSuccess,
    400: true,
    401: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getTeacherSubjectSchema = createRouteValidationSchema(
  {
    params: teacherParam,
    tags: ['Users'],
  },
  {
    200: { type: 'array', items: subjectEntitySchema },
    400: true,
    401: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getTeachersSchema = createRouteValidationSchema(
  {
    params: createObjectSchema(
      {
        type: 'object',
        properties: {
          search: {
            type: 'string',
            maxLength: maxTextFieldLetters,
          },
        },
      },
      {
        nullable: 'onNotRequired',
      }
    ),
    tags: ['Users'],
  },
  {
    200: {
      type: 'array',
      items: teacherResponseSuccess,
    },
    400: true,
    401: true,
    403: true,
    protectedJWT: true,
  }
);
