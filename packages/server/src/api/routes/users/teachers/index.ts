import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { TeachersService } from 'src/api/services/teachers';
import {
  CreateTeacherRequest,
  GetTeacherRequest,
  GetTeachersRequest,
  GetTeacherSubjectsRequest,
  UpdateTeacherRequest,
} from './interface';
import {
  createTeacherSchema,
  getTeacherSchema,
  getTeachersSchema,
  getTeacherSubjectSchema,
  updateTeacherSchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/', { schema: getTeachersSchema }, async (request: GetTeachersRequest) => {
    const teachers = await TeachersService.getMany(request.query);
    return teachers;
  });

  fastify.get('/:teacherId', { schema: getTeacherSchema }, async (request: GetTeacherRequest) => {
    const teacher = await TeachersService.getOne(request.params.teacherId);
    return teacher;
  });

  fastify.post('/', { schema: createTeacherSchema }, async (request: CreateTeacherRequest) => {
    const teacher = await TeachersService.createOne(request.body);
    return teacher;
  });

  fastify.put('/:teacherId', { schema: updateTeacherSchema }, async (request: UpdateTeacherRequest) => {
    const teacher = await TeachersService.updateOne(request.params.teacherId, request.body);
    return teacher;
  });

  fastify.get(
    '/:teacherId/subjects',
    { schema: getTeacherSubjectSchema },
    async (request: GetTeacherSubjectsRequest) => {
      const subjects = await TeachersService.getTeacherSubjects(request.params.teacherId);
      return subjects;
    }
  );

  next();
};
