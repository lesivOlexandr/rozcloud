import { FastifyRequest } from 'fastify';
import { ICreateTeacherPayload, ITeacherFilter, IUpdateTeacherPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type TeacherParam = { teacherId: string };

export type CreateTeacherRequest = AppRequest<ICreateTeacherPayload>;
export type UpdateTeacherRequest = AppRequest<IUpdateTeacherPayload, TeacherParam>;
export type GetTeacherRequest = AppRequest<undefined, TeacherParam>;
export type GetTeacherSubjectsRequest = AppRequest<undefined, TeacherParam>;
export type GetTeachersRequest = FastifyRequest<{ Querystring: ITeacherFilter }>;
