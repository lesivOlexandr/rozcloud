import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { employeeFullSchema } from './employee-full.schema';

export const employeeEntitySchema = createObjectSchema(employeeFullSchema, {
  required: ['login', 'createdAt', 'updatedAt', 'id', 'userType'],
});
