import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { employeeFullSchema } from './employee-full.schema';

export const employeeResponseSuccess = createObjectSchema(employeeFullSchema, {
  required: ['createdAt', 'updatedAt', 'id', 'login', 'userType'],
  removeProperties: ['password', 'structureId'],
  defaultNull: 'onNotRequired',
});

export const employeeWithUserTypeResponseSuccess = createObjectSchema(employeeFullSchema, {
  required: ['createdAt', 'updatedAt', 'id', 'login'],
  removeProperties: ['password', 'structureId'],
  defaultNull: 'onNotRequired',
});
