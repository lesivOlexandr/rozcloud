import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const employeeParam = createObjectSchema({
  type: 'object',
  properties: {
    employeeId: idSchemaChunk,
  },
});
