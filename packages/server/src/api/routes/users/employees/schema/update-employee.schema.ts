import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { employeeFullSchema } from './employee-full.schema';

export const updateEmployeeBody = createObjectSchema(employeeFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id', 'userType'],
  nullable: 'onNotRequired',
});
