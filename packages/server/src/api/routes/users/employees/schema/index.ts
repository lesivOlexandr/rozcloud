import { unknownTypeStructureChunk } from 'src/api/routes/_schema-chunks/unknown-type-structure.schema';
import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { employeeParam } from './chunks/employee-param.schema';
import { createEmployeeBody } from './create-employee.schema';
import { employeeResponseSuccess } from './employee-response-success.schema';
import { updateEmployeeBody } from './update-employee.schema';

export const createEmployeeSchema = createRouteValidationSchema(
  {
    body: createEmployeeBody,
    tags: ['Users'],
  },
  {
    200: employeeResponseSuccess,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateEmployeeSchema = createRouteValidationSchema(
  {
    body: updateEmployeeBody,
    params: employeeParam,
    tags: ['Users'],
  },
  {
    200: employeeResponseSuccess,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getEmployeeSchema = createRouteValidationSchema(
  {
    params: employeeParam,
    tags: ['Users'],
  },
  {
    200: employeeResponseSuccess,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getEmployeeStructureSchema = createRouteValidationSchema(
  {
    params: employeeParam,
    tags: ['Users'],
  },
  {
    200: unknownTypeStructureChunk,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
