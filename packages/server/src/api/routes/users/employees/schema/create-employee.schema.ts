import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { employeeFullSchema } from './employee-full.schema';

export const createEmployeeBody = createObjectSchema(employeeFullSchema, {
  required: ['login', 'password', 'structureId', 'employeeType'],
  removeProperties: ['createdAt', 'updatedAt', 'id', 'userType'],
  defaultNull: 'onNotRequired',
});
