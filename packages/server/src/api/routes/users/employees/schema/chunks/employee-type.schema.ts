import { maxTextFieldLetters } from 'src/common/constants';
import { EmployeeTypes } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const employeeTypeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  enum: Object.values(EmployeeTypes),
});
