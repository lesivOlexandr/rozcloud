import { ICreateEmployeePayload, IUpdateEmployeePayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type EmployeeParam = { employeeId: string };

export type CreateEmployeeRequest = AppRequest<ICreateEmployeePayload>;
export type UpdateEmployeeRequest = AppRequest<IUpdateEmployeePayload, EmployeeParam>;
export type GetEmployeeStructureRequest = AppRequest<undefined, EmployeeParam>;
export type GetEmployeeRequest = AppRequest<undefined, EmployeeParam>;
