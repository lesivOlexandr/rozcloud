import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { EmployeesService } from 'src/api/services/employees';
import {
  CreateEmployeeRequest,
  GetEmployeeRequest,
  GetEmployeeStructureRequest,
  UpdateEmployeeRequest,
} from './interfaces';
import { createEmployeeSchema, getEmployeeSchema, getEmployeeStructureSchema, updateEmployeeSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/:employeeId', { schema: getEmployeeSchema }, async (request: GetEmployeeRequest) => {
    const employee = await EmployeesService.getOne(request.params.employeeId);
    return employee;
  });

  fastify.post('/', { schema: createEmployeeSchema }, async (request: CreateEmployeeRequest) => {
    const employee = await EmployeesService.createOne(request.body);
    return employee;
  });

  fastify.put('/:employeeId', { schema: updateEmployeeSchema }, async (request: UpdateEmployeeRequest) => {
    const employee = await EmployeesService.updateOne(request.params.employeeId, request.body);
    return employee;
  });

  fastify.get(
    '/:employeeId/structure',
    { schema: getEmployeeStructureSchema },
    async (request: GetEmployeeStructureRequest) => {
      const structure = await EmployeesService.getEmployeeStructure(request.params.employeeId);
      return structure;
    }
  );

  next();
};
