import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { unknownUserSchema } from '../auth/schema/auth-response-success.schema';
import { scheduleFilledFullSchema } from '../schedule/schedule-week/schema/schedule-week-filled-full-data';
import { weekNumSchemaChunk } from '../schedule/_schema-chunks/week-num.schema';
import { telegramEncodedIdentifier } from './_schema-chunks/telegram-encoded-identifier.schema';
import { telegramEncodedPayload } from './_schema-chunks/telegram-encoded-payload.schema';

export const bindTelegramPayload = createObjectSchema({
  type: 'object',
  properties: {
    id: telegramEncodedIdentifier,
    payload: telegramEncodedPayload,
  },
});

export const bindTelegramSchema = createRouteValidationSchema(
  {
    body: bindTelegramPayload,
    tags: ['Users'],
  },
  {
    200: bindTelegramPayload,
    400: true,
    401: true,
    protectedJWT: true,
  }
);

export const getUserByTelegramIdSchema = createRouteValidationSchema(
  {
    params: { type: 'object', properties: { telegramUserId: telegramEncodedIdentifier } },
    tags: ['Users', 'Telegram'],
  },
  {
    200: unknownUserSchema,
    400: true,
    404: true,
  }
);

export const getScheduleWeekByTelegramIdSchema = createRouteValidationSchema(
  {
    params: {
      type: 'object',
      properties: {
        telegramUserId: telegramEncodedIdentifier,
        weekNumber: weekNumSchemaChunk,
      },
    },
    tags: ['Schedule', 'Telegram'],
  },
  {
    200: scheduleFilledFullSchema,
    400: true,
    403: true,
    404: true,
  }
);
