import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const telegramEncodedPayload = createScalarSchema({
  type: 'object',
  properties: {
    username: {
      type: 'string',
      maxLength: maxTextFieldLetters,
      nullable: true,
    },
  },
});
