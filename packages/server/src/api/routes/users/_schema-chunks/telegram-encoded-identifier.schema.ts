import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const telegramEncodedIdentifier = createScalarSchema({
  type: 'string',
  maxLength: maxTextFieldLetters,
  minLength: 0,
});
