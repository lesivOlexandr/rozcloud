import { maxTextFieldLetters } from 'src/common/constants';
import { Genders } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const userGenderChunk = createScalarSchema({
  type: 'string',
  minLength: 0,
  maxLength: maxTextFieldLetters,
  enum: [...Object.values(Genders), null],
});
