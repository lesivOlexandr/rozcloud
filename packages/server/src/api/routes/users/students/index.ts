import { FastifyInstance } from 'fastify';
import { StudentsService } from 'src/api/services/students';
import { CreateStudentRequest, GetStudentRequest, UpdateStudentRequest } from './interfaces';
import { createStudentSchema, getStudentSchema, updateStudentSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/:studentId', { schema: getStudentSchema }, async (request: GetStudentRequest) => {
    const student = await StudentsService.getOne(request.params.studentId);
    return student;
  });

  fastify.post('/', { schema: createStudentSchema }, async (request: CreateStudentRequest) => {
    const student = await StudentsService.createOne(request.body);
    return student;
  });

  fastify.put('/:studentId', { schema: updateStudentSchema }, async (request: UpdateStudentRequest) => {
    const student = await StudentsService.updateOne(request.params.studentId, request.body);
    return student;
  });

  next();
};
