import { maxTextFieldLetters } from 'src/common/constants';
import { StudyPaymentForms } from 'src/common/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const paymentFormChunk = createScalarSchema({
  type: 'string',
  minLength: 0,
  maxLength: maxTextFieldLetters,
  enum: Object.values(StudyPaymentForms),
});
