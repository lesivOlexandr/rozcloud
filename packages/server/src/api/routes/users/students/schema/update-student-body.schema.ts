import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { studentFullSchema } from './student-full.schema';

export const updateStudentBody = createObjectSchema(studentFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id', 'userType'],
  nullable: 'onNotRequired',
});
