import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { loginSchemaChunk } from 'src/api/routes/_schema-chunks/login.schema';
import { passwordSchemaChunk } from 'src/api/routes/_schema-chunks/password.schema';
import { userNameSchemaChunk } from 'src/api/routes/users/_schema-chunks/user-name.schema';
import { birthDaySchemaChunk } from 'src/api/routes/users/_schema-chunks/birthday.schema';
import { avatarSchemaChunk } from 'src/api/routes/users/_schema-chunks/avatar.schema';
import { phoneNumberSchemaChunk } from 'src/api/routes/_schema-chunks/phone.schema';
import { addressSchemaChunk } from 'src/api/routes/users/_schema-chunks/address.schema';
import { emailSchemaChunk } from 'src/api/routes/_schema-chunks/email.schema';
import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { studTicketSeriaSchemaChunk } from 'src/api/routes/users/students/schema/chunks/stud-ticket-seria.schema';
import { studTicketNumberSchemaChunk } from 'src/api/routes/users/students/schema/chunks/stud-ticket-number.schema';
import { userTypeSchemaChunk } from 'src/api/routes/_schema-chunks/user-type.schema';
import { userGenderChunk } from 'src/api/routes/users/_schema-chunks/gender-chunk.schema';
import { paymentFormChunk } from './chunks/payment-form.schema';

export const studentFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    login: loginSchemaChunk,
    password: passwordSchemaChunk,
    firstName: userNameSchemaChunk,
    middleName: userNameSchemaChunk,
    lastName: userNameSchemaChunk,
    gender: userGenderChunk,
    paymentForm: paymentFormChunk,
    birthday: birthDaySchemaChunk,
    avatar: avatarSchemaChunk,
    phone: phoneNumberSchemaChunk,
    address: addressSchemaChunk,
    email: emailSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
    studTicketSeria: studTicketSeriaSchemaChunk,
    studTicketNumber: studTicketNumberSchemaChunk,
    userType: userTypeSchemaChunk,
  },
});
