import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const studTicketSeriaSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 2,
  maxLength: 2,
  example: 'XE',
});
