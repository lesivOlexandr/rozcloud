import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { studentParam } from './chunks/student-param.schema';
import { createStudentBody } from './create-student-body.schema';
import { studentResponseSuccess } from './student-response-success.schema';
import { updateStudentBody } from './update-student-body.schema';

export const createStudentSchema = createRouteValidationSchema(
  {
    body: createStudentBody,
    tags: ['Users'],
  },
  {
    200: studentResponseSuccess,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateStudentSchema = createRouteValidationSchema(
  {
    body: updateStudentBody,
    params: studentParam,
    tags: ['Users'],
  },
  {
    200: studentResponseSuccess,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getStudentSchema = createRouteValidationSchema(
  {
    params: studentParam,
    tags: ['Users'],
  },
  {
    200: studentResponseSuccess,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
