import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { studentFullSchema } from './student-full.schema';

export const createStudentBody = createObjectSchema(studentFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id', 'userType'],
  required: ['login', 'password', 'academicGroupId'],
  defaultNull: 'onNotRequired',
});
