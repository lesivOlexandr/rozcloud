import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const studentParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      studentId: idSchemaChunk,
    },
  },
  {
    required: ['studentId'],
  }
);
