import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { studentFullSchema } from './student-full.schema';

export const studentResponseSuccess = createObjectSchema(studentFullSchema, {
  removeProperties: ['password', 'userType'],
  defaultNull: 'onNotRequired',
  required: ['login', 'createdAt', 'updatedAt'],
});

export const studentWithUserTypeResponseSuccess = createObjectSchema(studentFullSchema, {
  removeProperties: ['password'],
  defaultNull: 'onNotRequired',
  required: ['login', 'createdAt', 'updatedAt'],
});
