import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { studentFullSchema } from './student-full.schema';

export const studentEntitySchema = createObjectSchema(studentFullSchema, {
  defaultNull: 'onNotRequired',
  required: ['login', 'createdAt', 'updatedAt', 'id'],
  removeProperties: ['userType'],
});
