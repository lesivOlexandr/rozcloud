import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const studTicketNumberSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 4,
  maxLength: 4,
});
