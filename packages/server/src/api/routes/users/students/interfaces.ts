import { ICreateStudentPayload, IUpdateStudentPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type StudentParam = { studentId: string };

export type CreateStudentRequest = AppRequest<ICreateStudentPayload>;
export type UpdateStudentRequest = AppRequest<IUpdateStudentPayload, StudentParam>;
export type GetStudentRequest = AppRequest<undefined, StudentParam>;
