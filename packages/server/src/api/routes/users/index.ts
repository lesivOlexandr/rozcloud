import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { ScheduleWeekService } from 'src/api/services/schedule-week';
import { TelegramService } from 'src/api/services/telegram';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import employeeRoutes from './employees';
import { IBindUserToTelegramRequest, IGetScheduleByTelegramIdRequest, IGetUserByTelegramIdRequest } from './interfaces';
import { bindTelegramSchema, getScheduleWeekByTelegramIdSchema, getUserByTelegramIdSchema } from './schema';
import studentRoutes from './students';
import teacherRoutes from './teachers';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(employeeRoutes, { prefix: 'employees' });
  fastify.register(studentRoutes, { prefix: 'students' });
  fastify.register(teacherRoutes, { prefix: 'teachers' });

  fastify.post(
    '/bind-telegram',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    { schema: bindTelegramSchema, preHandler: jwtInjectUser as any },
    async (request: IBindUserToTelegramRequest) => {
      const { id: telegramUserId, payload } = request.body;
      const userId = request.appUser?.id;
      if (!userId) {
        return triggerServerError('User is not found', 400);
      }
      const result = await TelegramService.bindTelegramData(userId, telegramUserId, payload);
      return result;
    }
  );

  fastify.get(
    '/telegram/:telegramUserId',
    { schema: getUserByTelegramIdSchema },
    async (request: IGetUserByTelegramIdRequest) => {
      const user = await TelegramService.getUserByTelegramId(request.params.telegramUserId);
      return user;
    }
  );

  fastify.get(
    '/telegram/:telegramUserId/schedule/:weekNumber',
    { schema: getScheduleWeekByTelegramIdSchema },
    async (request: IGetScheduleByTelegramIdRequest) => {
      const user = await TelegramService.getUserByTelegramId(request.params.telegramUserId);
      const { id: userId, userType } = user;
      const schedule = await ScheduleWeekService.getOne(userId, request.params.weekNumber, userType);
      return schedule;
    }
  );

  next();
};
