import { IBindUserToTelegramRequestBody } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type IBindUserToTelegramRequest = AppRequest<IBindUserToTelegramRequestBody>;
export type IGetUserByTelegramIdRequest = AppRequest<undefined, { telegramUserId: string }>;
export type IGetScheduleByTelegramIdRequest = AppRequest<undefined, { telegramUserId: string; weekNumber: number }>;
