import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectFullSchema } from './subject-full.schema';

export const updateSubjectBody = createObjectSchema(subjectFullSchema, {
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  nullable: 'onNotRequired',
});
