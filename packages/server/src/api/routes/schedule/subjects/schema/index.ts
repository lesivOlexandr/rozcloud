import { teacherResponseSuccess } from 'src/api/routes/users/teachers/schema/teacher-response-success.schema';
import { maxTextFieldLetters } from 'src/common/constants';
import { createObjectSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { subjectParam } from './chunks/subject-param.schema';
import { createSubjectBody } from './create-subject.schema';
import { subjectResponseSuccessSchema } from './subject-response-success.schema';
import { updateSubjectBody } from './update-subject.schema';

export const getSubjectsSchema = createRouteValidationSchema(
  {
    querystring: createObjectSchema(
      {
        type: 'object',
        properties: {
          search: {
            type: 'string',
            maxLength: maxTextFieldLetters,
          },
        },
      },
      {
        nullable: 'onNotRequired',
      }
    ),
    tags: ['Schedule'],
  },
  {
    200: { type: 'array', items: subjectResponseSuccessSchema },
    400: true,
    401: true,
    403: true,
    protectedJWT: true,
  }
);

export const createSubjectSchema = createRouteValidationSchema(
  {
    body: createSubjectBody,
    tags: ['Schedule'],
  },
  {
    200: subjectResponseSuccessSchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateSubjectSchema = createRouteValidationSchema(
  {
    body: updateSubjectBody,
    params: subjectParam,
    tags: ['Schedule'],
  },
  {
    200: subjectResponseSuccessSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getSubjectSchema = createRouteValidationSchema(
  {
    params: subjectParam,
    tags: ['Schedule'],
  },
  {
    200: subjectResponseSuccessSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getSubjectTeachersSchema = createRouteValidationSchema(
  {
    params: subjectParam,
    tags: ['Schedule'],
  },
  {
    200: { type: 'array', items: teacherResponseSuccess },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
