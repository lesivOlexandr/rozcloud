import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectFullSchema } from './subject-full.schema';

export const subjectEntitySchema = createObjectSchema(subjectFullSchema, {
  required: ['name', 'id'],
  defaultNull: 'onNotRequired',
  removeProperties: ['createdAt', 'updatedAt'],
});
