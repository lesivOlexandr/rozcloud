import { timePointSchema } from 'src/api/routes/_schema-chunks';
import { idSchemaChunk } from 'src/api/routes/_schema-chunks/id.schema';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectNameSchemaChunk } from './chunks/subject-name.schema';

export const subjectFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: subjectNameSchemaChunk,
    departmentId: idSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
