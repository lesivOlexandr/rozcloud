import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectFullSchema } from './subject-full.schema';

export const createSubjectBody = createObjectSchema(subjectFullSchema, {
  required: ['name'],
  defaultNull: 'onNotRequired',
  removeProperties: ['createdAt', 'updatedAt', 'id'],
});
