import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { SubjectService } from 'src/api/services/subjects';
import {
  CreateSubjectRequest,
  GetSubjectRequest,
  GetSubjectsRequest,
  GetSubjectTeachersRequest,
  UpdateSubjectRequest,
} from './interfaces';
import {
  createSubjectSchema,
  getSubjectSchema,
  getSubjectsSchema,
  getSubjectTeachersSchema,
  updateSubjectSchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/', { schema: getSubjectsSchema }, async (request: GetSubjectsRequest) => {
    const subjects = await SubjectService.getMany(request.query);
    return subjects;
  });

  fastify.get('/:subjectId', { schema: getSubjectSchema }, async (request: GetSubjectRequest) => {
    const subject = await SubjectService.getOne(request.params.subjectId);
    return subject;
  });

  fastify.post('/', { schema: createSubjectSchema }, async (request: CreateSubjectRequest) => {
    const subject = await SubjectService.createOne(request.body);
    return subject;
  });

  fastify.put('/:subjectId', { schema: updateSubjectSchema }, async (request: UpdateSubjectRequest) => {
    const subject = await SubjectService.updateOne(request.params.subjectId, request.body);
    return subject;
  });

  fastify.get(
    '/:subjectId/teachers',
    { schema: getSubjectTeachersSchema },
    async (request: GetSubjectTeachersRequest) => {
      const teachers = SubjectService.getSubjectTeachers(request.params.subjectId);
      return teachers;
    }
  );

  next();
};
