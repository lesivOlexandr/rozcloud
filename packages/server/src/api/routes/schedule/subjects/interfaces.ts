import { FastifyRequest } from 'fastify';
import { ICreateSubjectPayload, ISubjectFilter, IUpdateSubjectPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type SubjectParam = { subjectId: string };

export type CreateSubjectRequest = AppRequest<ICreateSubjectPayload>;
export type UpdateSubjectRequest = AppRequest<IUpdateSubjectPayload, SubjectParam>;
export type GetSubjectRequest = AppRequest<undefined, SubjectParam>;
export type GetSubjectTeachersRequest = AppRequest<undefined, SubjectParam>;
export type GetSubjectsRequest = FastifyRequest<{ Querystring: ISubjectFilter }>;
