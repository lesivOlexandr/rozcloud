import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { EducationalPlansService } from 'src/api/services/educational-plan';
import {
  ICreateEducationalPlanRequest,
  IGetEducationalPlanByGroupRequest,
  IGetEducationalPlanRequest,
  IUpdateEducationalPlanRequest,
} from './interface';
import { createEducPlanSchema, getEducPlanByGroupSchema, getEducPlanSchema, updateEducPlanSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get(
    '/:educationalPlanId',
    { schema: getEducPlanByGroupSchema },
    async (request: IGetEducationalPlanRequest) => {
      const educationalPlan = await EducationalPlansService.getOne(request.params.educPlanId);
      return educationalPlan;
    }
  );

  fastify.post('/', { schema: createEducPlanSchema }, async (request: ICreateEducationalPlanRequest) => {
    const educationalPlan = await EducationalPlansService.createOne(request.body);
    return educationalPlan;
  });

  fastify.put('/:educPlanId', { schema: updateEducPlanSchema }, async (request: IUpdateEducationalPlanRequest) => {
    const educationalPlan = await EducationalPlansService.updateOne(request.params.educPlanId, request.body);
    return educationalPlan;
  });

  fastify.get(
    '/by-group/:groupId',
    { schema: getEducPlanSchema },
    async (request: IGetEducationalPlanByGroupRequest) => {
      const educationalPlan = await EducationalPlansService.getOne(request.params.groupId);
      return educationalPlan;
    }
  );

  next();
};
