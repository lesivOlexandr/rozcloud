import { ICreateUpdateEducationalPlanPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';
import { GroupParam } from '../../structures/groups/interfaces';

export type EducationalPlanParam = { educPlanId: string };

export type ICreateEducationalPlanRequest = AppRequest<ICreateUpdateEducationalPlanPayload>;
export type IUpdateEducationalPlanRequest = AppRequest<ICreateUpdateEducationalPlanPayload, EducationalPlanParam>;
export type IGetEducationalPlanRequest = AppRequest<undefined, EducationalPlanParam>;
export type IGetEducationalPlanByGroupRequest = AppRequest<undefined, GroupParam>;
