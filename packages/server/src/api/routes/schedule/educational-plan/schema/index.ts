import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { createUpdateEducPlanBody, educPlanParam } from './create-update-educational-plan.schema';
import { educationalPlanEntitySchema, getEducationalPlanByGroupParams } from './educational-plan-entity.schema';
import { educationalPlanGetFilledSchema } from './educational-plan-get-response';

export const createEducPlanSchema = createRouteValidationSchema(
  {
    body: createUpdateEducPlanBody,
    tags: ['Schedule'],
  },
  {
    200: educationalPlanEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateEducPlanSchema = createRouteValidationSchema(
  {
    body: educPlanParam,
    params: educPlanParam,
    tags: ['Schedule'],
  },
  {
    200: educationalPlanEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getEducPlanByGroupSchema = createRouteValidationSchema(
  {
    params: getEducationalPlanByGroupParams,
    tags: ['Schedule'],
  },
  {
    200: educationalPlanGetFilledSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getEducPlanSchema = createRouteValidationSchema(
  {
    params: educPlanParam,
    tags: ['Schedule'],
  },
  {
    200: educationalPlanGetFilledSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
