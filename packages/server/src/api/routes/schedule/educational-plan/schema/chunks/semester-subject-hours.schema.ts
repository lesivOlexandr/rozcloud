import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { semesterNumberChunk } from '../../../_schema-chunks/semester-number.schema';
import { controlHoursChunk } from './control-hours.schema';

export const semesterSubjectSchemaChunk = createObjectSchema(
  {
    type: 'object',
    properties: {
      semesterNumber: semesterNumberChunk,
      controlHours: controlHoursChunk,
    },
  },
  {
    required: ['semesterNumber', 'controlHours'],
  }
);
