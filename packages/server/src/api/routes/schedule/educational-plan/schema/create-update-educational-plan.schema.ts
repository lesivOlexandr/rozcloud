import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { educationalPlanFullSchema } from './educational-plan-full.schema';

export const createUpdateEducPlanBody = createObjectSchema(educationalPlanFullSchema, {
  required: ['groupId', 'subjectsHoursMap'],
  removeProperties: ['id', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});

export const educPlanParam = createObjectSchema(
  { type: 'object', properties: { educPlanId: idSchemaChunk } },
  { required: ['educPlanId'] }
);
