import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { educPlanSubjectHoursMap } from './chunks/educational-plan-subject-hours-map.schema';

export const educationalPlanFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    groupId: idSchemaChunk,
    subjectsHoursMap: {
      type: 'array',
      items: educPlanSubjectHoursMap,
      minItems: 1,
    },
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
