import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const controlHoursChunk = createScalarSchema({
  type: 'number',
  minimum: 0,
});
