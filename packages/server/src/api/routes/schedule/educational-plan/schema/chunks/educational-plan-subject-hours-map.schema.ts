import { idSchemaChunk } from 'src/api/routes/_schema-chunks/id.schema';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { hoursTimeSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/time-hours.schema';
import { subjectPlanTypeChunk } from 'src/api/routes/schedule/_schema-chunks/subject-plan-type.schema';
import { PlanSubjectTypes } from '@rozcloud/shared/enums';
import { ektsCreditsCountChunk } from 'src/api/routes/schedule/_schema-chunks/ekts-credits.schema';
import { semesterControlsSchema } from './semester-controls.schema';
import { oppSubjectAbbreviationChunk } from 'src/api/routes/schedule/_schema-chunks/opp-subject-abbreviation.schema';

export const educPlanSubjectHoursMap = createObjectSchema(
  {
    type: 'object',
    properties: {
      subjectId: idSchemaChunk,
      subjectPlanType: subjectPlanTypeChunk,
      ektsCreditsCount: ektsCreditsCountChunk,
      semesterSubjectControls: semesterControlsSchema,
      laboratoryHours: hoursTimeSchemaChunk,
      practicalHours: hoursTimeSchemaChunk,
      auditorialHours: hoursTimeSchemaChunk,
      independentWorkHours: hoursTimeSchemaChunk,
      seminarHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      oppSubjectAbbreviation: oppSubjectAbbreviationChunk,
    },
  },
  {
    required: ['subjectId'],
    mergeProperties: [
      ['subjectPlanType', { default: PlanSubjectTypes.Mandatory }],
      ['ektsCreditsCountChunk', { default: 0 }],
      ['oppSubjectAbbreviation', { default: '' }],
    ],
  }
);
