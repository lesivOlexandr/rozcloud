import { SemesterControlTypes } from '@rozcloud/shared/enums';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { semesterNumberChunk } from 'src/api/routes/schedule/_schema-chunks/semester-number.schema';
import { semesterControlTypeChunk } from 'src/api/routes/schedule/_schema-chunks/semester-control-type.schema';

export const semesterControlSchemaChunk = createObjectSchema(
  {
    type: 'object',
    properties: {
      semesterNumber: semesterNumberChunk,
      controlType: semesterControlTypeChunk,
      hoursPerWeek: {
        type: 'number',
        minimum: 0,
      },
    },
  },
  {
    required: ['semesterNumber'],
    mergeProperties: [
      ['controlType', { default: SemesterControlTypes.None }],
      ['hoursPerWeek', { default: 0 }],
    ],
  }
);
