import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { semesterControlSchemaChunk } from './semester-control.schema';

export const semesterControlsSchema = createScalarSchema({
  type: 'array',
  items: semesterControlSchemaChunk,
  minItems: 0,
  maxItems: 8,
});
