import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { educationalPlanFullSchema } from './educational-plan-full.schema';

export const educationalPlanEntitySchema = createObjectSchema(educationalPlanFullSchema, {
  required: ['subjectsHoursMap', 'groupId', 'id', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});

export const getEducationalPlanByGroupParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      groupId: idSchemaChunk,
    },
  },
  {
    required: ['groupId'],
  }
);
