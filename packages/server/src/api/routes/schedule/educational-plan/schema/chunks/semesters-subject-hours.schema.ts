import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { semesterSubjectSchemaChunk } from './semester-subject-hours.schema';

export const semestersSubjectSchemaChunk = createScalarSchema({
  type: 'array',
  items: semesterSubjectSchemaChunk,
  minItems: 0,
  maxItems: 8,
});
