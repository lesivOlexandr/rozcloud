import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { educPlanSubjectHoursFilledMap } from './chunks/educational-plan-subject-hours-map-filled.schema';

export const educationalPlanGetFilledSchema = createObjectSchema(
  {
    type: 'object',
    properties: {
      id: idSchemaChunk,
      group: groupEntitySchema,
      subjectsHoursMap: {
        type: 'array',
        items: educPlanSubjectHoursFilledMap,
        minItems: 1,
      },
      createdAt: timePointSchema,
      updatedAt: timePointSchema,
    },
  },
  {
    required: ['group', 'subjectsHoursMap'],
    defaultNull: 'onNotRequired',
  }
);
