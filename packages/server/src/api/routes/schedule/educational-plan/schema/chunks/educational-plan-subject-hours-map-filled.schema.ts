import { PlanSubjectTypes } from '@rozcloud/shared/enums';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectEntitySchema } from '../../../subjects/schema/subject-entity.schema';
import { ektsCreditsCountChunk } from '../../../_schema-chunks/ekts-credits.schema';
import { oppSubjectAbbreviationChunk } from '../../../_schema-chunks/opp-subject-abbreviation.schema';
import { subjectPlanTypeChunk } from '../../../_schema-chunks/subject-plan-type.schema';
import { hoursTimeSchemaChunk } from '../../../_schema-chunks/time-hours.schema';
import { semesterControlsSchema } from './semester-controls.schema';

export const educPlanSubjectHoursFilledMap = createObjectSchema(
  {
    type: 'object',
    properties: {
      subject: subjectEntitySchema,
      subjectPlanType: subjectPlanTypeChunk,
      ektsCreditsCount: ektsCreditsCountChunk,
      semesterSubjectControls: semesterControlsSchema,
      laboratoryHours: hoursTimeSchemaChunk,
      practicalHours: hoursTimeSchemaChunk,
      auditorialHours: hoursTimeSchemaChunk,
      independentWorkHours: hoursTimeSchemaChunk,
      seminarHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      oppSubjectAbbreviation: oppSubjectAbbreviationChunk,
    },
  },
  { required: ['subject'], mergeProperties: [['subjectPlanType', { default: PlanSubjectTypes.Mandatory }]] }
);
