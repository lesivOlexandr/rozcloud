import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { createSpecializationBody } from './create-specialization';
import { getSpecializationGroupsParams } from './get-specialization-groups';
import { specializationEntitySchema } from './specialization-entity';
import { updateSpecializationBody, updateSpecializationParams } from './update-specialization';

export const createSpecializationSchema = createRouteValidationSchema(
  {
    body: createSpecializationBody,
    tags: ['Schedule'],
  },
  {
    200: specializationEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateSpecializationSchema = createRouteValidationSchema(
  {
    body: updateSpecializationBody,
    params: updateSpecializationParams,
    tags: ['Schedule'],
  },
  {
    200: specializationEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getSpecializationSchema = createRouteValidationSchema(
  {
    params: getSpecializationGroupsParams,
    tags: ['Schedule'],
  },
  {
    200: specializationEntitySchema,
    400: true,
    404: true,
  }
);

export const getAllSpecializationsSchema = createRouteValidationSchema(
  {
    tags: ['Schedule'],
  },
  {
    200: { type: 'array', items: specializationEntitySchema },
    400: true,
    403: true,
    404: true,
  }
);

export const getSpecializationGroupsSchema = createRouteValidationSchema(
  {
    params: getSpecializationGroupsParams,
    tags: ['Schedule'],
  },
  {
    200: { type: 'array', items: groupEntitySchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
