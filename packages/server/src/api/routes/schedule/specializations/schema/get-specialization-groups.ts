import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';

export const getSpecializationGroupsParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      specializationId: idSchemaChunk,
    },
  },
  {
    required: ['specializationId'],
  }
);
