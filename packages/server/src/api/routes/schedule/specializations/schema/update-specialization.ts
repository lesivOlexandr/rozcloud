import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { specializationFullSchema } from './specialization-full.schema';

export const updateSpecializationBody = createObjectSchema(specializationFullSchema, {
  removeProperties: ['id', 'createdAt', 'updatedAt'],
  nullable: 'onNotRequired',
});

export const updateSpecializationParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      specializationId: idSchemaChunk,
    },
  },
  {
    required: ['specializationId'],
  }
);
