import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { specializationCodeSchemaChunk } from './chunks/specialization-code.schema';
import { specializationNameSchemaChunk } from './chunks/specialization-name.schema';

export const specializationFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    name: specializationNameSchemaChunk,
    specializationCode: specializationCodeSchemaChunk,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
