import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { specializationFullSchema } from './specialization-full.schema';

export const specializationEntitySchema = createObjectSchema(specializationFullSchema, {
  required: ['id', 'specializationCode', 'name', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});
