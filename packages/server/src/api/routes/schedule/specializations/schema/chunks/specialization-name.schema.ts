import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const specializationNameSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
});
