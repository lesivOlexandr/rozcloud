import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const specializationCodeSchemaChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: 4,
});
