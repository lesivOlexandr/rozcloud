import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { specializationFullSchema } from './specialization-full.schema';

export const createSpecializationBody = createObjectSchema(specializationFullSchema, {
  removeProperties: ['id', 'createdAt', 'updatedAt'],
  required: ['name', 'specializationCode'],
  defaultNull: 'onNotRequired',
});
