import { CreateSpecializationPayload, UpdateSpecializationPayload } from 'src/common/interfaces';
import { AppRequest } from 'src/common/interfaces/server';

export type SpecializationParam = { specializationId: string };

export type CreateSpecializationRequest = AppRequest<CreateSpecializationPayload>;
export type UpdateSpecializationRequest = AppRequest<UpdateSpecializationPayload, SpecializationParam>;
export type GetSpecializationRequest = AppRequest<undefined, SpecializationParam>;
export type GetSpecializationGroupsRequest = AppRequest<unknown, SpecializationParam>;
