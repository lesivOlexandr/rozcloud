import { FastifyRequest } from 'fastify';
import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { SpecializationsService } from 'src/api/services/specializations';
import {
  CreateSpecializationRequest,
  GetSpecializationGroupsRequest,
  GetSpecializationRequest,
  UpdateSpecializationRequest,
} from './interfaces';
import {
  createSpecializationSchema,
  getAllSpecializationsSchema,
  getSpecializationGroupsSchema,
  getSpecializationSchema,
  updateSpecializationSchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/', { schema: getAllSpecializationsSchema }, async (request: FastifyRequest) => {
    const specializations = await SpecializationsService.getAllSpecializations();
    return specializations;
  });

  fastify.get('/:specializationId', { schema: getSpecializationSchema }, (request: GetSpecializationRequest) => {
    const specialization = SpecializationsService.getOne(request.params.specializationId);
    return specialization;
  });

  fastify.post('/', { schema: createSpecializationSchema, preHandler: jwtInjectUser }, async (request: unknown) => {
    const customRequest = request as CreateSpecializationRequest;
    const specialization = await SpecializationsService.createOne(customRequest.body);
    return specialization;
  });

  fastify.put(
    '/:specializationId',
    { schema: updateSpecializationSchema, preHandler: jwtInjectUser },
    async (request: unknown) => {
      const customRequest = request as UpdateSpecializationRequest;
      const specialization = await SpecializationsService.updateOne(
        customRequest.params.specializationId,
        customRequest.body
      );
      return specialization;
    }
  );

  fastify.get(
    '/:specializationId/groups',
    { schema: getSpecializationGroupsSchema, preHandler: jwtInjectUser },
    async (request: unknown) => {
      const requestCustom = request as GetSpecializationGroupsRequest;
      const groups = await SpecializationsService.getSpecializationGroups(requestCustom.params.specializationId);
      return groups;
    }
  );

  next();
};
