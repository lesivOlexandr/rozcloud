import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const weekNumSchemaChunk = createScalarSchema({
  type: 'integer',
  minimum: 0,
  maximum: Number.MAX_SAFE_INTEGER,
});
