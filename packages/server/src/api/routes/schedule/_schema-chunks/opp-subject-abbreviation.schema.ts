import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const oppSubjectAbbreviationChunk = createScalarSchema({
  type: 'string',
  minLength: 0,
  maxLength: maxTextFieldLetters,
});
