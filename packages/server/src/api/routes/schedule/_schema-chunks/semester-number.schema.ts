import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const semesterNumberChunk = createScalarSchema({
  type: 'integer',
  minimum: 1,
  maximum: 8,
  example: 1,
});
