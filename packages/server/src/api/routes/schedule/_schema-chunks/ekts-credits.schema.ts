import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const ektsCreditsCountChunk = createScalarSchema({
  type: 'number',
  minimum: 0,
  maximum: Number.MAX_SAFE_INTEGER,
});
