import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const hoursTimeSchemaChunk = createScalarSchema({
  type: 'integer',
  minimum: 0,
  default: 0,
  maximum: Number.MAX_SAFE_INTEGER,
});
