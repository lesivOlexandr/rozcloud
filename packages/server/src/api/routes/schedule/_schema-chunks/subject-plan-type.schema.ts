import { PlanSubjectTypes } from '@rozcloud/shared/enums';
import { maxTextFieldLetters } from 'src/common/constants';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const subjectPlanTypeChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  maxLength: maxTextFieldLetters,
  enum: Object.values(PlanSubjectTypes),
});
