import { FastifyInstance } from 'fastify';
import subjectRoutes from './subjects';
import educPlanRoutes from './educational-plan';
import workPlanRoutes from './working-plan';
import scheduleWeekRoutes from './schedule-week';
import specializationRoutes from './specializations';
import academicPlanRoutes from './academic-plan';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(subjectRoutes, { prefix: 'subjects' });
  fastify.register(educPlanRoutes, { prefix: 'educational-plan' });
  fastify.register(workPlanRoutes, { prefix: 'working-plan' });
  fastify.register(academicPlanRoutes, { prefix: 'academic-plan' });
  fastify.register(scheduleWeekRoutes, { prefix: 'week' });
  fastify.register(specializationRoutes, { prefix: 'specializations' });

  next();
};
