import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { subjectEntitySchema } from '../../../subjects/schema/subject-entity.schema';
import { hoursTimeSchemaChunk } from '../../../_schema-chunks/time-hours.schema';
import { academicPlanWeekData } from './week-data.schema';

export const filledAcademicPlanSubjectHoursMap = createScalarSchema({
  type: 'array',
  items: {
    type: 'object',
    properties: {
      auditorialHours: hoursTimeSchemaChunk,
      subject: subjectEntitySchema,
      independentWorkHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      weeksData: {
        type: 'array',
        items: academicPlanWeekData,
      },
    },
  },
});
