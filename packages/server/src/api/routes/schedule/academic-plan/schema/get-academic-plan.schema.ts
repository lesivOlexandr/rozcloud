import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { yearChunk } from 'src/api/routes/_schema-chunks/year.scheme';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { filledAcademicPlanSubjectHoursMap } from './chunks/subject-hours-filled';

export const filledAcademicPlanSchema = createObjectSchema(
  {
    type: 'object',
    properties: {
      id: idSchemaChunk,
      year: yearChunk,
      group: groupEntitySchema,
      subjectsHoursMap: filledAcademicPlanSubjectHoursMap,
      createdAt: timePointSchema,
      updatedAt: timePointSchema,
    },
  },
  {
    required: ['group', 'createdAt', 'updatedAt'],
  }
);
