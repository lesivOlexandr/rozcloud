import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { hoursTimeSchemaChunk } from '../../../_schema-chunks/time-hours.schema';
import { academicPlanWeekData } from './week-data.schema';

export const academicPlanSubjectHoursMap = createScalarSchema({
  type: 'array',
  items: {
    type: 'object',
    properties: {
      auditorialHours: hoursTimeSchemaChunk,
      subjectId: idSchemaChunk,
      independentWorkHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      weeksData: {
        type: 'array',
        items: academicPlanWeekData,
      },
    },
  },
});
