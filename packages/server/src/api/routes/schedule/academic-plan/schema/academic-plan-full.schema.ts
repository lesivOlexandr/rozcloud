import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { yearChunk } from 'src/api/routes/_schema-chunks/year.scheme';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { academicPlanSubjectHoursMap } from './chunks/subject-hours-map';

export const academicPlanFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    year: yearChunk,
    groupId: idSchemaChunk,
    subjectsHoursMap: academicPlanSubjectHoursMap,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
