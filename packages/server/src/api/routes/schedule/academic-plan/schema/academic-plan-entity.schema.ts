import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { academicPlanFullSchema } from './academic-plan-full.schema';

export const academicPlanEntitySchema = createObjectSchema(academicPlanFullSchema, {
  required: ['subjectsHoursMap', 'groupId', 'id', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});

export const getAcademicPlanByGroupParams = createObjectSchema(
  {
    type: 'object',
    properties: {
      groupId: idSchemaChunk,
    },
  },
  {
    required: ['groupId'],
  }
);

export const academicPlanParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      academicPlanId: idSchemaChunk,
    },
  },
  {
    required: ['academicPlanId'],
  }
);
