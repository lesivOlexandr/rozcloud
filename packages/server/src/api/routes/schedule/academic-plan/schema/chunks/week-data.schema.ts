import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { hoursTimeSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/time-hours.schema';
import { weekNumSchemaChunk } from '../../../_schema-chunks/week-num.schema';

export const academicPlanWeekData = createObjectSchema(
  {
    type: 'object',
    properties: {
      laboratoryHours: hoursTimeSchemaChunk,
      practicalHours: hoursTimeSchemaChunk,
      seminarHours: hoursTimeSchemaChunk,
      weekNumber: weekNumSchemaChunk,
    },
  },
  {
    required: [],
    nullable: 'onNotRequired',
  }
);
