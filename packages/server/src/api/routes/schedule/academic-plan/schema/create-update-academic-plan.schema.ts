import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { academicPlanFullSchema } from './academic-plan-full.schema';

export const createUpdateAcademicPlanBody = createObjectSchema(academicPlanFullSchema, {
  required: ['groupId', 'subjectsHoursMap'],
  removeProperties: ['id', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});
