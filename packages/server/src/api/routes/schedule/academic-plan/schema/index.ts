import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { createUpdateAcademicPlanBody } from './create-update-academic-plan.schema';
import {
  academicPlanEntitySchema,
  academicPlanParam,
  getAcademicPlanByGroupParams,
} from './academic-plan-entity.schema';
import { filledAcademicPlanSchema } from './get-academic-plan.schema';
import { yearChunk } from 'src/api/routes/_schema-chunks/year.scheme';

export const createAcademicPlanSchema = createRouteValidationSchema(
  {
    body: createUpdateAcademicPlanBody,
    tags: ['Schedule'],
  },
  {
    200: academicPlanEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateAcademicPlanSchema = createRouteValidationSchema(
  {
    body: createUpdateAcademicPlanBody,
    params: academicPlanParam,
    tags: ['Schedule'],
  },
  {
    200: academicPlanEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getAcademicsPlanByGroupSchema = createRouteValidationSchema(
  {
    params: getAcademicPlanByGroupParams,
    tags: ['Schedule'],
  },
  {
    200: { type: 'array', items: filledAcademicPlanSchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getAcademicPlanSchema = createRouteValidationSchema(
  {
    params: academicPlanParam,
    tags: ['Schedule'],
  },
  {
    200: filledAcademicPlanSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getAcademicPlanByGroupSchema = createRouteValidationSchema(
  {
    params: {
      type: 'object',
      properties: { ...getAcademicPlanByGroupParams.properties, year: yearChunk },
    },
    tags: ['Schedule'],
  },
  {
    200: filledAcademicPlanSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
