import { ICreateUpdateAcademicPlanPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';
import { GroupParam } from '../../structures/groups/interfaces';

export type AcademicPlanParam = { academicPlanId: string };

export type CreateAcademicPlanRequest = AppRequest<ICreateUpdateAcademicPlanPayload>;
export type IUpdateAcademicPlanRequest = AppRequest<ICreateUpdateAcademicPlanPayload, AcademicPlanParam>;
export type GetAcademicPlanRequest = AppRequest<undefined, AcademicPlanParam>;
export type GetAcademicPlanByGroupRequest = AppRequest<undefined, AcademicPlanParam>;
export type GetAllGroupAcademicPlansRequest = AppRequest<undefined, GroupParam>;
export type GetGroupAcademicPlanRequest = AppRequest<undefined, GroupParam & { year: number }>;
