import { FastifyInstance } from 'fastify';
import { AcademicPlansService } from 'src/api/services/academic-plan';
import {
  CreateAcademicPlanRequest,
  GetAcademicPlanRequest,
  GetAllGroupAcademicPlansRequest,
  GetGroupAcademicPlanRequest,
  IUpdateAcademicPlanRequest,
} from './interfaces';
import {
  createAcademicPlanSchema,
  getAcademicsPlanByGroupSchema,
  getAcademicPlanSchema,
  updateAcademicPlanSchema,
  getAcademicPlanByGroupSchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.get('/:academicPlanId', { schema: getAcademicPlanSchema }, async (request: GetAcademicPlanRequest) => {
    const academicPlan = await AcademicPlansService.getOne(request.params.academicPlanId);
    return academicPlan;
  });

  fastify.post('/', { schema: createAcademicPlanSchema }, async (request: CreateAcademicPlanRequest) => {
    const academicPlan = await AcademicPlansService.createOne(request.body);
    return academicPlan;
  });

  fastify.put('/:academicPlanId', { schema: updateAcademicPlanSchema }, async (request: IUpdateAcademicPlanRequest) => {
    const academicPlan = await AcademicPlansService.updateOne(request.params.academicPlanId, request.body);
    return academicPlan;
  });

  fastify.get(
    '/group/:groupId/all',
    { schema: getAcademicsPlanByGroupSchema },
    async (request: GetAllGroupAcademicPlansRequest) => {
      const academicPlans = await AcademicPlansService.getAllOfGroup(request.params.groupId);
      return academicPlans;
    }
  );

  fastify.get(
    '/group/:groupId/:year',
    { schema: getAcademicPlanByGroupSchema },
    async (request: GetGroupAcademicPlanRequest) => {
      const { groupId, year } = request.params;
      const academicPlan = await AcademicPlansService.getOneAcademicPlanByGroup(groupId, year);
      return academicPlan;
    }
  );

  next();
};
