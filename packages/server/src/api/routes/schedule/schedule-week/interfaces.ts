import { ICreateScheduleWeekPayload } from '@rozcloud/shared/interfaces';
import { FastifyRequest } from 'fastify';
import { AppRequest } from 'src/common/interfaces/server';

export type WeekNumberParam = { weekNumber: number };
export type GroupIds = { groupIds: string[] };

export type CreateOneScheduleWeekRequest = AppRequest<ICreateScheduleWeekPayload>;
export type GetScheduleByWeekNumber = AppRequest<undefined, WeekNumberParam>;
export type GetScheduleByGroupsRequest = FastifyRequest<{ Params: WeekNumberParam; Querystring: GroupIds }>;
