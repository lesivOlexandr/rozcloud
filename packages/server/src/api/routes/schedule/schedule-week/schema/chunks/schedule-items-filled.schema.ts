import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleItemFilledSchema } from './schedule-item-filled.schema';

export const scheduleItemsFilledSchema = createScalarSchema({
  type: 'array',
  items: scheduleItemFilledSchema,
});
