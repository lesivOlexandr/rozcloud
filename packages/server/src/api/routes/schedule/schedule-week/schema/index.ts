import { createArrayOfSchema, createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleWeekFilledSchema } from './chunks/schedule-week-filled.schema';
import { createUpdateScheduleWeekSchema, scheduleWeekParam } from './create-update-schedule-week.schema';
import { getScheduleBody, getScheduleByGroupQuery, getScheduleByGroupsParams } from './get-schedule.schema';
import { scheduleWeekEntitySchema } from './schedule-entity.schema';
import { scheduleFilledFullSchema } from './schedule-week-filled-full-data';

export const createWeekScheduleSchema = createRouteValidationSchema(
  {
    body: createUpdateScheduleWeekSchema,
    tags: ['Schedule'],
  },
  {
    200: createArrayOfSchema(scheduleWeekFilledSchema),
    400: true,
    403: true,
  }
);

export const updateWeekScheduleSchema = createRouteValidationSchema(
  {
    body: createUpdateScheduleWeekSchema,
    params: scheduleWeekParam,
    tags: ['Schedule'],
  },
  {
    200: scheduleWeekEntitySchema,
    400: true,
    403: true,
    404: true,
  }
);

export const getScheduleWeekSchema = createRouteValidationSchema(
  {
    body: getScheduleBody,
    tags: ['Schedule'],
  },
  {
    200: scheduleFilledFullSchema,
    400: true,
    403: true,
    404: true,
  }
);

export const getScheduleWeekByGroupsSchema = createRouteValidationSchema(
  {
    params: getScheduleByGroupsParams,
    querystring: getScheduleByGroupQuery,
    tags: ['Schedule'],
  },
  {
    200: createArrayOfSchema(scheduleWeekFilledSchema),
    400: true,
    403: true,
    404: true,
  }
);
