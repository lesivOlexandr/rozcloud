import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleDaySchema } from './schedule-day-item.schema';

export const scheduleWeekSchema = createObjectSchema(
  {
    type: 'object',
    properties: {
      1: scheduleDaySchema,
      2: scheduleDaySchema,
      3: scheduleDaySchema,
      4: scheduleDaySchema,
      5: scheduleDaySchema,
      6: scheduleDaySchema,
      0: scheduleDaySchema,
    },
    minProperties: 1,
  },
  {
    required: [],
    defaultNull: 'onNotRequired',
  }
);
