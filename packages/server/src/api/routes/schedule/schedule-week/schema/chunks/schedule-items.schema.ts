import { createScalarSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleItemSchema } from './schedule-subject-item.schema';

export const scheduleItemsSchema = createScalarSchema({
  type: 'array',
  items: scheduleItemSchema,
});
