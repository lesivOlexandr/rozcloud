import { auditoryResponseSuccessSchema } from 'src/api/routes/locations/auditories/schema/auditory-response-success.schema';
import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';
import { teacherEntitySchema } from 'src/api/routes/users/teachers/schema/teacher-entity.schema';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { subjectEntitySchema } from '../../../subjects/schema/subject-entity.schema';
import { scheduleItemChunk } from './schedule-item-type.schema';

export const scheduleItemFilledSchema = createObjectSchema(
  {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacher: teacherEntitySchema,
      subject: subjectEntitySchema,
      group: groupEntitySchema,
      auditory: auditoryResponseSuccessSchema,
      type: scheduleItemChunk,
    },
  },
  {
    required: ['group'],
    defaultNull: 'onNotRequired',
  }
);
