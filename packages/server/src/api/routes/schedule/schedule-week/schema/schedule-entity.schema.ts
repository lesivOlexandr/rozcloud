import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleFullSchema } from './schedule-full.schema';

export const scheduleWeekEntitySchema = createObjectSchema(scheduleFullSchema, {
  required: ['createdAt', 'updatedAt', 'id', 'daySchedules'],
  defaultNull: 'onNotRequired',
});
