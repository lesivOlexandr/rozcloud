import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleItemChunk } from './schedule-item-type.schema';

export const scheduleItemSchema = createObjectSchema(
  {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: idSchemaChunk,
      subjectId: idSchemaChunk,
      groupId: idSchemaChunk,
      auditoryId: idSchemaChunk,
      type: scheduleItemChunk,
    },
  },
  {
    required: ['groupId'],
    defaultNull: 'onNotRequired',
  }
);
