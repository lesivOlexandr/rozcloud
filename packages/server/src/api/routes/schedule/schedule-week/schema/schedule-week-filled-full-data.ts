import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { weekNumSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/week-num.schema';
import { scheduleWeekFilledArraySchema } from './chunks/schedule-week-filled.schema';

export const scheduleFilledFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    weekNumber: weekNumSchemaChunk,
    daySchedules: scheduleWeekFilledArraySchema,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
