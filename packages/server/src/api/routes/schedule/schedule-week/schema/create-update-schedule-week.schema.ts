import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleFullSchema } from './schedule-full.schema';

export const createUpdateScheduleWeekSchema = createObjectSchema(scheduleFullSchema, {
  required: ['weekNumber', 'daySchedules'],
  removeProperties: ['createdAt', 'updatedAt', 'id'],
  defaultNull: 'onNotRequired',
});

export const scheduleWeekParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      scheduleWeekId: idSchemaChunk,
    },
  },
  {
    required: ['scheduleWeekId'],
  }
);
