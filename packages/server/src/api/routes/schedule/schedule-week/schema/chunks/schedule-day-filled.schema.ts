import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { Schema } from 'swagger-schema-official';
import { scheduleItemFilledSchema } from './schedule-item-filled.schema';

export const scheduleDayFilledSchemaBuilder = (itemSchema: Schema) =>
  createObjectSchema(
    {
      type: 'object',
      additionalProperties: false,
      nullable: false,
      properties: {
        1: itemSchema,
        2: itemSchema,
        3: itemSchema,
        4: itemSchema,
        5: itemSchema,
        6: itemSchema,
      },
    },
    {
      required: [],
      defaultNull: 'onNotRequired',
    }
  );

export const scheduleDayFilledSchema = scheduleDayFilledSchemaBuilder(scheduleItemFilledSchema);
