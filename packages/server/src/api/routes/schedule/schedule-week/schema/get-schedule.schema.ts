import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { userTypeSchemaChunk } from 'src/api/routes/_schema-chunks/user-type.schema';
import { createArrayOfSchema, createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { weekNumSchemaChunk } from '../../_schema-chunks/week-num.schema';

export const getScheduleBody = createObjectSchema({
  type: 'object',
  properties: {
    userType: userTypeSchemaChunk,
    weekNum: weekNumSchemaChunk,
  },
});

export const getScheduleByGroupQuery = createObjectSchema(
  {
    type: 'object',
    properties: {
      groupIds: createArrayOfSchema(idSchemaChunk),
    },
  },
  {
    required: ['groupsIds'],
  }
);

export const getScheduleByGroupsParams = createObjectSchema({
  type: 'object',
  properties: {
    weekNumber: weekNumSchemaChunk,
  },
});
