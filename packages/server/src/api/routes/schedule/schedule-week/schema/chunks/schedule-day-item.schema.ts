import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleItemsSchema } from './schedule-items.schema';

export const scheduleDaySchema = createObjectSchema(
  {
    type: 'object',
    additionalProperties: false,
    nullable: false,
    properties: {
      1: scheduleItemsSchema,
      2: scheduleItemsSchema,
      3: scheduleItemsSchema,
      4: scheduleItemsSchema,
      5: scheduleItemsSchema,
      6: scheduleItemsSchema,
    },
    minProperties: 6,
  },
  {
    required: [],
    defaultNull: 'onNotRequired',
  }
);
