import { SubjectTypes } from '@rozcloud/shared/enums';
import { createScalarSchema } from 'src/common/helpers/swagger.helpers';

export const scheduleItemChunk = createScalarSchema({
  type: 'string',
  minLength: 1,
  enum: [...Object.values(SubjectTypes), null],
  nullable: true,
});
