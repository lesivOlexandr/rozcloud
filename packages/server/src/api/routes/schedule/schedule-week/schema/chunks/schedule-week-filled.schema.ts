import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { Schema } from 'swagger-schema-official';
import { scheduleDayFilledArraySchema } from './schedule-day-filled-array.schema';
import { scheduleDayFilledSchema } from './schedule-day-filled.schema';

export const scheduleWeekFilledSchemaBuilder = (daySchema: Schema) =>
  createObjectSchema(
    {
      type: 'object',
      properties: {
        1: daySchema,
        2: daySchema,
        3: daySchema,
        4: daySchema,
        5: daySchema,
        6: daySchema,
        0: daySchema,
      },
      minProperties: 1,
    },
    {
      required: [],
      defaultNull: 'onNotRequired',
    }
  );

export const scheduleWeekFilledSchema = scheduleWeekFilledSchemaBuilder(scheduleDayFilledSchema);
export const scheduleWeekFilledArraySchema = scheduleWeekFilledSchemaBuilder(scheduleDayFilledArraySchema);
