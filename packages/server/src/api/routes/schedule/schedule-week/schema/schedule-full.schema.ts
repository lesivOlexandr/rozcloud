import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { weekNumSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/week-num.schema';
import { scheduleWeekSchema } from './chunks/schedule-week.schema';

export const scheduleFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    weekNumber: weekNumSchemaChunk,
    daySchedules: scheduleWeekSchema,
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
