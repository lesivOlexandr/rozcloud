import { createArrayOfSchema } from 'src/common/helpers/swagger.helpers';
import { scheduleDayFilledSchemaBuilder } from './schedule-day-filled.schema';
import { scheduleItemFilledSchema } from './schedule-item-filled.schema';

export const scheduleDayFilledArraySchema = scheduleDayFilledSchemaBuilder(
  createArrayOfSchema(scheduleItemFilledSchema)
);
