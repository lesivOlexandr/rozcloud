import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { ScheduleWeekService } from 'src/api/services/schedule-week';
import { triggerServerError } from 'src/common/helpers/error.helpers';
import { CreateOneScheduleWeekRequest, GetScheduleByGroupsRequest, GetScheduleByWeekNumber } from './interfaces';
import { createWeekScheduleSchema, getScheduleWeekByGroupsSchema, getScheduleWeekSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.post('/', { schema: createWeekScheduleSchema }, async (request: CreateOneScheduleWeekRequest) => {
    const schedulesWeek = await ScheduleWeekService.createOne(request.body);
    return schedulesWeek;
  });

  fastify.get(
    '/groups/:weekNumber',
    { schema: getScheduleWeekByGroupsSchema },
    async (request: GetScheduleByGroupsRequest) => {
      const { weekNumber } = request.params;
      const { groupIds = [] } = request.query;
      const scheduleWeek = await ScheduleWeekService.getScheduleGroups(groupIds, weekNumber);
      return scheduleWeek;
    }
  );

  fastify.get('/user/:weekNumber', { schema: getScheduleWeekSchema }, async (request: GetScheduleByWeekNumber) => {
    if (!request.appUser) {
      return triggerServerError('At first you need to authorize', 401);
    }
    const { userType, id: userId } = request.appUser;
    const schedule = await ScheduleWeekService.getOne(userId, request.params.weekNumber, userType);
    return schedule;
  });

  next();
};
