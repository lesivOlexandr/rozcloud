import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { semesterNumberChunk } from 'src/api/routes/schedule/_schema-chunks/semester-number.schema';
import { filledWorkingPlanSubjectHoursMap } from './chunks/working-plan-subject-hours-map-filled.schema';
import { groupEntitySchema } from 'src/api/routes/structures/groups/schema/group-entity.schema';

export const getFilledWorkingPlanSchema = createObjectSchema(
  {
    type: 'object',
    properties: {
      id: idSchemaChunk,
      semesterNumber: semesterNumberChunk,
      group: groupEntitySchema,
      subjectsHoursMap: {
        type: 'array',
        items: filledWorkingPlanSubjectHoursMap,
        minItems: 1,
      },
      createdAt: timePointSchema,
      updatedAt: timePointSchema,
    },
  },
  {
    required: ['id', 'group', 'subjectsHoursMap', 'createdAt', 'updatedAt'],
    defaultNull: 'onNotRequired',
  }
);
