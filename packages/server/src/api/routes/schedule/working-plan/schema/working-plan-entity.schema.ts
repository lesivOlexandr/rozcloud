import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { workingPlanFullSchema } from './working-plan-full.schema';

export const workingPlanEntitySchema = createObjectSchema(workingPlanFullSchema, {
  required: ['id', 'groupId', 'subjectsHoursMap', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});
