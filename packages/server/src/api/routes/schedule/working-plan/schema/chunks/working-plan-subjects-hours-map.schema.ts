import { idSchemaChunk } from 'src/api/routes/_schema-chunks/id.schema';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { hoursTimeSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/time-hours.schema';
import { ektsCreditsCountChunk } from 'src/api/routes/schedule/_schema-chunks/ekts-credits.schema';
import { oppSubjectAbbreviationChunk } from 'src/api/routes/schedule/_schema-chunks/opp-subject-abbreviation.schema';
import { semesterControlTypeChunk } from 'src/api/routes/schedule/_schema-chunks/semester-control-type.schema';

export const workingPlanSubjectHoursMap = createObjectSchema(
  {
    type: 'object',
    properties: {
      subjectId: idSchemaChunk,
      oppSubjectAbbreviation: oppSubjectAbbreviationChunk,
      ektsCreditsCount: ektsCreditsCountChunk,
      laboratoryHours: hoursTimeSchemaChunk,
      practicalHours: hoursTimeSchemaChunk,
      auditorialHours: hoursTimeSchemaChunk,
      independentWorkHours: hoursTimeSchemaChunk,
      seminarHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      teacherLectorId: idSchemaChunk,
      teacherPracticeId: idSchemaChunk,
      teacherSeminarId: idSchemaChunk,
      semesterControlType: semesterControlTypeChunk,
    },
  },
  {
    required: ['subjectId', 'semesterControlType'],
    defaultNull: 'onNotRequired',
    mergeProperties: [
      ['ektsCreditsCountChunk', { default: 0 }],
      ['oppSubjectAbbreviation', { default: '' }],
    ],
  }
);
