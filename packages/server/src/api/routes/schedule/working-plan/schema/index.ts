import { groupParamChunk } from 'src/api/routes/structures/groups/schema/chunks/group-param.schema';
import { createRouteValidationSchema } from 'src/common/helpers/swagger.helpers';
import { semesterNumberChunk } from '../../_schema-chunks/semester-number.schema';
import { createUpdateWorkingPlanBody, workingPlanParam } from './create-update-working-plan.schema';
import { getFilledWorkingPlanSchema } from './get-workin-plan.schema';
import { workingPlanEntitySchema } from './working-plan-entity.schema';

export const createWorkingPlanSchema = createRouteValidationSchema(
  {
    body: createUpdateWorkingPlanBody,
    tags: ['Schedule'],
  },
  {
    200: workingPlanEntitySchema,
    400: true,
    403: true,
    protectedJWT: true,
  }
);

export const updateWorkingPlanSchema = createRouteValidationSchema(
  {
    body: createUpdateWorkingPlanBody,
    params: workingPlanParam,
    tags: ['Schedule'],
  },
  {
    200: workingPlanEntitySchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getAllGroupWorkingPlansSchema = createRouteValidationSchema(
  {
    params: groupParamChunk,
    tags: ['Schedule'],
  },
  {
    200: { type: 'object', items: getFilledWorkingPlanSchema },
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getWorkingPlanSchema = createRouteValidationSchema(
  {
    params: groupParamChunk,
    tags: ['Schedule'],
  },
  {
    200: getFilledWorkingPlanSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);

export const getWorkingPlaGroupSemesterSchema = createRouteValidationSchema(
  {
    params: {
      type: 'object',
      properties: {
        ...workingPlanParam.properties,
        semesterNumber: semesterNumberChunk,
      },
    },
    tags: ['Schedule'],
  },
  {
    200: getFilledWorkingPlanSchema,
    400: true,
    403: true,
    404: true,
    protectedJWT: true,
  }
);
