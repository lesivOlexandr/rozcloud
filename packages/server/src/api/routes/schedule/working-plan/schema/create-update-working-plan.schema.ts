import { idSchemaChunk } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { workingPlanFullSchema } from './working-plan-full.schema';

export const createUpdateWorkingPlanBody = createObjectSchema(workingPlanFullSchema, {
  required: ['groupId', 'subjectsHoursMap'],
  removeProperties: ['id', 'createdAt', 'updatedAt'],
  defaultNull: 'onNotRequired',
});

export const workingPlanParam = createObjectSchema(
  {
    type: 'object',
    properties: {
      workingPlanId: idSchemaChunk,
    },
  },
  {
    required: ['workingPlanId'],
  }
);
