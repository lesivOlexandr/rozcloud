import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { hoursTimeSchemaChunk } from 'src/api/routes/schedule/_schema-chunks/time-hours.schema';
import { ektsCreditsCountChunk } from 'src/api/routes/schedule/_schema-chunks/ekts-credits.schema';
import { oppSubjectAbbreviationChunk } from 'src/api/routes/schedule/_schema-chunks/opp-subject-abbreviation.schema';
import { semesterControlTypeChunk } from 'src/api/routes/schedule/_schema-chunks/semester-control-type.schema';
import { teacherEntitySchema } from 'src/api/routes/users/teachers/schema/teacher-entity.schema';
import { subjectEntitySchema } from '../../../subjects/schema/subject-entity.schema';

export const filledWorkingPlanSubjectHoursMap = createObjectSchema(
  {
    type: 'object',
    properties: {
      subject: subjectEntitySchema,
      oppSubjectAbbreviation: oppSubjectAbbreviationChunk,
      ektsCreditsCount: ektsCreditsCountChunk,
      laboratoryHours: hoursTimeSchemaChunk,
      practicalHours: hoursTimeSchemaChunk,
      auditorialHours: hoursTimeSchemaChunk,
      independentWorkHours: hoursTimeSchemaChunk,
      seminarHours: hoursTimeSchemaChunk,
      allHours: hoursTimeSchemaChunk,
      teacherLector: teacherEntitySchema,
      teacherPractice: teacherEntitySchema,
      teacherSeminar: teacherEntitySchema,
      semesterControlType: semesterControlTypeChunk,
    },
  },
  {
    required: ['subject', 'semesterControlType'],
    defaultNull: 'onNotRequired',
    mergeProperties: [
      ['ektsCreditsCountChunk', { default: 0 }],
      ['oppSubjectAbbreviation', { default: '' }],
    ],
  }
);
