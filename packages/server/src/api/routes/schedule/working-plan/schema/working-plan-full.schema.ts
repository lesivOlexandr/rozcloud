import { idSchemaChunk, timePointSchema } from 'src/api/routes/_schema-chunks';
import { createObjectSchema } from 'src/common/helpers/swagger.helpers';
import { semesterNumberChunk } from 'src/api/routes/schedule/_schema-chunks/semester-number.schema';
import { workingPlanSubjectHoursMap } from './chunks/working-plan-subjects-hours-map.schema';

export const workingPlanFullSchema = createObjectSchema({
  type: 'object',
  properties: {
    id: idSchemaChunk,
    semesterNumber: semesterNumberChunk,
    groupId: idSchemaChunk,
    subjectsHoursMap: {
      type: 'array',
      items: workingPlanSubjectHoursMap,
      minItems: 1,
    },
    createdAt: timePointSchema,
    updatedAt: timePointSchema,
  },
});
