import { ICreateUpdateWorkingPlanPayload } from '@rozcloud/shared/interfaces';
import { AppRequest } from 'src/common/interfaces/server';
import { GroupParam } from '../../structures/groups/interfaces';

export type WorkingPlanParam = { workingPlanId: string };

export type CreateWorkingPlanRequest = AppRequest<ICreateUpdateWorkingPlanPayload>;
export type UpdateWorkingPlanRequest = AppRequest<ICreateUpdateWorkingPlanPayload, WorkingPlanParam>;
export type GetAllGroupWorkingPlansRequest = AppRequest<undefined, GroupParam>;
export type GetWorkingPlanRequest = AppRequest<undefined, WorkingPlanParam>;
export type GetWorkingPlanGroupSemesterRequest = AppRequest<undefined, GroupParam & { semesterNumber }>;
