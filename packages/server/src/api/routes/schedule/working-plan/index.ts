import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { WorkingPlansService } from 'src/api/services/working-plan';
import {
  CreateWorkingPlanRequest,
  GetAllGroupWorkingPlansRequest,
  GetWorkingPlanGroupSemesterRequest,
  GetWorkingPlanRequest,
  UpdateWorkingPlanRequest,
} from './interfaces';
import {
  createWorkingPlanSchema,
  getAllGroupWorkingPlansSchema,
  getWorkingPlaGroupSemesterSchema,
  getWorkingPlanSchema,
  updateWorkingPlanSchema,
} from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.get('/:workingPlanId', { schema: getWorkingPlanSchema }, async (request: GetWorkingPlanRequest) => {
    const workingPlan = await WorkingPlansService.getOne(request.params.workingPlanId);
    return workingPlan;
  });

  fastify.post('/', { schema: createWorkingPlanSchema }, async (request: CreateWorkingPlanRequest) => {
    const workingPlan = await WorkingPlansService.createOne(request.body);
    return workingPlan;
  });

  fastify.put('/:workingPlanId', { schema: updateWorkingPlanSchema }, async (request: UpdateWorkingPlanRequest) => {
    const workingPlan = await WorkingPlansService.updateOne(request.params.workingPlanId, request.body);
    return workingPlan;
  });

  fastify.get(
    '/group/:groupId/all',
    { schema: getAllGroupWorkingPlansSchema },
    async (request: GetAllGroupWorkingPlansRequest) => {
      const workingPlans = await WorkingPlansService.getGroupAllWorkingPlans(request.params.groupId);
      return workingPlans;
    }
  );

  fastify.get(
    '/group/:groupId/:semesterNumber',
    { schema: getWorkingPlaGroupSemesterSchema },
    async (request: GetWorkingPlanGroupSemesterRequest) => {
      const { groupId, semesterNumber } = request.params;
      const workingPlans = await WorkingPlansService.getOneByGroup(groupId, semesterNumber);
      return workingPlans;
    }
  );

  next();
};
