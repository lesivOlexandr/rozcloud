import {
  createObjectSchema,
  createRouteValidationSchema,
  createScalarSchema,
} from 'src/common/helpers/swagger.helpers';
import { avatarSchemaChunk } from '../users/_schema-chunks/avatar.schema';
import { fileSizeSchema } from '../_schema-chunks/file-size.schema';
import { fileSchema } from '../_schema-chunks/file.schema';

export const fileRouteSchema = createObjectSchema({
  type: 'object',
  properties: {
    file: fileSchema,
  },
});

export const fileResponseSchema = createScalarSchema({
  type: 'object',
  properties: {
    fileUrl: avatarSchemaChunk,
    size: fileSizeSchema,
  },
});

export const fileRouteRequestSchema = createRouteValidationSchema(
  {
    tags: ['File'],
  },
  { 200: fileResponseSchema, 403: true, protectedJWT: true }
);
