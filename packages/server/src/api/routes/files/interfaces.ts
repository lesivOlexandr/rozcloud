import { AppRequest } from 'src/common/interfaces/server';

export type FileRequest = AppRequest;
