import { FileResponse } from '@rozcloud/shared/interfaces';
import { FastifyInstance } from 'fastify';
import { jwtInjectUser } from 'src/api/middlewares/get-user-from-jwt.middleware';
import { singleFile } from 'src/api/services/image-processing-service';
import { serverFullBaseUrl, staticFilesPrefix } from 'src/common/constants';
import { FileRequest } from './interfaces';
import { fileRouteRequestSchema } from './schema';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.addHook('preHandler', jwtInjectUser);

  fastify.post(
    '/',
    { schema: fileRouteRequestSchema, preHandler: singleFile('file') },
    async (request: FileRequest) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const file = request.file as any;
      const fileUrl = serverFullBaseUrl + staticFilesPrefix + file.filename;
      return {
        fileUrl,
        size: file.size,
      } as FileResponse;
    }
  );

  next();
};
