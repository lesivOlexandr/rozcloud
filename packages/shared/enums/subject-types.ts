export enum SubjectTypes {
  Lecture = 'lecture',
  Practice = 'practice',
  Seminar = 'seminar',
  Exam = 'exam',
  Test = 'test',
  DiffTest = 'diffTest',
}
