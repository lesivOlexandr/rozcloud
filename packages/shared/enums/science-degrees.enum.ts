export enum ScienceDegrees {
  Professor = 'professor',
  Doctor = 'doctor',
  Candidate = 'candidate',
  Aspirant = 'aspirant',
}
