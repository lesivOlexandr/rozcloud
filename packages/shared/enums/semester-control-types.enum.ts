// note you may need to create seeders for db whenever you update this enum
export enum SemesterControlTypes {
  None = 'none',
  Credit = 'credit', // Зачёт
  DiffCredit = 'diffCredit',
  Exam = 'exam',
  CourseWork = 'courseWork', // Курсовая работа
}
