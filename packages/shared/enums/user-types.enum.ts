export enum UserTypes {
  Employee = 'employee',
  Teacher = 'teacher',
  Student = 'student',
}
