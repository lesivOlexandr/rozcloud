export enum HigherEducationType {
  University = 'university',
  Institute = 'institute',
}
