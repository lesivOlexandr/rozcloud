export enum StructureTypes {
  University = 'university',
  Faculty = 'faculty',
  Department = 'department',
  AcademicGroup = 'academicGroup',
}
