export enum EmployeeTypes {
  Admin = 'admin',
  University = 'university',
  Faculty = 'faculty',
  Department = 'department',
}
