// generally, this is used in educational plan
// note you may need to create seeders for db whenever you update this enum
export enum PlanSubjectTypes {
  Mandatory = 'mandatory',
  ChosenByUniversity = 'chosenByUniversity',
  ChosenByStudent = 'chosenByStudent',
  Facultative = 'facultative',
}
