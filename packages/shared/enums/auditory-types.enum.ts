export enum AuditoryTypes {
  Lecture = 'lecture',
  Practice = 'practice',
  Seminar = 'seminar',
  Virtual = 'virtual',
}
