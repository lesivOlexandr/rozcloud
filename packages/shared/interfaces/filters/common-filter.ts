export interface ICommonFilter {
  search?: string;
};

export interface ISubjectFilter extends ICommonFilter {}

export interface ITeacherFilter extends ICommonFilter {}

export interface IAuditoryFilter extends ICommonFilter {}
