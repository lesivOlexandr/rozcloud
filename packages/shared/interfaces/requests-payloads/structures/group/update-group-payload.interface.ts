import { ICreateGroupPayload } from './create-group-payload.interface';

export type IUpdateGroupPayload = Partial<ICreateGroupPayload>;
