export interface ICreateGroupPayload {
  name: string;
  logo: string | null;
  specializationId: string | null;
  departmentId: string | null;
  superGroupId: string | null;
  groupCreatedYear: number | null;
  curatorId: string | null;
  groupPresidentId: string | null;
}
