import { IGroup } from '../../../api-responses';

export type BaseGetGroupProperties = Partial<IGroup>;
