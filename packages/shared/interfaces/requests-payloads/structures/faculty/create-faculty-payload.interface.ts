import { StructureBase } from '../structure-base.interface';

export interface ICreateFacultyPayload extends StructureBase {
  universityId: string;
}
