import { StructureBase } from '../structure-base.interface';

export type IUpdateFacultyPayload = Partial<StructureBase>;
