export * from './university';
export * from './faculty';
export * from './department';
export * from './group';
export * from './structure-base.interface';
