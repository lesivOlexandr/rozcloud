import { HigherEducationType } from '../../../../enums';
import { StructureBase } from '../structure-base.interface';

export interface ICreateUniversityPayload extends StructureBase {
  city: string | null;
  type: HigherEducationType;
}
