import { ICreateUniversityPayload } from './create-university-payload.interface';

export type IUpdateUniversityPayload = Partial<ICreateUniversityPayload>;
