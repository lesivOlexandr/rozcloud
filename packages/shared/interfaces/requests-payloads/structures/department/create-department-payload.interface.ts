import { StructureBase } from '../structure-base.interface';

export interface ICreateDepartmentPayload extends StructureBase {
  facultyId: string;
}
