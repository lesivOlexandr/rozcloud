import { StructureBase } from '../structure-base.interface';

export type IUpdateDepartmentPayload = Partial<StructureBase>;
