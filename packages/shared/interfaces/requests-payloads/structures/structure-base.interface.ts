export interface StructureBase {
  name: string;
  abbreviation: string | null;
  logo: string | null;
}
