export interface IUserLoginPayload {
  loginOrEmailOrPhone: string;
  password: string;
}
