export interface UserRegisterPayload {
  login: string;
  password: string;
}
