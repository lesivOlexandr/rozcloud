export interface JSONWebTokenDecodedPayload {
  userId: string;
  iat: number;
  exp: number;
}
