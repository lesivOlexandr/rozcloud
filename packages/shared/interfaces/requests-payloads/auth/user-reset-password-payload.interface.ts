export interface UserResetPasswordPayload {
  email: string;
}
