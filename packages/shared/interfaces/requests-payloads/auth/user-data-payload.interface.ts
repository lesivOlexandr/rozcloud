export interface IUserDataPayload {
  jwtToken: string;
}
