import { AuditoryTypes } from '../../../enums';
import { IAuditoryProperties } from '../../api-responses/locations/auditory.interface';

export interface ICreateAuditoryPayload extends IAuditoryProperties {
  auditoryTypes: AuditoryTypes[];
  universityId: string | null;
  corpusId: string;
}
