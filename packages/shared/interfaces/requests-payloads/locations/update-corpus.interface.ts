import { ICreateCorpusPayload } from './create-corpus.interface';

export type IUpdateCorpusPayload = Partial<ICreateCorpusPayload>;
