import { ICreateAuditoryPayload } from './create-auditory.interface';

export type IUpdateAuditoryPayload = Partial<ICreateAuditoryPayload>;
