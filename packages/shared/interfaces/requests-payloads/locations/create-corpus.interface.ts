import { ICorpusProperties } from '../../api-responses/locations/corpus.interface';

export interface ICreateCorpusPayload extends ICorpusProperties {
  universityId: string;
}
