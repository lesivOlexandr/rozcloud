export * from './create-corpus.interface';
export * from './update-corpus.interface';
export * from './create-auditory.interface';
export * from './update-auditory.interface';
