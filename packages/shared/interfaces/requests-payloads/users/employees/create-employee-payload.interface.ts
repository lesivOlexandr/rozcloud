import { EmployeeTypes } from '../../../../enums';
import { ICreateUserBasePayload } from '../create-user-payload.interface';

export interface ICreateEmployeePayload extends ICreateUserBasePayload {
  employeeType: EmployeeTypes;
  structureId: string;
}
