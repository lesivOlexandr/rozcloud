import { ICreateEmployeePayload } from './create-employee-payload.interface';

export type IUpdateEmployeePayload = Partial<ICreateEmployeePayload>;
