import { ScienceDegrees } from '../../../../enums';
import { ICreateUserBasePayload } from '../create-user-payload.interface';

export interface ICreateTeacherPayload extends ICreateUserBasePayload {
  scienceDegree: ScienceDegrees;
  subjectIds: string[];
  departmentId: string;
}
