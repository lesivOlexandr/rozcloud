import { ICreateTeacherPayload } from './create-teacher-payload.interface';

export type IUpdateTeacherPayload = Partial<ICreateTeacherPayload>;
