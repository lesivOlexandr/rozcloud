export interface IBindUserToTelegramRequestBody {
  id: string;
  payload: IBindUserToTelegramPayload;
}

export interface IBindUserToTelegramPayload {
  username: string | null;
  accessAllowed?: boolean;
}

export type IBindUserToTelegramData = IBindUserToTelegramPayload & { id: string };
