import { IUserProperties } from '../..';

export interface ICreateUserBaseProperties extends IUserProperties {
  password: string;
}

export type ICreateUserBasePayload = Omit<ICreateUserBaseProperties, 'id'>;
