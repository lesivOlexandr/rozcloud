import { ICreateUserBasePayload } from '../create-user-payload.interface';

interface IUpdateStudentProperties extends ICreateUserBasePayload {
  studTicketSeria: string | null;
  studTicketNum: string | null;
}

export type IUpdateStudentPayload = Partial<IUpdateStudentProperties>;
