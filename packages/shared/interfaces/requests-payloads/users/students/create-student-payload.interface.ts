import { StudyPaymentForms } from '../../../../enums';
import { ICreateUserBasePayload } from '../create-user-payload.interface';

export interface ICreateStudentPayload extends ICreateUserBasePayload {
  studTicketSeria: string | null;
  studTicketNum: string | null;
  academicGroupId: string;
  paymentForm: StudyPaymentForms;
}
