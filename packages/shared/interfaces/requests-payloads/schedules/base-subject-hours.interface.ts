export interface IBaseSubjectHoursMapCut {
  laboratoryHours: number;
  practicalHours: number;
  seminarHours: number;
}

export interface IBaseSubjectHours extends IBaseSubjectHoursMapCut {
  subjectId: string;
  ektsCreditsCount: number;
  auditorialHours: number;
  independentWorkHours: number;
  allHours: number;
}
