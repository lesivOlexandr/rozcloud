import { CreateSpecializationPayload } from './create-specialization';

export type UpdateSpecializationPayload = Partial<CreateSpecializationPayload>;
