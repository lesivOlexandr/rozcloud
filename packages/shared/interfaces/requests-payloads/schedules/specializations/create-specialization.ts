export interface CreateSpecializationPayload {
  name: string;
  specializationCode: string;
}
