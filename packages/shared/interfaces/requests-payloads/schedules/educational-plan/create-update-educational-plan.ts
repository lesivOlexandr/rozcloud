import { IEducationalPlanProperties } from '../../..';

export interface ICreateUpdateEducationalPlanPayload extends IEducationalPlanProperties {
  groupId: string;
}
