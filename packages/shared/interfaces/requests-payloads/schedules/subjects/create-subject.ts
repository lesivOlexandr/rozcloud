export interface ICreateSubjectPayload {
  name: string;
  departmentId: string;
}
