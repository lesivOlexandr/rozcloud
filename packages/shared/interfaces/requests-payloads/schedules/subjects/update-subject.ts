import { ICreateSubjectPayload } from './create-subject';

export type IUpdateSubjectPayload = Partial<ICreateSubjectPayload>;
