import { SubjectTypes } from '../../../../enums';

export type ICreateScheduleWeekPayload = IScheduleBuilder<IDaySchedules>;

export interface IScheduleBuilder<T> {
  weekNumber: number;
  daySchedules: T;
}

export interface IDayScheduleBuilder<DayItem> {
  '0': DayItem | null;
  '1': DayItem | null;
  '2': DayItem | null;
  '3': DayItem | null;
  '4': DayItem | null;
  '5': DayItem | null;
  '6': DayItem | null;
}

export type IDaySchedules = IDayScheduleBuilder<IScheduleDay>;

export interface ScheduleBuilder<Item> {
  1: Item | null;
  2: Item | null;
  3: Item | null;
  4: Item | null;
  5: Item | null;
  6: Item | null;
}

export type IScheduleDay = ScheduleBuilder<IScheduleItem[]>;
export interface IScheduleItem {
  teacherId: string | null;
  subjectId: string | null;
  auditoryId: string | null;
  type: SubjectTypes | null;
  groupId: string;
}
