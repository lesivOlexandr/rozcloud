import { IBaseSubjectHours } from '..';
import { SemesterControlTypes } from '../../../../enums';

export interface IWorkingPlanSubjectHoursMap extends IBaseSubjectHours {
  subjectId: string;
  oppSubjectAbbreviation: string;
  teacherLectorId: string | null;
  teacherPracticeId: string | null;
  teacherSeminarId: string | null;
  semesterControlType: SemesterControlTypes;
}

export interface ICreateUpdateWorkingPlanPayload {
  semesterNumber: number;
  groupId: string;
  subjectsHoursMap: IWorkingPlanSubjectHoursMap[];
}
