export * from './specializations';
export * from './subjects';
export * from './educational-plan';
export * from './working-plan';
export * from './base-subject-hours.interface';
export * from './academic-plan';
export * from './schedule-week';
