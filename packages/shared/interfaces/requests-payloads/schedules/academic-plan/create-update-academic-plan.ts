import { IBaseSubjectHoursMapCut } from '..';

export interface ICreateUpdateAcademicPlanPayload {
  groupId: string;
  year: number;
  subjectsHoursMap: IWeekDataAcademicPlan[];
}

export interface ISubjectHoursMapAcademicPlan extends IBaseSubjectHoursMapCut {
  weekNumber: number;
}

export interface IWeekDataAcademicPlan {
  auditorialWorkHours: number;
  independentWorkHours: number;
  allHours: number;
  subjectId: string;
  weeksData: ISubjectHoursMapAcademicPlan[];
}
