import { WithId } from '../../interfaces/utilities';
import { inverseDateHierarchy } from '../../helpers/time.helpers';

export type monthNumber = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
export type monthName =
  | 'January'
  | 'February'
  | 'March'
  | 'April'
  | 'May'
  | 'June'
  | 'July'
  | 'August'
  | 'September'
  | 'October'
  | 'November'
  | 'December';

export type IDateMeta = {
  id: string;
  dayName: string;
  date: number;
  weekNumber?: number;
};

export interface IWeekMeta {
  weekNumber: number;
  dates: IDateMeta[];
}

export interface IWeekMetaExtended extends IWeekMeta {
  dates: WithId<IDateMeta>[];
}

export interface IMonthMeta {
  id: string;
  monthName: monthName;
  monthNumber: monthNumber;
}

export interface IMonthMetaExtended extends IMonthMeta {
  dates: IDateMeta[];
}

export interface DateHierarchy {
  studyYear: number;
  months: IMonthMetaExtended[];
}

export interface IYearInverted {
  id: string;
  yearNumber: number;
}

export interface ISemesterInverted {
  id: string;
  semesterNumber: number;
  year: string;
}

export interface IMonthInverted {
  id: string;
  monthNumber: number;
  monthName: string;
  semester: string;
}

export interface IWeekInverted {
  id: number;
  weekNumber: number;
  months: [string, string?];
}

export type InvertedDateHierarchy = ReturnType<typeof inverseDateHierarchy>;
