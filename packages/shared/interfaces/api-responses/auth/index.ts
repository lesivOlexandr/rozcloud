import { UnknownTypeUser } from '../users';

export interface ILoginUserResponse {
  jwtToken: string;
  user: UnknownTypeUser;
}

export interface IUserDataResponse {
  user: UnknownTypeUser;
}
