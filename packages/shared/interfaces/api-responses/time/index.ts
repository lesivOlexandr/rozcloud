import { IDateMeta } from '../../functions';

export interface DatePayload {
  dayName: string;
  studyMonth?: string;
  studyYear?: string;
  formattedDate: string;
}

export interface IWeekPayload {
  weekNumber: number;
  dates: DatePayload[];
}

export interface IDateFullData extends IDateMeta {
  studyYear: number;
  studyMonth: number;
}

export interface IGetWeekPayload {
  weekNumber: number;
  dates: IDateFullData[];
}
