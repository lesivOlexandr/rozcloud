import { ISubject } from '../schedules';
import { IStudent, ITeacher } from '../users';
import { BaseStructure } from './base-structure.interface';

export interface IGroup extends BaseStructure {
  groupCreatedYear: number;
}

export interface IGroupExtended extends IGroup {
  subjects?: ISubject[];
  teachers?: ITeacher[];
  students?: IStudent[];
}
