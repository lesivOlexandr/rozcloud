export * from './university.interface';
export * from './faculty.interface';
export * from './department.interface';
export * from './group.interface';
export * from './unknown-type-structure.interface';
