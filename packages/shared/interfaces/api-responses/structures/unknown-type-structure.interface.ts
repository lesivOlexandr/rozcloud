import { StructureTypes } from '../../../enums';
import { IDepartment, IFaculty, IUniversity } from '.';

export type UnknownTypeStructure = (IUniversity | IFaculty | IDepartment) & { structureType: StructureTypes };
