import { BaseStructure } from './base-structure.interface';

export interface IDepartment extends BaseStructure {
  teachersQuantity: number;
  academicGroupsQuantity: number;
}
