import { WithDbMeta } from '../../utilities';

interface StructureProperties {
  id: string;
  name: string;
  abbreviation: string;
  logo: string;
  studentsQuantity: number;
  createdAt: number;
  updatedAt: number;
}

export type BaseStructure = WithDbMeta<StructureProperties>;
