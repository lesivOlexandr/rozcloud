import { BaseStructure } from './base-structure.interface';
import { HigherEducationType } from '../../../enums';

export interface IUniversity extends BaseStructure {
  city: string;
  type: HigherEducationType;
  facultiesQuantity: number;
  departmentsQuantity: number;
  academicGroupsQuantity: number;
  teachersQuantity: number;
}
