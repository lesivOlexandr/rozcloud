import { BaseStructure } from './base-structure.interface';

export interface IFaculty extends BaseStructure {
  teachersQuantity: number;
  academicGroupsQuantity: number;
  departmentsQuantity: number;
}
