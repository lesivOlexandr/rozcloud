import { WithDbMeta } from '../../utilities';

export interface ICorpusProperties {
  name: string;
  logo: string | null;
  address: string | null;
}

export type ICorpus = WithDbMeta<ICorpusProperties>;
