import { WithDbMeta } from '../../../interfaces/utilities';

export interface IAuditoryProperties {
  name: string;
  logo: string | null;
}

export type IAuditory = WithDbMeta<IAuditoryProperties>;
