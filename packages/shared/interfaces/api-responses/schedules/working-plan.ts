import { ICreateUpdateWorkingPlanPayload } from '../../requests-payloads';
import { WithDbMeta } from '../../../interfaces/utilities';

export type IWorkingPlan = WithDbMeta<ICreateUpdateWorkingPlanPayload>;
