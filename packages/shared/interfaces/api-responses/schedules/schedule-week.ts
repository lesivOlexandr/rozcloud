import { SubjectTypes } from '../../../enums';
import { IDayScheduleBuilder, IScheduleBuilder, ScheduleBuilder } from '../../requests-payloads';
import { IAuditory } from '../locations/auditory.interface';
import { IGroup } from '../structures/group.interface';
import { ITeacher } from '../users/teacher.interface';
import { ISubject } from './subject';

export type IWeekSchedulesFull = IDayScheduleBuilder<IScheduleDayFull>;
export type IScheduleDayFull = ScheduleBuilder<IScheduleItemFull>;
export interface IScheduleItemFull {
  teacher: ITeacher | null;
  subject: ISubject | null;
  auditory: IAuditory | null;
  group: IGroup;
  type: SubjectTypes | null;
}

export type IScheduleDayFilledArrayFull = ScheduleBuilder<IScheduleItemFull[]>;
export type IScheduleWeekFilledArrayFull = IDayScheduleBuilder<IScheduleDayFilledArrayFull>;

export type IScheduleWeekFullArray = IScheduleBuilder<IScheduleWeekFilledArrayFull>;
