export interface ISpecialization {
  id: string;
  name: string;
  specializationCode: string;
  createdAt: number;
  updatedAt: number;
}
