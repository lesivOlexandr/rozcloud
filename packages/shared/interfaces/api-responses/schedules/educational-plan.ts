import { IBaseSubjectHours } from '../../requests-payloads';
import { PlanSubjectTypes, SemesterControlTypes } from '../../../enums';
import { WithDbMeta } from '../../utilities';
import { IGroup } from '../structures';
import { ISubject } from './subject';

export interface SemesterSubjectControl {
  semesterNumber: number;
  hoursPerWeek: number;
  controlType: SemesterControlTypes;
}

export interface IEducationalPlanSubjectHoursMap extends IBaseSubjectHours {
  subjectId: string;
  subjectPlanType: PlanSubjectTypes;
  semesterSubjectControls: SemesterSubjectControl[];
  ektsCreditsCount: number;
  oppSubjectAbbreviation: string;
  laboratoryHours: number;
  practicalHours: number;
  auditorialHours: number;
  independentWorkHours: number;
  seminarHours: number;
  allHours: number;
}

export interface IEducationalPlanProperties {
  subjectsHoursMap: IEducationalPlanSubjectHoursMap[];
}

export type IEducationalPlan = WithDbMeta<IEducationalPlanProperties>;

export interface IEducationalPlanSubjectHoursMapExtended extends IEducationalPlanSubjectHoursMap {
  subject: ISubject;
  subjectId: never;
}

export interface IEducPlanFull extends IEducationalPlan {
  group: IGroup;
  subjectsHoursMap: IEducationalPlanSubjectHoursMapExtended[];
}
