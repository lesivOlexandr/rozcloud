export * from './specialization';
export * from './subject';
export * from './educational-plan';
export * from './working-plan';
export * from './schedule-week';
