import { WithDbMeta } from '../../utilities';

interface ISubjectProperties {
  name: string;
}

export type ISubject = WithDbMeta<ISubjectProperties>;
