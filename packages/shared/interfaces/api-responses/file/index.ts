export interface FileResponse {
  fileUrl: string;
  size: number;
}
