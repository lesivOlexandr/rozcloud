import { UserTypes } from '../../../enums';
import { IEmployee } from './employee.interface';
import { IStudent } from './student.interface';
import { ITeacher } from './teacher.interface';

export type UnknownTypeUser = (IEmployee | ITeacher | IStudent) & { userType: UserTypes };
