import { IUserBase } from './user-base.interface';

export interface IStudent extends IUserBase {
  studTicketSeria: string;
  studTicketNum: string;
  academicGroupId: string;
}
