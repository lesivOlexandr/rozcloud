import { ScienceDegrees } from '../../../enums';
import { IUserBase } from './user-base.interface';

export interface ITeacher extends IUserBase {
  scienceDegree: ScienceDegrees;
}
