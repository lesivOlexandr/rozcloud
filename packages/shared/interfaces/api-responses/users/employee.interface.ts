import { EmployeeTypes } from '../../../enums';
import { IUserBase } from './user-base.interface';

export interface IEmployee extends IUserBase {
  employeeType: EmployeeTypes;
}
