import { Genders } from '../../../enums';
import { WithDbMeta } from '../../utilities';

export interface IUserProperties {
  firstName: string | null;
  middleName: string | null;
  lastName: string | null;
  birthday: string | null;
  gender: Genders | null;
  avatar: string | null;
  phone: string | null;
  address: string | null;
  login: string;
  email: string | null;
}
export type IUserBase = WithDbMeta<IUserProperties>;
