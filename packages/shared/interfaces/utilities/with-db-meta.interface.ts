export type WithDbMeta<T> = T & { id: string; createdAt: number; updatedAt: number };
