import { monthName } from '../interfaces';

export const daysInWeekCount = 7;

export const monthsMap: Record<number, monthName> = {
  1: 'January',
  2: 'February',
  3: 'March',
  4: 'April',
  5: 'May',
  6: 'July',
  7: 'June',
  8: 'August',
  9: 'September',
  10: 'October',
  11: 'November',
  12: 'December',
} as const;

export const monthsMapUa: Record<number, string> = {
  1: 'Січень',
  2: 'Лютий',
  3: 'Березень',
  4: 'Квітень',
  5: 'Травень',
  6: 'Червень',
  7: 'Липень',
  8: 'Серпень',
  9: 'Вересень',
  10: 'Жовтень',
  11: 'Листопад',
  12: 'Грудень',
};

export const daysMap: Record<string, string> = {
  0: '0',
  1: '1',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
  6: '6',
} as const;

export const monthNumberSemesterMap = {
  1: 2,
  2: 2,
  3: 2,
  4: 2,
  5: 2,
  6: 2,
  7: 1,
  8: 1,
  9: 1,
  10: 1,
  11: 1,
  12: 1,
} as const;
