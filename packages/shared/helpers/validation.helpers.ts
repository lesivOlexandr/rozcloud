import { UserTypes } from 'enums';
import { IEmployee, IStudent, ITeacher, UnknownTypeUser } from 'interfaces';

export const checkUserType = (user: UnknownTypeUser) => {
  if (user.userType === UserTypes.Employee) {
    return user as IEmployee & { userType: UserTypes.Employee };
  }
  if (user.userType === UserTypes.Student) {
    return user as IStudent & { userType: UserTypes.Student };
  }
  if (user.userType === UserTypes.Teacher) {
    return user as ITeacher & { userType: UserTypes.Teacher };
  }
  return null;
};
