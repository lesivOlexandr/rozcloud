import { v4 } from 'uuid';
import { nanoid } from 'nanoid';

export const getAbbreviation = (text: string): string => {
  if (typeof text != 'string' || !text) {
    return '';
  }
  const words = text.match(/[\p{Alpha}\p{Nd}]+/gu) || [];
  const acronym = words
    .reduce(
      (previous, next) => previous + (parseInt(next) === 0 || parseInt(next) ? parseInt(next) : next[0] || ''),
      ''
    )
    .toUpperCase();
  return acronym;
};

export const generateUUID = () => v4();

export const generateNanoid = () => nanoid();
