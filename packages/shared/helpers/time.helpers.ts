import { daysInWeekCount, daysMap, monthNumberSemesterMap, monthsMap } from '../constants';
import {
  DateHierarchy,
  IDateMeta,
  IMonthInverted,
  IMonthMeta,
  IMonthMetaExtended,
  ISemesterInverted,
  IWeekInverted,
  IWeekMetaExtended,
  IYearInverted,
  monthNumber,
  WithId,
} from '../interfaces';
import { format } from 'date-fns';
import { generateNanoid, generateUUID } from './text.helpers';

export const formatDate = (date: Date | number | string) => format(new Date(date), 'yyyy-MM-dd');

// count days in month in efficient way
// see details https://overcoder.net/q/52502/%D0%BA%D0%B0%D0%BA-%D0%BB%D1%83%D1%87%D1%88%D0%B5-%D0%B2%D1%81%D0%B5%D0%B3%D0%BE-%D0%BE%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B8%D1%82%D1%8C-%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE-%D0%B4%D0%BD%D0%B5%D0%B9-%D0%B2-%D0%BC%D0%B5%D1%81%D1%8F%D1%86%D0%B5-%D1%81-%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%D1%8E-javascript
export const countDaysInMonth = (m: monthNumber, y: number) =>
  m === 2 ? (y & 3 || (!(y % 25) && y & 15) ? 28 : 29) : 30 + ((m + (m >> 3)) & 1);

export const generateMonths = (): IMonthMeta[] => {
  const monthsMeta: IMonthMeta[] = [];
  for (let i = 0; i < 12; i++) {
    const monthNumber = (i + 1) as monthNumber;
    const monthName = monthsMap[monthNumber];
    const monthMeta: IMonthMeta = {
      id: generateNanoid(),
      monthNumber,
      monthName,
    };
    monthsMeta.push(monthMeta);
  }
  return monthsMeta;
};

export const generateMonthDates = (year: number, monthNumber: monthNumber): IDateMeta[] => {
  const datesMeta: IDateMeta[] = [];
  const daysInMonth = countDaysInMonth(monthNumber, year);
  for (let i = 1; i <= daysInMonth; i++) {
    const fullDate = new Date(year, monthNumber - 1, i);
    const date = fullDate.getDate();
    const dayNumber = fullDate.getDay();
    const dayName = daysMap[dayNumber];
    const dateMeta: IDateMeta = { date, dayName, id: generateUUID() };
    datesMeta.push(dateMeta);
  }
  return datesMeta;
};

export const generateDateHierarchy = (studyYear: number): DateHierarchy => {
  const monthsMeta = generateMonths();
  for (const monthMeta of monthsMeta) {
    const extendedMonthMeta = monthMeta as IMonthMetaExtended;
    extendedMonthMeta.dates = generateMonthDates(studyYear, monthMeta.monthNumber);
  }
  const hierarchy: DateHierarchy = { studyYear, months: monthsMeta as IMonthMetaExtended[] };
  return hierarchy;
};

export const generateWeeksMeta = (dates: WithId<IDateMeta>[], offset: number): IWeekMetaExtended[] => {
  let allDates: WithId<IDateMeta>[] = [];
  allDates = allDates.concat(...dates);
  const weekDates: IWeekMetaExtended[] = [];
  for (let i = 0; i < allDates.length; i += daysInWeekCount) {
    const weekNumber = Math.trunc(i / daysInWeekCount);
    weekDates[weekNumber] = {
      weekNumber: weekNumber + 1 + offset,
      dates: allDates.slice(i, i + daysInWeekCount),
    };
  }
  return weekDates;
};

export const inverseDateHierarchy = (dateHierarchy: DateHierarchy) => {
  const years: IYearInverted[] = [];
  const semesters: ISemesterInverted[] = [];
  const months: IMonthInverted[] = [];
  const weeks: IWeekInverted[] = [];
  const dates: IDateMeta[] = [];

  const yearId = generateNanoid();
  const yearInverted: IYearInverted = { yearNumber: dateHierarchy.studyYear, id: yearId };

  years.push(yearInverted);

  const [semesters1Id, semester2Id] = [generateNanoid(), generateNanoid()];
  const semester1Inverted: ISemesterInverted = { id: semesters1Id, year: yearId, semesterNumber: 1 };
  const semester2Inverted: ISemesterInverted = { id: semester2Id, year: yearId, semesterNumber: 2 };

  semesters.push(semester1Inverted, semester2Inverted);

  dateHierarchy.months.forEach((month) => {
    const monthSemesterNumber = monthNumberSemesterMap[month.monthNumber];
    const iversedMonth: IMonthInverted = {
      id: month.id,
      monthName: month.monthName,
      monthNumber: month.monthNumber,
      semester: monthSemesterNumber === 1 ? semesters1Id : semester2Id,
    };
    months.push(iversedMonth);

    const monthWeeks = new Set<number>();
    month.dates.forEach((date) => date.weekNumber && monthWeeks.add(date.weekNumber));
    month.dates.forEach((date) => dates.push(date));
    monthWeeks.forEach((monthWeek) => {
      const week = weeks.find((week) => week.weekNumber === monthWeek);
      if (week) {
        week.months.push(month.id);
      }
      if (!week) {
        const weekData: IWeekInverted = {
          id: monthWeek,
          weekNumber: monthWeek,
          months: [iversedMonth.id],
        };
        weeks.push(weekData);
      }
    });
  });

  return {
    years: years.sort((year1, year2) => year1.yearNumber - year2.yearNumber),
    semesters: semesters.sort((sem1, sem2) => sem1.semesterNumber - sem2.semesterNumber),
    months: months.sort((month1, month2) => month1.monthNumber - month2.monthNumber),
    weeks: weeks.sort((week1, week2) => week1.weekNumber - week2.weekNumber),
    dates,
  };
};
