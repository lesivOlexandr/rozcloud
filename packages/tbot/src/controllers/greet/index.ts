import { clientBaseUrl } from '../../common/configs';
import { Telegraf } from 'telegraf';

export default async function(tgbot: Telegraf) {
  tgbot.command('start', ctx => {
    const encodedId = Buffer.from(String(ctx.from.id), 'binary').toString('base64');
    const encodedPayload = Buffer.from(String(ctx.from.username || ''), 'binary').toString('base64');
    const redirectUrl = clientBaseUrl + '/external-app/tbot?identifier=' + encodedId + '&payload=' + encodedPayload;
    const responseText = `Для під'єднання боту до системи Rozcloud дозвольте додатку доступ до даних за посиланням \n\n${redirectUrl}`;
    ctx.reply(responseText);
  });

  // tgbot.command('help')
}
