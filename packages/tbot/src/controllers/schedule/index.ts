import { ScheduleService } from '../../services/schedule.service';
import { SchedulePainting } from '../..//services/schedule-paint.service';
import { Telegraf } from 'telegraf';

export default async function(tgbot: Telegraf) {
  tgbot.command('schedule', async ctx => {
    const userId = String(ctx.from.id);
    const weekData = await ScheduleService.getWeekData(183);
    const schedule = await ScheduleService.getSchedule(userId, 183);
    if (!schedule) {
      return ctx.reply('Не знайдено розкладу на вказаний тиждень');
    }
    const schedulePicture = new SchedulePainting(schedule, weekData);
    const pictureBuffer = schedulePicture.getBuffer();

    if (!pictureBuffer) {
      return;
    }

    await ctx.replyWithPhoto({ source: pictureBuffer });
  });
}
