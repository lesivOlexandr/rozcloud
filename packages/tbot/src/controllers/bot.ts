import { Telegraf } from 'telegraf';
import scheduleCommands from './schedule';
import greetCommands from './greet';

export const prepareTgBot = async (token: string) => {
  const telegramBot = new Telegraf(token);

  await scheduleCommands(telegramBot);
  await greetCommands(telegramBot);

  return telegramBot;
};
