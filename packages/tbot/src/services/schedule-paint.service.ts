/* eslint-disable @typescript-eslint/no-explicit-any */
import { Canvas, createCanvas, NodeCanvasRenderingContext2D } from 'canvas';
import {
  DatePayload,
  IScheduleDayFilledArrayFull,
  IScheduleItemFull,
  IScheduleWeekFullArray,
} from '@rozcloud/shared/interfaces';
import bind from 'bind-decorator';
import fs from 'fs';
import { userToFullName } from '../common/helpers/mappers';
import { dayNumberNameMapUa, subjectTypesMap } from '../common/constants';

export class SchedulePainting {
  canvas: Canvas | null = null;
  dayKeys: string[] = [];
  itemKeys = [1, 2, 3, 4, 5, 6];
  setting = {
    bordersColor: '#699eee',
    bordersWidth: 10,
    font: '42px Impact',
  };

  constructor(public schedule: IScheduleWeekFullArray, public weekDates: DatePayload) {
    const schedulePainting = this.calcScheduleValues(schedule);

    this.canvas = createCanvas(schedulePainting.width, schedulePainting.height);
    this.paintSchedule(schedulePainting);
  }

  getBuffer() {
    const canvas = this.canvas;
    if (!canvas) {
      return null;
    }

    return canvas.toBuffer();
  }

  private paintSchedule(schedulePainting: ReturnType<SchedulePainting['calcScheduleValues']>) {
    if (!this.canvas) {
      return;
    }
    const dayItemWidth = schedulePainting.width;
    const ctx = this.canvas.getContext('2d');
    ctx.strokeStyle = this.setting.bordersColor;
    ctx.lineWidth = this.setting.bordersWidth;
    ctx.font = this.setting.font;

    let previousItemsHeight = 0;

    for (let i = 0; i < schedulePainting.schedulePainting.length; i++) {
      this.paintDaySchedule(previousItemsHeight, dayItemWidth, ctx, i, schedulePainting.schedulePainting[i]);

      const dayItemHeight = schedulePainting.schedulePainting[i].daySchedulesData.height;
      previousItemsHeight += dayItemHeight;
    }
  }

  private paintDaySchedule(
    yOffset: number,
    width: number,
    ctx: NodeCanvasRenderingContext2D,
    itemNumber: number,
    dayItemData: { dayLabel: string; daySchedulesData: ReturnType<SchedulePainting['calcDaySchedule']> }
  ) {
    const delta = this.setting.bordersWidth;
    const dayItemHeight = dayItemData.daySchedulesData.height;
    ctx.strokeRect(0, yOffset, width, yOffset + dayItemHeight - delta * itemNumber);

    const textWidth = ctx.measureText(dayItemData.dayLabel).width;
    const textStartXPoint = width / 2 - textWidth / 2;
    const textStartYPoint = parseInt(this.setting.font) + yOffset + 8;

    const dayTextOffset = textStartYPoint + 8;
    let previousItemsYOffset = dayTextOffset;

    ctx.fillText(dayItemData.dayLabel, textStartXPoint, textStartYPoint);

    for (const payload of dayItemData.daySchedulesData.payloads) {
      const itemHeight = payload.payloads.height - dayTextOffset / dayItemData.daySchedulesData.payloads.length;
      const fullYOffset = previousItemsYOffset + itemHeight / 2;
      this.paintDayItems(payload, ctx, fullYOffset);
      previousItemsYOffset += itemHeight;
    }
  }

  private paintDayItems(
    scheduleItem: {
      itemNumber: number;
      payloads: ReturnType<SchedulePainting['calcScheduleItems']>;
    },
    ctx: NodeCanvasRenderingContext2D,
    yOffset: number
  ) {
    console.log(yOffset, scheduleItem.itemNumber);
    const xOffset = 16;
    const itemText = String(scheduleItem.itemNumber);
    ctx.fillText(itemText, xOffset, yOffset);
  }

  private calcScheduleValues(schedule: IScheduleWeekFullArray) {
    const { daySchedules } = schedule;
    const scheduleKeys = Object.keys(daySchedules) as (keyof IScheduleWeekFullArray)[];

    const activeScheduleKeys = scheduleKeys.filter(key => this.itemKeys.find(itemKey => daySchedules[key]?.[itemKey]));

    const schedulePainting: {
      dayLabel: string;
      daySchedulesData: ReturnType<SchedulePainting['calcDaySchedule']>;
    }[] = [];
    let maxWidth = 0;
    let maxHeight = 0;

    for (const activeScheduleKey of activeScheduleKeys) {
      const { width, height, payloads } = this.calcDaySchedule(daySchedules[activeScheduleKey]);
      if (width > maxWidth) {
        maxWidth = width;
      }
      maxHeight += height;
      const dayLabel = dayNumberNameMapUa[activeScheduleKey];

      schedulePainting.push({ dayLabel, daySchedulesData: { payloads, width, height } });
    }

    return { width: maxWidth, height: maxHeight, schedulePainting };
  }

  private calcDaySchedule(daySchedule: IScheduleDayFilledArrayFull) {
    const activeScheduleKeys = this.itemKeys.filter(key => daySchedule[key]);

    let maxWidth = 0;
    let maxHeight = 0;
    const dayPayloads: { itemNumber: number; payloads: ReturnType<SchedulePainting['calcScheduleItems']> }[] = [];

    for (const activeScheduleKey of activeScheduleKeys) {
      const { width, height, paintingPayload } = this.calcScheduleItems(daySchedule[activeScheduleKey]);
      if (width > maxWidth) {
        maxWidth = width;
      }
      maxHeight += height;
      dayPayloads.push({ itemNumber: activeScheduleKey, payloads: { width, height, paintingPayload } });
    }

    return {
      width: maxWidth,
      height: maxHeight,
      payloads: dayPayloads,
    };
  }

  private calcScheduleItems(scheduleItems: IScheduleItemFull[]) {
    const width = scheduleItems.length * 600;
    const height = 200;

    const paintingPayload = scheduleItems.map(this.getScheduleItemPayload);

    return { width, height, paintingPayload };
  }

  @bind
  private getScheduleItemPayload(scheduleItem: IScheduleItemFull) {
    const auditoryText = scheduleItem.auditory?.name || '';
    const groupText = scheduleItem.group.name || '';
    const teacherText = userToFullName(scheduleItem.teacher);
    const typeText = !scheduleItem.type ? '' : subjectTypesMap[scheduleItem.type];
    const subjectText = scheduleItem.subject?.name || '';

    return {
      auditoryText,
      groupText,
      teacherText,
      typeText,
      subjectText,
    };
  }
}
