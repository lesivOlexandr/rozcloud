import { ApiService } from './api-service';
import { DatePayload, IScheduleWeekFullArray } from '@rozcloud/shared/interfaces';

export class ScheduleService {
  static async getSchedule(telegramUserId: string, weekNumber: number) {
    const apiUrl = `users/telegram/${telegramUserId}/schedule/${weekNumber}`;
    try {
      const schedule: IScheduleWeekFullArray = await ApiService.get(apiUrl);
      return schedule;
    } catch (e) {
      console.log(e);
    }
    return null;
  }

  static async getWeekData(weekNumber: number) {
    const apiUrl = `time/weeks-data/${weekNumber}`;
    const weekData: DatePayload = await ApiService.get(apiUrl);
    return weekData;
  }
}
