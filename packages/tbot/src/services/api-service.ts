import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { serverUrl } from '../common/configs/bot';
import { ApiError } from '../common/models/api-error';

export class ApiService {
  static instance: AxiosInstance = axios.create({
    baseURL: serverUrl + '/api',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  static async get<T = unknown>(url: string, params?: T) {
    return await this.instance
      .get(url, {
        params,
      })
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  private static handleResponse(response: AxiosResponse) {
    return response.data;
  }

  private static handleError(e: AxiosError) {
    if (e.response) {
      const { error, status } = e.response.data;
      throw new ApiError(error, +status);
    } else if (e.request) {
      throw new Error(e.request.responseText);
    } else {
      throw new Error(e.message);
    }
  }
}
