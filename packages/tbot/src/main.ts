import { telegramBotToken } from './common/configs/bot';
import { prepareTgBot } from './controllers/bot';

export const main = async () => {
  const bot = await prepareTgBot(telegramBotToken);
  bot.launch();
};

main();
