import dotenv from 'dotenv';

export const getEnvironmentVariable = (variableName: string, defaultValue?: string | null) => {
  dotenv.config();
  const variableValue = process.env[variableName];
  if (variableValue === undefined) {
    console.warn(`Environment variable ${variableName} is not set`);
    return defaultValue;
  }
  return variableValue;
};
