import { IUserBase } from '@rozcloud/shared/interfaces';

export const userToFullName = (user: IUserBase | null) => {
  if (!user) {
    return '';
  }
  if (!user.firstName?.trim() && !user.lastName?.trim() && !user.middleName?.trim()) {
    return user.login;
  }
  return `${user.lastName?.trim() || ''} ${user.firstName?.trim() || ''} ${user.middleName?.trim() || ''}`
    .replace(/\s+/g, ' ')
    .trim();
};
