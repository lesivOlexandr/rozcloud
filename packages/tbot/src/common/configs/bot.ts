import { getEnvironmentVariable } from '../../common/helpers/os-helpers';

export const telegramBotToken = getEnvironmentVariable('BOT_TOKEN') as string;
export const serverBaseUrl = getEnvironmentVariable('SERVER_BASE_URL', 'http://localhost') as string;
export const serverPort = getEnvironmentVariable('SERVER_PORT', '5001') as string;

export const serverUrl = serverBaseUrl + ':' + serverPort;
