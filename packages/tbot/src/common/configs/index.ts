import { getEnvironmentVariable } from '../../common/helpers/os-helpers';

export const clientBaseUrl = getEnvironmentVariable('CLIENT_BASE_URL');
