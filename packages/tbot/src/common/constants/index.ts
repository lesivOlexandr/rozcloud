import { SubjectTypes } from '@rozcloud/shared/enums';

export const subjectTypesMap: Record<SubjectTypes, string> = {
  [SubjectTypes.Lecture]: 'Лекція',
  [SubjectTypes.Practice]: 'Практика',
  [SubjectTypes.Seminar]: 'Семінар',
  [SubjectTypes.Test]: 'Залік',
  [SubjectTypes.DiffTest]: 'Дифференційований залік',
  [SubjectTypes.Exam]: 'Екзамен',
};

export const dayNumberNameMapUa: Record<number, string> = {
  0: 'Неділя',
  1: 'Понеділок',
  2: 'Вівторок',
  3: 'Середа',
  4: 'Четверг',
  5: "П'ятниця",
  6: 'Субота',
};
